/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: Test
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/templates/aot_stdio.h $
 *  $Revision: 20913 $
 *      $Date: 2017-10-04 16:01:13 +0200 (Wed, 04 Oct 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#include "module_config.h"

#ifdef AOT_STDOUT

#ifndef SUBSYS_AOT_STDIO_H_
#define SUBSYS_AOT_STDIO_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/

#define AOTIO_BUFFER_SIZE 512

/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/
//extern int printf (__const char *__restrict __format, ...);
uint32_t aot_stdio_test (int file, char *ptr, int len);
uint32_t aot_printf(const char *fmt, ... );


#ifdef __cplusplus
}
#endif

#endif /* SUBSYS_AOT_STDIO_H_ */
/* eof */

#endif // AOT_STDOUT



