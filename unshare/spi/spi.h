/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: SPI driver
 *      State: reviewed
 * Originator: M.Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/spi/spi.h $
 *  $Revision: 21538 $
 *      $Date: 2017-11-27 17:55:25 +0100 (Mon, 27 Nov 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Module SPI:
 *
 * Spi driver for the NRF which can be configured as hard- and soft spi.
 * A hard-spi is the on-chip spi controller managed by Nordic NRF library.
 * A soft-spi is a software controlled spi interface, which uses general purpose pins.
 *            ... This is NOT yet implemented.
 *
 * Using more than 1 device on the same SPI interface,
 * must the CS be manually handled.
 * The generic device driver must be written that way.
 *
 ****************************************************************************
 */

#ifndef SPI_H_
#define SPI_H_

/* Includes needed by this header -----------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "error.h"
#include "nrf_drv_spi.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
 |  global constants                                                         |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  global types                                                             |
 +--------------------------------------------------------------------------*/
/*
 * *** sample software SPI configuration ***
 *
 * static SPI_Context m_spi_flash =
 * {
 *     .Instance = NRF_DRV_SPI_INSTANCE(BRD_SPI_MODULE_FLASH), // (2)
 *     .CLK_Pin  = BRD_FLASH_CLK,
 *     .MOSI_Pin = BRD_FLASH_MOSI,
 *     .MISO_Pin = BRD_FLASH_MISO,
 *     .CS_Pin   = BRD_FLASH_CS,
 *     .useManualCS  = false,
 *     .eType        = SPI_TYPE_HW,
 *     .u32Frequency = 8000000,
 *     .ClockMode    = NRF_DRV_SPI_MODE_0,
 *     .BitOrder     = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
 * };
 *
 * */

/**
 * SPI Mode definition to decide which type of hardware to use
 *
 * SPI_MODE_SW only available if module is compiled with option CFG_SPI_SW_USED
 * SPI_MODE_HW only available if module is compiled with option CFG_SPI_HW_USED
 */
typedef enum tagSPI_Type
{
    SPI_TYPE_SW = 0x01,     ///< fully SW using PIO
    SPI_TYPE_HW = 0x02,     ///< use processor HW support
} SPI_Type;

/**
 * Context definition to setup all the required hardware
 */
typedef struct tagSPI_Context
{
    SPI_Type eType;         ///< SPI type (Hardware, Software, ...)
    uint32_t u32Frequency;  ///< Baud rate. 125,250,500 kbps, 1,2,4,8 Mbps
    uint32_t CLK_Pin;       ///< CLK pin number.
    uint32_t MOSI_Pin;      ///< MOSI pin number.
    uint32_t MISO_Pin;      ///< MISO pin number.
    uint32_t CS_Pin;        ///< CS pin number.
    bool     useManualCS;   ///< true: No hw support, use SPI_setCsOn/off or do it external
                            // this parameter is not used in SAM FW as defined there via PIN definition
    nrf_drv_spi_t           Instance;   ///< SPI instance
    nrf_drv_spi_mode_t      ClockMode;  ///< SPI mode
    nrf_drv_spi_bit_order_t BitOrder;   ///< Bit order
} SPI_Context;


/*--------------------------------------------------------------------------+
 |  function prototypes for C                                                |
 +--------------------------------------------------------------------------*/

/**
 * @brief Initializes the SPI.
 *
 * The chip select is deactivated. SPI is enabled (connected)
 *
 * @param pSpi:     Pointer to the SPI context structure
 *
 * @return          ERR_OK, ..., the error status
 */
uint32_t SPI_init(const SPI_Context *pSpi);

/*-------------------------------------------------------------------------*/

/**
 * @brief De-Init SPI bus. Internal context memory will be freed.
 *
 * Does not control GPIO CS pins!
 *
 * @param pContext : Pointer to SPI context
 */
void SPI_deinit(const SPI_Context *pSpi);

/*-------------------------------------------------------------------------*/

/**
 * @brief Sends one message (one character) while receiving one message (character) on the SPI bus.
 *
 * @param pSpi		pointer to the SPI context structure
 * @param u8TxData  Byte to be transmitted.
 * @param u8RxData[OUT] Byte received.
 * @param pError    pointer to the error structure
 * 
 * @return          ERR_OK, ..., the error status
 */
uint32_t SPI_txRxSingle(const SPI_Context *pSpi, uint8_t u8TxData,
                        uint8_t *pu8RxData, ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @brief Transmits/receives an entire block over SPI
 *
 * @param pSpi:		        pointer to the SPI context structure
 * @param pau8Transmit:     data to be transmitted.
 * @param pau8Receive[OUT]: buffer for received data.
 *                          Can be NULL if there is nothing to receive.
 * @param u32Len:           data size
 * @param pError:           pointer to the error structure
 * 
 * @return                  ERR_OK, ..., the error status
 */
uint32_t SPI_txRxBlock(const SPI_Context *pSpi, const uint8_t *pau8Transmit,
                       uint8_t *pau8Receive, uint32_t u32Len,
                       ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @brief It activates the defined chip select. Only a single chip select s used.
 * When the internal CS generator is used,
 * then is the spi interface enabled and the CS goes down. (Active LOW)
 *
 * @param pSpi:  pointer to the SPI context structure
 */
void SPI_setCsOn(const SPI_Context *pSpi);

/*-------------------------------------------------------------------------*/

/**
 * @brief It deactivates the defined chip select.
 *
 * When the internal CS generator is used,
 * then is the spi interface disabled and the CS goes up. (In-Active HIGH)
 *
 * @param pSpi:  pointer to the SPI context structure
 */
void SPI_setCsOff(const SPI_Context *pSpi);

/*-------------------------------------------------------------------------*/

/**
 * @brief Disconnects an SPI device.
 *
 * The spi interface is de-activated and all pins are switched to
 * input without Pullup to avoid power dissipation through outputs.
 *
 * Must be called for all devices on the same SPI instance
 * ATTENTION:
 * Un-called contexts on the same instance will be deactivated.
 *
 * @param pSpi:  pointer to the SPI context structure
 */
void SPI_disconnect(const SPI_Context *pSpi);

/*-------------------------------------------------------------------------*/

/**
 * @brief Connects an SPI device.
 *
 * The spi interface is activated and all pins are configured.
 *
 * Must be called for all devices on the same SPI instance
 *
 * @param pSpi:  pointer to the SPI context structure
 */
void SPI_connect(const SPI_Context *pSpi);

/*-------------------------------------------------------------------------*/

/**
 * @brief Set baud rate for PDC/HW SPI (optional)
 *
 * When not set, use configuration from u32CSR_wait set by SPI_init
 *
 * @param pSpi:		pointer to the SPI context structure
 * @param u32mck:	MCK clock frequency, recommend to call @code CLK_GetMCKspeed() @endcode
 * @param u32baud:	desired baud rate
 */
void SPI_setBaudRate(const SPI_Context *pSpi, uint32_t u32mck, uint32_t u32baud);

/*-------------------------------------------------------------------------*/

/**
 * @brief Turns off the clock but does not turn off CS or set the outputs low
 * Call disconnect before stopClock
 *
 * @param pSpi:  pointer to the SPI context structure
 */
void SPI_stopClock(const SPI_Context *pSpi);

/*-------------------------------------------------------------------------*/

/**
 * @brief Turns on the clock but does not change CS or outputs
 *
 * @param pSpi:  pointer to the SPI context structure
 */
void SPI_enableClock(const SPI_Context *pSpi);

/*-------------------------------------------------------------------------*/

#ifdef  __cplusplus
}
#endif

#endif // SPI_H_
