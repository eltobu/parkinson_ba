/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: SPI driver
 *      State: reviewed
 * Originator: M.Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/spi/spi.c $
 *  $Revision: 23770 $
 *      $Date: 2018-03-23 16:41:36 +0100 (Fri, 23 Mar 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/
#include "spi.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "nrf_drv_spi.h"
#include "module_config.h"
#include "trace.h"
#include "gpio.h"
#include "system_time.h"
#include "memorymanagement.h"


// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_SPI
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_SPI undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_SPI
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

// check for valid configuration
#if !defined(CFG_SPI_HW_USED) && !defined(CFG_SPI_SW_USED)
    // Minimum definitions in module_config.h
    #error At least one of CFG_SPI_HW_USED or CFG_SPI_SW_USED must be defined!
#endif

/**
 * timeout occurred
 */
const ERR_ErrorInfo SPI_TIMEOUT_ERROR = {
        .moduleNumber = ERR_MOD_SPI,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 1,
        .lineNumber = 0,
        ERRORSTRING("SPI Timeout")
};

/**
 * Range error occurred
 */
const ERR_ErrorInfo SPI_RANGE_ERROR = {
        .moduleNumber = ERR_MOD_SPI,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 2,
        .lineNumber = 0,
        ERRORSTRING("SPI Range Error")
};

/**
 * Internal low level function error
 */
const ERR_ErrorInfo SPI_DRIVER_ERROR = {
        .moduleNumber = ERR_MOD_SPI,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 3,
        .lineNumber = 0,
        ERRORSTRING("SPI Low Level Function Error")
};

/**
 * Transmission still busy
 */
const ERR_ErrorInfo SPI_BUSY = {
        .moduleNumber = ERR_MOD_SPI,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 4,
        .lineNumber = 0,
        ERRORSTRING("SPI busy")
};

/**
 * Wrong parameters given
 */
const ERR_ErrorInfo SPI_PARAMETER_ERROR = {
        .moduleNumber = ERR_MOD_SPI,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 5,
        .lineNumber = 0,
        ERRORSTRING("SPI: Wrong parameters given")
};


#define TRANSFER_TIMEOUT   50 ///< [ms] Maximum timeout

#define MAX_TRANSFER_SIZE 255 ///< Nordic transfer size has a byte limitation


/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/**
 * Core structure who must be equal when using multiple contexts for same instance.
 * For multiple contexts must the CS be handled manually and external.
 */
typedef struct tag_Core
{
    nrf_drv_spi_t        Instance;     ///< SPI instance
    nrf_drv_spi_config_t Config;       ///< Initialization structure
    bool                 useManualCS;  ///< HW support, or external switching
} Core_t;

/**
 * Core structure who must be equal when using multiple contexts for same instance.
 * For multiple contexts must the CS be handled manually and external.
 *
 * The configured Devices have to be stored in order to init only instances that are not yet inited
 * and to de-init instances only when de-init the last device on an instance
 *
 */
typedef struct tag_Device
{
    void*    pNextDevice;   ///< Next device structure, if any
    void*    pLastDevice;   ///< Last device structure, if any
    uint32_t CS_Pin;        ///< CS pin number.
    Core_t   Core;          ///< SPI internal core configuration
} Device_t;


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/

static volatile ret_code_t m_EventHandlerStatus; ///< Detect transfer done

static Device_t* m_pRootDevice = NULL;  ///< Chain of initialized SPI contexts.


/*--------------------------------------------------------------------------+
|  local functions (simple queue functions)                                 |
+--------------------------------------------------------------------------*/

/**
 * @brief DEBUG function to trace the queue entries.
 */
//static void printQueue(void)
//{
//    Device_t* p;
//
//    if (m_pRootDevice != NULL)
//    {
//        TRACE_DBG_L("###***... SPI Queue ROOT CS-%d ...***###\n", m_pRootDevice->CS_Pin);
//        p = m_pRootDevice->pLastDevice;
//        while (p != NULL)
//        {
//            TRACE_DBG_L("###***... SPI Queue LAST CS-%d ...***###\n", p->CS_Pin);
//            p = p->pLastDevice;
//        }
//        p = m_pRootDevice->pNextDevice;
//        while (p != NULL)
//        {
//            TRACE_DBG_L("###***... SPI Queue NEXT CS-%d ...***###\n", p->CS_Pin);
//            p = p->pNextDevice;
//        }
//    }
//    else
//    {
//        TRACE_DBG_L("###***... SPI Queue empty ...***###\n");
//    }
//} // End of printQueue

/*-------------------------------------------------------------------------*/

/**
 * @brief Insert a new device at the root position.
 *
 * @param pDevice  pointer to an allocated, filled device structure
 */
static void insertQueue(Device_t* pDevice)
{
    if (pDevice != NULL)
    {
        Device_t* pNext = NULL;

        if (m_pRootDevice != NULL)
        {
            m_pRootDevice->pLastDevice = pDevice;
            pNext = m_pRootDevice;
        }
        pDevice->pNextDevice = pNext;
        pDevice->pLastDevice = NULL;
        m_pRootDevice = pDevice;
    }
} // End of insertQueue

/*-------------------------------------------------------------------------*/

/**
 * @brief Delete a device from the active instance list.
 *
 * @param pDevice  pointer to an allocated, filled device structure
 */
static void deleteFromQueue(Device_t* pDevice)
{
    if (pDevice != NULL)
    {
        Device_t* pNext = pDevice->pNextDevice;
        Device_t* pLast = pDevice->pLastDevice;

        if (pNext != NULL)
        {
            pNext->pLastDevice = pLast;
        }
        if (pLast != NULL)
        {
            pLast->pNextDevice = pNext;
        }
        else
        {
            m_pRootDevice = pNext;
        }
        MM_free(pDevice);
    }
} // End of deleteFromQueue

/*-------------------------------------------------------------------------*/

/**
 * @brief It fills the config structure used for the spi init and deinit function.
 *
 * PRESET: It must be pre set with NRF_DRV_SPI_DEFAULT_CONFIG
 *
 * @param pSpi      pointer to the SPI context structure
 * @param pConfig   pointer to a pre-initialized configuration structure.
 *
 * @return          status
 */
static ERR_StatusType createConfig(const SPI_Context *pSpi,
                                   nrf_drv_spi_config_t *pConfig)
{
    pConfig->sck_pin  = pSpi->CLK_Pin;
    pConfig->mosi_pin = pSpi->MOSI_Pin;
    pConfig->miso_pin = pSpi->MISO_Pin;
    if (!pSpi->useManualCS)
    {
        pConfig->ss_pin = pSpi->CS_Pin;;
    }
    pConfig->mode      = pSpi->ClockMode;
    pConfig->bit_order = pSpi->BitOrder;

    switch(pSpi->u32Frequency)
    {
    case 125000:
        pConfig->frequency = NRF_DRV_SPI_FREQ_125K;
        break;
    case 250000:
        pConfig->frequency = NRF_DRV_SPI_FREQ_250K;
        break;
    case 500000:
        pConfig->frequency = NRF_DRV_SPI_FREQ_500K;
        break;
    case 1000000:
        pConfig->frequency = NRF_DRV_SPI_FREQ_1M;
        break;
    case 2000000:
        pConfig->frequency = NRF_DRV_SPI_FREQ_2M;
        break;
    case 4000000:
        pConfig->frequency = NRF_DRV_SPI_FREQ_4M;
        break;
    case 8000000:
        pConfig->frequency = NRF_DRV_SPI_FREQ_8M;
        break;
    default:
        TRACE_DBG_H("Only 125,250,500 kbps, 1,2,4,8 Mbps allowed\n");
        ERR_setErrorParam(&SPI_RANGE_ERROR, __LINE__, NULL,
                pSpi->Instance.drv_inst_idx, pSpi->u32Frequency);
        return(ERR_ERROR);
    }
    return(ERR_OK);

} // End of createConfig

/*-------------------------------------------------------------------------*/

/**
 * @brief Validates an instance for uniqueness or multiple CS usability.
 *
 * @param pInstance        pointer to a filled instance
 * @param pConfig          pointer to a filled configuration structure.
 * @param CS_Pin           Pin definition for Chip-Select.
 * @param useManualCS      true / false.
 * @param ppInsertedDevice Address of a pointer where the inserted device is.
 *
 * @return   What to do next
 *      0 = run nrf_drv_spi_init(),
 *      1 = duplicated context error,
 *      2 = duplicated valid context
 */
static uint32_t allowInstance(const nrf_drv_spi_t *pInstance,
                              nrf_drv_spi_config_t *pConfig,
                              uint32_t CS_Pin, bool useManualCS,
                              Device_t **ppInsertedDevice)
{
    Device_t *p       = m_pRootDevice;
    uint32_t RetValue = 0;

#if (CFG_MM_SMALL_BLOCK_HEADER_MODE == 1)
    Device_t *pDevice = MM_malloc(sizeof(Device_t));
#endif
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
    Device_t *pDevice = MM_malloc(sizeof(Device_t), pInstance, "SPII");
#endif
    //Device_t *pDevice = MM_malloc(sizeof(Device_t));

    // HACK: Set unused memory (alignment bytes) for save memcmp()
    memset(pDevice, 0, sizeof(Device_t));

    pDevice->CS_Pin = CS_Pin;
    pDevice->Core.useManualCS = useManualCS;
    memcpy(&pDevice->Core.Config, pConfig, sizeof(nrf_drv_spi_config_t));
    memcpy(&pDevice->Core.Instance, pInstance, sizeof(nrf_drv_spi_t));

    // Check if the device is good enough to be stored in queue
    while (p != NULL)
    {
        TRACE_DBG_L("Reference Device CS-%d, New Device CS-%d\n",
                     p->CS_Pin, pDevice->CS_Pin);

        if (memcmp(&pDevice->Core, &p->Core, sizeof(Core_t)) == 0)
        {
            TRACE_DBG_L("Same core, double SPI-instance usage\n");
            if (! useManualCS)
            {
                // CS-HW support for two instances is not possible
                RetValue = 1;
            }
            if (pDevice->CS_Pin != p->CS_Pin)
            {
                // It is the same instance with different CS pins
                // Check if it is still not set to error
                if (RetValue == 0)
                {
                    RetValue = 2;
                }
            }
            else
            {
                // It is the same instance with the same CS pin
                RetValue = 1;  // Set to error
            }
        }
        p = p->pNextDevice;
    }
    if (RetValue != 1)
    {
        insertQueue(pDevice);
        *ppInsertedDevice = pDevice;
    }
    else
    {
        MM_free(pDevice);
        *ppInsertedDevice = NULL;
    }
    return (RetValue);

} // End of allowInstance

/*-------------------------------------------------------------------------*/

/**
 * @brief Remove this context from queue and decide if there is still one left
 * to run on that instance.
 * Check and remove if last device on an instance.
 *
 * @param pSpi      pointer to the SPI context structure
 *
 * @return   true, if nrf_drv_spi_uninit() can be called
 */
static bool lastContextRemoved(const SPI_Context *pSpi)
{
    bool deleteInstance         = true;
    Device_t *pDevice           = m_pRootDevice;
    Device_t *pDeviceToDelete   = NULL;
    nrf_drv_spi_config_t Config = NRF_DRV_SPI_DEFAULT_CONFIG;

    (void) createConfig(pSpi, &Config); // No error possible

    while (pDevice != NULL)
    {
        // Compare the core part which has to be equal.
        // Hack Compiler-Bug: using memcmp over the whole Core part would not work
        if ((memcmp(&Config, &pDevice->Core.Config, sizeof(nrf_drv_spi_config_t)) == 0)
        &&  (memcmp(&pSpi->Instance, &pDevice->Core.Instance, sizeof(nrf_drv_spi_t)) == 0)
        &&  pSpi->useManualCS == pDevice->Core.useManualCS)
        {
            // The instances are equal
            if (pSpi->CS_Pin == pDevice->CS_Pin)
            {
                // AND same CS. This device has to be taken from queue
                pDeviceToDelete = pDevice;
            }
            else
            {
                // There is another device on the same instance
                // Do NOT call nrf_drv_spi_uninit()
                deleteInstance = false;
            }
        }
        pDevice = pDevice->pNextDevice;
    }
    if (pDeviceToDelete != NULL)
    {
        deleteFromQueue(pDeviceToDelete);
    }
    return (deleteInstance);

} // End of lastContextRemoved

/*--------------------------------------------------------------------------+
|  local functions (helper functions)                                       |
+--------------------------------------------------------------------------*/

/**
 * This function shall be called to handle SPI events.
 * It shall be mainly used by SPI IRQ for finished transfer.
 *
 * @param p_event      pointer to the event structure
 * @param p_context    pointer to the event context, which is not given during init
 */
static void EventHandler(nrf_drv_spi_evt_t const *p_event, void *p_context)
{
    (void)p_context; // Unused context parameter

    // Assert the correct usage
    if (p_event == NULL)
    {
        m_EventHandlerStatus = NRF_ERROR_NULL;
        return;
    }

    // Check for work done
    switch (p_event->type)
    {
    case NRF_DRV_SPI_EVENT_DONE:
        m_EventHandlerStatus = NRF_SUCCESS;
        break;
    default:
        m_EventHandlerStatus = NRF_ERROR_INTERNAL;
    }
} // End of EventHandler

/*-------------------------------------------------------------------------*/

/**
 * Must be set before every SPI transmission to catch the events from an interrupt.
 */
static void prepareTransfer(void)
{
    // Set it to timeout to detect a change and end of transfer
    // Timeout is already set and will remain if the event handler is not called
    m_EventHandlerStatus = NRF_ERROR_TIMEOUT;
}

/*-------------------------------------------------------------------------*/

/**
 * Must be called after every SPI transmission to wait until the job is done.
 *
 * @param TransferStatus   Status of the transfer function, which initiates the transfer
 *
 * @return   The status after the job is done
 */
static ret_code_t waitTransferEnds(ret_code_t TransferStatus)
{
    if (TransferStatus == NRF_SUCCESS)
    {
        uint64_t TriggerTime = TIME_setTimeout(TRANSFER_TIMEOUT);

        // Transfer started successfully. Wait until done.
        while ((m_EventHandlerStatus == NRF_ERROR_TIMEOUT)
           &&  (!TIME_isTimeout(TriggerTime)));

        TransferStatus = m_EventHandlerStatus;
        m_EventHandlerStatus = NRF_SUCCESS; // Reset to 'not used'
    }
    return (TransferStatus);
}

/*-------------------------------------------------------------------------*/

/**
 * Must be called after every SPI transmission to wait until the job is done.
 *
 * @param ErrorCode  The error code from the NRF library
 * @param u16Line    Line number where this function got called
 * @param Par1       Any parameter who liked to be given to the error handler
 * @param Par1       Any parameter who liked to be given to the error handler
 * @param pError     Pointer to an additional error structure. Most likely NULL
 *
 * @return   The AoT error status
 */
static uint32_t checkErrorCode(ret_code_t ErrorCode, uint16_t u16Line,
                               uint32_t Par1, uint32_t Par2,
                               ERR_pErrorInfo pError)
{
    switch (ErrorCode)
    {
    case NRF_SUCCESS:
        return (ERR_OK);

    case NRF_ERROR_BUSY:
        ERR_setErrorParam(&SPI_BUSY, u16Line, pError, Par1, Par2);
        return (ERR_BUSY);

    case NRF_ERROR_TIMEOUT:
        ERR_setErrorParam(&SPI_TIMEOUT_ERROR, u16Line, pError, Par1, Par2);
        return (ERR_TIMEDOUT);

    default:  // NRF_ERROR_INTERNAL, ...
        ERR_setErrorParam(&SPI_RANGE_ERROR, u16Line, pError, Par1, Par2);
        return (ERR_ERROR);
    }
} // End of checkErrorCode

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

uint32_t SPI_init(const SPI_Context *pSpi)
{
    ret_code_t ErrorCode;
    Device_t* pInsertedDevice;

    if (pSpi->eType == SPI_TYPE_HW)
    {
        // Set up SPI module
        nrf_drv_spi_config_t Config = NRF_DRV_SPI_DEFAULT_CONFIG;

        ErrorCode = createConfig(pSpi, &Config);
        if (ErrorCode != ERR_OK)
        {
            return (ErrorCode);
        }
        if (pSpi->useManualCS)
        {
            // CS pin controlled by software, initialize as output
            SPI_setCsOff(pSpi);
            GPIO_pinConfigOutput(pSpi->CS_Pin);
        }

        switch (allowInstance(&pSpi->Instance, &Config, pSpi->CS_Pin,
                pSpi->useManualCS, &pInsertedDevice))
        {
        case 0:
            ErrorCode = nrf_drv_spi_init(&pSpi->Instance, &Config, EventHandler, NULL);
            TRACE_DBG_L("*** SPI Init, Err-%d, CS-%d\n", ErrorCode, pSpi->CS_Pin);
            if (ErrorCode != NRF_SUCCESS)
            {
                // Initialization failed.
                ERR_setErrorParam(&SPI_DRIVER_ERROR, __LINE__, NULL,
                                  pSpi->Instance.drv_inst_idx, ErrorCode);
                deleteFromQueue(pInsertedDevice);
                return (ERR_ERROR);
            }
            break;

        case 1:
            // There is probably a duplicated context use.
            TRACE_DBG_H("*** SPI Illegal context, CS-%d\n", pSpi->CS_Pin);
            ERR_setErrorParam(&SPI_PARAMETER_ERROR, __LINE__, NULL,
                              pSpi->CS_Pin, pSpi->CLK_Pin);
            return (ERR_ERROR);

        default:;
            TRACE_DBG_L("*** SPI NO Init needed, CS-%d\n", pSpi->CS_Pin);
            // Context is already initialized with different CS
        }
    }
    else
    {
        ERR_setErrorParam(&SPI_RANGE_ERROR, __LINE__, NULL,
                          pSpi->eType, pSpi->u32Frequency);
        return (ERR_ERROR);

    }
    // printQueue();
    return(ERR_OK);

} // End of SPI_init

/*-------------------------------------------------------------------------*/

void SPI_deinit(const SPI_Context *pSpi)
{
    if (lastContextRemoved(pSpi)) // Last context for this instance
    {
        TRACE_DBG_L("nrf_drv_spi_uninit, CS-%d\n", pSpi->CS_Pin);
        nrf_drv_spi_uninit(&pSpi->Instance);
    }
    else
    {
        TRACE_DBG_L("SPI_deinit NO-ACTION, CS-%d\n", pSpi->CS_Pin);
    }
    // printQueue();

} // End of SPI_deinit

/*-------------------------------------------------------------------------*/

uint32_t SPI_txRxSingle(const SPI_Context *pSpi, uint8_t u8TxData,
                        uint8_t *pu8RxData, ERR_pErrorInfo pError)
{
    ret_code_t ErrorCode;

    if (pSpi->eType == SPI_TYPE_HW)
    {
        prepareTransfer();
        ErrorCode = nrf_drv_spi_transfer(&pSpi->Instance, &u8TxData, 1, pu8RxData, 1);
        ErrorCode = waitTransferEnds(ErrorCode);
        return (checkErrorCode(ErrorCode, __LINE__, (uint32_t)u8TxData, ErrorCode, pError));
    }
    return (ERR_ERROR);

} // End of SPI_txRxSingle

/*-------------------------------------------------------------------------*/

uint32_t SPI_txRxBlock(const SPI_Context *pSpi, const uint8_t *pau8Transmit,
                       uint8_t *pau8Receive, uint32_t u32Len, ERR_pErrorInfo pError)
{
    ret_code_t ErrorCode;
    uint8_t    u8BlockLen;

    if (pSpi->eType == SPI_TYPE_HW)
    {
        do
        {
            if (u32Len <= MAX_TRANSFER_SIZE)
            {
                u8BlockLen = u32Len;
                u32Len = 0;
            }
            else
            {
                u8BlockLen = MAX_TRANSFER_SIZE;
                u32Len -= MAX_TRANSFER_SIZE;
            }
            prepareTransfer();
            ErrorCode = nrf_drv_spi_transfer(&pSpi->Instance, pau8Transmit, u8BlockLen,
                                             pau8Receive, u8BlockLen);

            pau8Transmit += MAX_TRANSFER_SIZE;
            pau8Receive  += MAX_TRANSFER_SIZE;

            ErrorCode = waitTransferEnds(ErrorCode);
        } while (u32Len > 0 && ErrorCode == NRF_SUCCESS);

        return (checkErrorCode(ErrorCode, __LINE__, u32Len, ErrorCode, pError));
    }
    return (ERR_ERROR);

} // End of SPI_txRxBlock

/*-------------------------------------------------------------------------*/

void SPI_setCsOn(const SPI_Context *pSpi)
{
    if (pSpi->useManualCS)
    {
        GPIO_pinClear(pSpi->CS_Pin);
    }
} // End of SPI_setCsOn

/*-------------------------------------------------------------------------*/

void SPI_setCsOff(const SPI_Context *pSpi)
{
    if (pSpi->useManualCS)
    {
        GPIO_pinSet(pSpi->CS_Pin);
    }
} // End of SPI_setCsOff

/*-------------------------------------------------------------------------*/

void SPI_disconnect(const SPI_Context *pSpi)
{
    SPI_deinit(pSpi);

    // Set all pins to Input and no pull-up
    GPIO_pinConfigInput(pSpi->CLK_Pin,  GPIO_PIN_NOPULL);
    GPIO_pinConfigInput(pSpi->CS_Pin,   GPIO_PIN_NOPULL);
    GPIO_pinConfigInput(pSpi->MISO_Pin, GPIO_PIN_NOPULL);
    GPIO_pinConfigInput(pSpi->MOSI_Pin, GPIO_PIN_NOPULL);

} // End of SPI_disconnect

/*-------------------------------------------------------------------------*/

void SPI_connect(const SPI_Context *pSpi)
{
    SPI_init(pSpi);

} // End of SPI_connect

/*-------------------------------------------------------------------------*/

void SPI_setBaudRate(const SPI_Context *pSpi, uint32_t u32mck, uint32_t u32baud)
{
    // Not implemented
} // End of SPI_setBaudRate

/*-------------------------------------------------------------------------*/

void SPI_stopClock(const SPI_Context *pSpi)
{
    // Not implemented
} // End of SPI_stopClock

/*-------------------------------------------------------------------------*/

void SPI_enableClock(const SPI_Context *pSpi)
{
    // Not implemented
} // End of SPI_enableClock

/*-------------------------------------------------------------------------*/

#if 0
/*--------------------------------------------------------------------------+
|  Test and Debug functions                                                 |
|  These functions are NOT declared inside the header file                  |
|  Declare it in your test file as:                                         |
|      extern uint32_t SPI_DevicesInQueue(void);                            |
|      extern void SPI_ModulTest(void);                                     |
+--------------------------------------------------------------------------*/

/**
 * SPI test context definitions. Make sure that in sdk_config has SPI2_ENABLED 1
 */
static SPI_Context m_spi_Dev1 =
{
    .Instance = NRF_DRV_SPI_INSTANCE(2),
    .CLK_Pin  = 13,
    .MOSI_Pin = 14,
    .MISO_Pin = 15,
    .CS_Pin   = 12,
    .useManualCS  = true,
    .eType        = SPI_TYPE_HW,
    .u32Frequency = 8000000,
    .ClockMode    = NRF_DRV_SPI_MODE_0,
    .BitOrder     = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
};

static SPI_Context m_spi_Dev2 =
{
    .Instance = NRF_DRV_SPI_INSTANCE(2),
    .CLK_Pin  = 13,
    .MOSI_Pin = 14,
    .MISO_Pin = 15,
    .CS_Pin   = 11,
    .useManualCS  = true,
    .eType        = SPI_TYPE_HW,
    .u32Frequency = 8000000,
    .ClockMode    = NRF_DRV_SPI_MODE_0,
    .BitOrder     = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
};

static SPI_Context m_spi_Dev3 =
{
    .Instance = NRF_DRV_SPI_INSTANCE(2),
    .CLK_Pin  = 33,
    .MOSI_Pin = 14,
    .MISO_Pin = 15,
    .CS_Pin   = 12,
    .useManualCS  = true,
    .eType        = SPI_TYPE_HW,
    .u32Frequency = 8000000,
    .ClockMode    = NRF_DRV_SPI_MODE_0,
    .BitOrder     = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
};

/*-------------------------------------------------------------------------*/

uint32_t SPI_DevicesInQueue(void)
{
    uint32_t i = 0;
    Device_t *p = m_pRootDevice;
    while (p != NULL)
    {
        i++;
        p = p->pNextDevice;
    }
    return(i);

} // End of SPI_DevicesInQueue

/*-------------------------------------------------------------------------*/

void SPI_ModulTest(void)
{
    ret_code_t ErrorCode;

    ErrorCode = SPI_init(&m_spi_Dev1);
    TRACE_DBG_H("SPI_init (Device-1) status: %d, Queue: %d\n",
                 ErrorCode, SPI_DevicesInQueue());

    ErrorCode = SPI_init(&m_spi_Dev2);
    TRACE_DBG_H("SPI_init (Device-2, CS-Diff) status: %d, Queue: %d\n",
                 ErrorCode, SPI_DevicesInQueue());

    ErrorCode = SPI_init(&m_spi_Dev2);
    TRACE_DBG_H("SPI_init (Device-2, Again MUST CRASH) status: %d, Queue: %d\n",
                 ErrorCode, SPI_DevicesInQueue());

    ErrorCode = SPI_init(&m_spi_Dev3);
    TRACE_DBG_H("SPI_init (Device-3, MUST CRASH same ID) status: %d, Queue: %d\n",
                 ErrorCode, SPI_DevicesInQueue());

    SPI_deinit(&m_spi_Dev2);
    TRACE_DBG_H("SPI_deinit (Device-2, CS-Diff) Queue: %d\n", SPI_DevicesInQueue());

    ErrorCode = SPI_init(&m_spi_Dev2);
    TRACE_DBG_H("SPI_init again (Device-2, CS-Diff) status: %d, Queue: %d\n",
                 ErrorCode, SPI_DevicesInQueue());

    SPI_deinit(&m_spi_Dev2);
    TRACE_DBG_H("SPI_deinit again (Device-2, CS-Diff) Queue: %d\n", SPI_DevicesInQueue());

    SPI_deinit(&m_spi_Dev1);
    TRACE_DBG_H("SPI_deinit (Device-1) Queue: %d\n", SPI_DevicesInQueue());

    ErrorCode = SPI_init(&m_spi_Dev1);
    TRACE_DBG_H("SPI_init again (Device-1) status: %d, Queue: %d\n",
                 ErrorCode, SPI_DevicesInQueue());

    SPI_deinit(&m_spi_Dev1);
    TRACE_DBG_H("SPI_deinit again (Device-1) Queue: %d\n", SPI_DevicesInQueue());

    TIME_waitMicroSeconds(50000); // Give output time to show the messages

} // End of SPI_ModulTest
#endif

/*-------------------------------------------------------------------------*/
