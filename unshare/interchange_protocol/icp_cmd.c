/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: ICP_CMD
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/interchange_protocol/icp_cmd.c $
 *  $Revision: 24740 $
 *      $Date: 2018-05-24 12:19:08 +0200 (Thu, 24 May 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "icp_cmd.h"
#include "icp_type.h"
#include "icp.h"
#include "trace.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_ICP
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_ICP undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_ICP
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/
Interchange_function_call	IFC[TYPE_LAST];


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * init function table
 */
uint32_t ICP_CMD_function_init (void)
{
	uint32_t	u32x;

	for (u32x = TYPE_DEFAULT; u32x < TYPE_LAST; u32x++)
	{
		IFC[u32x].function = NULL;
	}

	return ERR_OK;
}
/*-------------------------------------------------------------------------*/

/**
 * Set function table
 */
uint32_t ICP_CMD_set_function (void *function, uint16_t u16type)
{
	uint32_t	u32_err = ERR_ERROR;

	if ( (u16type < TYPE_LAST) && (u16type > TYPE_DEFAULT) )
	{
		IFC[u16type].function = function;
		u32_err = ERR_OK;
	}
	else if (u16type == TYPE_DEFAULT)
	{
		IFC[u16type].function = function;
		u32_err = ERR_OK;
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Call function
 */
uint32_t ICP_CMD_call_function (uint16_t u16type, Interchange_Protocol_Header *iph)
{
	uint32_t	u32_err = ERR_ERROR;

	TRACE_DBG_L ("call_function: %x\n", u16type);

	if ( (u16type < TYPE_LAST) && (u16type > TYPE_DEFAULT) )
	{
		if (IFC[u16type].function == NULL)
		{
			if (IFC[TYPE_DEFAULT].function != NULL)
			{
				u32_err = IFC[TYPE_DEFAULT].function (iph);
			}
		}
		else
		{
			u32_err = IFC[u16type].function (iph);
		}
	}
	else // TYPE_DEFAULT
	{
		if (IFC[TYPE_DEFAULT].function != NULL)
		{
			u32_err = IFC[TYPE_DEFAULT].function (iph);
		}
	}

	return u32_err;
}

/* eof */
