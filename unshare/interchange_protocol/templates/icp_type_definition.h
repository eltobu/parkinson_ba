/* ***************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: ICP_TYPES_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/A.M.I/Uroseal/Uroseal_Interchange/trunk/icp_types.h $
 *  $Revision: 24128 $
 *      $Date: 2018-04-13 09:54:55 +0200 (Fr, 13 Apr 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2018
 *
 *************************************************************************** */

#ifndef __ICP_TYPES_H
#define __ICP_TYPES_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/

typedef enum tag_Interchange_Protocol_Type
{                                                          ///<
    TYPE_DEFAULT                                 = 0x0000, ///< DEFAULT for UNKNOWN TYPE
    TYPE_PROTOCOL_VERSION                        = 0x0001, ///< 1
    TYPE_CONTAINER_GENERIC_1                     = 0x0002, ///< 2 - Container
    TYPE_RESPONSE_1                              = 0x0003, ///< 3
    TYPE_STATUS_1                                = 0x0004, ///< 4
    TYPE_FW_UPDATE_PREPARE_1                     = 0x0005, ///< 5
    TYPE_FW_UPDATE_IMAGE_1                       = 0x0006, ///< 6
    TYPE_FW_UPDATE_EXECUTE_1                     = 0x0007, ///< 7 - Container
    TYPE_FW_UPDATE_START_ADDR_1                  = 0x0008, ///< 8
    TYPE_FW_UPDATE_STOP_ADDR_1                   = 0x0009, ///< 9
    TYPE_FW_UPDATE_CRC_1                         = 0x000A, ///< 10
    TYPE_CERTIFICATE_1                           = 0x000B, ///< 11 - Container
    TYPE_CERTIFICATE_HEADER_1                    = 0x000C, ///< 12
    TYPE_PUBLIC_KEY_1                            = 0x000D, ///< 13
    TYPE_SIGNATURE_1                             = 0x000E, ///< 14
    TYPE_LAST                                            , ///<
} Interchange_Protocol_Type;

/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/

/** Error Codes Base number definitions.
 */
#define US_ERROR_BASE_NUM           (0x0)           ///< Global error base, reserved for AoT default errors
#define US_ERROR_FWU_BASE_NUM       (0x00020000)    ///< Firmware update error base

/** Firmware update error codes.
 */
#define FW_UPDATE_OK                    (US_ERROR_FWU_BASE_NUM + 0)     ///< FW update for implant ok
#define FW_UPDATE_ERROR                 (US_ERROR_FWU_BASE_NUM + 1)     ///< FW update for implant error

/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/
/**
 * @brief returns pointer to string of type name
 */
const char* ICPT_getTypeName(Interchange_Protocol_Type type);

/**
 * @brief returns pointer of container definition array
 */
uint16_t * get_container_definition (void);

/**
 * @brief returns size of container definition array
 */
uint32_t get_container_definition_length (void);

#ifdef __cplusplus
}
#endif

#endif // __ICP_TYPES_H
/* eof */
