/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: interchange_protocol_type_definition
 *      State: Not formally tested
 * Originator: Hedinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/interchange_protocol/templates/icp_type_definition.c $
 *  $Revision: 24740 $
 *      $Date: 2018-05-24 12:19:08 +0200 (Thu, 24 May 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "icp_type.h"

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/
static const uint16_t m_au16_container_definition[] = {	TYPE_CONTAINER_GENERIC_1, \
														TYPE_FW_UPDATE_EXECUTE_1, \
														TYPE_CERTIFICATE_1, \
													};

/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/
const char* ICPT_getTypeName(Interchange_Protocol_Type type)
{
    switch (type)
    {
    case TYPE_DEFAULT: return "TYPE_DEFAULT";
    case TYPE_PROTOCOL_VERSION: return "TYPE_PROTOCOL_VERSION";
    case TYPE_CONTAINER_GENERIC_1: return "TYPE_CONTAINER_GENERIC_1";
    case TYPE_RESPONSE_1: return "TYPE_RESPONSE_1";
    case TYPE_STATUS_1: return "TYPE_STATUS_1";
    case TYPE_FW_UPDATE_PREPARE_1: return "TYPE_FW_UPDATE_PREPARE_1";
    case TYPE_FW_UPDATE_IMAGE_1: return "TYPE_FW_UPDATE_IMAGE_1";
    case TYPE_FW_UPDATE_EXECUTE_1: return "TYPE_FW_UPDATE_EXECUTE_1";
    case TYPE_FW_UPDATE_START_ADDR_1: return "TYPE_FW_UPDATE_START_ADDR_1";
    case TYPE_FW_UPDATE_STOP_ADDR_1: return "TYPE_FW_UPDATE_STOP_ADDR_1";
    case TYPE_FW_UPDATE_CRC_1: return "TYPE_FW_UPDATE_CRC_1";
    case TYPE_CERTIFICATE_1: return "TYPE_CERTIFICATE_1";
    case TYPE_CERTIFICATE_HEADER_1: return "TYPE_CERTIFICATE_HEADER_1";
    case TYPE_PUBLIC_KEY_1: return "TYPE_PUBLIC_KEY_1";
    case TYPE_SIGNATURE_1: return "TYPE_SIGNATURE_1";
    case TYPE_GET_PRESSURE_1: return "TYPE_GET_PRESSURE_1";
    case TYPE_SET_PRESSURE_CHANNEL_1: return "TYPE_SET_PRESSURE_CHANNEL_1";
    case TYPE_GET_AIR_PRESSURE_1: return "TYPE_GET_AIR_PRESSURE_1";
    case TYPE_GET_POSITION_1: return "TYPE_GET_POSITION_1";
    case TYPE_GET_BAT_VOLTAGE_1: return "TYPE_GET_BAT_VOLTAGE_1";
    case TYPE_GET_NRF_ID_1: return "TYPE_GET_NRF_ID_1";
    case TYPE_GET_AVERAGE_CURRENT_1: return "TYPE_GET_AVERAGE_CURRENT_1";
    case TYPE_START_PROD_TEST_1: return "TYPE_START_PROD_TEST_1";
    case TYPE_STOP_PROD_TEST_1: return "TYPE_STOP_PROD_TEST_1";
    case TYPE_DRIVE_MOTOR_TO_POS_1: return "TYPE_DRIVE_MOTOR_TO_POS_1";
    case TYPE_DRIVE_MOTOR_CYCLE_1: return "TYPE_DRIVE_MOTOR_CYCLE_1";
    case TYPE_DRIVE_MOTOR_CYCLE_ABORT_1: return "TYPE_DRIVE_MOTOR_CYCLE_ABORT_1";
    case TYPE_SET_SYS_POWER_OFF_1: return "TYPE_SET_SYS_POWER_OFF_1";
    case TYPE_SYSTEM_RESET_1: return "TYPE_SYSTEM_RESET_1";
    case TYPE_GET_RAW_POSITION_1: return "TYPE_GET_RAW_POSITION_1";
    case TYPE_MEASURED_VALUES_1: return "TYPE_MEASURED_VALUES_1";
    case TYPE_SET_PARAMETERS_1: return "TYPE_SET_PARAMETERS_1";
    case TYPE_SET_MOT_SPEED_1: return "TYPE_SET_MOT_SPEED_1";
    case TYPE_SET_MOT_CUR_ACCEL_1: return "TYPE_SET_MOT_CUR_ACCEL_1";
    case TYPE_SET_MOT_CUR_DRIVE_1: return "TYPE_SET_MOT_CUR_DRIVE_1";
    case TYPE_SET_CUR_TRSH_TOP_1: return "TYPE_SET_CUR_TRSH_TOP_1";
    case TYPE_SET_CUR_TRSH_BOT_1: return "TYPE_SET_CUR_TRSH_BOT_1";
    case TYPE_SET_ACCEL_TIME_1: return "TYPE_SET_ACCEL_TIME_1";
    case TYPE_LAST: return "Warning: TYPE_LAST found";
    }
    return "Warning: TYPE unknown!";
}

uint32_t get_container_definition_length (void)
{
	return (sizeof (m_au16_container_definition));
}

uint16_t * get_container_definition (void)
{
	return (uint16_t *)m_au16_container_definition;
}

/* eof */
