/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: ICP helper functions
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/interchange_protocol/icp_tools.c $
 *  $Revision: 24542 $
 *      $Date: 2018-05-04 16:46:46 +0200 (Fri, 04 May 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This module implements helper functions for the protocol service
 * and for sending TYPE_* messages.
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "icp_tools.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

/* headers from other modules ---------------------------------------------*/
#include "trace.h"
#include "app_error_aot.h"
#include "icp.h"
#include "module_config.h"
#include "nrf.h"
#include "queue.h"
#include "generic_tools.h"

/*--------------------------------------------------------------------------+
 |  debugging and check configuration                                       |
 +--------------------------------------------------------------------------*/
#undef LOCAL_DBG
#ifndef CFG_DBG_ICP
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_ICP undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_ICP
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  local functions                                                          |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * Get a pointer to the data of a decoded iph message containing one type.
 */
uint8_t* ICPT_getData(Interchange_Protocol_Header* iph)
{
    return iph->au8Data;
}

/*-------------------------------------------------------------------------*/
/**
 * Get the length of a decoded iph message containing one type.
 */
uint16_t ICPT_getLength(Interchange_Protocol_Header* iph)
{
    return GENT_bswap_16(iph->u16Length);
}

/*-------------------------------------------------------------------------*/
/**@brief Get the type of a single decoded ICP message token.
 */
uint16_t ICPT_getType(Interchange_Protocol_Header* iph)
{
    return GENT_bswap_16(iph->u16Type);
}

/*-------------------------------------------------------------------------*/
/** Traces a single type based on a decoded iph message.
 */
void ICPT_traceType(Interchange_Protocol_Header* iph)
{
    uint16_t type = GENT_bswap_16(iph->u16Type);
    uint16_t length = GENT_bswap_16 (iph->u16Length);
    uint16_t i;

    // check if it is a container, do not trace container payload as it
    // is already represented by the command types.
    if (ICP_check_container(iph) == ERR_OK)
    {
        // container
        // trace type name and type number
        TRACE_PRINT("%s:%u:0x%02x [container]\n", ICPT_getTypeName(type), type, type);
    }
    else
    {
        // data
        // trace type name and type number
    	TRACE_PRINT("%s:%u:0x%02x, ", ICPT_getTypeName(type), type, type);
        // trace length
    	TRACE_PRINT("length=%u: ", length);
        // trace data
        for (i = 0; i < length; i++)
        {
        	TRACE_PRINT("%02x ", iph->au8Data[i]);
        }
        TRACE_PRINT("\n");
    }
}


