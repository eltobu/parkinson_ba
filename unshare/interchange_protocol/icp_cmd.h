/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: ICP_CMD_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/interchange_protocol/icp_cmd.h $
 *  $Revision: 24740 $
 *      $Date: 2018-05-24 12:19:08 +0200 (Thu, 24 May 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __ICP_CMD_H
#define __ICP_CMD_H

#include "icp_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/
typedef uint32_t (*IPED_call_function)(Interchange_Protocol_Header *iph);


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/
typedef struct tag_Interchange_function_call
{
	IPED_call_function function;                       ///< function
} Interchange_function_call;


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/

/**
 * init function table
 */
uint32_t ICP_CMD_function_init (void);

/**
 * Set function table
 */
uint32_t ICP_CMD_set_function (void *function, uint16_t u16type);

/**
 * Call function
 */
uint32_t ICP_CMD_call_function (uint16_t u16type, Interchange_Protocol_Header *iph);


#ifdef __cplusplus
}
#endif

#endif // __ICP_CMD_H
/* eof */
