/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: ICP_TOOLS_H_
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/interchange_protocol/icp_tools.h $
 *  $Revision: 24740 $
 *      $Date: 2018-05-24 12:19:08 +0200 (Thu, 24 May 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This is the protocol service handler header file.
 *
 ****************************************************************************
 */

#ifndef ICP_TOOLS_H_
#define ICP_TOOLS_H_

#include "icp_type.h"

// type "renaming"
typedef Interchange_Protocol_Type ICP_Type_t;
typedef Interchange_Protocol_Header ICP_Msg_t;


/*--------------------------------------------------------------------------+
|  function prototypes                                                      |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * Get a pointer to the data of a decoded iph message containing one type.
 */
uint8_t* ICPT_getData(Interchange_Protocol_Header* iph);

/*-------------------------------------------------------------------------*/
/**
 * Get the length of a decoded iph message containing one type.
 */
uint16_t ICPT_getLength(Interchange_Protocol_Header* iph);

/*-------------------------------------------------------------------------*/
/**
 * Get the type of a decoded iph message containing one type.
 */
uint16_t ICPT_getType(Interchange_Protocol_Header* iph);

/*-------------------------------------------------------------------------*/
/** Traces a single type based on a decoded iph message.
 */
void ICPT_traceType(Interchange_Protocol_Header* iph);


#endif /* ICP_TOOLS_H_ */
