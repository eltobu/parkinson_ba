/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: ICP_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/interchange_protocol/icp.h $
 *  $Revision: 24740 $
 *      $Date: 2018-05-24 12:19:08 +0200 (Thu, 24 May 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __ICP_H
#define __ICP_H

#include "icp_type.h"
#include "icp_cmd.h"
#include "error.h"

#ifdef __cplusplus
extern "C" {
#endif

/* --------------------------------------------------------------------------+
|  global types                                                              |
+-------------------------------------------------------------------------- */
typedef struct tag_IPED
{
    uint8_t	*pu8_iped_data;					///< Data Buffer
    uint16_t	u16Bufferlength;				///< Buffer length
    Interchange_Protocol_Header *iph;		///< Length - Little-Endian
    uint8_t 	*iph_base_addr;					///< First CMD Start Address
    uint16_t	u16Container_length;			///< Container length
    uint32_t	u32position;					///< Position
} IPED_base_struct;

/** Additional errors to the base errors from error.h */
typedef enum tagInterchange_Protocol_Error
{
    //ERR_OK                        = 0x00000000, ///< Used as confirmation of transfer on serial port
    //ERR_ERROR                     = 0x00000001, ///< any error
    //ERR_AGAIN                     = 0x00000002, ///< CRC error or other problem. Ask for repeating the request
    //ERR_BUSY                      = 0x00000003, ///< Device or resource busy
    //ERR_NOMEM                     = 0x00000004, ///< Not enough space
    //ERR_TIMEDOUT                  = 0x00000005, ///< Connection timed out
    //ERR_NOSYS                     = 0x00000006, ///< Function not implemented (NO SUCH SYSTEM CALL)
    ICP_ERR_WAITING_FOR_DATA          = 0x00000007, ///< Waiting for more data. Do not use ERR_BADE for Publibike
    //ERR_NAK                       = 0x00000008, ///< Message not acknowledged
    ICP_ERR_NO_SYNC_FOUND             = 0x00000009, ///< No Sync
    ICP_ERR_SYNC_PARTLY               = 0x0000000A, ///< part of Sync, found a 0x55 but not 0x5555
    ICP_ERR_PROTOCOL_TOO_OLD          = 0x0000000B, ///< Protocol not supported/Allowed any more
    ICP_ERR_DISCONNECT_APP            = 0x0000000C, ///< Lock main processor commanded cancel for SEARCH_FOR_SMARTPHONE
    ICP_ERR_NO_DATA                   = 0x0000000D, ///< No Data
    ICP_ERR_WRONG_DATA_SIZE           = 0x0000000E, ///< Data length greater than buffer
    ICP_ERR_CONTAINER_DECODE_STOP     = 0x0000000F, ///< Stop decode inside a container
    ICP_ERR_DATA_RECEIVED_CORRECT     = 0x00000010, ///< Data received correct
} Interchange_Protocol_Error_t;

/* --------------------------------------------------------------------------+
|  global constants                                                          |
+-------------------------------------------------------------------------- */
#define IPED_MAX_LENGTH_PROTOCOL_SIZE	1032	// Look at: https://publibike.atlassian.net/wiki/spaces/SYS/pages/42273869/Internal+Communication
#define IPED_LENGTH_BUFFER				1100	// Buffer greater than: IPED_MAX_LENGTH_PROTOCOL_SIZE include reserve
#define IPED_MOVE_SIZE					(IPED_LENGTH_BUFFER - 10)	//

#define ERASE_SYNC	0x01
#define SEARCH_SYNC	0x00

#define ENCODE_ADD_CMD			0x01
#define ENCODE_ADD_CONTAINER	0x02
#define ENCODE_ADD_SYNC			0x03
#define ENCODE_ADD_PREAMBLE_SYNC	0x04

#define COUNTDOWN	40

/* --------------------------------------------------------------------------+
|  function prototypes for C                                                 |
+-------------------------------------------------------------------------- */
// Decode
uint32_t ICP_init (IPED_base_struct *iped_base, uint8_t *pu8_iped_data, uint16_t u16length, uint32_t u32position);
uint32_t ICP_search_sync (uint8_t *pu8_data, uint16_t u16length, uint32_t *u32position, uint8_t u8_flag);
uint32_t ICP_decode (uint8_t *pu8_data, uint16_t u16length, uint32_t *u32pos, uint8_t *au8_Buffer);

// function
uint32_t ICP_check_container (Interchange_Protocol_Header *iph);
uint32_t ICP_move (uint8_t *pu8_data, uint16_t u16length, uint32_t u32jump);

// Encode
uint32_t ICP_encode (uint8_t u8cmd, uint16_t u16Type, uint8_t *au8_text_to_encode, uint16_t u16srclength, uint8_t *au8_encoded_data, uint16_t *u16dstlength);


/*--------------------------------------------------------------------------+
|  Macro                                                                    |
+--------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // __ICP_H
/* eof */
