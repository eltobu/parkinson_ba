/* ***************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: __INTERCHANGE_PROTOCOL_TYPE_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/interchange_protocol/icp_type.h $
 *  $Revision: 24740 $
 *      $Date: 2018-05-24 12:19:08 +0200 (Thu, 24 May 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __INTERCHANGE_PROTOCOL_TYPE_H
#define __INTERCHANGE_PROTOCOL_TYPE_H

#include <stdint.h>
#include "icp_type_definition.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/
#define SIG_BIT_CONTAINER	0x8000

#define CONTAINER_POS_OUT_OF_RANGE	UINT16_MAX


typedef struct tag_Interchange_Protocol_Header
{
    uint16_t u16Type;                               ///< Type - human readable
    uint16_t u16Length;                             ///< Length - human readable
    uint8_t  au8Data[];                             ///< Data
} Interchange_Protocol_Header;


#define IPH_MIN_SIZE                            (sizeof(Interchange_Protocol_Header))
#define IPED_SYNC                               0x5555
#define IPED_SYNC_SIZE                          0x0002
#define IPH_CRC_SIZE                            0x0002
#define IPED_SYNC_HEADER_SIZE                   (IPH_MIN_SIZE + IPED_SYNC_SIZE)


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif // __INTERCHANGE_PROTOCOL_TYPE_H
/* eof */
