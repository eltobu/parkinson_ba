/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: UART service
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/A.M.I/Uroseal/PC-Interface2Implant/trunk/subsys/uart/uart.c $
 *  $Revision: 23920 $
 *      $Date: 2018-04-04 11:59:20 +0200 (Mi, 04 Apr 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This module implements a UART service.
 * Received data is stored in a fifo.
 * Data present in the UART_TX message queue is transmitted by the service.
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "uart.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "app_uart.h"
#include "boards.h" // todo: replace boards by board
#include "fifo.h"
#include "module_config.h"
#include "trace.h"

/*--------------------------------------------------------------------------+
 |  debugging and check configuration                                       |
 +--------------------------------------------------------------------------*/
#undef LOCAL_DBG
#ifndef CFG_DBG_UART
    #define LOCAL_DBG TR_DBG_NO
    #warning "Debug level CFG_UART_TRACE_LEVEL undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG CFG_DBG_UART
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  debug                                                                    |
+------------------------------------------------------------------------- */
#define DBG_UART
#undef DBG_UART
#define DBG_UART_PRINT_INIT // do not print early inits as the system is not initialized
#undef DBG_UART_PRINT_INIT

/*--------------------------------------------------------------------------+
|  constants                                                                |
+--------------------------------------------------------------------------*/
#define UART_TX_BUF_SIZE	64		/**< UART TX buffer size. */
#define UART_RX_BUF_SIZE	64		/**< UART RX buffer size. */

/*--------------------------------------------------------------------------+
|  module global variables                                                  |
+--------------------------------------------------------------------------*/
static Queue_t m_tUartTxQueue;
char	UART_Tx_Fifo[CFG_UART_FIFO_SIZE_TX + 1];		// Sendebuffer
char	UART_Rx_Fifo[CFG_UART_FIFO_SIZE_RX + 1];		// Empfangsbuffer
int		UART_Rx_FIFOin = 0;	  					// Input Zaehler
int 	UART_Rx_FIFOout = 0;		  			// Output Zaehler
int 	UART_Tx_FIFOin = 0;	  					// Input Zaehler
int 	UART_Tx_FIFOout = 0;		  			// Output Zaehler

/*--------------------------------------------------------------------------+
|  callback functions                                                       |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the
 *          app_uart module and add it to a fifo.
 */
/**@snippet [Handling the data received over UART] */
void uart_event_handle(app_uart_evt_t * p_event)
{
    uint8_t		u8_uart_data;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&u8_uart_data)); // get get uart data
            if (UART_Rx_Status(DATEN_FREE_IM_FIFO) > 1)
            {
            	UART_Rx_PUSH (u8_uart_data); // Copy the data into FIFO
            }
            break;

        case APP_UART_COMMUNICATION_ERROR:
        	TRACE_DBG_L("\nAPP_UART_COMMUNICATION_ERROR\r\n");
            //APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
        	TRACE_DBG_L("\nAPP_UART_FIFO_ERROR\r\n");
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

//        Note: TX can be stopped on UART to save power
//        case APP_UART_TX_EMPTY:
//            NRF_UART0->TASKS_STOPTX = 1;
//            break;

        default:
            break;
    }
}
/**@snippet [Handling the data received over UART] */

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/


Queue_t* UART_getTxQueue(void)
{
    return &m_tUartTxQueue;
}

void UART_initTxQueue(void)
{
    queue_init(&m_tUartTxQueue, "UTXQ");
}

/*-------------------------------------------------------------------------*/
/**
 * UART service handler.
 */
void UART_handler(void)
{
	uint8_t		*pu8_msg;
	uint16_t	u16UartTxMsgLength;
	uint16_t 	u16Offset = 0;

	/// Send all messages now
	while (queue_peek(&m_tUartTxQueue) != NULL)
	{
		pu8_msg = queue_pop(&m_tUartTxQueue);
		u16UartTxMsgLength = queue_get_object_length(pu8_msg);
		u16Offset = 0; // reset offset for each object
		while (u16UartTxMsgLength > 0)
		{
			while (app_uart_put(pu8_msg[u16Offset]) != NRF_SUCCESS);
			u16Offset++;
			u16UartTxMsgLength--;
		}
		queue_release_object_memory(pu8_msg);
	}
}

/*-------------------------------------------------------------------------*/
/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
void UART_init(void)
{
    uint32_t                     err_code;

    queue_init(&m_tUartTxQueue, "UTXQ");

    const app_uart_comm_params_t comm_params =
    {
    	BRD_UART_RX,
		BRD_UART_TX,
		BRD_UART_RTS,
		BRD_UART_CTS,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT( &comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);
}
/**@snippet [UART Initialization] */

char UART_Tx_POP (void)
	{
	return fifo_POP (UART_Tx_Fifo, &UART_Tx_FIFOout, CFG_UART_FIFO_SIZE_TX);
	}

void UART_Tx_PUSH (char x)
	{
	fifo_PUSH (UART_Tx_Fifo, &UART_Tx_FIFOin, CFG_UART_FIFO_SIZE_TX, x);
	}

char UART_Rx_POP (void)
	{
	return fifo_POP(UART_Rx_Fifo, &UART_Rx_FIFOout, CFG_UART_FIFO_SIZE_RX);
	}

void UART_Rx_PUSH (char x)
	{
	fifo_PUSH (UART_Rx_Fifo, &UART_Rx_FIFOin, CFG_UART_FIFO_SIZE_RX, x);
	}

int UART_Rx_Status (int s)
	{
	return fifo_Status (s, UART_Rx_FIFOin, UART_Rx_FIFOout, CFG_UART_FIFO_SIZE_RX);
	}

int UART_Tx_Status (int s)
	{
	return fifo_Status (s, UART_Tx_FIFOin, UART_Tx_FIFOout, CFG_UART_FIFO_SIZE_TX);
	}
