/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: UART_H
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/A.M.I/Uroseal/PC-Interface2Implant/trunk/subsys/uart/uart.h $
 *  $Revision: 23920 $
 *      $Date: 2018-04-04 11:59:20 +0200 (Mi, 04 Apr 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This is the UART service handler header file.
 *
 ****************************************************************************
 */

#ifndef SUBSYS_UART_H_
#define SUBSYS_UART_H_

#include <stdint.h>
#include "queue.h"

/*--------------------------------------------------------------------------+
|  function prototypes                                                      |
+--------------------------------------------------------------------------*/

Queue_t* UART_getTxQueue(void);
void UART_initTxQueue(void);

void 	UART_handler(void);
void 	UART_init(void);

char UART_Tx_POP (void);
void UART_Tx_PUSH (char x);
char UART_Rx_POP (void);
void UART_Rx_PUSH (char x);
int UART_Rx_Status (int s);
int UART_Tx_Status (int s);

#endif /* SUBSYS_UART_H_ */
