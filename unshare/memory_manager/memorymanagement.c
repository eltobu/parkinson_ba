
#include "module_config.h"
#if (CFG_MM_ENABLE == 1)

#include "memorymanagement.h"

#include <stdlib.h>
#include <string.h>

#include "function_run.h"
#include "trace.h"

#if (CFG_MM_BLOCK_TRACING_DELAY_MS > 0)
#include "system_time.h"
#endif

/*--------------------------------------------------------------------------+
 |  debugging and check configuration                                       |
 +--------------------------------------------------------------------------*/
#undef LOCAL_DBG
#ifndef CFG_MM_TRACE_LEVEL
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_MM_TRACE_LEVEL undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_MM_TRACE_LEVEL
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

#define MM_DBG_MEMORY_MANAGEMENT
#undef MM_DBG_MEMORY_MANAGEMENT

#if (CFG_MM_SMALL_BLOCK_HEADER_MODE && CFG_MM_EXTENDED_BLOCK_HEADER_MODE)
    #error "Small and Extended block header modes are both enabled! Enable only one of them."
#endif
#if ((CFG_MM_SMALL_BLOCK_HEADER_MODE == 0) && (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 0))
    #error "Small and Extended block header modes are both disabled! Enable one of them."
#endif

/************************************************************************************//**
* @defgroup   MM_size           memory size definitions
* @{
****************************************************************************************/
#define C_MM_HEAP_SIZE      CFG_MM_HEAP_SIZE        //!< Size of memory to be managed
#define C_MM_CHUNK_SIZE     CFG_MM_CHUNK_SIZE

/************************************************************************************//**
* @defgroup   MM_flags            memory block flags
* @{
****************************************************************************************/
// Speicher definierungen
#define C_MM_BLOCK_FREE           0x01      /*!< block is free                         */
#define C_MM_BLOCK_INUSE          0x02      /*!< block is in use                       */
#define C_MM_BLOCK_LOCKED         0x04      /*!< block is locked (may not be moved     */
/*!@}*/

/************************************************************************************//**
* @struct     __mm_block_t
* @brief      memory block header
* @typedef    mm_block_t
* @see        __mm_block_t
****************************************************************************************/
typedef struct __mm_block_t
{
  uint8_t   u8_flags;                       /*!< flags (usage, mode, ...)               */
  uint8_t   u8_align;                       /*!< size of aligemnt                       */
  uint16_t  u16_length;                     /*!< netto size of memory block             */
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
  void* pObjRef;   //todo: better change to a void* field and then store the pointer/address of the object
  char acTag[MM_TAG_SIZE];
#endif
  uint8_t   au8_memory[];                  /*!< placeholder for memory block           */
} mm_block_t;

/************************************************************************************//**
* @var        au8_heap
* @brief      global memory
****************************************************************************************/
static uint8_t m_au8Heap[C_MM_HEAP_SIZE];

/************************************************************************************//**
* @var        pt_heap
* @brief      pointer to 32bit aligned heap
****************************************************************************************/
static mm_block_t* m_ptHeap = NULL;

static uint32_t m_u32MonitoringInterval_ms = 0;

/*--------------------------------------------------------------------------+
 |  local functions                                                         |
 +--------------------------------------------------------------------------*/

#ifdef MM_DBG_MEMORY_MANAGEMENT
static void traceBlockList(void)
{
	mm_block_t* pt_memory = m_ptHeap;

	do
	{
	    uint32_t u32_c;

	    TRACE_DBG_L ("MM: %02x %u ", pt_memory->u8_flags, pt_memory->u16_length);
		for (u32_c = 0; u32_c < 0x08; u32_c++)
		{
		    TRACE_DBG_L ("%02x ", pt_memory->au8_memory[u32_c]);
		}
		TRACE_DBG_L ("\n");
		pt_memory  = (mm_block_t*)&pt_memory->au8_memory[pt_memory->u16_length + pt_memory->u8_align];
	}
	while (pt_memory < (mm_block_t*)&m_ptHeap->au8_memory[C_MM_HEAP_SIZE - sizeof(mm_block_t)]);
}
#endif

static void memoryMonitoring(void)
{
    MM_traceBlocks();
    FUNR_register(memoryMonitoring, m_u32MonitoringInterval_ms);
}

/*--------------------------------------------------------------------------+
 |  functions                                                               |
 +--------------------------------------------------------------------------*/

/************************************************************************************//**
* @fn         void mm_Initialisierung (void)
* @brief      initialize memory management
****************************************************************************************/
void MM_initialize(void)
{
	m_ptHeap = (mm_block_t*)m_au8Heap;

#if (CFG_MM_TRACE_INITIALIZATION == 1)
	TRACE_DBG_L("Initializing memory management\n");
#endif
	m_ptHeap->u8_flags		= C_MM_BLOCK_FREE;
	m_ptHeap->u8_align      = 0;
	m_ptHeap->u16_length	= C_MM_HEAP_SIZE - sizeof(mm_block_t);

#ifdef MM_DBG_MEMORY_MANAGEMENT
	TRACE_DBG_L("IIII\n");
	traceBlockList ();
	TRACE_DBG_L("IIII\n");
#endif
}

#if (CFG_MM_SMALL_BLOCK_HEADER_MODE == 1)
/************************************************************************************//**
* @fn         void* mm_malloc (uint16_t u16_length)
* @brief      allocate memory from heap
* @details    memory must be release by calling @ref mm_mfree
* @param[in]    u16_length
*                     size of memory block to be allocated
* @return       pointer to memory block
* @retval     NULL
*             no memory available
****************************************************************************************/
void* MM_malloc(const uint16_t u16_length)
#endif
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
/**
 *
 */
void* MM_malloc(const uint16_t u16_length, void* pObjRef, char* pcTag)
#endif
{
	mm_block_t*       pt_memory = m_ptHeap;
	mm_block_t*       pt_next;
	uint16_t    	    u16_old_length;
	uint16_t	        u16_align;

	// align to chunk size
	u16_align = ((u16_length + sizeof(mm_block_t) + C_MM_CHUNK_SIZE -1) & ~(C_MM_CHUNK_SIZE -1));
	u16_align -= u16_length + sizeof(mm_block_t);

	// search for free memory block
	do
	{
		// is block free
		if (pt_memory->u8_flags == C_MM_BLOCK_FREE)
		{
			// is block length sufficient
			if (pt_memory->u16_length >= (u16_length + u16_align))
			{
				// memory found, look if space for extra block
				if (pt_memory->u16_length >= (u16_length + u16_align + C_MM_CHUNK_SIZE))
				{
					u16_old_length      = pt_memory->u16_length;
					// reserve part for current malloc request
					pt_memory->u8_flags   = C_MM_BLOCK_INUSE | C_MM_BLOCK_LOCKED;
					pt_memory->u8_align  = u16_align;
					pt_memory->u16_length = u16_length;
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
					pt_memory->pObjRef = pObjRef;
					memcpy(pt_memory->acTag, pcTag, MM_TAG_SIZE);
#endif

					// update (create new) free block
					pt_next             = (mm_block_t*)&pt_memory->au8_memory[u16_length + u16_align];
					pt_next->u8_flags   = C_MM_BLOCK_FREE;
					pt_next->u8_align   = 0;
					pt_next->u16_length = u16_old_length - u16_length - u16_align - sizeof(mm_block_t);
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
					pt_next->pObjRef = NULL;
                    memcpy(pt_next->acTag, "free", MM_TAG_SIZE);
#endif
				}
				else
				{
					// allocate complete block
					pt_memory->u8_flags	=	C_MM_BLOCK_INUSE | C_MM_BLOCK_LOCKED;
					pt_memory->u8_align  = u16_align;
					pt_memory->u16_length = u16_length;
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
                    pt_memory->pObjRef = pObjRef;
                    memcpy(pt_memory->acTag, pcTag, MM_TAG_SIZE);
#endif
				}
				return pt_memory->au8_memory;
			}
		}
		pt_memory  = (mm_block_t*)&pt_memory->au8_memory[pt_memory->u16_length + pt_memory->u8_align];
	}
	while (pt_memory < (mm_block_t*)&m_ptHeap->au8_memory[C_MM_HEAP_SIZE - sizeof(mm_block_t)]);

	return NULL;	// Kein Speicher verf�gbar
}

/************************************************************************************//**
* @fn         void mm_mfree (const void* const p_memory)
* @brief      free allocated memory
* @details    put back memory block to heap
* @param[in]	p_memory
*             pointer to memory block
****************************************************************************************/
void* MM_free(const void* const p_memory)
{
	if (p_memory != NULL)
    {
		mm_block_t* pt_memory = (mm_block_t*)p_memory;
		pt_memory--; // adjust to block header

		// release memory block
		pt_memory->u8_flags = C_MM_BLOCK_FREE;
		pt_memory->u16_length += pt_memory->u8_align;
		pt_memory->u8_align = 0;
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
        pt_memory->pObjRef = NULL;
        memcpy(pt_memory->acTag, "free", MM_TAG_SIZE);
#endif

		// clean up memory
		if (&pt_memory->au8_memory[pt_memory->u16_length] < &m_ptHeap->au8_memory[C_MM_HEAP_SIZE - sizeof(mm_block_t)])
		{
			mm_block_t* pt_next = (mm_block_t*)(&pt_memory->au8_memory[pt_memory->u16_length + pt_memory->u8_align]);

			// is next block free
			if (C_MM_BLOCK_FREE == pt_next->u8_flags)
			{
				// join memory blocks
				pt_memory->u16_length	+=	sizeof(mm_block_t) + pt_next->u16_length;
			}
		}

		if (pt_memory > m_ptHeap)
		{
			mm_block_t* pt_prev = m_ptHeap;
			// search previous block
			do
			{
				if (pt_memory == (mm_block_t*)(&pt_prev->au8_memory[pt_prev->u16_length + pt_prev->u8_align]))
				{
					// is prevoius block free
					if (pt_prev->u8_flags == C_MM_BLOCK_FREE)
					{
						// join memory blocks
						pt_prev->u16_length +=  sizeof(mm_block_t) + pt_memory->u16_length;
					}
					break;
				}
				// next
				pt_prev  = (mm_block_t*)&pt_prev->au8_memory[pt_prev->u16_length + pt_prev->u8_align];
			}
			while (pt_prev < (mm_block_t*)&m_ptHeap->au8_memory[C_MM_HEAP_SIZE - sizeof(mm_block_t)]);
		}
    }

	return NULL;
}

/************************************************************************************//**
* @fn         uint16_t mm_get_length (const void* const p_memory)
* @brief      get length of memory block
* @details
* @param[in]  p_memory
*             pointer to memory block
* @return     length of memory block (without alignment)
****************************************************************************************/
uint16_t MM_getLength(const void* const p_memory)
{
	mm_block_t* pt_memory = (mm_block_t*)p_memory;
	//adjust pointer to block header
	pt_memory--;

	return (pt_memory->u16_length);
}

/************************************************************************************//**
* @fn         uint16_t mm_get_max_free_block (void)
* @brief      get length of largest free memory block
* @return     max length of free memory block
****************************************************************************************/
uint16_t MM_getMaxFreeBlock(void)
{
	mm_block_t* pt_memory = m_ptHeap;
	uint16_t    u16_max_length = 0;

	do
	{
		if (C_MM_BLOCK_FREE == pt_memory->u8_flags)
		{
			if (pt_memory->u16_length > u16_max_length)
			{
				u16_max_length = pt_memory->u16_length;
			}
		}
		pt_memory  = (mm_block_t*)&pt_memory->au8_memory[pt_memory->u16_length + pt_memory->u8_align];
	}
	while (pt_memory < (mm_block_t*)&m_ptHeap->au8_memory[C_MM_HEAP_SIZE - sizeof(mm_block_t)]);

	return u16_max_length;
}

/**
 *
 */
void MM_traceBlocks(void)
{
    mm_block_t* pt_memory = m_ptHeap;
    uint16_t u16I = 1;

    TRACE_DBG_L("Memory block usage:\n");

    // trace all memory blocks
    do
    {
        // print a block
        /*
         * !!! DO NOT USE MM_malloc OR FUNCTIONS WHICH USE IT INSIDE THIS DO-WHILE LOOP !!!
         * This will be recursive...
         * e.g. Use queues
         */
        TRACE_DBG_L("Block: %03u   ", u16I);
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
        TRACE_DBG_L("ObjRef: x%08x   ", (unsigned int) pt_memory->pObjRef);
        TRACE_DBG_L("Tag: %c%c%c%c   ", pt_memory->acTag[0], pt_memory->acTag[1], pt_memory->acTag[2], pt_memory->acTag[3]);
#endif
        TRACE_DBG_L("Length: %u", pt_memory->u16_length);
        TRACE_DBG_L("\n");

        // get next block
        u16I++;
        pt_memory  = (mm_block_t*)&pt_memory->au8_memory[pt_memory->u16_length + pt_memory->u8_align];
		
#if (CFG_MM_BLOCK_TRACING_DELAY_MS > 0)
        // delay iterations for RTT print
        TIME_waitMicroSeconds(CFG_MM_BLOCK_TRACING_DELAY_MS * 1000);
#endif
    }
    while (pt_memory < (mm_block_t*)&m_ptHeap->au8_memory[C_MM_HEAP_SIZE - sizeof(mm_block_t)]);
}

/**
 *
 */
void MM_startMemoryMonitor(uint32_t u32MonitoringInterval_ms)
{
    m_u32MonitoringInterval_ms = u32MonitoringInterval_ms;
    FUNR_register(memoryMonitoring, m_u32MonitoringInterval_ms);
}

/**
 *
 */
void MM_stopMemoryMonitor(void)
{
    FUNR_unregister(memoryMonitoring);
}
#endif // (CFG_MM_ENABLE == 1)
