
#ifndef __MEMORYMANAGEMENT_H__
#define __MEMORYMANAGEMENT_H__

#include "module_config.h"
#include <stdint.h>

#define MM_TAG_SIZE     4   // length of the node tag string

/* ---------  function prototypes  --------------------------------------------------- */

/**
 *
 */
void MM_initialize(void);

#if (CFG_MM_SMALL_BLOCK_HEADER_MODE == 1)
/**
 *
 */
void* MM_malloc(const uint16_t u16_length);
#endif

#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
/**
 *
 */
void* MM_malloc(const uint16_t u16_length, void* pObjRef, char* pcTag);
#endif

/**
 *
 */
void* MM_free(const void* const p_memory);

/**
 *
 */
uint16_t MM_getLength(const void* const p_memory);

/**
 *
 */
uint16_t MM_getMaxFreeBlock(void);

/**
 *
 */
void MM_traceBlocks(void);

/**
 *
 */
void MM_startMemoryMonitor(uint32_t u32MonitoringInterval_ms);

/**
 *
 */
void MM_stopMemoryMonitor(void);

#endif /* __MEMORYMANAGEMENT_H__ */
