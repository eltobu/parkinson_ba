/* ***************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: Generic Library
 *
 *     Module: RB
 *      State: reviewed by Art of Technology xx.2017
 * Originator: Schmidlin
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/ringbuffer/rb.c $
 *  $Revision: 21371 $
 *      $Date: 2017-11-13 11:16:43 +0100 (Mon, 13 Nov 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

#include "rb.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <string.h>

/* headers from other modules ---------------------------------------------*/

/* headers from other modules ---------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  local constants and macros                                               |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  module-global types                                                      |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  module-global variables                                                  |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  local functions (helper functions)                                       |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  functions                                                                |
 +--------------------------------------------------------------------------*/

RB_RingBuf* RB_init(RB_RingBuf *rb, uint8_t *pu8buf,
                    uint16_t u16maxQty, uint16_t u16itemSize)
{
    if (rb != NULL)
    {
        // Initializes ring buffer with u16MaxQty and u16ItemSize
        rb->pu8Buf = pu8buf;
        rb->u16MaxQty = u16maxQty;
        rb->itemSize = u16itemSize;
        // clear ring buffer
        RB_purge(rb);
    }
    return rb;
}

/*-------------------------------------------------------------------------*/

uint16_t RB_getQty(const RB_RingBuf *rb)
{
    uint16_t u16Head;
    uint16_t u16Tail;
    uint16_t u16RetVal = 0; // temporary variable to hold the return value

    if (rb != NULL)
    {
        u16Head = rb->u16Head;
        u16Tail = rb->u16Tail;
        // check which pointer is less than the other
        if (u16Head < u16Tail)
        {
            u16RetVal = u16Tail - u16Head;
        }
        else if (u16Head > u16Tail)
        {
            u16RetVal = (rb->u16MaxQty - u16Head) + u16Tail;
        }
        else if (rb->b8Full)
        {
            // return u16MaxQty in case the ring buffer is full
            u16RetVal = rb->u16MaxQty;
        }
    }
    return u16RetVal;
}

/*-------------------------------------------------------------------------*/

uint16_t RB_getFreeQty(const RB_RingBuf *rb)
{
    uint16_t u16Head;
    uint16_t u16Tail;
    uint16_t u16RetVal = 0; // temporary variable to hold the return value

    if (rb != NULL)
    {
        u16Head = rb->u16Head;
        u16Tail = rb->u16Tail;
        // check which pointer is less than the other
        if (u16Tail < u16Head)
        {
            u16RetVal = u16Head - u16Tail;
        }
        else if (u16Tail > u16Head)
        {
            u16RetVal = (rb->u16MaxQty - u16Tail) + u16Head;
        }
        else if (rb->b8Full)
        {
            // return 0 in case the ring buffer is full
            u16RetVal = 0;
        }
        else
        {
            u16RetVal = rb->u16MaxQty;
        }
    }
    return u16RetVal;
}

/*-------------------------------------------------------------------------*/

bool RB_isFull(const RB_RingBuf *rb)
{
    bool b8RetVal = true; // temporary variable to hold the return value

    if (rb != NULL)
    {
        b8RetVal = rb->b8Full;
    }
    return b8RetVal;
}

/*-------------------------------------------------------------------------*/

uint8_t* RB_inspectAt(const RB_RingBuf *rb, uint16_t u16Index)
{
    uint8_t *pu8RetVal;

    pu8RetVal = NULL;

    if (rb != NULL)
    {
        // calculate the address depending on the index
        u16Index += rb->u16Head;

        while (u16Index >= rb->u16MaxQty)
        {
            // for valid indices, an 'if' would suffice

            // the 'while' loop maps invalid indices back to the valid range

            u16Index -= rb->u16MaxQty;
        }
        pu8RetVal = ((u16Index * rb->itemSize) + rb->pu8Buf);
    }
    return pu8RetVal;
}

/*-------------------------------------------------------------------------*/

bool RB_put(RB_RingBuf *rb, const uint8_t *pu8Item)
{
    uint16_t u16Head;
    uint16_t u16Tail;
    bool b8RetVal = false; // temporary variable to hold the return value

    if ((rb != NULL) && (pu8Item != NULL))
    {
        if (RB_getFreeQty(rb) > 0)
        {
            u16Head = rb->u16Head;
            u16Tail = rb->u16Tail;
            // copy the item into the ring buffer
            (void)memcpy((u16Tail * rb->itemSize) + rb->pu8Buf, pu8Item, rb->itemSize);
            // calculate new address for tail
            ++u16Tail;
            if (u16Tail >= rb->u16MaxQty)
            {
                u16Tail = 0u;
            }
            if (u16Head == u16Tail)
            {
                rb->b8Full = true;
            }
            rb->u16Tail = u16Tail;
            b8RetVal = true;
        }
    }
    return b8RetVal;
}

/*-------------------------------------------------------------------------*/

bool RB_putMulti(RB_RingBuf *rb, const uint8_t *pu8Item,
                 uint8_t u8NumberOfItems)
{
    uint16_t u16Head;
    uint16_t u16Tail;
    bool b8RetVal = false; // temporary variable to hold the return value

    if ((rb != NULL) && (pu8Item != NULL) && (u8NumberOfItems != 0u))
    {
        if (RB_getFreeQty(rb) >= u8NumberOfItems)
        {
            u16Head = rb->u16Head;
            u16Tail = rb->u16Tail;
            // copy the item into the ring buffer
            while (u8NumberOfItems > 0)
            {
                (void)memcpy((u16Tail * rb->itemSize) + rb->pu8Buf, pu8Item, rb->itemSize);
                // calculate new address for u16Tail
                ++u16Tail;
                if (u16Tail >= rb->u16MaxQty)
                {
                    u16Tail = 0u;
                }
                pu8Item += rb->itemSize;
                u8NumberOfItems--;
            }
            if (u16Head == u16Tail)
            {
                rb->b8Full = true;
            }
            rb->u16Tail = u16Tail;
            b8RetVal = true;
        }
    }
    return b8RetVal;
}

/*-------------------------------------------------------------------------*/

uint8_t* RB_get(RB_RingBuf *rb, uint8_t *pu8Item)
{
    uint16_t u16Head;
    uint16_t u16Tail;
    uint8_t *pu8RetVal = NULL; // temporary pointer to hold the return value

    if ((rb != NULL) && (pu8Item != NULL))
    {
        if (RB_getQty(rb) > 0)
        {
            u16Head = rb->u16Head;
            u16Tail = rb->u16Tail;
            // copy the item from the ring buffer
            (void)memcpy(pu8Item, (u16Head * rb->itemSize) + rb->pu8Buf, rb->itemSize);
            // calculate new address for head
            ++u16Head;
            if (u16Head >= rb->u16MaxQty)
            {
                u16Head = 0u;
            }
            if (u16Head != u16Tail)
            {
                rb->b8Full = false;
            }
            rb->u16Head = u16Head;
            pu8RetVal = pu8Item;
        }
    }
    return pu8RetVal;
}

/*-------------------------------------------------------------------------*/

void RB_deleteLast(RB_RingBuf *rb)
{
    uint16_t u16Head;
    uint16_t u16Tail;

    if (rb != NULL)
    {
        if (RB_getQty(rb) > 0)
        {
            u16Head = rb->u16Head;
            u16Tail = rb->u16Tail;
            ++u16Head;
            if (u16Head >= rb->u16MaxQty)
            {
                u16Head = 0u;
            }
            if (u16Head != u16Tail)
            {
                rb->b8Full = false;
            }
            rb->u16Head = u16Head;
        }
    }
}

/*-------------------------------------------------------------------------*/

void RB_purge(RB_RingBuf *rb)
{
    if (rb != NULL)
    {
        rb->u16Head = 0;
        rb->u16Tail = 0;
        rb->b8Full  = false;
    }
}

/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
