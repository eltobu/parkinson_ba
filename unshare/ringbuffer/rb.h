/* ***************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: Generic Library
 *
 *     Module: RB
 *      State: reviewed by Art of Technology xx.2017
 * Originator: Schmidlin
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/ringbuffer/rb.h $
 *  $Revision: 21371 $
 *      $Date: 2017-11-13 11:16:43 +0100 (Mon, 13 Nov 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/**
 * @file
 * Module RB: Ring buffer
 *
 * This module implements structures and functions for ring buffers<br>
 *
 ****************************************************************************
 */

#ifndef RB_H_
#define RB_H_

/* Includes needed by this header ----------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*--------------------------------------------------------------------------+
 |  global constants                                                         |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  global types                                                             |
 +--------------------------------------------------------------------------*/

/** 
 * Define the ring buffer internal control structure. 
 */
typedef struct tagRB_RingBuf
{
    uint16_t u16Head;    ///< array index for head element
    uint16_t u16Tail;    ///< array index for tail element
    bool     b8Full;     ///< indicates full ringbuffer
    uint16_t u16MaxQty;  ///< max number of items in ringbuffer
    size_t   itemSize;   ///< number of bytes for one item
    uint8_t  *pu8Buf;    ///< memory location used to store data
} RB_RingBuf;

/*--------------------------------------------------------------------------+
 |  function prototypes for C                                                |
 +--------------------------------------------------------------------------*/

/**
 Initializes a ring buffer

 @param rb*      Pointer to structure of ring buffer
 @param pu8Buf*     Pointer to ring buffer as character array
 @param u16MaxQty   Max quantity of items in the ring buffer
 @param u16ItemSize Size of one item in the ring buffer

 @return         Pointer to structure of ring buffer
 */
RB_RingBuf* RB_init(RB_RingBuf *rb, uint8_t *pu8Buf,
                    uint16_t u16MaxQty, uint16_t u16ItemSize);

/*-------------------------------------------------------------------------*/

/**
 Gets the quantity of items in the ring buffer

 @param rb       Pointer to structure of ring buffer

 @return         Number of items in the ring buffer
 */
uint16_t RB_getQty(const RB_RingBuf *rb);

/*-------------------------------------------------------------------------*/

/**
 Gets the free quantity of items in the ring buffer

 @param rb       Pointer to structure of ring buffer

 @return         Number of free items in the ring buffer
 */
uint16_t RB_getFreeQty(const RB_RingBuf *rb);

/*-------------------------------------------------------------------------*/

/**
 Gets the state of the ring buffer

 @param rb       Pointer to structure of ring buffer

 @return         true = full, false = not full
 */
bool RB_isFull(const RB_RingBuf *rb);

/*-------------------------------------------------------------------------*/

/**
 Returns a pointer to the item at u16Index. Does not consume the item.

 @param rb       Pointer to structure of ring buffer
 @param u16Index Index of the item within the ring buffer (coerced to 0...u16MaxQty)

 @return         Pointer to buffer where the item is stored
 */
uint8_t* RB_inspectAt(const RB_RingBuf *rb, uint16_t u16Index);

/*-------------------------------------------------------------------------*/

/**
 Puts one item into the ring buffer

 @param rb       Pointer to structure of ring buffer
 @param pu8Item  Pointer to the item for putting into the ring buffer

 @return         true = successful
 */
bool RB_put(RB_RingBuf *rb, const uint8_t *pu8Item);

/*-------------------------------------------------------------------------*/

/**
 Puts several items into the ring buffer

 @param rb              Pointer to structure of ring buffer
 @param pu8Item         Pointer to the item for putting into the ring buffer
 @param u8NumberOfItems number of items to add 
                        1...number of free positions in ring buffer (
                        u16MaxQty is absolute max)

 @return         true = successful
 */
bool RB_putMulti(RB_RingBuf *rb, const uint8_t *pu8Item,
                 uint8_t u8NumberOfItems);

/*-------------------------------------------------------------------------*/

/**
 Gets one item from the ring buffer

 @param rb       Pointer to structure of ring buffer
 @param pu8Item     Pointer to the item which will be filled in

 @return         Pointer to the item which was filled in, return NULL on failure/empty
 */
uint8_t* RB_get(RB_RingBuf *rb, uint8_t *pu8Item);

/*-------------------------------------------------------------------------*/

/**
 Delets the last item in the ring buffer

 @param rb       Pointer to structure of ring buffer
 */
void RB_deleteLast(RB_RingBuf *rb);

/*-------------------------------------------------------------------------*/

/**
 Purges the ring buffer

 @param rb       Pointer to structure of ring buffer
 */
void RB_purge(RB_RingBuf *rb);

/*-------------------------------------------------------------------------*/

#ifdef  __cplusplus
}
#endif

#endif // RB_H_
