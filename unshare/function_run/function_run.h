/* ***************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: Generic Library
 *
 *     Module: FUNCTION_RUN
 *      State: reviewed by Art of Technology xx.2017
 * Originator: Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/scheduler/function_run/function_run.h $
 *  $Revision: 24802 $
 *      $Date: 2018-05-30 14:09:54 +0200 (Wed, 30 May 2018) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/* *
 * @file
 * Module FUNCTION_RUN: Let's functions run after a defined time.
 *
 * This module implements the Interface for the function running module<br>
 * Sample code is shown at the function definition<br>
 *
 * Project specific defines, defined in build-environment variables:
 * CFG_FUNR_MAX_FUNCTIONS        If not defined, function queue can have 20 entries
 * CFG_MODULE_FUNR_TESTS_ACTIVE  Test code is active when defined
 *
 * ********************************************************************************
 * CONFIGURATION example for nRF52:
 * ********************************************************************************
 * To get a time-base the real time clock module (RTC) on nRF52 and a module called
 * system-time is used. The time-base has to be initialized in main.c.
 *
 * *** used modules ***
 * #include "system_time.h"
 * #include "rtc.h"
 *
 * *** Real Time Clock context and RTC update handler running at interrupt priority. ***
 * static void RtcUpdateHandler(uint32_t SecondsSince1970);
 * static RTC_Context m_RTC_Context =
 * {
 *     .u32UpdateSeconds = 300, // [sec] 300 = 5 minutes
 *     .Callback         = RtcUpdateHandler,
 * };
 *
 * *** Callback function for RTC ***
 * static void RtcUpdateHandler(uint32_t u32SecondsSince1970)
 * {
 *     TIME_synch((uint64_t)u32SecondsSince1970 * (uint64_t)1000);
 *
 * } // End of RtcUpdateHandler
 *
 * *** in main before endless loop ***
 * uint32_t u32Status = ERR_OK;
 * u32Status = RTC_init(&m_RTC_Context);
 * if (u32Status != ERR_OK)
 * {
 *       TRACE_DBG_H("RTC_init() failed %ld\n", u32Status);
 * }
 * if (RTC_getRTC() < 315532800) // Tue, 01 Jan 1980 00:00:00 GMT
 * {
 *     RTC_setRTC(1514764800); // set RTC to Mon, 01 Jan 2018 00:00:00 GMT
 * }
 * TIME_init();
 * TIME_synch((uint64_t)RTC_getRTC() * 1000);     // Maybe a soft-reset and the time remained
 * FUNR_init(TIME_getMilliSeconds, irqControl);	  // register callback functions for system time
 * 											      // and interrupt control if necessary (otherwise NULL)
 *
 * *** in endless loop ***
 * FUNR_runFunctions(NULL);		// Calls the registered functions
 **************************************************************************** */

#ifndef FUNCTION_RUN_H_
#define FUNCTION_RUN_H_

/* Includes needed by this header -----------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*--------------------------------------------------------------------------+
 |  global constants                                                         |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  global types                                                             |
 +--------------------------------------------------------------------------*/
/**
 *
 * Callback, a function that is called when the time expires
 *
 * */
typedef void (*FUNR_CallbackType)(void);

/**
 * Callback, the target specific millisecond delivery function
 * */
typedef uint64_t (*FUNR_GetMillisecondType)(void);

/**
 * Callback, Interrupt enable / disable function
 */
typedef void (*FUNR_InterruptControl)(bool);

/*--------------------------------------------------------------------------+
 |  function prototypes for C                                                |
 +--------------------------------------------------------------------------*/

/**
 Initializes the function scheduler module during startup
 *
 * Code samples:
 * uint64_t GetMsFunction(void)			// function pointer type for system time
 * void irqControl(bool disable)		// function pointer type for interrupt control
 * FUNR_init(GetMsFunction, NULL); 		// register Target specific time function, no interrupt blocking
 * or
 * FUNR_init(GetMsFunction, irqControl); // register Target specific time and interrupt control function
 *
 * @param getMilliseconds    Address of the callback function who delivers ms
 * @param interruptControl	 Address of the callback function to enable / disable interrupts
 *
 * */
void FUNR_init(FUNR_GetMillisecondType getMilliseconds, FUNR_InterruptControl interruptControl);

/*--------------------------------------------------------------------------*/

/**
 * Check if function run queue is empty
 *
 * @return                 true = Empty, false = Jobs pending
 *
 * */
bool FUNR_queueIsEmpty(void);

/*--------------------------------------------------------------------------*/

/**
 * Add a function to the schedule list.
 *
 * Code samples:
 * void doIt(void) { ThatsCode(); };
 * FUNR_register (doIt, 500); // Unique callback, no parameter, no return value
 *
 * @param Function         Address of the callback function
 * @param u32Milliseconds  Time after the function is activated (max. 0x7FFFFFFF ~=24.8 days)
 * @return                 true = Error, false = OK
 *
 * */
bool FUNR_register(FUNR_CallbackType Function, uint32_t u32Milliseconds);

/*--------------------------------------------------------------------------*/

/**
 * Clear a function in the schedule list before it got called.
 *
 * Code samples:
 * void doIt(void) { ... };
 * FUNR_register (doIt, 1234); // Unique callback, no parameter, no return value
 * FUNR_unregister (doIt); // Deinstall the callback function before get called
 *
 * @param Function      Address of the callback function
 *
 * */
void FUNR_unregister(FUNR_CallbackType Function);

/*--------------------------------------------------------------------------*/

/**
 * Removes all registered functions from the list.
 *
 * Code samples:
 * FUNR_unregisterAll();
 *
 * */
void FUNR_unregisterAll(void);

/*--------------------------------------------------------------------------*/

/**
 * It calls the registered functions who have reached there time.
 *
 * Code samples:
 * --- function prototype ---
 * void setLowPower(void);
 *
 * while (true)
 * {
 *     do_something();
 *     FUNR_runFunctions(NULL);			// system stay active
 *     or
 *     FUNR_runFunctions(setLowPower);	// set system to low power mode
 * }
 *
 * @param lowPowerFunct		pointer to function to set CPU in low power mode
 * 							NULL if system doesn't go to low power mode
 *
 * */
void FUNR_runFunctions(FUNR_CallbackType lowPowerFunct);

/*--------------------------------------------------------------------------*/

/**
 * update registered functions time - has to be called after RTC update
 *
 * Code samples:
 * uint64_t u64OldTime;
 * uint64_t u64NewTime;
 *
 * u64OldTime = getMilliseconds();
 * // update RTC
 * RTC_setNewTime(xx); // Seconds since 1970
 * // sync system time
 * u64NewTime = getMilliseconds();
 * FUNR_synchTime(u64OldTime, u64NewTime);
 *
 * @param u64OldTime	System Time before RTC update [ms]
 * @param u64NewTime	System Time after RTC update [ms]
 * */
void FUNR_synchTime(uint64_t u64OldTime, uint64_t u64NewTime);

/*--------------------------------------------------------------------------*/

/**
 * Trace all registered functions and their execution times
 */
void FUNR_showRegistered(void);

/*--------------------------------------------------------------------------*/

/**
 * checks if the function is registered
 * */
bool FUNR_IsRegistered (FUNR_CallbackType Function);

/*--------------------------------------------------------------------------*/

/**
 * It starts an endless running test program.
 * Define in the project configuration "CFG_MODULE_FUNR_TESTS_ACTIVE"
 * This function should not be activated in a productive environment.
 *
 * Code samples main.c:
 * FUNR_init();
 * FUNR_test();
 * while (true)
 * {
 *     do_something();
 *     FUNR_runFunctions();
 * }
 * */
void FUNR_test(void);

/*==========================================================================*/
#ifdef  __cplusplus
}
#endif

#endif  // FUNCTION_RUN_H_
