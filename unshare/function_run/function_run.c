/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: Generic Library
 *
 *     Module: FUNCTION_RUN
 *      State: reviewed by Art of Technology xx.2017
 * Originator: Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/scheduler/function_run/function_run.c $
 *  $Revision: 24950 $
 *      $Date: 2018-06-01 16:48:33 +0200 (Fri, 01 Jun 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* pragma's and makefile-generated #define's ------------------------------*/
//#define CFG_MODULE_FUNR_TESTS_ACTIVE  1

/* headers for this module ------------------------------------------------*/
#include "function_run.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stddef.h>


/* headers from other modules ---------------------------------------------*/
#include "error.h"
#include "module_config.h"
#include "trace.h"

#ifdef CFG_MODULE_FUNR_TESTS_ACTIVE
#include <stdio.h>
#include "system_time.h"
#endif

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_FRUN
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_FRUN undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_FRUN
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 | check configuration                                                      |
 +--------------------------------------------------------------------------*/
// make necessary structures as small as possible, use typical if not defined in module_config.h
#ifndef CFG_FUNR_MAX_FUNCTIONS
    #define CFG_FUNR_MAX_FUNCTIONS     20  ///< nr of allowed functions (typical default)
#endif

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/



/*--------------------------------------------------------------------------+
 |  module-global types                                                      |
 +--------------------------------------------------------------------------*/
/**
 Structure to handle internal function list.
 */
typedef struct tag_FunctionInfo
{
    FUNR_CallbackType Function; ///< NULL or the address of an active function
    uint32_t u32CallTime;       ///< time to trigger
}FunctionInfo_t;

/*--------------------------------------------------------------------------+
 |  module-global variables                                                  |
 +--------------------------------------------------------------------------*/

/**
 * Context for maximum of registered functions reached
 */
static const ERR_ErrorInfo MAX_FUNCTION_ERROR =
{
    .moduleNumber = ERR_MOD_SCHEDULER,
    .errorClass = ERR_CLASS_ERROR,
    .errorNumber = 1,
    .lineNumber = 0,
    ERRORSTRING("Maximum of registered functions reached")
};

/**
 * Context register time over maximum register time
 */
static const ERR_ErrorInfo MAX_TIME_ERROR =
{
    .moduleNumber = ERR_MOD_SCHEDULER,
    .errorClass = ERR_CLASS_ERROR,
    .errorNumber = 2,
    .lineNumber = 0,
    ERRORSTRING("Desired trigger time out of range")
};

/**
 * Context no time callback function registered
 */
static const ERR_ErrorInfo NO_TIME_FUNCTION =
{
    .moduleNumber = ERR_MOD_SCHEDULER,
    .errorClass = ERR_CLASS_ERROR,
    .errorNumber = 3,
    .lineNumber = 0,
    ERRORSTRING("No time function registered")
};

/**
 Sorted queue of functions
 */
static FunctionInfo_t 			m_FunctionInfo[CFG_FUNR_MAX_FUNCTIONS];

static uint64_t 				m_u64BaseTime; 					///< System time when the module got initialized
static uint32_t 				m_u32NextTriggerTime; 			///< [ms] time to call the event

static FUNR_GetMillisecondType 	m_getMilliseconds; 				///< Target specific function
static FUNR_InterruptControl	m_interruptControl;				///< interrupt control function (enable / disable)

const  uint32_t					ku32Uint31Max = 0x7FFFFFFF;	///<

/*--------------------------------------------------------------------------+
 |  local functions                                                          |
 +--------------------------------------------------------------------------*/

/**
 * Stop calling the event function. No work to do until started again.
 *
 * Code samples:
 * if (Nothing to do)
 *     disableEvent();
 *
 * */
static void disableEvent(void)
{
    m_u32NextTriggerTime = UINT32_MAX; // Maximum time, does not trigger

} // End of disableEvent

/*--------------------------------------------------------------------------*/

/**
 * disable / enable interrupts over callback function if registered
 *
 * @params disable		true: disable interrupt, false: enable interrupt
 */
static inline void interruptControl(bool disable)
{
	// check if callback function is registered
	if(m_interruptControl != NULL)
	{
		// execute callback function
		m_interruptControl(disable);
	}
}

/*--------------------------------------------------------------------------*/

/**
 * update trigger time of registered functions
 * subtracting the difference (diff)
 *
 * @param diff			difference for time update
 */
static void updateRegisteredFunctionsTime(uint32_t diff)
{
	uint16_t u16i;

	// check if functions are registered
	if(m_u32NextTriggerTime != UINT32_MAX)
	{
		// update call time of functions
		for(u16i=0; u16i<CFG_FUNR_MAX_FUNCTIONS; u16i++)
		{
			TRACE_DBG_L("entry %u, old %u ", u16i, m_FunctionInfo[u16i].u32CallTime);
			if(m_FunctionInfo[u16i].u32CallTime > diff)
			{
				m_FunctionInfo[u16i].u32CallTime -= diff;
			}
			else
			{
				// function should be executed as soon as possible
				m_FunctionInfo[u16i].u32CallTime = 0;
			}
			TRACE_DBG_L("new time: %u \n", m_FunctionInfo[u16i].u32CallTime);
		}

		// update next trigger time
		m_u32NextTriggerTime = m_FunctionInfo[0].u32CallTime;
		TRACE_DBG_L("next trigger time: %u \n", m_u32NextTriggerTime);
	}
} // End of updateRegisteredFunctionsTime

/*--------------------------------------------------------------------------*/

/**
 * calculate time difference between actual system time and stored base time
 * check if difference is greater than 2^31 - 1 and update base time
 * and registered functions time.
 *
 * @return     time difference [ms]
 *
 * */
static uint32_t getActualTime(void)
{
	uint32_t u32ActualTime; ///< [ms] time since startup

	// calculate time difference
	u32ActualTime = (uint32_t)(m_getMilliseconds() - m_u64BaseTime);

    // check overflow
    if(u32ActualTime > ku32Uint31Max)
    {
    	TRACE_DBG_H("update bootTime\n");

    	// update time of registered functions
    	updateRegisteredFunctionsTime(ku32Uint31Max + 1);

    	// update base time
		m_u64BaseTime += (uint64_t)(ku32Uint31Max + 1);
		TRACE_DBG_L("m_u64BaseTime: 0x%08X:%08X \n",
					(uint32_t)(m_u64BaseTime >> 32),
					(uint32_t)(m_u64BaseTime & 0xFFFFFFFF));

    	// update actual time difference
    	u32ActualTime &= ku32Uint31Max;
    	TRACE_DBG_L("Actual Time difference: %u \n", u32ActualTime);
    }

    return (u32ActualTime); // [ms] since start

} // End of getActualTime

/*--------------------------------------------------------------------------*/

/**
 * It sets the event callback time inside the HAL layer.
 * */
static void setNextEvent(void)
{
    if (m_FunctionInfo[0].Function == NULL)
    {
        disableEvent();
    }
    else
    {
        m_u32NextTriggerTime = m_FunctionInfo[0].u32CallTime;
    }
} // End of setNextEvent

/*--------------------------------------------------------------------------*/

/**
 * Inserts a callback function in the function struct array.
 * */
static void insertFunction(uint16_t u16Index,
                           FUNR_CallbackType Function, uint32_t u32Time)
{
    m_FunctionInfo[u16Index].Function    = Function;
    m_FunctionInfo[u16Index].u32CallTime = u32Time;
//    printf("insert %d Time: %lu\r\n", u16Index, u64Time);

} // End of insertFunction

/*--------------------------------------------------------------------------*/

/**
 * It copies the next lower function in the list one position up.
 * */
static void shiftFunctionUp(uint16_t u16Index)
{
    uint16_t i = u16Index - 1;
    m_FunctionInfo[u16Index].Function    = m_FunctionInfo[i].Function;
    m_FunctionInfo[u16Index].u32CallTime = m_FunctionInfo[i].u32CallTime;
//    printf("shift up %d > %d Time: %lu\r\n", i, u16Index,
//            m_FunctionInfo[u16Index].u64CallTime);

} // End of shiftFunctionUp

/*--------------------------------------------------------------------------*/

/**
 * It copies the next higher function in the list one position down.
 * */
static void shiftFunctionDown(uint16_t u16Index)
{
    uint16_t i = u16Index + 1;
    m_FunctionInfo[u16Index].Function    = m_FunctionInfo[i].Function;
    m_FunctionInfo[u16Index].u32CallTime = m_FunctionInfo[i].u32CallTime;
    m_FunctionInfo[i].Function = NULL;
//    printf("shift down %d > %d Time: %lu\r\n", i, u16Index,
//            m_FunctionInfo[u16Index].u64CallTime);

} // End of shiftFunctionDown

/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  functions                                                                |
 +--------------------------------------------------------------------------*/

bool FUNR_register(FUNR_CallbackType Function, uint32_t u32Milliseconds)
{
    uint32_t u32TriggerTime;
    uint16_t u16i;
	uint16_t u16x;

    // check if desired trigger time is in range
    if(u32Milliseconds <= ku32Uint31Max)
    {
    	u32TriggerTime = u32Milliseconds + getActualTime();
    }
    else
    {
    	ERR_setErrorParam(&MAX_TIME_ERROR, __LINE__, NULL, u32Milliseconds, 0);
		return (true);
    }

    // Make sure that the function is only registered once. Else re-trigger it.
    for (u16i = 0; u16i < CFG_FUNR_MAX_FUNCTIONS; u16i++)
    {
        if (m_FunctionInfo[u16i].Function == Function)
        {
            FUNR_unregister(Function);
        }
    }

    // disable interrupts
    interruptControl(true);

    // Insert callback sorted into the queue
    for (u16i = 0; u16i < CFG_FUNR_MAX_FUNCTIONS; u16i++)
    {
        if (m_FunctionInfo[u16i].Function == NULL)
        {
        	u16x = u16i;
            while (u16x > 0)
            {
                if (m_FunctionInfo[u16x - 1].u32CallTime > u32TriggerTime)
                {
                    shiftFunctionUp(u16x);
                }
                else
                {
                    insertFunction(u16x, Function, u32TriggerTime);
                    break;
                }
                u16x--;
            }
            if (u16x == 0)
            {
                insertFunction(0, Function, u32TriggerTime);
            }
            break;
        }
        else
        {
            if (u16i == CFG_FUNR_MAX_FUNCTIONS-1)
            {
                ERR_setErrorParam(&MAX_FUNCTION_ERROR, __LINE__, NULL,
                				  u16i, u32Milliseconds);
            	// enable interrupts
                interruptControl(false);
                return (true);
            }
        }
    }
    setNextEvent();
	// enable interrupts
    interruptControl(false);
    return (false);

} // End of FUNR_register

/*--------------------------------------------------------------------------*/

void FUNR_unregister (FUNR_CallbackType Function)
{
    uint16_t u16i;
	uint16_t u16x;

	// disable interrupts
	interruptControl(true);

    for (u16i = 0; u16i < CFG_FUNR_MAX_FUNCTIONS; u16i++)
    {
        if (m_FunctionInfo[u16i].Function == Function)
        {
        	u16x = u16i;
            while (u16x < CFG_FUNR_MAX_FUNCTIONS-1)
            {
                shiftFunctionDown(u16x);
                u16x++;
            }
            break;
        }
    }
    setNextEvent();

	// enable interrupts
    interruptControl(false);

} // End of FUNR_unregister

/*--------------------------------------------------------------------------*/

void FUNR_unregisterAll (void)
{
    uint16_t u16i;

	// disable interrupts
    interruptControl(true);

    for (u16i = 0; u16i < CFG_FUNR_MAX_FUNCTIONS; u16i++)
    {
        m_FunctionInfo[u16i].Function = NULL;  // NULL, nothing registered
        m_FunctionInfo[u16i].u32CallTime = 0;  // OFF
    }
    setNextEvent();

	// enable interrupts
    interruptControl(false);

} // End of FUNR_unregisterAll

/*--------------------------------------------------------------------------*/

void FUNR_runFunctions(FUNR_CallbackType lowPowerFunct)
{
    uint32_t          u32Time;
    FUNR_CallbackType Function;

    if (getActualTime() >= m_u32NextTriggerTime)
    {
        // It calls all the functions who have an old execution time.
        u32Time = getActualTime();
        while (true)
        {
            if (m_FunctionInfo[0].Function == NULL)
            {
                disableEvent(); // All work done
                break;
            }
            if (m_FunctionInfo[0].u32CallTime <= u32Time)
            {
                Function = m_FunctionInfo[0].Function;
                FUNR_unregister(Function);
                Function();
            }
            else
            {
                break;  // Nothing to do so far
            }
        }
    }

	// power function registered
	if(lowPowerFunct != NULL)
	{
		TRACE_DBG_L("start low power mode\n");
		// start low power mode
		lowPowerFunct();
	}

 } // End of FUNR_runFunctions

/*--------------------------------------------------------------------------*/

void FUNR_synchTime(uint64_t u64OldTime, uint64_t u64NewTime)
{
    uint32_t u32Diff;

    TRACE_DBG_L("FUNR_syncTime called\n");

    u32Diff = (uint32_t)(u64OldTime - m_u64BaseTime);
    TRACE_DBG_L("difference %u \n", u32Diff);

    // update base time to new actual time
    m_u64BaseTime = u64NewTime;
    TRACE_DBG_H("set base time to 0x%08X:%08X \n",
				(uint32_t)(m_u64BaseTime >> 32),
				(uint32_t)(m_u64BaseTime & 0xFFFFFFFF));

    // update time of registered functions
    updateRegisteredFunctionsTime(u32Diff);

 } // End of FUNR_synchTime

/*--------------------------------------------------------------------------*/

void FUNR_init(FUNR_GetMillisecondType getMilliseconds, FUNR_InterruptControl interruptControl)
{
    // store function pointer to Variables
    m_getMilliseconds = getMilliseconds;
    m_interruptControl = interruptControl;

    if (m_getMilliseconds == NULL)
    {
    	// no time function registered
    	ERR_setErrorParam(&NO_TIME_FUNCTION, __LINE__, NULL, 0, 0);
    }
    else
    {
        m_u64BaseTime = m_getMilliseconds();
        FUNR_unregisterAll();
    }
} // End of FUNR_init

/*--------------------------------------------------------------------------*/

bool FUNR_queueIsEmpty(void)
{
    bool ret = false;

        if (m_FunctionInfo[0].Function == NULL)
        {
                ret = true;
        }

        return ret;

 } // End of FUNR_queueIsEmpty

/*--------------------------------------------------------------------------*/

void FUNR_showRegistered(void)
{
	uint32_t u32ActTime;
	uint16_t u16i;

	u32ActTime = getActualTime();

	TRACE_PRINT("Actual Time diff: %lu \n", u32ActTime);

	for(u16i=0; u16i<CFG_FUNR_MAX_FUNCTIONS; u16i++)
	{
		// check if function is registered
		if(m_FunctionInfo[u16i].Function != NULL)
		{
			TRACE_PRINT("Funct %u, exec time %lu  of function 0x%08X \n",
						 u16i, m_FunctionInfo[u16i].u32CallTime,
						 m_FunctionInfo[u16i].Function-1);
		}
		else
		{
			break;
		}
	}
} // End of FUNR_showRegistered

/*--------------------------------------------------------------------------*/

/*
 * Function not tested !!!
 * todo: Test the function
 */
bool FUNR_IsRegistered (FUNR_CallbackType Function)
{
	bool b_erg = false;
	uint16_t u16i;

	for (u16i = 0; u16i < CFG_FUNR_MAX_FUNCTIONS; u16i++)
	{
		// check if function is registered
		if(m_FunctionInfo[u16i].Function == Function)
		{
			b_erg = true;
			u16i = CFG_FUNR_MAX_FUNCTIONS;
		}
	}

	return b_erg;
} // End FUNR_IsRegistered

/*--------------------------------------------------------------------------*/

#ifdef CFG_MODULE_FUNR_TESTS_ACTIVE

/*--------------------------------------------------------------------------+
 |  module test function NOT active for the final product                    |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  local test functions                                                     |
 +--------------------------------------------------------------------------*/

static const char* getTimeString(void)
{
    static char TimeString[30];
    uint32_t Time = getActualTime();

    sprintf(TimeString, "%lu.%03lu sec ",
            (Time / 1000), (Time % 1000));
    return(TimeString);
}
/*--------------------------------------------------------------------------*/

static void StopAll(void)
{
	TRACE_PRINT("%s    StopAll\r\n", getTimeString());
    FUNR_unregisterAll();
}

/*--------------------------------------------------------------------------*/

static void Test1(void)
{
	TRACE_PRINT("%s   Test 1\r\n", getTimeString());
    if (FUNR_register(StopAll, 1000))
    {
    	TRACE_PRINT("StopAll ended with error\r\n");
    }
}

/*--------------------------------------------------------------------------*/

static void Test2(void)
{
	TRACE_PRINT("%s Test 2\r\n", getTimeString());
    if (FUNR_register(Test2, 50))
    {
    	TRACE_PRINT("Test-2 50 ms setup failed\r\n");
    }
}

/*--------------------------------------------------------------------------*/

static void Test3(void)
{
	TRACE_PRINT("%s    Test 3\r\n", getTimeString());
    if (FUNR_register(Test3, 200))
    {
    	TRACE_PRINT("Test-3 200 ms setup failed\r\n");
    }
}

/*--------------------------------------------------------------------------+
 |  global test functions                                                    |
 +--------------------------------------------------------------------------*/

void FUNR_test(void)
{
	uint64_t u64Time;

	TRACE_PRINT("FUNR_test started\n");

	// remove all registered functions
	FUNR_unregisterAll();

	// no function is registered - overflow
	TRACE_PRINT("-------------------------------------------------------------\n");
	TRACE_PRINT("check overflow with no function registered\n");
	TRACE_PRINT("-------------------------------------------------------------\n\n");
	// set bootTime 2s before Overflow
	u64Time = m_getMilliseconds();
	m_u64BaseTime = u64Time + 2000 - ku32Uint31Max;
	TRACE_PRINT("set base time to 0x%08X:%08X \n",
				(uint32_t)(m_u64BaseTime >> 32),
				(uint32_t)(m_u64BaseTime & 0xFFFFFFFF));

	// busy wait - runFunctions() is called after overflow
	TRACE_PRINT("busy wait 3.5s ...\n")
	TIME_waitMicroSeconds(3500000);	// wait 3.5s
	FUNR_runFunctions(NULL);

	// one function is registered to be executed before overflow
	// system is busy or go to sleep - runFunctions is called after overflow
	// check if this function is called right after bootTime-update
	TRACE_PRINT("-------------------------------------------------------------\n");
	TRACE_PRINT("check if function is executed after overflow with busy wait\n");
	TRACE_PRINT("-------------------------------------------------------------\n\n");
	// set bootTime 2s before Overflow
	u64Time = m_getMilliseconds();
	m_u64BaseTime = u64Time + 2000 - ku32Uint31Max;
	TRACE_PRINT("set base time to 0x%08X:%08X \n",
				(uint32_t)(m_u64BaseTime >> 32),
				(uint32_t)(m_u64BaseTime & 0xFFFFFFFF));

	// register function to be executed before overflow
	if (FUNR_register(StopAll, 1000))
	{
		TRACE_PRINT("StopAll ended with error\r\n");
	}
	// register function to be executed after overflow
	if (FUNR_register(Test1, 5000))
	{
		TRACE_PRINT("StopAll ended with error\r\n");
	}

	// busy wait - runFunctions() is called after overflow
    TRACE_PRINT("busy wait 3.5s ...\n")
    TIME_waitMicroSeconds(3500000);	// wait 3.5s
    FUNR_runFunctions(NULL);


    TRACE_PRINT("-------------------------------------------------------------\n");
    TRACE_PRINT("try to register functions with to big, max and min trig time\n");
    TRACE_PRINT("-------------------------------------------------------------\n\n");
	// try to register functions with to big time
    if (FUNR_register(StopAll, 0x80000000))
    {
    	TRACE_PRINT("StopAll ended with error - trig time to big\r\n");
    }
    if (FUNR_register(StopAll, 0xFFFFFFFF))
    {
    	TRACE_PRINT("StopAll ended with error - trig time to big\r\n");
    }
    // register max possible time
    if (FUNR_register(StopAll, 0x7FFFFFFF))
    {
    	TRACE_PRINT("StopAll ended with error\r\n");
    }
    // register min possible time
	if (FUNR_register(StopAll, 0))
	{
		TRACE_PRINT("StopAll ended with error\r\n");
	}
	TRACE_PRINT("min trig time set\n");
	FUNR_runFunctions(NULL);

    TRACE_PRINT("-------------------------------------------------------------\n");
    TRACE_PRINT("check overflow with several registered functions\n");
    TRACE_PRINT("-------------------------------------------------------------\n\n");

    // set bootTime 10s before Overflow
	u64Time = m_getMilliseconds();
	m_u64BaseTime = u64Time + 10000 - ku32Uint31Max;
	TRACE_PRINT("set base time to 0x%08X:%08X \n",
				(uint32_t)(m_u64BaseTime >> 32),
				(uint32_t)(m_u64BaseTime & 0xFFFFFFFF));

    // register function to be executed after overflow
    if (FUNR_register(Test1, 15000))
    {
    	TRACE_PRINT("Test-1 ended with error\r\n");
    }
    // register functions to be executed before overflow
    if (FUNR_register(Test2, 3000))
    {
    	TRACE_PRINT("Test-2 ended with error\r\n");
    }
    if (FUNR_register(Test3, 2500))
    {
    	TRACE_PRINT("Test-3 ended with error\r\n");
    }
    TRACE_PRINT("function run test will run for about 16s \n");
	
	// FUNR_runFunctions() has to be called in main loop

} // End of FUNR_test

/*--------------------------------------------------------------------------*/
#endif

//// *** move to mainloop for test ***
///**
// * test function to register
// */
//void test_funct(void)
//{
//	static uint8_t u8Count = 0;
//	uint64_t u64ActTime;
//
//	u64ActTime = TIME_getMilliSeconds();
//	TRACE_PRINT("act time %08X \n",
//				(uint32_t)(u64ActTime & 0xFFFFFFFF));
//
//	if(u8Count < 100)
//	{
//		TRACE_PRINT("re-reg\n");
//		FUNR_register(test_funct, 500);
//	}
//	++u8Count;
//}
//
///**
// *	update RTC and update trigger time of all registered functions
// */
//void test_rtc_update(void)
//{
//	uint64_t u64OldTime;
//	uint64_t u64NewTime;
//
//	TRACE_PRINT("register test_funct\n");
//	FUNR_register(test_funct, 200);
//
//	// busy wait to execute function one time before RTC-Update
//	TIME_waitMicroSeconds(200000);
//	FUNR_runFunctions();
//
//	u64OldTime = TIME_getMilliSeconds();
//	// update RTC
//	RTC_setRTC(1524482218);	// 23.04.2018, 11:16:58 GMT
//	// sync system time
//	TIME_synch((uint64_t)RTC_getRTC() * 1000);
//	u64NewTime = TIME_getMilliSeconds();
//	// update scheduled functions
//	FUNR_synchTime(u64OldTime, u64NewTime);
//}
/*--------------------------------------------------------------------------*/
