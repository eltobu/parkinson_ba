/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Generic Library
 *
 *     Module: KX122
 *      State: Not formally tested
 * Originator: Büchli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */
/**
 * @file
 * Module KX122: Driver for Accelerometer.
 *
 * This module implements the HAL-Interface for
 * Accelerometer Low-Level Routines, accessed over SPI interface
 *
 ****************************************************************************
 */

#ifndef KX122_H
#define KX122_H

#include <stdint.h>
#include "spi.h"
#include "error.h"

#define KX122_16BIT_2G_CONVERSION_TO_MG(x)		((uint32_t)((uint32_t)x*(uint32_t)1000)/16384)
#define KX122_16BIT_4G_CONVERSION_TO_MG(x)		((uint32_t)((uint32_t)x*(uint32_t)1000)/8192)
#define KX122_16BIT_8G_CONVERSION_TO_MG(x)		((uint32_t)((uint32_t)x*(uint32_t)1000)/4096)
#define KX122_8BIT_2G_CONVERSION_TO_MG(x)		((uint32_t)((uint32_t)x*(uint32_t)1000)/64)
#define KX122_8BIT_4G_CONVERSION_TO_MG(x)		((uint32_t)((uint32_t)x*(uint32_t)1000)/32)
#define KX122_8BIT_8G_CONVERSION_TO_MG(x)		((uint32_t)((uint32_t)x*(uint32_t)1000)/16)

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Context definition for KX122
 */
typedef struct KX122_Context {
	uint32_t INT1_Pin;           ///< INT1 pin number
	uint32_t INT2_Pin;           ///< INT2 pin number
	SPI_Context spi;            ///< SPI interface context
} KX122_Context;

/**
 * Operating modes
 */
typedef enum KX122_OperatingMode {
	KX122_STANDBY = 0x00, KX122_OPERATING = 0x80
} KX122_OperatingMode;

/**
 * Data rates
 */
typedef enum KX122_DataRate {
	KX122_OSA_12_5 = 0x00,
	KX122_OSA_25 = 0x01,
	KX122_OSA_50 = 0x02,
	KX122_OSA_100 = 0x03,
	KX122_OSA_200 = 0x04,
	KX122_OSA_400 = 0x05,
	KX122_OSA_800 = 0x06,
	KX122_OSA_1600 = 0x07,
	KX122_OSA_0_781 = 0x08,
	KX122_OSA_1_563 = 0x09,
	KX122_OSA_3_125 = 0x0A,
	KX122_OSA_6_25 = 0x0B,
	KX122_OSA_3200 = 0x0C,
	KX122_OSA_4600 = 0x0D,
	KX122_OSA_12800 = 0x0E,
	KX122_OSA_25600 = 0x0F,
} KX122_DataRate;

/**
 * Data rates (tap detection)
 */
typedef enum KX122_TapDataRate {
	KX122_OTDT_50 = 0x00,
	KX122_OTDT_100 = 0x01,
	KX122_OTDT_200 = 0x02,
	KX122_OTDT_400 = 0x03,
	KX122_OTDT_12_5 = 0x04,
	KX122_OTDT_25 = 0x05,
	KX122_OTDT_800 = 0x06,
	KX122_OTDT_1600 = 0x07,
} KX122_TapDataRate;

/**
 * Resolutions
 */
typedef enum KX122_Resolution {
	KX122_8BIT = 0x00, KX122_16BIT = 0x40
} KX122_Resolution;

/**
 * Ranges
 */
typedef enum KX122_Range {
	KX122_2G = 0x00, KX122_4G = 0x08, KX122_8G = 0x10
} KX122_Range;

/**
 * Low-pass filter modes
 */
typedef enum KX122_LowPassMode {
	KX122_LP_ODR_9 = 0x00, KX122_LP_ODR_2 = 0x40, KX122_LP_BYPASS = 0x80
} KX122_LowPassMode;

/**
 * Averaged samples
 */
typedef enum KX122_Average {
	KX122_AVG_NO = 0x00,
	KX122_AVG_2 = 0x10,
	KX122_AVG_4 = 0x20,
	KX122_AVG_8 = 0x30,
	KX122_AVG_16 = 0x40,
	KX122_AVG_32 = 0x50,
	KX122_AVG_64 = 0x60,
	KX122_AVG_128 = 0x70,
} KX122_Average;

/**
 * Interrupt sources
 */
typedef enum KX122_InterruptSource {
	KX122_INT_TILT = 0x01,
	KX122_INT_MOTION = 0x02,
	KX122_INT_TAP = 0x04,
	KX122_INT_DATA_READY = 0x10,
	KX122_INT_WATERMARK = 0x20,
	KX122_INT_BUFFER_FULL = 0x40,
	KX122_INT_FREE_FALL = 0x80
} KX122_InterruptSource;

/**
 * Buffer modes
 */
typedef enum KX122_BufferMode {
	KX122_BUFFER_INACTIVE = 0x00,
	KX122_BUFFER_FIFO = 0x80,
	KX122_BUFFER_STREAM = 0x81,
	KX122_BUFFER_TRIGGER = 0x82,
	KX122_BUFFER_FILO = 0x83
} KX122_BufferMode;

/**
 * Buffer full interrupt state
 */
typedef enum KX122_BufferFullInterruptState {
	KX122_BFI_DISABLE = 0x00, KX122_BFI_ENABLE = 0x20
} KX122_BufferFullInterruptState;

/**
 * Initializes the device
 *
 * @param pContext : Context for accelerometer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_init(const KX122_Context *pContext);

/**
 * Deinitializes the device
 *
 * @param pContext : Context for accelerometer
 */
void KX122_deinit(const KX122_Context *pContext);

/**
 * Resets the device
 *
 * @param pContext : Context for accelerometer
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_reset(const KX122_Context *pContext, ERR_pErrorInfo pError);

/**
 * Test SPI communication
 *
 * @param pContext : Context for accelerometer
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_comTest(const KX122_Context *pContext, ERR_pErrorInfo pError);

/**
 * Who am I
 * It's something like: 0x1B
 *
 * @param pContext : Context for accelerometer
 * @param pData : pointer to data (1 byte)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_whoAmI(const KX122_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError);

/**
 * Sets the operating mode
 *
 * @param pContext : Context for accelerometer
 * @param mode : operating mode
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setOperatingMode(const KX122_Context *pContext,
		KX122_OperatingMode mode, ERR_pErrorInfo pError);

/**
 * Sets the data rate
 *
 * @param pContext : Context for accelerometer
 * @param rate : data rate
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setDataRate(const KX122_Context *pContext, KX122_DataRate rate,
		ERR_pErrorInfo pError);

/**
 * Sets the data resolution
 *
 * @param pContext : Context for accelerometer
 * @param res : data resolution
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setResolution(const KX122_Context *pContext,
		KX122_Resolution res, ERR_pErrorInfo pError);

/**
 * Sets the data range
 *
 * @param pContext : Context for accelerometer
 * @param range : data range
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setRange(const KX122_Context *pContext, KX122_Range range,
		ERR_pErrorInfo pError);

/**
 * Configures the low-pass filter
 *
 * @param pContext : Context for accelerometer
 * @param mode : low-pass filter mode
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setLowPassFilter(const KX122_Context *pContext,
		KX122_LowPassMode mode, ERR_pErrorInfo pError);

/**
 * Sets the number of samples to be averaged
 *
 * @param pContext : Context for accelerometer
 * @param avg : averaged samples
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setAverage(const KX122_Context *pContext, KX122_Average avg,
		ERR_pErrorInfo pError);

/**
 * Set tap detection on/off
 *
 * @param pContext : Context for accelerometer
 * @param on : wether tap detection should turn on
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setTapDetection(const KX122_Context *pContext, bool on,
		ERR_pErrorInfo pError);

/**
 * Sets the tap data rate
 *
 * @param pContext : Context for accelerometer
 * @param rate : tap data rate
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setTapDataRate(const KX122_Context *pContext,
		KX122_TapDataRate rate, ERR_pErrorInfo pError);

/**
 * Configures interrupt sources for INT1 pin
 *
 * @param pContext : Context for accelerometer
 * @param source : interrupt source
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setInterrupt1(const KX122_Context *pContext,
		KX122_InterruptSource source, ERR_pErrorInfo pError);

/**
 * Configures interrupt sources for INT2 pin
 *
 * @param pContext : Context for accelerometer
 * @param source : interrupt source
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setInterrupt2(const KX122_Context *pContext,
		KX122_InterruptSource source, ERR_pErrorInfo pError);

/**
 * Sets the buffer mode
 *
 * @param pContext : Context for accelerometer
 * @param mode : buffer mode
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setBufferMode(const KX122_Context *pContext,
		KX122_BufferMode mode, ERR_pErrorInfo pError);

/**
 * Sets the buffer resolution
 *
 * @param pContext : Context for accelerometer
 * @param res : buffer resolution
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setBufferResolution(const KX122_Context *pContext,
		KX122_Resolution res, ERR_pErrorInfo pError);

/**
 * Sets the buffer watermark
 *
 * @param pContext : Context for accelerometer
 * @param mark : buffer watermark (10 bit)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setBufferWatermark(const KX122_Context *pContext, uint16_t mark,
		ERR_pErrorInfo pError);

/**
 * Enables/Disables the buffer full interrupt
 *
 * @param pContext : Context for accelerometer
 * @param enable : enable interrupt
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_setBufferFullInterrupt(const KX122_Context *pContext,
		KX122_BufferFullInterruptState state, ERR_pErrorInfo pError);

/**
 * Reads acceleration data output
 *
 * @param pContext : Context for accelerometer
 * @param pData : pointer to data (3 [low res] or 6 [high res] bytes)
 * @param res : data resolution
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_readOutput(const KX122_Context *pContext, uint8_t* pData,
		KX122_Resolution res, ERR_pErrorInfo pError);

/**
 * Reads buffer content
 *
 * @param pContext : Context for accelerometer
 * @param pData : pointer to data (samples * 3 [low res] or 6 [high res] bytes)
 * @param res : data resolution
 * @param samples : number of samples to read
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_readBuffer(const KX122_Context *pContext, uint8_t* pData,
		KX122_Resolution res, uint16_t samples, ERR_pErrorInfo pError);

/**
 * Clears buffer
 *
 * @param pContext : Context for accelerometer
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_clearBuffer(const KX122_Context *pContext, ERR_pErrorInfo pError);

#ifdef  __cplusplus
}
#endif

#endif /* KX122_H */
