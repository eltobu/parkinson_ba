/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Generic Library
 *
 *     Module: KX122
 *      State: Not formally tested
 * Originator: Büchli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

#include "KX122.h"

#include <string.h>

#include "error.h"
#include "system_time.h"
#include "gpio.h"

/**
 * Internally used accelerometer registers
 */
typedef enum KX122_InternalReg {
	KX122_XHP_L = 0x00,
	KX122_XHP_H = 0x01,
	KX122_YHP_L = 0x02,
	KX122_YHP_H = 0x03,
	KX122_ZHP_L = 0x04,
	KX122_ZHP_H = 0x05,
	KX122_XOUT_L = 0x06,
	KX122_XOUT_H = 0x07,
	KX122_YOUT_L = 0x08,
	KX122_YOUT_H = 0x09,
	KX122_ZOUT_L = 0x0A,
	KX122_ZOUT_H = 0x0B,
	KX122_COTR = 0x0C,
	KX122_WHO_AM_I = 0x0F,
	KX122_TSCP = 0x10,
	KX122_TSPP = 0x11,
	KX122_INS1 = 0x12,
	KX122_INS2 = 0x13,
	KX122_INS3 = 0x14,
	KX122_STATUS_REG = 0x15,
	KX122_INT_REL = 0x17,
	KX122_CNTL1 = 0x18,
	KX122_CNTL2 = 0x19,
	KX122_CNTL3 = 0x1A,
	KX122_ODCNTL = 0x1B,
	KX122_INC1 = 0x1C,
	KX122_INC2 = 0x1D,
	KX122_INC3 = 0x1E,
	KX122_INC4 = 0x1F,
	KX122_INC5 = 0x20,
	KX122_INC6 = 0x21,
	KX122_TILT_TIMER = 0x22,
	KX122_WUFC = 0x23,
	KX122_TDTRC = 0x24,
	KX122_TDTC = 0x25,
	KX122_TTH = 0x26,
	KX122_TTL = 0x27,
	KX122_FTD = 0x28,
	KX122_STD = 0x29,
	KX122_TLT = 0x2A,
	KX122_TWS = 0x2B,
	KX122_FFTH = 0x2C,
	KX122_FFC = 0x2D,
	KX122_FFCNTL = 0x2E,
	KX122_ATH = 0x30,
	KX122_TILT_ANGLE_LL = 0x32,
	KX122_TILT_ANGLE_HL = 0x33,
	KX122_HYST_SET = 0x34,
	KX122_LP_CNTL = 0x35,
	KX122_BUF_CNTL1 = 0x3A,
	KX122_BUF_CNTL2 = 0x3B,
	KX122_BUF_STATUS_1 = 0x3C,
	KX122_BUF_STATUS_2 = 0x3D,
	KX122_BUF_CLEAR = 0x3E,
	KX122_BUF_READ = 0x3F,
	KX122_SELF_TEST = 0x60
} KX122_InternalReg;

/**
 * Internally used register bits
 */
typedef enum KX122_InternalRegBit {
	KX122_CNTL1_TDTE = 0x02,
	KX122_CNTL1_GSEL = 0x18,
	KX122_CNTL1_RES = 0x40,
	KX122_CNTL1_PC1 = 0x80,
	KX122_CNTL2_COTC = 0x40,
	KX122_CNTL2_SRST = 0x80,
	KX122_CNTL3_OTDT = 0x38,
	KX122_ODCNTL_OSA = 0x0F,
	KX122_ODCNTL_LP = 0xC0,
	KX122_INC_PWSEL = 0xC0,
	KX122_INC_IEN = 0x20,
	KX122_INC_IEA = 0x10,
	KX122_INC_IEL = 0x08,
	KX122_LP_CNTL_AVC = 0x70,
	KX122_BUF_CNTL2_SMP_TH = 0x0C,
	KX122_BUF_CNTL2_BFIE = 0x20,
	KX122_BUF_CNTL2_BRES = 0x40,
	KX122_BUF_CNTL2_BUFM_BUFE = 0x83
} KX122_InternalRegBit;

/**
 * Reads registers
 *
 * @param pContext : Context for accelerometer
 * @param reg : start register address
 * @param pData : pointer to data
 * @param size : size of data
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_readRegisters(const KX122_Context *pContext,
		const KX122_InternalReg reg, uint8_t* pData, uint8_t size,
		ERR_pErrorInfo pError);

/**
 * Reads from register
 *
 * @param pContext : Context for accelerometer
 * @param reg : register address
 * @param pData : pointer to data
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_readRegister(const KX122_Context *pContext,
		const KX122_InternalReg reg, uint8_t* pData, ERR_pErrorInfo pError);

/**
 * Writes to register
 *
 * @param pContext : Context for accelerometer
 * @param reg : register address
 * @param pData : pointer to data
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_writeRegister(const KX122_Context *pContext,
		const KX122_InternalReg reg, uint8_t* pData, ERR_pErrorInfo pError);

/**
 * Update bits in register
 *
 * @param pContext : Context for accelerometer
 * @param reg : register address
 * @param pData : pointer to data
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t KX122_updateRegisterBits(const KX122_Context *pContext,
		const KX122_InternalReg reg, uint8_t* pMask, uint8_t* pData,
		ERR_pErrorInfo pError);

uint32_t KX122_init(const KX122_Context *pContext) {
	GPIO_pinConfigInput(pContext->INT1_Pin, GPIO_PIN_NOPULL);
	GPIO_pinConfigInput(pContext->INT2_Pin, GPIO_PIN_NOPULL);

	return (SPI_init(&pContext->spi));
}

void KX122_deinit(const KX122_Context *pContext) {
	SPI_deinit(&pContext->spi);
}

uint32_t KX122_reset(const KX122_Context *pContext, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_CNTL2_SRST;
	status = KX122_writeRegister(pContext, KX122_CNTL2, &buf, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_comTest(const KX122_Context *pContext, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t mask;
	uint8_t buf;

	status = KX122_readRegister(pContext, KX122_COTR, &buf, pError);
	RETURN_ON_ERROR(status);
	if (buf != 0x55) {
		return ERR_ERROR;
	}

	mask = KX122_CNTL2_COTC;
	buf = KX122_CNTL2_COTC;
	status = KX122_updateRegisterBits(pContext, KX122_CNTL2, &mask, &buf,
			pError);
	RETURN_ON_ERROR(status);

	status = KX122_readRegister(pContext, KX122_COTR, &buf, pError);
	RETURN_ON_ERROR(status);
	if (buf != 0xAA) {
		return ERR_ERROR;
	}

	mask = KX122_CNTL2_COTC;
	buf = 0;
	status = KX122_updateRegisterBits(pContext, KX122_CNTL2, &mask, &buf,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_whoAmI(const KX122_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError) {
	uint32_t status;

	status = KX122_readRegister(pContext, KX122_WHO_AM_I, pData, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setOperatingMode(const KX122_Context *pContext,
		KX122_OperatingMode mode, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_CNTL1_PC1;
	status = KX122_updateRegisterBits(pContext, KX122_CNTL1, &buf, &mode,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setDataRate(const KX122_Context *pContext, KX122_DataRate rate,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_ODCNTL_OSA;
	status = KX122_updateRegisterBits(pContext, KX122_ODCNTL, &buf, &rate,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setResolution(const KX122_Context *pContext,
		KX122_Resolution res, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_CNTL1_RES;
	status = KX122_updateRegisterBits(pContext, KX122_CNTL1, &buf, &res,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setRange(const KX122_Context *pContext, KX122_Range range,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_CNTL1_GSEL;
	status = KX122_updateRegisterBits(pContext, KX122_CNTL1, &buf, &range,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setLowPassFilter(const KX122_Context *pContext,
		KX122_LowPassMode mode, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_ODCNTL_LP;
	status = KX122_updateRegisterBits(pContext, KX122_ODCNTL, &buf, &mode,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setAverage(const KX122_Context *pContext, KX122_Average avg,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_LP_CNTL_AVC;
	status = KX122_updateRegisterBits(pContext, KX122_LP_CNTL, &buf, &avg,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setTapDetection(const KX122_Context *pContext, bool on,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t mask, data;

	mask = KX122_CNTL1_TDTE;
	data = (on) ? KX122_CNTL1_TDTE : 0;
	status = KX122_updateRegisterBits(pContext, KX122_CNTL1, &mask, &data,
			pError);
	RETURN_ON_ERROR(status);
	data = 0;

	mask = 0b11;
	data = 0b01;
	status = KX122_updateRegisterBits(pContext, KX122_TDTRC, &mask, &data,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setTapDataRate(const KX122_Context *pContext,
		KX122_TapDataRate rate, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_CNTL3_OTDT;
	status = KX122_updateRegisterBits(pContext, KX122_CNTL3, &buf, &rate,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setInterrupt1(const KX122_Context *pContext,
		KX122_InterruptSource source, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	if (source) {
		buf = KX122_INC_PWSEL | KX122_INC_IEN | KX122_INC_IEA | KX122_INC_IEL;
		status = KX122_writeRegister(pContext, KX122_INC1, &buf, pError);
		RETURN_ON_ERROR(status);
	} else {
		buf = KX122_INC_PWSEL | KX122_INC_IEA | KX122_INC_IEL;
		status = KX122_writeRegister(pContext, KX122_INC1, &buf, pError);
		RETURN_ON_ERROR(status);
	}
	status = KX122_writeRegister(pContext, KX122_INC4, &source, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setInterrupt2(const KX122_Context *pContext,
		KX122_InterruptSource source, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	if (source) {
		buf = KX122_INC_PWSEL | KX122_INC_IEN | KX122_INC_IEA | KX122_INC_IEL;
		status = KX122_writeRegister(pContext, KX122_INC5, &buf, pError);
		RETURN_ON_ERROR(status);
	} else {
		buf = KX122_INC_PWSEL | KX122_INC_IEA | KX122_INC_IEL;
		status = KX122_writeRegister(pContext, KX122_INC5, &buf, pError);
		RETURN_ON_ERROR(status);
	}
	status = KX122_writeRegister(pContext, KX122_INC6, &source, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setBufferMode(const KX122_Context *pContext,
		KX122_BufferMode mode, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_BUF_CNTL2_BUFM_BUFE;
	status = KX122_updateRegisterBits(pContext, KX122_BUF_CNTL2, &buf, &mode,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setBufferResolution(const KX122_Context *pContext,
		KX122_Resolution res, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_BUF_CNTL2_BRES;
	status = KX122_updateRegisterBits(pContext, KX122_BUF_CNTL2, &buf, &res,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setBufferWatermark(const KX122_Context *pContext, uint16_t mark,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	status = KX122_writeRegister(pContext, KX122_BUF_CNTL1, (uint8_t*) &mark,
			pError);
	RETURN_ON_ERROR(status);
	mark >>= 6;
	mark &= KX122_BUF_CNTL2_SMP_TH;
	buf = KX122_BUF_CNTL2_SMP_TH;
	status = KX122_updateRegisterBits(pContext, KX122_BUF_CNTL2, &buf,
			(uint8_t*) &mark, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_setBufferFullInterrupt(const KX122_Context *pContext,
		KX122_BufferFullInterruptState state, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = KX122_BUF_CNTL2_BFIE;
	status = KX122_updateRegisterBits(pContext, KX122_BUF_CNTL2, &buf, &state,
			pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_readOutput(const KX122_Context *pContext, uint8_t* pData,
		KX122_Resolution res, ERR_pErrorInfo pError) {
	uint32_t status;

	status = KX122_readRegisters(pContext, KX122_XOUT_L, pData,
			(res == KX122_8BIT) ? 3 : 6, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_readBuffer(const KX122_Context *pContext, uint8_t* pData,
		KX122_Resolution res, uint16_t samples, ERR_pErrorInfo pError) {
	uint32_t status;
	uint16_t size = samples * ((res == KX122_8BIT) ? 3 : 6);

	uint8_t num_blocks = (size + 254) / 255;
	for (uint8_t i = 0; i < num_blocks - 1; ++i) {
		status = KX122_readRegisters(pContext, KX122_BUF_READ, pData, 255,
				pError);
		size -= 255;
		pData += 255;
	}
	status = KX122_readRegisters(pContext, KX122_BUF_READ, pData, size, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_clearBuffer(const KX122_Context *pContext, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	buf = 0;
	status = KX122_writeRegister(pContext, KX122_BUF_CLEAR, &buf, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t KX122_readRegisters(const KX122_Context *pContext,
		const KX122_InternalReg reg, uint8_t* pData, uint8_t size,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	// read from register
	buf = reg | 0x80;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, &buf, &buf, 1, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	status = SPI_txRxBlock(&pContext->spi, pData, pData, size, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// move data
	return ERR_OK;
}

uint32_t KX122_readRegister(const KX122_Context *pContext,
		const KX122_InternalReg reg, uint8_t* pData, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[2];

	// read from register
	buf[0] = reg | 0x80;
	buf[1] = 0;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// move data
	*pData = buf[1];
	return ERR_OK;
}

uint32_t KX122_writeRegister(const KX122_Context *pContext,
		const KX122_InternalReg reg, uint8_t* pData, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[2];

	// write to register
	buf[0] = reg & 0x7F;
	buf[1] = *pData;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// move data
	*pData = buf[1];
	return ERR_OK;
}

uint32_t KX122_updateRegisterBits(const KX122_Context *pContext,
		const KX122_InternalReg reg, uint8_t* pMask, uint8_t* pData,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf;

	status = KX122_readRegister(pContext, reg, &buf, pError);
	RETURN_ON_ERROR(status);

	buf &= ~*pMask;
	buf |= *pData;
	status = KX122_writeRegister(pContext, reg, &buf, pError);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}
