/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Generic Library
 *
 *     Module: MT29F
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

#include "MT29F.h"

#include <string.h>

#include "error.h"
#include "system_time.h"

/**
 * Flash commands
 */
typedef enum MT29F_Cmd {
	MT29F_RESET = 0xFF,
	MT29F_GET_FEATURES = 0x0F,
	MT29F_SET_FEATURES = 0x1F,
	MT29F_READ_ID = 0x9F,
	MT29F_PAGE_READ = 0x13,
	MT29F_READ_PAGE_CACHE_RANDOM = 0x30,
	MT29F_READ_PAGE_CACHE_LAST = 0x3F,
	MT29F_READ_PAGE_CACHE_X1 = 0x0B,
	MT29F_READ_PAGE_CACHE_X2 = 0x3B,
	MT29F_READ_PAGE_CACHE_X4 = 0x6B,
	MT29F_READ_PAGE_CACHE_DUAL_IO = 0xBB,
	MT29F_READ_PAGE_CACHE_QUAD_IO = 0xEB,
	MT29F_WRITE_ENABLE = 0x06,
	MT29F_WRITE_DISABLE = 0x04,
	MT29F_BLOCK_ERASE = 0xD8,
	MT29F_PROGRAM_EXECUTE = 0x10,
	MT29F_PROGRAM_LOAD_X1 = 0x02,
	MT29F_PROGRAM_LOAD_X4 = 0x32,
	MT29F_PROGRAM_LOAD_RANDOM_DATA_X1 = 0x84,
	MT29F_PROGRAM_LOAD_RANDOM_DATA_X4 = 0x34
} MT29F_Cmd;

uint32_t MT29F_init(const MT29F_Context *pContext) {
	return SPI_init(&pContext->spi);
}

void MT29F_deinit(const MT29F_Context *pContext) {
	SPI_deinit(&pContext->spi);
}

uint32_t MT29F_reset(const MT29F_Context *pContext, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[1];

	// read device id
	buf[0] = MT29F_RESET;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);
	TIME_waitMicroSeconds(1250);
	return ERR_OK;
}

uint32_t MT29F_readId(const MT29F_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[4];

	// read device id
	buf[0] = MT29F_READ_ID;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// move data
	memcpy(pData, &buf[2], 4);
	return ERR_OK;
}

uint32_t MT29F_readUniqueId(const MT29F_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[4];
	uint32_t wait;

	// change to otp, parameter and unique id pages
	buf[0] = MT29F_SET_FEATURES;
	buf[1] = MT29F_CONFIG;
	buf[2] = MT29F_CFG1;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// read page to cache
	buf[0] = MT29F_PAGE_READ;
	buf[1] = 0;
	buf[2] = 0;
	buf[3] = 0;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);
	TIME_waitMicroSeconds(40);

	// wait for ready
	buf[0] = MT29F_GET_FEATURES;
	buf[1] = MT29F_STATUS;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	wait = 0;
	do {
		TIME_waitMicroSeconds(10);
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		++wait;
	} while ((buf[0] & MT29F_OIP) && wait < MT29F_MAX_WAIT_LOOPS);
	if (wait >= MT29F_MAX_WAIT_LOOPS)
		return ERR_ERROR;
	SPI_setCsOff(&pContext->spi);

	// read page area from cache
	buf[0] = MT29F_READ_PAGE_CACHE_X1;
	buf[1] = 0;
	buf[2] = 0;
	buf[3] = 0;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	status = SPI_txRxBlock(&pContext->spi, pData, pData, 16, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// change to normal operation
	buf[0] = MT29F_SET_FEATURES;
	buf[1] = MT29F_CONFIG;
	buf[2] = MT29F_ECC_EN;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	return ERR_OK;
}

uint32_t MT29F_readFeature(const MT29F_Context *pContext, const uint8_t address,
		uint8_t* pData, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[3];

	// read feature
	buf[0] = MT29F_GET_FEATURES;
	buf[1] = address;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// move data
	*pData = buf[2];
	return ERR_OK;
}

uint32_t MT29F_writeFeature(const MT29F_Context *pContext,
		const uint8_t address, uint8_t* pData, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[3];

	// write feature
	buf[0] = MT29F_SET_FEATURES;
	buf[1] = address;
	buf[2] = *pData;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	return ERR_OK;
}

uint32_t MT29F_readChunk(const MT29F_Context *pContext, const uint32_t address,
		const uint16_t start, uint8_t* pData, const uint16_t size,
		ERR_pErrorInfo pError) {
	if (address >= pContext->deviceSize * pContext->blockSize) {
		return ERR_ERROR;
	}

	uint32_t status;
	uint8_t buf[4];
	uint32_t wait;

	uint32_t raddr = address;
	uint16_t caddr = start;

	// select die
	if (pContext->dies > 1) {
		buf[0] = MT29F_SET_FEATURES;
		buf[1] = MT29F_DIE_SELECT;
		buf[2] =
				(address & (1 << pContext->dieSelectPos)) ?
						MT29F_DIE_SELECT0 : 0;
		SPI_setCsOn(&pContext->spi);
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		SPI_setCsOff(&pContext->spi);
	}

	// read page to cache
	buf[0] = MT29F_PAGE_READ;
	buf[1] = raddr >> 16;
	buf[2] = raddr >> 8;
	buf[3] = raddr;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);
	TIME_waitMicroSeconds(40);

	// wait for ready
	buf[0] = MT29F_GET_FEATURES;
	buf[1] = MT29F_STATUS;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	wait = 0;
	do {
		TIME_waitMicroSeconds(10);
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		++wait;
	} while ((buf[0] & MT29F_OIP) && wait < MT29F_MAX_WAIT_LOOPS);
	if (wait >= MT29F_MAX_WAIT_LOOPS)
		return ERR_ERROR;
	SPI_setCsOff(&pContext->spi);

	if (size > 0) {
		// read page area from cache
		buf[0] = MT29F_READ_PAGE_CACHE_X1;
		buf[1] = caddr >> 8 | (address & (1 << 6)) >> 2;
		buf[2] = caddr;
		buf[3] = 0;
		SPI_setCsOn(&pContext->spi);
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		status = SPI_txRxBlock(&pContext->spi, pData, pData, size, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		SPI_setCsOff(&pContext->spi);

		// check success
		buf[0] = MT29F_GET_FEATURES;
		buf[1] = MT29F_STATUS;
		SPI_setCsOn(&pContext->spi);
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		SPI_setCsOff(&pContext->spi);
		switch ((buf[2] >> 4) & 0x07) {
		case 0: // no errors detected
			break;
		case 1: // 1-3 errors corrected
			break;
		case 3: // 4-6 errors corrected
			break;
		case 5: // 7-8 errors corrected
			if (pContext->badBlockCallback != NULL)
				pContext->badBlockCallback(raddr);
			break;
		case 2: // >8 errors detected
			if (pContext->badBlockCallback != NULL)
				pContext->badBlockCallback(raddr);
			return ERR_ERROR;
		default:
			return ERR_ERROR;
		}
	}

	return ERR_OK;
}

uint32_t MT29F_writeChunk(const MT29F_Context *pContext, const uint32_t address,
		const uint16_t start, uint8_t* pData, const uint16_t size,
		ERR_pErrorInfo pError) {
	if (address >= pContext->deviceSize * pContext->blockSize) {
		return ERR_ERROR;
	}

	uint32_t status;
	uint8_t buf[4];
	uint32_t wait;

	uint32_t raddr = address;
	uint16_t caddr = start;

	// select die
	buf[0] = MT29F_SET_FEATURES;
	buf[1] = MT29F_DIE_SELECT;
	buf[2] = (address & (1 << 17)) ? MT29F_DIE_SELECT0 : 0;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// read page to cache
	buf[0] = MT29F_PAGE_READ;
	buf[1] = raddr >> 16;
	buf[2] = raddr >> 8;
	buf[3] = raddr;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);
	TIME_waitMicroSeconds(40);

	// wait for ready
	buf[0] = MT29F_GET_FEATURES;
	buf[1] = MT29F_STATUS;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	wait = 0;
	do {
		TIME_waitMicroSeconds(10);
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		++wait;
	} while ((buf[0] & MT29F_OIP) && wait < MT29F_MAX_WAIT_LOOPS);
	if (wait >= MT29F_MAX_WAIT_LOOPS)
		return ERR_ERROR;
	SPI_setCsOff(&pContext->spi);

	// unlock all blocks
	buf[0] = MT29F_SET_FEATURES;
	buf[1] = MT29F_BLOCK_LOCK;
	buf[2] = 0x00;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// set write enable latch
	buf[0] = MT29F_WRITE_ENABLE;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// wait for ready
	buf[0] = MT29F_GET_FEATURES;
	buf[1] = MT29F_STATUS;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	wait = 0;
	do {
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		++wait;
	} while (!(buf[0] & MT29F_WEL) && wait < MT29F_MAX_WAIT_LOOPS);
	if (wait >= MT29F_MAX_WAIT_LOOPS)
		return ERR_ERROR;
	SPI_setCsOff(&pContext->spi);

	// write page area to cache
	buf[0] = MT29F_PROGRAM_LOAD_RANDOM_DATA_X1;
	buf[1] = caddr >> 8 | (address & (1 << 6)) >> 2;
	buf[2] = caddr;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 3, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	status = SPI_txRxBlock(&pContext->spi, pData, pData, size, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// write from cache
	buf[0] = MT29F_PROGRAM_EXECUTE;
	buf[1] = raddr >> 16;
	buf[2] = raddr >> 8;
	buf[3] = raddr;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);
	TIME_waitMicroSeconds(200);

	// wait for ready & check success
	buf[0] = MT29F_GET_FEATURES;
	buf[1] = MT29F_STATUS;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	wait = 0;
	do {
		TIME_waitMicroSeconds(20);
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		++wait;
	} while ((buf[0] & MT29F_OIP) && wait < MT29F_MAX_WAIT_LOOPS);
	if (wait >= MT29F_MAX_WAIT_LOOPS)
		return ERR_ERROR;
	SPI_setCsOff(&pContext->spi);
	if (buf[2] & MT29F_P_FAIL) {
		if (pContext->badBlockCallback != NULL)
			pContext->badBlockCallback(raddr);
		return ERR_ERROR;
	}

	return ERR_OK;
}

uint32_t MT29F_readPage(const MT29F_Context *pContext, const uint32_t address,
		uint8_t* pData, const MT29F_PageArea area, ERR_pErrorInfo pError) {
	uint16_t start = (area != MT29F_SPARE) ? 0 : 0x800;
	uint16_t size = 0;
	if (area == MT29F_MAIN || area == MT29F_ALL)
		size += pContext->pageSize;
	if (area == MT29F_SPARE || area == MT29F_ALL)
		size += pContext->spareSize;

	return MT29F_readChunk(pContext, address, start, pData, size, pError);
}

uint32_t MT29F_writePage(const MT29F_Context *pContext, const uint32_t address,
		uint8_t* pData, const MT29F_PageArea area, ERR_pErrorInfo pError) {
	uint16_t start = (area != MT29F_SPARE) ? 0 : 0x800;
	uint16_t size = 0;
	if (area == MT29F_MAIN || area == MT29F_ALL)
		size += pContext->pageSize;
	if (area == MT29F_SPARE || area == MT29F_ALL)
		size += pContext->spareSize;

	return MT29F_writeChunk(pContext, address, start, pData, size, pError);
}

uint32_t MT29F_eraseBlock(const MT29F_Context *pContext, const uint16_t block,
		ERR_pErrorInfo pError) {
	if (block >= pContext->deviceSize) {
		return ERR_ERROR;
	}

	uint32_t status;
	uint8_t buf[4];
	uint32_t wait;

	uint32_t raddr = block * pContext->blockSize;

	// set write enable latch
	buf[0] = MT29F_WRITE_ENABLE;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// erase block
	buf[0] = MT29F_BLOCK_ERASE;
	buf[1] = raddr >> 16;
	buf[2] = raddr >> 8;
	buf[3] = raddr;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);
	TIME_waitMicroSeconds(1800);

	// wait for ready & check success
	buf[0] = MT29F_GET_FEATURES;
	buf[1] = MT29F_STATUS;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	wait = 0;
	do {
		TIME_waitMicroSeconds(200);
		status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
		RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
		++wait;
	} while ((buf[0] & MT29F_OIP) && wait < MT29F_MAX_WAIT_LOOPS);
	if (wait >= MT29F_MAX_WAIT_LOOPS)
		return ERR_ERROR;
	SPI_setCsOff(&pContext->spi);
	if (buf[2] & MT29F_E_FAIL) {
		if (pContext->badBlockCallback != NULL)
			pContext->badBlockCallback(raddr);
		return ERR_ERROR;
	}

	return ERR_OK;
}
