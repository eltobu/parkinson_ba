/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Generic Library
 *
 *     Module: MT29F
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */
/**
 * @file
 * Module MT29F: Driver for external data Flash. Originally written
 * for MT29F4G01ADA, made configurable over context to access different types
 * of the same family.
 *
 * This module implements the HAL-Interface for
 * Flash Low-Level Routines, accessed over SPI interface
 *
 ****************************************************************************
 */

#ifndef MT29F_H
#define MT29F_H

#include <stdint.h>
#include "spi.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MT29F_MAX_WAIT_LOOPS	100

/**
 *
 * Bad Block Callback, a function that is called when a bad block is detected
 *
 * */
typedef uint32_t (*MT29F_BadBlockCallback)(uint32_t address);

/**
 * Context definition for MT29F
 *
 * Define the memory organization.
 */
typedef struct tagMT29F_Context {
	uint16_t pageSize;          ///< Number of bytes per page
	uint16_t spareSize;         ///< Number of spare bytes per page (w/o ECC)
	uint16_t blockSize;         ///< Number of pages per block
	uint16_t deviceSize;        ///< Total number of blocks
	uint8_t dies;               ///< Number of dies
	uint8_t dieSelectPos;        ///< Position in page address for die selection
	SPI_Context spi;            ///< SPI interface context
	MT29F_BadBlockCallback badBlockCallback;
} MT29F_Context;

/**
 * Feature addresses
 */
typedef enum MT29F_Feature {
	MT29F_BLOCK_LOCK = 0xA0,
	MT29F_CONFIG = 0xB0,
	MT29F_STATUS = 0xC0,
	MT29F_DIE_SELECT = 0xD0
} MT29F_Feature;

/**
 * Feature data bits
 */
typedef enum MT29F_FeatureData {
	MT29F_WP_HOLD_DISABLE = 0x02,
	MT29F_TB = 0x04,
	MT29F_BP0 = 0x08,
	MT29F_BP1 = 0x10,
	MT29F_BP2 = 0x20,
	MT29F_BP3 = 0x40,
	MT29F_BRWD = 0x80,
	MT29F_CFG0 = 0x02,
	MT29F_ECC_EN = 0x10,
	MT29F_LOT_EN = 0x20,
	MT29F_CFG1 = 0x40,
	MT29F_CFG2 = 0x80,
	MT29F_OIP = 0x01,
	MT29F_WEL = 0x02,
	MT29F_E_FAIL = 0x04,
	MT29F_P_FAIL = 0x08,
	MT29F_ECCS0 = 0x10,
	MT29F_ECCS1 = 0x20,
	MT29F_ECCS2 = 0x40,
	MT29F_CRBSY = 0x80,
	MT29F_DIE_SELECT0 = 0x40,
} MT29F_FeatureData;

typedef enum MT29F_PageArea {
	MT29F_MAIN, MT29F_SPARE, MT29F_ALL
} MT29F_PageArea;

/**
 * Initializes the device
 *
 * @param pContext : Context for flash
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_init(const MT29F_Context *pContext);

/**
 * Deinitializes the device
 *
 * @param pContext : Context for flash
 */
void MT29F_deinit(const MT29F_Context *pContext);

/**
 * Reset the device
 *
 * @param pContext : Context for flash
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_reset(const MT29F_Context *pContext, ERR_pErrorInfo pError);

/**
 * Manufacturer and Device ID Read
 * It's something like: 0x2C36
 *
 * @param pContext : Context for flash
 * @param pData : pointer to data (2 bytes)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_readId(const MT29F_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError);

/**
 * Unique ID Page Read
 *
 * @param pContext : Context for flash
 * @param pData : pointer to data (16 bytes)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_readUniqueId(const MT29F_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError);

/**
 * Read from a feature register
 *
 * @param pContext : Context for flash
 * @param address : feature address
 * @param pData : pointer to data (1 byte)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_readFeature(const MT29F_Context *pContext, const uint8_t address,
		uint8_t* pData, ERR_pErrorInfo pError);

/**
 * Write to a feature register
 *
 * @param pContext : Context for flash
 * @param address : feature address
 * @param pData : pointer to data (1 byte)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_writeFeature(const MT29F_Context *pContext,
		const uint8_t address, uint8_t* pData, ERR_pErrorInfo pError);

/**
 * Read a chunk of data within a page
 *
 * @param pContext : Context for flash
 * @param address : page address
 * @param start : start byte address
 * @param pData : pointer to data chunk
 * @param size : size of data chunk
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_readChunk(const MT29F_Context *pContext, const uint32_t address,
		const uint16_t start, uint8_t* pData, const uint16_t size,
		ERR_pErrorInfo pError);

/**
 * Write a chunk of data within a page
 *
 * @param pContext : Context for flash
 * @param address : page address
 * @param start : start byte address
 * @param pData : pointer to data chunk
 * @param size : size of data chunk
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_writeChunk(const MT29F_Context *pContext, const uint32_t address,
		const uint16_t start, uint8_t* pData, const uint16_t size,
		ERR_pErrorInfo pError);

/**
 * Read a whole page
 *
 * @param pContext : Context for flash
 * @param address : page address
 * @param pData : pointer to data (page and/or spare size)
 * @param area : area of page concerned
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_readPage(const MT29F_Context *pContext, const uint32_t address,
		uint8_t* pData, const MT29F_PageArea area, ERR_pErrorInfo pError);

/**
 * Write a whole page
 *
 * @param pContext : Context for flash
 * @param address : page address
 * @param pData : pointer to data (page and/or spare size)
 * @param area : area of page concerned
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_writePage(const MT29F_Context *pContext, const uint32_t address,
		uint8_t* pData, const MT29F_PageArea area, ERR_pErrorInfo pError);

/**
 * Erase blockwise
 *
 * @param pContext : Context for flash
 * @param block : block address
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t MT29F_eraseBlock(const MT29F_Context *pContext, const uint16_t block,
		ERR_pErrorInfo pError);

#ifdef  __cplusplus
}
#endif

#endif /* MT29F_H */
