/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Generic Library
 *
 *     Module: FM25V
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

#include "FM25V.h"

#include <string.h>

#include "error.h"
#include "system_time.h"

/**
 * FRAM commands
 */
typedef enum FM25V_Cmd {
	FM25V_WRITE_ENABLE = 0x06,
	FM25V_WRITE_DISABLE = 0x04,
	FM25V_READ_STATUS = 0x05,
	FM25V_WRITE_STATUS = 0x01,
	FM25V_READ_MEMORY = 0x03,
	FM25V_FAST_READ_MEMORY = 0x0B,
	FM25V_WRITE_MEMORY = 0x02,
	FM25V_ENTER_SLEEP = 0xB9,
	FM25V_READ_ID = 0x9F
} FM25V_Cmd;

uint32_t FM25V_init(const FM25V_Context *pContext) {
	return (SPI_init(&pContext->spi));
}

void FM25V_deinit(const FM25V_Context *pContext) {
	SPI_deinit(&pContext->spi);
}

uint32_t FM25V_readId(const FM25V_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[10];

	// read device id
	buf[0] = FM25V_READ_ID;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 10, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// move data
	memcpy(pData, &buf[1], 9);
	return ERR_OK;
}

uint32_t FM25V_sleep(const FM25V_Context *pContext, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[1];

	buf[0] = FM25V_ENTER_SLEEP;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	return ERR_OK;
}

uint32_t FM25V_wakeUp(const FM25V_Context *pContext, ERR_pErrorInfo pError) {
	SPI_setCsOn(&pContext->spi);
	TIME_waitMicroSeconds(450);
	SPI_setCsOff(&pContext->spi);

	return ERR_OK;
}

uint32_t FM25V_readStatus(const FM25V_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[2];

	// read status
	buf[0] = FM25V_READ_STATUS;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// move data
	*pData = buf[1];
	return ERR_OK;
}

uint32_t FM25V_writeStatus(const FM25V_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[2];

	// set write enable latch
	buf[0] = FM25V_WRITE_ENABLE;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// write status
	buf[0] = FM25V_WRITE_STATUS;
	buf[1] = *pData;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 2, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	return ERR_OK;
}

uint32_t FM25V_readChunk(const FM25V_Context *pContext, const uint32_t address,
		uint8_t* pData, const uint16_t size, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[4];

	// read from memory
	buf[0] = FM25V_READ_MEMORY;
	buf[1] = address >> 16;
	buf[2] = address >> 8;
	buf[3] = address;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	status = SPI_txRxBlock(&pContext->spi, pData, pData, size, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	return ERR_OK;
}

uint32_t FM25V_writeChunk(const FM25V_Context *pContext, const uint32_t address,
		uint8_t* pData, const uint16_t size, ERR_pErrorInfo pError) {
	uint32_t status;
	uint8_t buf[4];

	// set write enable latch
	buf[0] = FM25V_WRITE_ENABLE;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 1, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	// write to memory
	buf[0] = FM25V_WRITE_MEMORY;
	buf[1] = address >> 16;
	buf[2] = address >> 8;
	buf[3] = address;
	SPI_setCsOn(&pContext->spi);
	status = SPI_txRxBlock(&pContext->spi, buf, buf, 4, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	status = SPI_txRxBlock(&pContext->spi, pData, pData, size, pError);
	RETURN_ON_ERROR_CLEAN(status, SPI_setCsOff(&pContext->spi));
	SPI_setCsOff(&pContext->spi);

	return ERR_OK;
}
