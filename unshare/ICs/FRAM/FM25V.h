/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Generic Library
 *
 *     Module: FM25V
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */
/**
 * @file
 * Module FM25V: Driver for external FRAM. Originally written
 * for FM25V20A, made configurable over context to access different types
 * of the same family.
 *
 * This module implements the HAL-Interface for
 * FRAM Low-Level Routines, accessed over SPI interface
 *
 ****************************************************************************
 */

#ifndef FM25V_H
#define FM25V_H

#include <stdint.h>
#include "spi.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Context definition for FM25V
 *
 * Define the memory organization.
 */
typedef struct tagFM25V_Context {
	uint32_t size;              ///< Total number of bytes
	SPI_Context spi;            ///< SPI interface context
} FM25V_Context;

/**
 * Status data bits
 */
typedef enum FM25V_StatusData {
	FM25V_WEL = 0x02, FM25V_BP0 = 0x04, FM25V_BP1 = 0x08, FM25V_WPEN = 0x80,
} FM25V_StatusData;

/**
 * Initializes the device
 *
 * @param pContext : Context for FRAM
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t FM25V_init(const FM25V_Context *pContext);

/**
 * Deinitializes the device
 *
 * @param pContext : Context for FRAM
 */
void FM25V_deinit(const FM25V_Context *pContext);

/**
 * Manufacturer and Device ID Read
 * It's something like: 0x7F7F7F7F7F7FC22508
 *
 * @param pContext : Context for FRAM
 * @param pData : pointer to data (10 byte)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t FM25V_readId(const FM25V_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError);

/**
 * Let FRAM sleep
 *
 * @param pContext : Context for FRAM
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t FM25V_sleep(const FM25V_Context *pContext, ERR_pErrorInfo pError);

/**
 * Wake up FRAM
 *
 * @param pContext : Context for FRAM
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t FM25V_wakeUp(const FM25V_Context *pContext, ERR_pErrorInfo pError);

/**
 * Read from status register
 *
 * @param pContext : Context for FRAM
 * @param pData : pointer to data (1 byte)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t FM25V_readStatus(const FM25V_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError);

/**
 * Write to status register
 *
 * @param pContext : Context for FRAM
 * @param pData : pointer to data (1 byte)
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t FM25V_writeStatus(const FM25V_Context *pContext, uint8_t* pData,
		ERR_pErrorInfo pError);

/**
 * Read a chunk of data
 *
 * @param pContext : Context for FRAM
 * @param address : start byte address
 * @param pData : pointer to data chunk
 * @param size : size of data chunk
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t FM25V_readChunk(const FM25V_Context *pContext, const uint32_t address,
		uint8_t* pData, const uint16_t size, ERR_pErrorInfo pError);

/**
 * Write a chunk of data
 *
 * @param pContext : Context for FRAM
 * @param address : start byte address
 * @param pData : pointer to data chunk
 * @param size : size of data chunk
 * @param pError : error info pointer
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t FM25V_writeChunk(const FM25V_Context *pContext, const uint32_t address,
		uint8_t* pData, const uint16_t size, ERR_pErrorInfo pError);

#ifdef  __cplusplus
}
#endif

#endif /* FM25V_H */
