/****************************************************************************
 *
 *   Customer: Art of Technology AG
 *   Project#: N/A
 *       Name: SAM7 Modules
 *
 *     Module: AOTTYPE
 *      State:
 * Originator: Daetwyler
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/general/aottype.h $
 *  $Revision: 25328 $
 *      $Date: 2018-07-03 14:16:17 +0200 (Tue, 03 Jul 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2005
 *
 ****************************************************************************
 */
/**
 * @file
 * Module aottype: Type definition for compiler portability
 *
 * This module defines ordinary data types for portability
 *
 ****************************************************************************
 */

#ifndef AOTTYPE_H_
#define AOTTYPE_H_

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Includes needed by this header (with include-guard!) -------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  helper defines                                                         |
+--------------------------------------------------------------------------*/
/**
 * generic DEPRECATED attribute
 * (produce compiler warning when a function marked as deprecated is called
 */
#define DEPRECATED __attribute__((deprecated))

/**
 * generic WEAK attribute
 */
#define WEAK __attribute__((weak))

/** 
 * define the declared function to be stored in SRAM 
 */
#define RAMFUNC  __attribute__ ((long_call, section (".ramfunc")))

/** 
 * generic UNUSED attribute
 *  
 * USAGE:
 * void myFunction(int8 UNUSED(i8var)) 
 * {
 *      // may not use "i8var" any more
 * }
 */
#ifdef UNUSED
#elif defined(__GNUC__)
    #define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
    #define UNUSED(x) /*@unused@*/ x
#else
    #define UNUSED(x) x
#endif    

/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/

#ifndef INT8_MAX
	#define INT8_MAX      127
#endif
#ifndef INT8_MIN
	#define INT8_MIN     -128
#endif
#ifndef INT16_MAX
	#define INT16_MAX   32767
#endif
#ifndef INT16_MIN
	#define INT16_MIN  -32768
#endif
#ifndef INT32_MAX
    #define INT32_MAX   2147483647L
#endif
#ifndef INT32_MIN
    #define INT32_MIN  -2147483648L
#endif
#ifndef INT64_MAX
    #define INT64_MAX   9223372036854775807LL
#endif
#ifndef INT64_MIN
    #define INT64_MIN  -9223372036854775808LL
#endif
#ifndef UINT8_MAX
    #define UINT8_MAX  255U
#endif
#ifndef UINT16_MAX
    #define UINT16_MAX 65535U
#endif
#ifndef UINT32_MAX
    #define UINT32_MAX 4294967295UL
#endif
#ifndef UINT64_MAX
    #define UINT64_MAX 18446744073709551615ULL
#endif

#ifndef EOF
    #define EOF (-1)
#endif

#ifndef NULL
    #define NULL ((void*)0)
#endif

#define WORDS_LITTLENDIAN
#define WORDSIZE 32
    
/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/

typedef int8_t  int8;       // signed char
typedef int16_t int16;      // signed short
typedef int32_t int32;      // signed long
typedef int64_t int64;      // signed long long

typedef uint8_t  uint8;     // unsigned char
typedef uint16_t uint16;    // unsigned short
typedef uint32_t uint32;    // unsigned long
typedef uint64_t uint64;    // unsigned long long

#ifndef __cplusplus

#endif
typedef uint8_t  bool8;     // smaller code
//typedef uint32_t bool32;    // obsolete, use bool 

/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/


#ifdef  __cplusplus
}
#endif


#endif // AOTTYPE_H_
