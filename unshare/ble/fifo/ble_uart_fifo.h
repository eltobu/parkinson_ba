/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: BLE_FIFO_H
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1/ble/fifo/ble_fifo.h $
 *  $Revision: 21052 $
 *      $Date: 2017-10-18 15:37:44 +0200 (Mi, 18 Okt 2017) $
 *    $Author: Schwinghammer $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This is the BLE UART FIFO header file.
 *
 ****************************************************************************
 */

 #ifndef SUBSYS_BLE_UART_FIFO_H_
 #define SUBSYS_BLE_UART_FIFO_H_

#include <stdint.h>

#include "fifo.h"

/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  function prototypes                                                      |
+--------------------------------------------------------------------------*/

/**@brief Clear the rx fifo.
 *
 */
void ble_fifo_clear_rx_fifo(uint8_t handle);

/**@brief Get one byte from the rx fifo.
 *
 */
char 	ble_fifo_pop_rx_fifo (uint8_t handle);

/**@brief Send/push one byte to the rx fifo.
 *
 */
void 	ble_fifo_push_rx_fifo (uint8_t handle, char x);

/**@brief Check different states of the fifo.
 *
 *@param[in] s	State to be checked. DATEN_IM_FIFO or DATEN_FREE_IM_FIFO.
 *
 *@retval requested state value.
 */
int 	ble_fifo_get_status_rx_fifo (uint8_t handle, int s);

void    BLE_Tx_CLEAR(uint8_t handle);
char 	BLE_Tx_POP (uint8_t handle);
void 	BLE_Tx_PUSH (uint8_t handle, char x);
int 	BLE_Tx_Status (uint8_t handle, int s);

#endif /* SUBSYS_BLE_UART_FIFO_H_ */
