/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: BLE FIFO
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1/ble/fifo/ble_fifo.c $
 *  $Revision: 21052 $
 *      $Date: 2017-10-18 15:37:44 +0200 (Mi, 18 Okt 2017) $
 *    $Author: Schwinghammer $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This module implements a FIFO for the storage of BLE event handler
 * user data.
 *
 ****************************************************************************
 */

/* standard-headers (ANSI) ------------------------------------------------*/

/* headers from other modules ---------------------------------------------*/
#include "ble_uart_fifo.h"
#include "module_config.h"

/*--------------------------------------------------------------------------+
|  module global variables                                                  |
+--------------------------------------------------------------------------*/
char	BLE_Rx_Fifo[BLE_MAX_TOTAL_LINK_COUNT][CONFIG_BLE_FIFO_RX_FIFO_SIZE + 1];	// Empfangsbuffer
int		BLE_Rx_FIFOin[BLE_MAX_TOTAL_LINK_COUNT];	  								// Input Zaehler
int 	BLE_Rx_FIFOout[BLE_MAX_TOTAL_LINK_COUNT];		  						// Output Zaehler

char	BLE_Tx_Fifo[BLE_MAX_TOTAL_LINK_COUNT][CONFIG_BLE_FIFO_TX_FIFO_SIZE + 1];	// Sendebuffer
int 	BLE_Tx_FIFOin[BLE_MAX_TOTAL_LINK_COUNT];	  								// Input Zaehler
int 	BLE_Tx_FIFOout[BLE_MAX_TOTAL_LINK_COUNT];		  						// Output Zaehler

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

void ble_fifo_clear_rx_fifo(uint8_t handle)
{
	BLE_Rx_FIFOin[handle] = 0;
	BLE_Rx_FIFOout[handle] = 0;
}

char ble_fifo_pop_rx_fifo(uint8_t handle)
{
	return fifo_POP(BLE_Rx_Fifo[handle], &BLE_Rx_FIFOout[handle], CONFIG_BLE_FIFO_RX_FIFO_SIZE);
}

void ble_fifo_push_rx_fifo(uint8_t handle, char x)
{
	fifo_PUSH (BLE_Rx_Fifo[handle], &BLE_Rx_FIFOin[handle], CONFIG_BLE_FIFO_RX_FIFO_SIZE, x);
}

int ble_fifo_get_status_rx_fifo(uint8_t handle, int s)
{
	return fifo_Status (s, BLE_Rx_FIFOin[handle], BLE_Rx_FIFOout[handle], CONFIG_BLE_FIFO_RX_FIFO_SIZE);
}

void BLE_Tx_CLEAR (uint8_t handle)
{
	BLE_Tx_FIFOin[handle] = 0;
	BLE_Tx_FIFOout[handle] = 0;
}

char BLE_Tx_POP (uint8_t handle)
{
	return fifo_POP (BLE_Tx_Fifo[handle], &BLE_Tx_FIFOout[handle], CONFIG_BLE_FIFO_TX_FIFO_SIZE);
}

void BLE_Tx_PUSH (uint8_t handle, char x)
{
	fifo_PUSH (BLE_Tx_Fifo[handle], &BLE_Tx_FIFOin[handle], CONFIG_BLE_FIFO_TX_FIFO_SIZE, x);
}

int BLE_Tx_Status (uint8_t handle, int s)
{
	return fifo_Status (s, BLE_Tx_FIFOin[handle], BLE_Tx_FIFOout[handle], CONFIG_BLE_FIFO_TX_FIFO_SIZE);
}
