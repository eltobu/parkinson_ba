/*
 * ble_uart_queue.h
 *
 *  Created on: 21.03.2018
 *      Author: VM
 */

#ifndef SHARE_AOT_NRF_BLE_SERVICES_UART_SERVICE_COMMON_BLE_UART_QUEUE_H_
#define SHARE_AOT_NRF_BLE_SERVICES_UART_SERVICE_COMMON_BLE_UART_QUEUE_H_

#include <stdint.h>

#include "queue.h"


Queue_t* BLE_getTxQueue(uint8_t handle);
void BLE_initTxQueue(uint8_t handle);


#endif /* SHARE_AOT_NRF_BLE_SERVICES_UART_SERVICE_COMMON_BLE_UART_QUEUE_H_ */
