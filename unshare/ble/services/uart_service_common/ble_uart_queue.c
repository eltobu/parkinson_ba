/*
 * ble_uart_queue.c
 *
 *  Created on: 21.03.2018
 *      Author: VM
 */


#include "ble_uart_queue.h"
#include "module_config.h"


static Queue_t m_tBleUartTxQueue[BLE_MAX_TOTAL_LINK_COUNT];


Queue_t* BLE_getTxQueue(uint8_t handle)
{
    return &m_tBleUartTxQueue[handle];
}

void BLE_initTxQueue(uint8_t handle)
{
    queue_init(&m_tBleUartTxQueue[handle], "BTXQ");
}
