/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: BLE_CENTRAL_H
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/ble/services/uart_service_c/ble_central.h $
 *  $Revision: 24163 $
 *      $Date: 2018-04-17 10:50:46 +0200 (Tue, 17 Apr 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This is the BLE central header file.
 *
 ****************************************************************************
 */

#ifndef SUBSYS_BLE_CENTRAL_H_
#define SUBSYS_BLE_CENTRAL_H_

#include <stdint.h>
#include <stdbool.h>
#include "ble_us_c.h"

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/
typedef enum {
	BLE_C_EVT_NONE = 0,
	//BLE_C_EVT_DISCONNECT_CLIENT, //todo: required?
	//BLE_C_EVT_BLE_GAP_EVT_DISCONNECTED, //removed as event only present on peripheral
	BLE_C_EVT_NUS_C_CONNECTED,
	BLE_C_EVT_NUS_C_DISCONNECTED,
} eBLE_C_AppEvt_t;

/**
 * context with central configuration
 *
 * code sample:
 *
 * static ble_uuid_t m_nus_uuid =
 * {
 *   .uuid = BLE_UUID_NUS_C_SERVICE,
 *   .type = BLE_UUID_TYPE_VENDOR_BEGIN
 * };
 *
 * static ble_gap_conn_params_t m_connection_param =
* {
*	.min_conn_interval = MSEC_TO_UNITS(20, UNIT_1_25_MS),  // Minimum connection interval
*	.max_conn_interval = MSEC_TO_UNITS(75, UNIT_1_25_MS),  // Maximum connection interval
*	.slave_latency     = 0,            					   // Slave latency
*	.conn_sup_timeout  = MSEC_TO_UNITS(4000, UNIT_10_MS)   // Supervision time-out
* };
*
* static ble_gap_scan_params_t m_scan_params =
* {
*   .active         = 1,								  // If 1, performs active scanning (scan requests).
*   .use_whitelist  = 0,								  // If 1, filter advertisers using current active whitelist
*   .adv_dir_report = 1,								  // If 1, ignore unknown devices (non whitelisted)
*   .interval       = MSEC_TO_UNITS(593, UNIT_0_625_MS),  // Determines scan interval in units of 0.625 millisecond
*   .window         = MSEC_TO_UNITS(593, UNIT_0_625_MS),  // Determines scan window in units of 0.625 millisecond
*   .timeout        = 0x0000,							  // Timeout when scanning. 0x0000 disables timeout
* };
*
* static BLE_C_Context m_bleContext =
* {
*  .u8_centralLinkCount = 1,
*  .u8_peripheralLinkCount = 0,
*  .scanParams = &m_scan_params,
*  .conParams  = &m_connection_param,
*  .f_dirAddrEna = true,
*  .p_bleUuid = &m_nus_uuid,
*  .pu8_macAddr = NULL,
* };
 */
typedef struct tagBLE_C_Context
{
	uint8_t					u8_centralLinkCount;	///< Number of central links used by the application
	uint8_t					u8_peripheralLinkCount;	///< Number of peripheral links used by the application
	ble_gap_scan_params_t* 	scanParams;				///< pointer to scan parameter structure
	ble_gap_conn_params_t* 	conParams;				///< pointer to connection parameter structure
	bool					f_dirAddrEna;			///< flag direct addressing mode enabled
	ble_uuid_t*				p_bleUuid;				///< NULL if UUID connection mode is unused
	uint8_t*				pu8_macAddr;			///< NULL if MAC-Address connection mode is unused
}BLE_C_Context;

/**@brief   Event handler type.
 *
 * @details This is the type of the event handler that should be provided by the application
 *          of this module to receive events.
 */
typedef void (*ble_c_evt_handler_t)(ble_evt_t* p_ble_evt);

/*--------------------------------------------------------------------------+
|  function prototypes                                                      |
+--------------------------------------------------------------------------*/
void         ble_central(void);
void         ble_central_set_application_event(uint8_t handle, eBLE_C_AppEvt_t tEvent);
uint16_t     ble_c_get_conn_handle(uint8_t handle);
ble_nus_c_t* ble_c_get_nus_c_service_instance(uint8_t handle);
void         ble_c_scan_start(void);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for BLE central service initialization.
 *
 * @details Initialize the BLE: SoftDevice, BLE stack, service.
 *
 * @param[in] pContext	  context with central configuration
 * @param[in] i8_tx_power Radio transmit power in dBm (accepted values are
 *                        -40, -30, -20, -16, -12, -8, -4, 0, 3, and 4 dBm)
 * @param[in] au8_irk     Identity Resolving Key (IRK), NULL when not used.
 * @param[in] u8_mode     0: The way it should be. 1: Use DEPRECATED frame number.
 */
//void ble_c_init(int8_t i8_tx_power, const uint8_t au8_irk[], uint8_t u8_mode);
void ble_c_init(BLE_C_Context* pContext, int8_t i8_tx_power, const uint8_t au8_irk[BLE_GAP_SEC_KEY_LEN], uint8_t u8_mode, ble_c_evt_handler_t evt_handler);

/**@brief Get actual data length
 *
 * @details max data length depending on MTU size
 */
uint16_t ble_central_get_data_length(void);

#endif /* SUBSYS_BLE_CENTRAL_H_ */
