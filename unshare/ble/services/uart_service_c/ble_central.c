/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: BLE central
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/ble/services/uart_service_c/ble_central.c $
 *  $Revision: 24163 $
 *      $Date: 2018-04-17 10:50:46 +0200 (Tue, 17 Apr 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This module implements a BLE central service running a BLE UART service.
 * Functions for interaction with the SoftDevice are implemented here.
 *
 ****************************************************************************
 */

/* standard-headers (ANSI) ------------------------------------------------*/
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "ble_central.h"

#include "board.h"
#include "ble_hci.h"
#include "ble_gap.h"
#include "ble_conn_state.h"
#include "ble_uart_fifo.h"
#include "ble_uart_queue.h"
#include "ble_advertising.h"

#include "nrf.h"
#include "nrf_ble_gatt.h"
#include "app_error.h"
#include "app_util.h"
#include "softdevice_handler.h"
#include "fifo.h"
#include "module_config.h"
#include "function_run.h"
#include "trace.h"
#include "queue.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_BLE
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_BLE undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_BLE
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

// *************************************************************

#if (NRF_SD_BLE_API_VERSION != 4)
#error "ERROR: This code is specified for Softdevice 4.xx"
#endif

/*--------------------------------------------------------------------------+
|  debug                                                                    |
+--------------------------------------------------------------------------*/
#define DBG_BLE_CENTRAL
#undef DBG_BLE_CENTRAL
#define DBG_BLE_CENTRAL_TX_DATA
#undef DBG_BLE_CENTRAL_TX_DATA
#define DBG_BLE_CENTRAL_TRACE_MAC_ADDRESS
#undef DBG_BLE_CENTRAL_TRACE_MAC_ADDRESS
#define DBG_BLE_C_TRACE_FREE_TX_PACKETS
#undef DBG_BLE_C_TRACE_FREE_TX_PACKETS
#define DBG_BLE_CENTRAL_MONITOR_BLE_NUS_C_EVT_NUS_TX_EVT
#undef DBG_BLE_CENTRAL_MONITOR_BLE_NUS_C_EVT_NUS_TX_EVT
#define DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
#undef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
#define DBG_BLE_CENTRAL_CONNECTION_MODE
#undef DBG_BLE_CENTRAL_CONNECTION_MODE

/*--------------------------------------------------------------------------+
|  constants                                                                |
+--------------------------------------------------------------------------*/

#define UUID16_SIZE             2                               /**< Size of 16 bit UUID */
#define UUID32_SIZE             4                               /**< Size of 32 bit UUID */
#define UUID128_SIZE            16                              /**< Size of 128 bit UUID */

#define OPCODE_HANDLE_LENGTH    3                               /**< OPCODE_LENGTH + HANDLE_LENGTH */
#define MAX_TX_PACKETS          3                               /**< There should be 6, 5 usable, but 4 do not crash */

/*--------------------------------------------------------------------------+
|  module global variables                                                  |
+--------------------------------------------------------------------------*/
static ble_nus_c_t          m_ble_nus_c[BLE_CENTRAL_TOTAL_LINK_COUNT];                    /**< Instance of NUS service. Must be passed to all NUS_C API calls. */
static ble_db_discovery_t   m_ble_db_discovery[BLE_CENTRAL_TOTAL_LINK_COUNT];             /**< Instance of database discovery module. Must be passed to all db_discovert API calls */

static eBLE_C_AppEvt_t		m_event = BLE_C_EVT_NONE;
static uint8_t              m_event_conn_handle;
static uint8_t 				m_u8_tx_packet_count[BLE_CENTRAL_TOTAL_LINK_COUNT];		///< check against total before/when connection is closed by central
static uint8_t              m_u8_mode = 0;              ///< 0: The way it should be, else: special transient code.

// MTU exchange variables
static nrf_ble_gatt_t       m_gatt;   ///< GATT module instance, taken from ble_ap_uart sample
static uint16_t             m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - OPCODE_HANDLE_LENGTH; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */

static BLE_C_Context* 		m_pBLE_Context;	///< pointer to BLE Central context, initialized in ble_c_init()

static ble_c_evt_handler_t  m_evt_handler;

/*--------------------------------------------------------------------------+
|  local functions                                                          |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**@brief Function for handling database discovery events.
 *
 * @details This function is callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function should forward the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void ble_c_db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    ble_nus_c_on_db_disc_evt(&m_ble_nus_c[p_evt->conn_handle], p_evt);
}

/*-------------------------------------------------------------------------*/
/**@brief Callback handling NUS Client events.
 *
 * @details This function is called to notify the application of NUS client events.
 *
 * @param[in]   p_ble_nus_c   NUS Client Handle. This identifies the NUS client
 * @param[in]   p_ble_nus_evt Pointer to the NUS Client event.
 */
/**@snippet [Handling events from the ble_nus_c module] */
static void ble_c_ble_nus_c_evt_handler(ble_nus_c_t * p_ble_nus_c, const ble_nus_c_evt_t * p_ble_nus_evt)
{
    uint32_t err_code;
    switch (p_ble_nus_evt->evt_type)
    {
        case BLE_NUS_C_EVT_DISCOVERY_COMPLETE:
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_NUS_C_EVT_DISCOVERY_COMPLETE\n");
#endif
            err_code = ble_nus_c_handles_assign(p_ble_nus_c, p_ble_nus_evt->conn_handle, &p_ble_nus_evt->handles);
            APP_ERROR_CHECK(err_code);

            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
            APP_ERROR_CHECK(err_code);
            ble_central_set_application_event(p_ble_nus_evt->conn_handle, BLE_C_EVT_NUS_C_CONNECTED);
            break;

        case BLE_NUS_C_EVT_NUS_TX_EVT:
#ifdef DBG_BLE_CENTRAL_MONITOR_BLE_NUS_C_EVT_NUS_TX_EVT
            TRACE_DBG_L("BLE_NUS_C_EVT_NUS_TX_EVT\n");
#endif
			for (uint8_t i = 0; i < p_ble_nus_evt->data_len; i++)
			{
				if (ble_fifo_get_status_rx_fifo(p_ble_nus_c->conn_handle, DATEN_FREE_IM_FIFO) > 1)
				{
					if (m_u8_mode == 0)
					{
						// Copy received message into the FiFo
						ble_fifo_push_rx_fifo (p_ble_nus_c->conn_handle, p_ble_nus_evt->p_data[i]);
					}
					else if (m_u8_mode == 1)
					{
						if (i > 0)  // reject frame number here (DEPRECATED)
						{
							ble_fifo_push_rx_fifo (p_ble_nus_c->conn_handle, p_ble_nus_evt->p_data[i]);
						}
					}
				}
			}
            break;

        case BLE_NUS_C_EVT_DISCONNECTED:
            TRACE_DBG_L("BLE_NUS_C_EVT_DISCONNECTED\r\n");
            if (ble_conn_state_n_centrals() == 0)
            	ble_central_set_application_event(p_ble_nus_evt->conn_handle, BLE_C_EVT_NUS_C_DISCONNECTED);
            break;

        default:
            TRACE_DBG_H("Warning: ble_nus_c_evt_handler() run in default case!\n");
    }
}
/**@snippet [Handling events from the ble_nus_c module] */

/*-------------------------------------------------------------------------*/
/**@brief Reads an advertising report and checks if a uuid is present in the service list.
 *
 * @details The function is able to search for 16-bit, 32-bit and 128-bit service uuids.
 *          To see the format of a advertisement packet, see
 *          https://www.bluetooth.org/Technical/AssignedNumbers/generic_access_profile.htm
 *
 * @param[in]   p_target_uuid The uuid to search fir
 * @param[in]   p_adv_report  Pointer to the advertisement report.
 *
 * @retval      true if the UUID is present in the advertisement report. Otherwise false
 */
static bool ble_c_is_uuid_present(const ble_uuid_t *p_target_uuid,
                            const ble_gap_evt_adv_report_t *p_adv_report)
{
    uint32_t err_code;
    uint32_t index = 0;
    uint8_t *p_data = (uint8_t *)p_adv_report->data;
    ble_uuid_t extracted_uuid;

#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
    TRACE_DBG_L("ble_c_is_uuid_present()\n");
#endif
   while (index < p_adv_report->dlen)
    {
        uint8_t field_length = p_data[index];
        uint8_t field_type   = p_data[index + 1];

        if ( (field_type == BLE_GAP_AD_TYPE_16BIT_SERVICE_UUID_MORE_AVAILABLE)
           || (field_type == BLE_GAP_AD_TYPE_16BIT_SERVICE_UUID_COMPLETE)
           )
        {
            for (uint32_t u_index = 0; u_index < (field_length / UUID16_SIZE); u_index++)
            {
                err_code = sd_ble_uuid_decode(  UUID16_SIZE,
                                                &p_data[u_index * UUID16_SIZE + index + 2],
                                                &extracted_uuid);
                if (err_code == NRF_SUCCESS)
                {
                    if ((extracted_uuid.uuid == p_target_uuid->uuid)
                        && (extracted_uuid.type == p_target_uuid->type))
                    {
                        return true;
                    }
                }
            }
        }

        else if ( (field_type == BLE_GAP_AD_TYPE_32BIT_SERVICE_UUID_MORE_AVAILABLE)
                || (field_type == BLE_GAP_AD_TYPE_32BIT_SERVICE_UUID_COMPLETE)
                )
        {
            for (uint32_t u_index = 0; u_index < (field_length / UUID32_SIZE); u_index++)
            {
                err_code = sd_ble_uuid_decode(UUID16_SIZE,
                &p_data[u_index * UUID32_SIZE + index + 2],
                &extracted_uuid);
                if (err_code == NRF_SUCCESS)
                {
                    if ((extracted_uuid.uuid == p_target_uuid->uuid)
                        && (extracted_uuid.type == p_target_uuid->type))
                    {
                        return true;
                    }
                }
            }
        }

        else if ( (field_type == BLE_GAP_AD_TYPE_128BIT_SERVICE_UUID_MORE_AVAILABLE)
                || (field_type == BLE_GAP_AD_TYPE_128BIT_SERVICE_UUID_COMPLETE)
                )
        {
            err_code = sd_ble_uuid_decode(UUID128_SIZE,
                                          &p_data[index + 2],
                                          &extracted_uuid);
            if (err_code == NRF_SUCCESS)
            {
                if ((extracted_uuid.uuid == p_target_uuid->uuid)
                    && (extracted_uuid.type == p_target_uuid->type))
                {
                    return true;
                }
            }
        }
        index += field_length + 1;
    }
    return false;
}

/*-------------------------------------------------------------------------*/
/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_c_on_ble_evt(ble_evt_t * p_ble_evt)
{
	uint32_t              err_code;
    const ble_gap_evt_t * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_ADV_REPORT:
        {
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_GAP_EVT_ADV_REPORT\n");
#endif
            const ble_gap_evt_adv_report_t * p_adv_report = &p_gap_evt->params.adv_report;

            // direct addressing mode
            if(m_pBLE_Context->f_dirAddrEna)
            {
#ifdef DBG_BLE_CENTRAL_CONNECTION_MODE
            	TRACE_DBG_L("connection mode - direct addressing\n");
#endif

            	if (p_gap_evt->params.adv_report.type == BLE_ADV_MODE_DIRECTED)
				{
					err_code = sd_ble_gap_connect(&p_adv_report->peer_addr,
												  m_pBLE_Context->scanParams,
												  m_pBLE_Context->conParams,
												  BLE_CONN_CFG_TAG);
					APP_ERROR_CHECK(err_code);
					TRACE_DBG_M("BLE_ADV_MODE_DIRECTED adr %02x%02x%02x%02x%02x%02x\n",
							 p_adv_report->peer_addr.addr[0],
							 p_adv_report->peer_addr.addr[1],
							 p_adv_report->peer_addr.addr[2],
							 p_adv_report->peer_addr.addr[3],
							 p_adv_report->peer_addr.addr[4],
							 p_adv_report->peer_addr.addr[5]);
				}
            }

            // connecting to device with defined MAC-Address
            if(m_pBLE_Context->pu8_macAddr != NULL)
            {
#ifdef DBG_BLE_CENTRAL_CONNECTION_MODE
            	TRACE_DBG_L("connection mode - MAC-Address\n");
#endif

            	// MAC address equal?
				if(memcmp(&p_adv_report->peer_addr.addr, m_pBLE_Context->pu8_macAddr, 6) == 0)
				{
					TRACE_DBG_L("MAC-Address is equal\n");
					err_code = sd_ble_gap_connect(&p_adv_report->peer_addr,
												  m_pBLE_Context->scanParams,
												  m_pBLE_Context->conParams,
												  BLE_CONN_CFG_TAG);
					APP_ERROR_CHECK(err_code);
					TRACE_DBG_M("connected to adr %02x%02x%02x%02x%02x%02x\n",
								 p_adv_report->peer_addr.addr[0],
								 p_adv_report->peer_addr.addr[1],
								 p_adv_report->peer_addr.addr[2],
								 p_adv_report->peer_addr.addr[3],
								 p_adv_report->peer_addr.addr[4],
								 p_adv_report->peer_addr.addr[5]);
				}
            }

            // connecting with UUID
            if(m_pBLE_Context->p_bleUuid != NULL)
            {
#ifdef DBG_BLE_CENTRAL_CONNECTION_MODE
            	TRACE_DBG_L("connection mode - service UUID\n");
#endif

				if(ble_c_is_uuid_present(m_pBLE_Context->p_bleUuid, p_adv_report))
				{
					err_code = sd_ble_gap_connect(&p_adv_report->peer_addr,
												  m_pBLE_Context->scanParams,
												  m_pBLE_Context->conParams,
												  BLE_CONN_CFG_TAG);

					TRACE_DBG_L("UUID is present %d\n", err_code);
					if (err_code == NRF_SUCCESS)
					{
						// scan is automatically stopped by the connect
						TRACE_DBG_M("Connecting to target %02x%02x%02x%02x%02x%02x\n",
								 p_adv_report->peer_addr.addr[0],
								 p_adv_report->peer_addr.addr[1],
								 p_adv_report->peer_addr.addr[2],
								 p_adv_report->peer_addr.addr[3],
								 p_adv_report->peer_addr.addr[4],
								 p_adv_report->peer_addr.addr[5]
								 );
					}
				}
            }

        }
        break; // BLE_GAP_EVT_ADV_REPORT

        case BLE_GAP_EVT_CONNECTED:
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_GAP_EVT_CONNECTED\n");
#endif
        	TRACE_DBG_M("Connected with handle %d\n", p_gap_evt->conn_handle);

            if (p_gap_evt->conn_handle < BLE_CENTRAL_TOTAL_LINK_COUNT) {
				// start discovery of services. The NUS Client waits for a discovery result
            	memset(&m_ble_db_discovery[p_gap_evt->conn_handle], 0x00, sizeof(ble_db_discovery_t));
				err_code = ble_db_discovery_start(&m_ble_db_discovery[p_gap_evt->conn_handle], p_ble_evt->evt.gap_evt.conn_handle);
				APP_ERROR_CHECK(err_code);
            }

            if (ble_conn_state_n_centrals() < BLE_CENTRAL_CENTRAL_LINK_COUNT) {
            	ble_c_scan_start();
            }
            break; // BLE_GAP_EVT_CONNECTED

        case BLE_GAP_EVT_DISCONNECTED:
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_GAP_EVT_DISCONNECTED\n");
#endif
        	TRACE_DBG_M("Disconnected with handle %d\n", p_gap_evt->conn_handle);

            ble_c_scan_start();
            break;

        case BLE_GAP_EVT_TIMEOUT:
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_GAP_EVT_TIMEOUT\n");
#endif
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_SCAN)
            {
                //NRF_LOG_DEBUG("Scan timed out.\r\n");
                ble_c_scan_start();
            }
            else if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                TRACE_DBG_L("Connection Request timed out.\r\n");
            }
            break; // BLE_GAP_EVT_TIMEOUT

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_GAP_EVT_SEC_PARAMS_REQUEST\n");
#endif
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(p_ble_evt->evt.gap_evt.conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GAP_EVT_SEC_PARAMS_REQUEST

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST\n");
#endif
            // Accepting parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                                    &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST

        case BLE_GATTC_EVT_TIMEOUT:
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_GATTC_EVT_TIMEOUT\n");
#endif
            // Disconnect on GATT Client timeout event.
            //NRF_LOG_DEBUG("GATT Client Timeout.\r\n");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GATTC_EVT_TIMEOUT

        case BLE_GATTS_EVT_TIMEOUT:
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
            TRACE_DBG_L("BLE_GATTS_EVT_TIMEOUT\n");
#endif
            // Disconnect on GATT Server timeout event.
            //NRF_LOG_DEBUG("GATT Server Timeout.\r\n");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GATTS_EVT_TIMEOUT

        case BLE_GATTC_EVT_WRITE_CMD_TX_COMPLETE:
        	m_u8_tx_packet_count[p_gap_evt->conn_handle]++;
#ifdef DBG_BLE_C_TRACE_FREE_TX_PACKETS
        	TRACE_DBG_L("BLE-TX+Ev free packets: %u\n", m_u8_tx_packet_count[p_gap_evt->conn_handle]);
#endif
        	break;

        default:
#ifdef DBG_BLE_CENTRAL
            TRACE_DBG_L("INFO: Unhandled event in ble_c_on_ble_evt(): %d\n",
                       p_ble_evt->header.evt_id);
#endif
            break;
    }
}

/*-------------------------------------------------------------------------*/
/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the scheduler in the main loop after a BLE stack event has
 *          been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_c_ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
    TRACE_DBG_L("ble_c_ble_evt_dispatch()\n");
#endif

#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
    TRACE_DBG_L("ble_conn_state_on_ble_evt()\n");
#endif
    ble_conn_state_on_ble_evt(p_ble_evt);

#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
    TRACE_DBG_L("\tble_c_on_ble_evt()\n");
#endif
    ble_c_on_ble_evt(p_ble_evt);

#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
    TRACE_DBG_L("\tnrf_ble_gatt_on_ble_evt()\n");
#endif
    nrf_ble_gatt_on_ble_evt(&m_gatt, p_ble_evt);

    uint16_t conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    if (conn_handle < BLE_CENTRAL_TOTAL_LINK_COUNT) {
#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
    	TRACE_DBG_L("\tble_db_discovery_on_ble_evt()\n");
#endif
    	ble_db_discovery_on_ble_evt(&m_ble_db_discovery[conn_handle], p_ble_evt);

#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
    	TRACE_DBG_L("\tble_nus_c_on_ble_evt()\n");
#endif
    	ble_nus_c_on_ble_evt(&m_ble_nus_c[conn_handle], p_ble_evt);
    }

    if (m_evt_handler != NULL) {
    	m_evt_handler(p_ble_evt);
    }

#ifdef DBG_BLE_CENTRAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
    TRACE_DBG_L("ble_c_ble_evt_dispatch(): exit\n");
#endif
}

/*-------------------------------------------------------------------------*/
/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_c_ble_stack_init(void)
{
    uint32_t  err_code;
    ble_cfg_t ble_cfg;
    uint32_t  ram_start = 0;

    nrf_clock_lf_cfg_t  clock_lf_cfg = NRF_CLOCK_LFCLKSRC;

    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);

    // Fetch the start address of the application RAM.
    err_code = softdevice_app_ram_start_get(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Configure the maximum number of connections.
    memset(&ble_cfg, 0, sizeof(ble_cfg));
    ble_cfg.gap_cfg.role_count_cfg.periph_role_count  = m_pBLE_Context->u8_peripheralLinkCount;
    ble_cfg.gap_cfg.role_count_cfg.central_role_count = m_pBLE_Context->u8_centralLinkCount;
    ble_cfg.gap_cfg.role_count_cfg.central_sec_count  = 1;
    err_code = sd_ble_cfg_set(BLE_GAP_CFG_ROLE_COUNT, &ble_cfg, ram_start);
    APP_ERROR_CHECK(err_code);

    // Configure the maximum ATT MTU.
    memset(&ble_cfg, 0x00, sizeof(ble_cfg));
    ble_cfg.conn_cfg.conn_cfg_tag                 = BLE_CONN_CFG_TAG;
    ble_cfg.conn_cfg.params.gatt_conn_cfg.att_mtu = NRF_BLE_GATT_MAX_MTU_SIZE;
    err_code = sd_ble_cfg_set(BLE_CONN_CFG_GATT, &ble_cfg, ram_start);
    APP_ERROR_CHECK(err_code);

    // Configure the maximum event length.
    memset(&ble_cfg, 0x00, sizeof(ble_cfg));
    ble_cfg.conn_cfg.conn_cfg_tag                     = BLE_CONN_CFG_TAG;
    ble_cfg.conn_cfg.params.gap_conn_cfg.event_length = 320;
    ble_cfg.conn_cfg.params.gap_conn_cfg.conn_count   = m_pBLE_Context->u8_peripheralLinkCount + m_pBLE_Context->u8_centralLinkCount;
    err_code = sd_ble_cfg_set(BLE_CONN_CFG_GAP, &ble_cfg, ram_start);
    APP_ERROR_CHECK(err_code);

    // Configure the ATT table size.
    memset(&ble_cfg, 0, sizeof(ble_cfg));
    ble_cfg.gatts_cfg.attr_tab_size.attr_tab_size = BLE_GATTS_ATTR_TAB_SIZE_MIN;
    err_code = sd_ble_cfg_set(BLE_GATTS_CFG_ATTR_TAB_SIZE, &ble_cfg, ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = softdevice_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_c_ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}

/*-------------------------------------------------------------------------*/
/**@brief Function for initializing the NUS Client.
 */
static void ble_c_nus_c_init(void)
{
    uint32_t         err_code;
    ble_nus_c_init_t nus_c_init_t;

    nus_c_init_t.evt_handler = ble_c_ble_nus_c_evt_handler;
    for (uint8_t i = 0; i < BLE_CENTRAL_TOTAL_LINK_COUNT; ++i) {
		err_code = ble_nus_c_init(&m_ble_nus_c[i], &nus_c_init_t);
		APP_ERROR_CHECK(err_code);
    }
}

/*-------------------------------------------------------------------------*/
/** @brief Function for initializing the Database Discovery Module.
 */
static void ble_c_db_discovery_init(void)
{
    uint32_t err_code = ble_db_discovery_init(ble_c_db_disc_handler);
    APP_ERROR_CHECK(err_code);
}

/*-------------------------------------------------------------------------*/
/**
 * Sends data on BLE.
 */
static void ble_central_send_data(void)
{
    static uint8_t* pu8_msg        = NULL;
    static uint16_t u16_left_bytes = 0;
    uint32_t        err_code       = NRF_SUCCESS;

    for (uint8_t i = 0; i < BLE_CENTRAL_TOTAL_LINK_COUNT; ++i) {
		if (ble_c_get_conn_handle(i) != BLE_CONN_HANDLE_INVALID)
		{
			// Check if there is something to send
			if ( (u16_left_bytes > 0 || queue_peek(BLE_getTxQueue(i)) != NULL)
			&& (m_u8_tx_packet_count[i] > 0) )
			{
				uint16_t bytes;
				uint8_t* pu8_actual;

				//todo: get current MTU length

				if (u16_left_bytes == 0)
				{
					pu8_msg = queue_pop(BLE_getTxQueue(i));
					u16_left_bytes = queue_get_object_length(pu8_msg);
#ifdef DBG_BLE_CENTRAL_TX_DATA
					TRACE_DBG_L("New BLE message/frame to send:\n");
					for (int i = 0; i < u16_left_bytes; i++)
					{
						TRACE_DBG_L("%02x ", pu8_msg[i]);
					}
					TRACE_DBG_L("\n");
#endif // DBG_BLE_CENTRAL_TX_DATA
				}
				// Calculate copy length
				if (u16_left_bytes <= m_ble_nus_max_data_len)
				{
					bytes = u16_left_bytes;
					u16_left_bytes = 0;
					pu8_actual = pu8_msg;
				}
				else
				{
					bytes = m_ble_nus_max_data_len;
					u16_left_bytes -= m_ble_nus_max_data_len;
					pu8_actual = &pu8_msg[queue_get_object_length(pu8_msg) - (bytes + u16_left_bytes)];
				}

				if (m_u8_mode == 0)
				{
					err_code = ble_nus_c_string_send(ble_c_get_nus_c_service_instance(i),
													 pu8_actual, bytes);
				}
				else if (m_u8_mode == 1)
				{
					// DEPRECATED code, NOT  tested for huge messages
					static uint8_t u8_fnum = 0;
					uint8_t au8_msg[NRF_BLE_GATT_MAX_MTU_SIZE - OPCODE_HANDLE_LENGTH];
					if (bytes >= m_ble_nus_max_data_len)
					{
						bytes--;
						u16_left_bytes++;
					}
					memcpy(&au8_msg[1], pu8_actual, bytes);
					au8_msg[0] = u8_fnum++; // set frame number
					err_code = ble_nus_c_string_send(ble_c_get_nus_c_service_instance(i), au8_msg, queue_get_object_length(pu8_msg) + 1);
				}
				if (err_code == NRF_SUCCESS)
				{
					m_u8_tx_packet_count[i]--;
#ifdef DBG_BLE_C_TRACE_FREE_TX_PACKETS
					TRACE_DBG_L("BLE-TX-send free packets: %u\n", m_u8_tx_packet_count[i]);
#endif
				}
				else
				{
					TRACE_DBG_L("ble_nus_c_string_send error: 0x%04x (%u)\n", err_code, err_code);
					APP_ERROR_CHECK(err_code); //todo: handle/return error
				}
				if (u16_left_bytes == 0)
				{
					queue_release_object_memory(pu8_msg);
				}
			}
		}
    }
}

/*-------------------------------------------------------------------------*/
/**
 * BLE service event processor. Checks for pending events and takes
 * appropriate actions.
 */
static void ble_central_application_event_processor(void)
{
	switch (m_event)
	{
	case BLE_C_EVT_NONE:
		//nop
		break;

	/* event from the nus client module when connected */
	case BLE_C_EVT_NUS_C_CONNECTED:
		TRACE_DBG_L("BLE_C_EVT_NUS_C_CONNECTED\n");
		m_event = BLE_C_EVT_NONE;	// clear the last event
		// reset the buffer count / tx packet count
		m_u8_tx_packet_count[m_event_conn_handle] = MAX_TX_PACKETS;
	    // change to state connected
		break;

	/* event from the nus client event module when disconnected */
	case BLE_C_EVT_NUS_C_DISCONNECTED:
	    TRACE_DBG_L("BLE_C_EVT_NUS_C_DISCONNECTED\n");
		m_event = BLE_C_EVT_NONE;	// clear the last event
		break;

	default:
	    TRACE_DBG_H("WARNING: BLE event is not defined!");
	}
}

/*--------------------------------------------------------------------------*/

/**
 * Initialize the BLE privacy.
 *
 * @param[in] au8_irk     Identity Resolving Key (IRK), NULL when not used.
 */
static void ble_c_init_privacy(const uint8_t au8_irk[BLE_GAP_SEC_KEY_LEN])
{
    /** @brief Peer identity, IRK */
    static ble_gap_id_key_t       t_peer;
    static const ble_gap_id_key_t *at_identity_list[] = {&t_peer};

    /** @brief Local IRK */
    static ble_gap_irk_t       t_local_irk;
    static const ble_gap_irk_t *at_local_irk_list[]={&t_local_irk};

    ble_gap_privacy_params_t t_privacy;
    ret_code_t err_code;

    if (au8_irk != NULL)
    {
        memcpy(t_peer.id_info.irk, au8_irk, BLE_GAP_SEC_KEY_LEN);
        memcpy(t_local_irk.irk,    au8_irk, BLE_GAP_SEC_KEY_LEN);

        // set privacy
        t_privacy.privacy_mode = BLE_GAP_PRIVACY_MODE_DEVICE_PRIVACY;
        t_privacy.private_addr_cycle_s = 15*60;
        t_privacy.private_addr_type = BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE;
        t_privacy.p_device_irk = &t_local_irk;
        err_code = sd_ble_gap_privacy_set(&t_privacy);
        APP_ERROR_CHECK(err_code);

        // add peer device to identity list
        err_code = sd_ble_gap_device_identities_set(at_identity_list,
                                                    at_local_irk_list, 1);
        APP_ERROR_CHECK(err_code);
    }
}

/*--------------------------------------------------------------------------*/

/**@brief Function for handling events from the GATT library. */
static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)
    {
        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_HANDLE_LENGTH;
        TRACE_DBG_L("MAX data length is set to 0x%X(%d)\n",
                    m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
}

/*--------------------------------------------------------------------------*/

/**@brief Function for initializing the GATT library. */
static void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_central_set(&m_gatt, NRF_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * BLE service handler. Runs the BLE main state machine.
 */
void ble_central(void)
{
	ble_central_application_event_processor();

	ble_central_send_data();
}

/*-------------------------------------------------------------------------*/

void ble_c_init(BLE_C_Context* pContext,
				int8_t i8_tx_power,
                const uint8_t au8_irk[BLE_GAP_SEC_KEY_LEN],
                uint8_t u8_mode,
				ble_c_evt_handler_t evt_handler)
{
    ret_code_t     ret;

    m_u8_mode = u8_mode;
    m_evt_handler = evt_handler;

    m_pBLE_Context = pContext;

    TRACE_DBG_L("Initializing db discovery, BLE central stack and BLE UART service\n");
    ble_c_db_discovery_init();
    ble_c_ble_stack_init();
    gatt_init();
    ble_c_nus_c_init();
    ble_conn_state_init();
    ble_c_init_privacy(au8_irk);

    ret = sd_ble_gap_tx_power_set(i8_tx_power);
    APP_ERROR_CHECK(ret);


    #ifdef DBG_BLE_CENTRAL_TRACE_MAC_ADDRESS
    sd_ble_gap_addr_get(&t_addr);
    TRACE_DBG_L("MAC address: %02x:%02x:%02x:%02x:%02x:%02x // LSB format\n",
    		t_addr.addr[0],	t_addr.addr[1], t_addr.addr[2],
			t_addr.addr[3], t_addr.addr[4], t_addr.addr[5]);
	#endif
}

/*-------------------------------------------------------------------------*/
/**
 * Returns the connection handle
 */
uint16_t ble_c_get_conn_handle(uint8_t handle)
{
	return m_ble_nus_c[handle].conn_handle;
}

/*-------------------------------------------------------------------------*/
/**
 * Returns the central uart service instance
 */
ble_nus_c_t * ble_c_get_nus_c_service_instance(uint8_t handle)
{
	return  &m_ble_nus_c[handle];
}

/*-------------------------------------------------------------------------*/
/**
 * Start scanning
 */
void ble_c_scan_start(void)
{
    ret_code_t ret;

    sd_ble_gap_scan_stop();

    ret = sd_ble_gap_scan_start(m_pBLE_Context->scanParams);
    APP_ERROR_CHECK(ret);

    TRACE_DBG_L("BLE_C_STATE_SCANNING\n");
}

/*-------------------------------------------------------------------------*/
/**
 * Set BLE service/state events.
 */
void ble_central_set_application_event(uint8_t handle, eBLE_C_AppEvt_t t_event)
{
	if (m_event == BLE_C_EVT_NONE)
	{
		m_event_conn_handle = handle;
		m_event = t_event;
		TRACE_DBG_L("BLE handler event: %u\n", m_event);
	}
	else
	{
	    TRACE_DBG_H("WARNING: An event is already pending! Event not sent to BLE handler.\n");
	}
}

/*-------------------------------------------------------------------------*/
uint16_t ble_central_get_data_length(void)
{
	return m_ble_nus_max_data_len;
}
