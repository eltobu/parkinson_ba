/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: BLE_PERIPHERAL_H
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/ble/services/uart_service/ble_peripheral.h $
 *  $Revision: 23455 $
 *      $Date: 2018-03-08 16:46:45 +0100 (Thu, 08 Mar 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This is the BLE peripheral header file.
 *
 ****************************************************************************
 */

#ifndef SUBSYS_BLE_PERIPHERAL_H_
#define SUBSYS_BLE_PERIPHERAL_H_

#include <stdint.h>
#include "ble_advertising.h"
#include "ble_us.h"
#include "ble_advdata.h"

/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/
typedef enum {
	BLE_P_EVT_NONE = 0,
	BLE_P_EVT_DISCONNECT_CLIENT,
	BLE_P_EVT_BLE_GAP_EVT_DISCONNECTED,
} eBLE_P_Event_t;

typedef enum {
	BLE_P_STATE_DISABLED = 0,
	//BLE_STATE_INITIALIZED,
	BLE_P_STATE_ADVERTISING,
	BLE_P_STATE_CONNECTED,
	BLE_P_STATE_SEND_PENDING_DATA_BEFORE_DISCONNECTION,
	BLE_P_STATE_WAITING_FOR_DISCONNECTION,
} eBLE_P_State_t;

/**@brief   Event handler type.
 *
 * @details This is the type of the event handler that should be provided by the application
 *          of this module to receive events.
 */
typedef void (*ble_evt_handler_t)(ble_evt_t* p_ble_evt);

/*--------------------------------------------------------------------------+
|  function prototypes                                                      |
+--------------------------------------------------------------------------*/

/**@brief BLE peripheral service.
 *
 * @details Runs the peripheral main state machine.
 */
void ble_peripheral(void);

/**@brief Disconnect the BLE connection.
 *
 */
uint32_t ble_peripheral_disconnect(void);

/**@brief Get BLE TX fifo buffer.
 *
 */
uint8_t ble_peripheral_GetBLEtxCnt(void);

/**@brief Process BLE TX queue
 *
 */
uint32_t ble_peripheral_send_data(void);

/**@brief Function for BLE peripheral service initialization.
 *
 * @details Initialize the BLE: SoftDevice, BLE stack, service and advertising.
 *
 * @param[in] i8_tx_power Radio transmit power in dBm (accepted values are
 *                        -40, -30, -20, -16, -12, -8, -4, 0, 3, and 4 dBm)
 * @param[in] au8_irk     Identity Resolving Key (IRK), NULL when not used.
 * @param[in] u8_mode     0: The way it should be. 1: Use DEPRECATED frame number.
 *
 * @retval NRF_SUCCESS If initialization was successful. Otherwise, an error code is returned.
 */
uint32_t ble_peripheral_init(
                    int8_t                      i8_tx_power,
                    ble_adv_mode_t              advertising_mode,
					uint8_t* 					pau8_dev_name,
					uint16_t 					u16_dev_name_len,
					ble_uuid_t*					p_ble_uuid,
					ble_advdata_manuf_data_t* 	p_manuf_specific_data,
					const uint8_t               au8_irk[],
					uint8_t                     u8_mode,
					ble_evt_handler_t         evt_handler);

/**@brief Get the connection handle.
 *
 * @details Returns the BLE connection handle.
 */
uint16_t ble_peripheral_get_conn_handle(void);

/**@brief Reset BLE TX buffer counter
 *
 * @details reset m_u8_tx_packet_count to reflect max available BLE TX buffer size
 */
void ble_peripheral_RstBLEtxCnt(void);

/**@brief Check notification enabled on Characteristics
 *
 * @details make sure notification is enabled before switching to connected state
 */
bool ble_peripheral_is_notification_enabled(void);

/**@brief Clear BLE TX queue
 *
 * @details
 */
void ble_peripheral_clear_queue(void);

/**@brief Get the service state.
 *
 * @details Returns the current state.
 */
eBLE_P_State_t ble_peripheral_get_state(void);

/**@brief Send events to the service.
 *
 * @details Set BLE service/state events.
 */
void ble_peripheral_set_event(eBLE_P_Event_t tEvent);

/**@brief Show MAC address.
 *
 * @details Print the MAC address of the BLE device.
 */
uint32_t ble_peripheral_trace_mac_address(void);

/**@brief Get actual data length
 *
 * @details max data length depending on MTU size
 */
uint16_t ble_peripheral_get_data_length(void);

#endif /* SUBSYS_BLE_PERIPHERAL_H_ */
