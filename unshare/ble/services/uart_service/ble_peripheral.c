/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: BLE peripheral
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/ble/services/uart_service/ble_peripheral.c $
 *  $Revision: 24217 $
 *      $Date: 2018-04-19 09:23:10 +0200 (Thu, 19 Apr 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This module implements a BLE peripheral running a UART service.
 * Functions for interaction with the SoftDevice are implemented here.
 *
 ****************************************************************************
 */

/* standard-headers (ANSI) ------------------------------------------------*/

/* headers from other modules ---------------------------------------------*/
#include "board.h"
#include "ble_peripheral.h"
#include "app_timer.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "nrf_ble_gatt.h"
#include "softdevice_handler.h"
#include "ble_us.h"
#include "module_config.h"
#include "ble_uart_fifo.h"
#include "fifo.h"
#include "error.h"
#include "ble_us_c.h"
#include "function_run.h"
#include "trace.h"
#include "app_error_aot.h"
#include "queue.h"
#include "ble_uart_queue.h"
#include "queue.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_BLE
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_BLE undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_BLE
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

// *************************************************************

#if (NRF_SD_BLE_API_VERSION != 4)
#error "ERROR: This code is specified for Softdevice 4.xx"
#endif

/* -------------------------------------------------------------------------+
 |  debug                                                                    |
 +------------------------------------------------------------------------- */
#define DBG_BLE_PERIPHERAL
#undef DBG_BLE_PERIPHERAL
#define DBG_BLE_PERIPHERAL_TX_DATA
#undef DBG_BLE_PERIPHERAL_TX_DATA
#define DBG_BLE_PERIPHERAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
#undef DBG_BLE_PERIPHERAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
#define DBG_BLE_PERIPHERAL_NUS_DATA_HANDLER_PRINT_RECEIVED_DATA
#undef DBG_BLE_PERIPHERAL_NUS_DATA_HANDLER_PRINT_RECEIVED_DATA
#define DBG_BLE_P_TRACE_FREE_TX_PACKETS
#undef DBG_BLE_P_TRACE_FREE_TX_PACKETS

/*--------------------------------------------------------------------------+
 |  global constants                                                         |
 +------------------------------------------------------------------------- */

#define IS_SRVC_CHANGED_CHARACT_PRESENT 0                                           /**< Include the service_changed characteristic. If not enabled, the server's database cannot be changed for the lifetime of the device. */

#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

#define APP_ADV_INTERVAL                MSEC_TO_UNITS(1000, UNIT_0_625_MS)          /**< The advertising interval (in units of 0.625 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      0                                        	/**< The advertising timeout (in units of seconds). */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(10, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define OPCODE_HANDLE_LENGTH            3  ///< OPCODE_LENGTH + HANDLE_LENGTH
#define MAX_TX_PACKETS                  4  ///< There should be 6, 5 usable, but 4 do not crash

/*--------------------------------------------------------------------------+
 |  module global variables                                                  |
 +--------------------------------------------------------------------------*/

static ble_nus_t m_nus; /**< Structure to identify the Nordic UART Service. */
static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID; /**< Handle of the current connection. */
/* service handling */
static eBLE_P_Event_t m_tEvent = BLE_P_EVT_NONE;
static eBLE_P_State_t m_tState = BLE_P_STATE_DISABLED; ///< Holds the current state

/*
 * NOTE regarding m_u8_tx_packet_count
 * This module variable is needed to keep track of available (free) BLE buffers (as defined by MAX_TX_PACKETS)
 * else data will be lost.
 *
 * If the call to ble_nus_string_send(..) returns the NRF_ERROR_RESOURCES error it means that
 * all the BLE buffers in the SoftDevice are full, and that you can not upload any more data
 * until the BLE_GATTS_EVT_HVN_TX_COMPLETE event occurs.
 */
static uint8_t m_u8_tx_packet_count;
static uint8_t m_u8_mode = 0; ///< 0: The way it should be, else: special transient code.

// MTU exchange variables
static nrf_ble_gatt_t m_gatt; ///< GATT module instance, taken from ble_ap_uart sample
static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT
		- OPCODE_HANDLE_LENGTH; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */

// GAP settings
static ble_gap_addr_t m_gap_peer_addr; ///< Directed advertising need an address

static ble_evt_handler_t m_evt_handler;

/*--------------------------------------------------------------------------+
 |  prototypes                                                       		|
 +--------------------------------------------------------------------------*/
static uint32_t ble_peripheral_advertising_init(int8_t i8_tx_power,
		ble_advdata_manuf_data_t *p_manuf_specific_data, ble_uuid_t *p_adv_uuid);
static void ble_peripheral_ble_evt_dispatch(ble_evt_t * p_ble_evt);
static uint32_t ble_peripheral_ble_stack_init(void);
static uint32_t ble_peripheral_conn_params_init(void);
static void ble_peripheral_conn_params_error_handler(uint32_t nrf_error);
static uint32_t ble_peripheral_gap_params_init(uint8_t const *p_dev_name,
		uint16_t len);
static uint32_t ble_peripheral_init_privacy(
		const uint8_t au8_irk[BLE_GAP_SEC_KEY_LEN]);
#ifdef CONF_BLE_RESET_ON_CONNECTION_PROBLEM
static void ble_peripheral_no_connection_timeout_handler(void);
#endif
static void ble_peripheral_nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data,
		uint16_t length);
static void ble_peripheral_on_adv_evt(ble_adv_evt_t ble_adv_evt);
static void ble_peripheral_on_conn_params_evt(ble_conn_params_evt_t * p_evt);
static void ble_peripheral_on_ble_evt(ble_evt_t * p_ble_evt);
static void ble_peripheral_process_app_events(void);
static uint32_t ble_peripheral_services_init(uint16_t u16_service_uuid);

/*--------------------------------------------------------------------------+
 |  local functions                                                          |
 +--------------------------------------------------------------------------*/

static void disable_softdevice(void) {
#if 1
	softdevice_handler_sd_disable();
#endif
}

/*-------------------------------------------------------------------------*/
void ble_peripheral_clear_queue(void) {
	while (queue_peek(BLE_getTxQueue(0)) != NULL) {
		queue_release_object_memory(queue_pop(BLE_getTxQueue(0)));
		TRACE_DBG_H("Warning: BLE tx message cleared without sending\n");
	}
}

/*-------------------------------------------------------------------------*/
/**@brief Function for initializing the Advertising functionality for the smartphone.
 * If p_manuf_specific_data == NULL no manufacturer data is applied.
 */
static uint32_t ble_peripheral_advertising_init(int8_t i8_tx_power,
		ble_advdata_manuf_data_t *p_manuf_specific_data, ble_uuid_t *p_adv_uuid) {
	uint32_t err_code;
	ble_advdata_t advdata;
	ble_advdata_t scanrsp;
	ble_adv_modes_config_t options;

	// Build advertising data struct to pass into @ref ble_advertising_init.
	memset(&advdata, 0, sizeof(advdata));
	advdata.name_type = BLE_ADVDATA_FULL_NAME;
	advdata.include_appearance = false;
	advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
	advdata.p_manuf_specific_data = p_manuf_specific_data;
	advdata.p_tx_power_level = &i8_tx_power;

	memset(&scanrsp, 0, sizeof(scanrsp));
	scanrsp.uuids_complete.uuid_cnt = 1;
	scanrsp.uuids_complete.p_uuids = p_adv_uuid;

	memset(&options, 0, sizeof(options));
	options.ble_adv_directed_slow_enabled = true;
	options.ble_adv_directed_slow_interval = APP_ADV_INTERVAL;
	options.ble_adv_directed_slow_timeout = 0;
	options.ble_adv_slow_enabled = true;
	options.ble_adv_slow_interval = APP_ADV_INTERVAL;
	options.ble_adv_slow_timeout = APP_ADV_TIMEOUT_IN_SECONDS;
	options.ble_adv_fast_enabled = true;
	options.ble_adv_fast_interval = APP_ADV_INTERVAL;
	options.ble_adv_fast_timeout = APP_ADV_TIMEOUT_IN_SECONDS;

	TRACE_DBG_L("ble_advertising_init() start\n");
	err_code = ble_advertising_init(&advdata, &scanrsp, &options,
			ble_peripheral_on_adv_evt, NULL);
	TRACE_DBG_L("ble_advertising_init() stop %d\n", err_code);
	RETURN_ON_NRF_ERROR(err_code);

	ble_advertising_conn_cfg_tag_set(BLE_CONN_CFG_TAG);

	return err_code;
}

/*-------------------------------------------------------------------------*/
/**@brief Function for dispatching a SoftDevice event to all modules with a SoftDevice
 *        event handler.
 *
 * @details This function is called from the SoftDevice event interrupt handler after a
 *          SoftDevice event has been received.
 *
 * @param[in] p_ble_evt  SoftDevice event.
 */
static void ble_peripheral_ble_evt_dispatch(ble_evt_t * p_ble_evt) {
	//TRACE_DBG_L("EVENT %u\n", p_ble_evt->header.evt_id);
	ble_conn_params_on_ble_evt(p_ble_evt);
	nrf_ble_gatt_on_ble_evt(&m_gatt, p_ble_evt);
	ble_nus_on_ble_evt(&m_nus, p_ble_evt);
	ble_peripheral_on_ble_evt(p_ble_evt);
	ble_advertising_on_ble_evt(p_ble_evt);
    if (m_evt_handler != NULL) {
    	m_evt_handler(p_ble_evt);
    }
}

/*-------------------------------------------------------------------------*/
/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static uint32_t ble_peripheral_ble_stack_init(void) {
	uint32_t err_code = NRF_SUCCESS;
	ble_cfg_t ble_cfg;
	nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
	uint32_t ram_start = 0;
	uint8_t isEnabled;

	TRACE_DBG_L("Initializing BLE peripheral stack\n");

	// Initialize SoftDevice.
	sd_softdevice_is_enabled(&isEnabled);
	if (!((bool) isEnabled)) {
		SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
	}

	// Fetch the start address of the application RAM.
	err_code = softdevice_app_ram_start_get(&ram_start);
	APP_ERROR_CHECK(err_code);

	// Overwrite some of the default configurations for the BLE stack.
	// todo: Evaluate if all of them are needed
	// Example initialization is taken from sample code: ble_periferal/ble_app_uart
	// for multi channel more config is required

	// Configure the maximum number of connections.
	memset(&ble_cfg, 0, sizeof(ble_cfg));
	ble_cfg.gap_cfg.role_count_cfg.periph_role_count =
	BLE_PERIPHERAL_PERIPHERAL_LINK_COUNT;
	ble_cfg.gap_cfg.role_count_cfg.central_role_count = 0;
	ble_cfg.gap_cfg.role_count_cfg.central_sec_count = 0;
	err_code = sd_ble_cfg_set(BLE_GAP_CFG_ROLE_COUNT, &ble_cfg, ram_start);
	APP_ERROR_CHECK(err_code);

	// Configure the maximum ATT MTU.
	memset(&ble_cfg, 0x00, sizeof(ble_cfg));
	ble_cfg.conn_cfg.conn_cfg_tag = BLE_CONN_CFG_TAG;
	ble_cfg.conn_cfg.params.gatt_conn_cfg.att_mtu = NRF_BLE_GATT_MAX_MTU_SIZE;
	err_code = sd_ble_cfg_set(BLE_CONN_CFG_GATT, &ble_cfg, ram_start);
	APP_ERROR_CHECK(err_code);

	// Configure the maximum event length.
	memset(&ble_cfg, 0x00, sizeof(ble_cfg));
	ble_cfg.conn_cfg.conn_cfg_tag = BLE_CONN_CFG_TAG;
	ble_cfg.conn_cfg.params.gap_conn_cfg.event_length = 320;
	ble_cfg.conn_cfg.params.gap_conn_cfg.conn_count =
	BLE_GAP_CONN_COUNT_DEFAULT;
	err_code = sd_ble_cfg_set(BLE_CONN_CFG_GAP, &ble_cfg, ram_start);
	APP_ERROR_CHECK(err_code);

	// Enable BLE stack.
	err_code = softdevice_enable(&ram_start);
	APP_ERROR_CHECK(err_code);

	// Subscribe for BLE events.
	err_code = softdevice_ble_evt_handler_set(ble_peripheral_ble_evt_dispatch);
	RETURN_ON_NRF_ERROR(err_code);

	return err_code;
}

/*-------------------------------------------------------------------------*/

/**@brief Function for initializing the Connection Parameters module.
 */
static uint32_t ble_peripheral_conn_params_init(void) {
	uint32_t err_code;
	ble_conn_params_init_t cp_init;

	memset(&cp_init, 0, sizeof(cp_init));

	cp_init.p_conn_params = NULL;
	cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
	cp_init.next_conn_params_update_delay = NEXT_CONN_PARAMS_UPDATE_DELAY;
	cp_init.max_conn_params_update_count = MAX_CONN_PARAMS_UPDATE_COUNT;
	cp_init.start_on_notify_cccd_handle = BLE_GATT_HANDLE_INVALID;
	cp_init.disconnect_on_fail = false;
	cp_init.evt_handler = ble_peripheral_on_conn_params_evt;
	cp_init.error_handler = ble_peripheral_conn_params_error_handler;

	err_code = ble_conn_params_init(&cp_init);
	RETURN_ON_NRF_ERROR(err_code);

	return err_code;
}

/*-------------------------------------------------------------------------*/
/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void ble_peripheral_conn_params_error_handler(uint32_t nrf_error) {
	APP_ERROR_HANDLER(nrf_error);
}

/*-------------------------------------------------------------------------*/
/**
 * Disconnect the BLE connection.
 */
uint32_t ble_peripheral_disconnect(void) {
	uint32_t err_code;

	err_code = sd_ble_gap_disconnect(ble_peripheral_get_conn_handle(),
	BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
	RETURN_ON_NRF_ERROR(err_code);

	return err_code;
}

/*-------------------------------------------------------------------------*/
/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
static uint32_t ble_peripheral_gap_params_init(uint8_t const *p_dev_name,
		uint16_t len) {
	uint32_t err_code = NRF_SUCCESS;
	ble_gap_conn_params_t gap_conn_params;
	ble_gap_conn_sec_mode_t sec_mode;

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

	err_code = sd_ble_gap_device_name_set(&sec_mode, p_dev_name, len);
	RETURN_ON_NRF_ERROR(err_code);

	memset(&gap_conn_params, 0, sizeof(gap_conn_params));

	gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
	gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
	gap_conn_params.slave_latency = SLAVE_LATENCY;
	gap_conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT;

	err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
	RETURN_ON_NRF_ERROR(err_code);

	return err_code;
}

/*--------------------------------------------------------------------------*/
/**
 * Initialize the BLE privacy.
 *
 * @param[in] au8_irk     Identity Resolving Key (IRK), NULL when not used.
 */
static uint32_t ble_peripheral_init_privacy(
		const uint8_t au8_irk[BLE_GAP_SEC_KEY_LEN]) {
	/** @brief Peer identity, IRK */
	static ble_gap_id_key_t t_peer;
	static const ble_gap_id_key_t *at_identity_list[] = { &t_peer };

	/** @brief Local IRK */
	static ble_gap_irk_t t_local_irk;

	ble_gap_privacy_params_t t_privacy;
	ret_code_t err_code = NRF_SUCCESS;

	if (au8_irk != NULL) {
		memcpy(t_peer.id_info.irk, au8_irk, BLE_GAP_SEC_KEY_LEN);
		memcpy(t_local_irk.irk, au8_irk, BLE_GAP_SEC_KEY_LEN);

		// set privacy
		// The visible BT address address changes randomly at this interval.
		t_privacy.private_addr_cycle_s = 15 * 60; ///< [sec] Address cycle interval.
		t_privacy.privacy_mode = BLE_GAP_PRIVACY_MODE_DEVICE_PRIVACY;
		t_privacy.private_addr_type =
		BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE;
		t_privacy.p_device_irk = &t_local_irk;
		err_code = sd_ble_gap_privacy_set(&t_privacy);
		RETURN_ON_NRF_ERROR(err_code);

		// add peer device to identity list
		err_code = sd_ble_gap_device_identities_set(at_identity_list, NULL, 1);
		RETURN_ON_NRF_ERROR(err_code);

		// setup peer address. Dummy, addr must be set to !0 for later check
		m_gap_peer_addr.addr_id_peer = 1;
		m_gap_peer_addr.addr_type = BLE_GAP_ADDR_TYPE_PUBLIC;
		memset(m_gap_peer_addr.addr, 0 /*x71*/, sizeof(m_gap_peer_addr.addr));
		// @Todo: What address is really needed
	} else {
		memset(&m_gap_peer_addr, 0, sizeof(m_gap_peer_addr));
	}
	return err_code;
}

#ifdef CONF_BLE_RESET_ON_CONNECTION_PROBLEM
/*-------------------------------------------------------------------------*/
/**
 * Handler when which resets the device when connecting fails.
 */
static void ble_peripheral_no_connection_timeout_handler(void)
{
	// reset the device!
	TRACE_DBG_L("[%s:%u] %sBLE peripheral connection timeout! Reseting nRF.\n",
			__FILENAME__, __LINE__, WARNING);
	NVIC_SystemReset();
}
#endif

/*-------------------------------------------------------------------------*/
/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service.
 *
 * @param[in] p_nus    Nordic UART Service structure.
 * @param[in] p_data   Data to be send to UART module.
 * @param[in] length   Length of the data.
 */
static void ble_peripheral_nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data,
		uint16_t length) {
	for (uint16_t i = 0; i < length; i++) {
		if (ble_fifo_get_status_rx_fifo(0, DATEN_FREE_IM_FIFO) > 1) {
			if (m_u8_mode == 0) {
				ble_fifo_push_rx_fifo(0, p_data[i]); // Copy the data into fifo
			} else if (m_u8_mode == 1) {
				if (i > 0) {
					ble_fifo_push_rx_fifo(0, p_data[i]); // Copy the data into fifo, reject frame number
				}
			}
		} else {
			TRACE_DBG_H("Warning: BLE RX FIFO is full! Clearing FIFO now.\n");
			ble_fifo_clear_rx_fifo(0);
			break;
		}
	}
#ifdef DBG_BLE_PERIPHERAL_NUS_DATA_HANDLER_PRINT_RECEIVED_DATA
	TRACE_DBG_L("\nBLE data reception:\n");
	for (uint32_t i = 0; i < length; i++)
	{
		TRACE_DBG_L("%02x ", p_data[i]);
	}
	TRACE_DBG_L("\n");
#endif //DBG_BLE_PERIPHERAL_NUS_DATA_HADNLER_PRINT_RECEIVED_DATA
}

/*-------------------------------------------------------------------------*/
/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void ble_peripheral_on_adv_evt(ble_adv_evt_t ble_adv_evt) {
	uint32_t err_code;

	switch (ble_adv_evt) {
	case BLE_ADV_EVT_FAST:
#ifdef DBG_BLE_PERIPHERAL
		TRACE_DBG_L("INFO: BLE_ADV_EVT_FAST has been started\n");
		break;
		case BLE_ADV_EVT_DIRECTED_SLOW:
		TRACE_DBG_L("INFO: BLE_ADV_EVT_DIRECTED_SLOW has been started\n");
		break;
		case BLE_ADV_EVT_SLOW:
		TRACE_DBG_L("INFO: BLE_ADV_EVT_SLOW has been started\n");
		break;
#endif
	case BLE_ADV_EVT_PEER_ADDR_REQUEST:
		// Now we have to find the peer address
		err_code = ble_advertising_peer_addr_reply(&m_gap_peer_addr);
		TRACE_DBG_L("ble_advertising_peer_addr_reply(): %d\n", err_code);
		APP_ERROR_CHECK(err_code);
		break;
	case BLE_ADV_EVT_IDLE:
#ifdef DBG_BLE_PERIPHERAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT

		TRACE_DBG_L("BLE_ADV_EVT_IDLE\n");
#endif
		//todo: IMPORTANT: what to do when adv timed out?
		//pwr_sleep_mode_enter();
		break;
	default:
		;
		// No implementation needed.
#ifdef DBG_BLE_PERIPHERAL
		TRACE_DBG_H("INFO: Unhandled event in ble_peripheral_on_adv_evt(): %d\n",
				ble_adv_evt);
#endif
	}
}

/*-------------------------------------------------------------------------*/
/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void ble_peripheral_on_conn_params_evt(ble_conn_params_evt_t * p_evt) {
	uint32_t err_code;

	if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED) {
#ifdef DBG_BLE_PERIPHERAL_INVESTIGATE_OCCASIONAL_NOT_CONNECT
		TRACE_DBG_H("BLE_CONN_PARAMS_EVT_FAILED\n");
#endif
		err_code = sd_ble_gap_disconnect(m_conn_handle,
		BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
		APP_ERROR_CHECK(err_code);
	}
}

/*-------------------------------------------------------------------------*/
/**@brief Function for the application's SoftDevice event handler.
 *
 * @param[in] p_ble_evt SoftDevice event.
 */
static void ble_peripheral_on_ble_evt(ble_evt_t * p_ble_evt) {
	uint32_t err_code;

	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		TRACE_DBG_M("Connected with handle %d\n", p_ble_evt->evt.gap_evt.conn_handle);

		m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
		err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
		m_u8_tx_packet_count = MAX_TX_PACKETS; // Reset BLE TX Buffer
		break; // BLE_GAP_EVT_CONNECTED

	case BLE_GAP_EVT_DISCONNECTED:
		TRACE_DBG_M("Disconnected with handle %d\n", p_ble_evt->evt.gap_evt.conn_handle);

		m_conn_handle = BLE_CONN_HANDLE_INVALID;
		break; // BLE_GAP_EVT_DISCONNECTED

	case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
		// Pairing not supported
		err_code = sd_ble_gap_sec_params_reply(m_conn_handle,
		BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
		APP_ERROR_CHECK(err_code);
		break; // BLE_GAP_EVT_SEC_PARAMS_REQUEST

	case BLE_GAP_EVT_TIMEOUT:
		// Check what timed out ************************************************* DEBUG
		TRACE_DBG_L("BLE_GAP_EVT_TIMEOUT: Source: %d\n",
				p_ble_evt->evt.gap_evt.params.timeout.src);
		if (p_ble_evt->evt.gap_evt.params.timeout.src
				== BLE_GAP_TIMEOUT_SRC_CONN) {
			m_conn_handle = BLE_CONN_HANDLE_INVALID;
		}
		break; // BLE_GAP_EVT_TIMEOUT

	case BLE_GATTS_EVT_SYS_ATTR_MISSING:
		// No system attributes have been stored.
		err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
		APP_ERROR_CHECK(err_code);
		break; // BLE_GATTS_EVT_SYS_ATTR_MISSING

	case BLE_GATTC_EVT_TIMEOUT:
		// Disconnect on GATT Client timeout event.
		err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
		BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
		APP_ERROR_CHECK(err_code);
		break; // BLE_GATTC_EVT_TIMEOUT

	case BLE_GATTS_EVT_TIMEOUT:
		// Disconnect on GATT Server timeout event.
		err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
		BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
		APP_ERROR_CHECK(err_code);
		break; // BLE_GATTS_EVT_TIMEOUT

	case BLE_EVT_USER_MEM_REQUEST:
		err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle,
		NULL);
		APP_ERROR_CHECK(err_code);
		break; // BLE_EVT_USER_MEM_REQUEST

	case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST: {
		ble_gatts_evt_rw_authorize_request_t req;
		ble_gatts_rw_authorize_reply_params_t auth_reply;

		req = p_ble_evt->evt.gatts_evt.params.authorize_request;

		if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID) {
			if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)
					|| (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW)
					|| (req.request.write.op
							== BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL)) {
				if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE) {
					auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
				} else {
					auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
				}
				auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
				err_code = sd_ble_gatts_rw_authorize_reply(
						p_ble_evt->evt.gatts_evt.conn_handle, &auth_reply);
				APP_ERROR_CHECK(err_code);
			}
		}
	}
		break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

	case BLE_GATTS_EVT_HVN_TX_COMPLETE:
		// Packet was sent, increase m_u8_tx_packet_count to reflect more free BLE TX buffer
		m_u8_tx_packet_count++;
#ifdef DBG_BLE_P_TRACE_FREE_TX_PACKETS
		TRACE_DBG_L("BLE free tx packets: %u\n", m_u8_tx_packet_count);
#endif
		break;

	case BLE_GAP_EVT_DATA_LENGTH_UPDATE:
#ifdef DBG_BLE_PERIPHERAL
		TRACE_DBG_L("BLE_GAP_EVT_DATA_LENGTH_UPDATE time tx %u rx %u\n",
				p_ble_evt->evt.gap_evt.params.data_length_update.effective_params.max_tx_time_us,
				p_ble_evt->evt.gap_evt.params.data_length_update.effective_params.max_rx_time_us);
#endif
		break;

	case BLE_GATTC_EVT_EXCHANGE_MTU_RSP:
#ifdef DBG_BLE_PERIPHERAL
		TRACE_DBG_L("BLE_GATTC_EVT_EXCHANGE_MTU_RSP\n");
#endif
		break;

	default:
		;
		// No implementation needed.
#ifdef DBG_BLE_PERIPHERAL
		TRACE_DBG_L("INFO: Unhandled event in ble_peripheral_on_ble_evt(): %d\n",
				p_ble_evt->header.evt_id);
#endif
	}
}

/*-------------------------------------------------------------------------*/
/**
 * BLE service event processor. Checks for pending events and takes
 * appropriate actions.
 */
static void ble_peripheral_process_app_events(void) {
	switch (m_tEvent) {
	case BLE_P_EVT_NONE:
		//nop
		break;

		/* event from the application module */
	case BLE_P_EVT_DISCONNECT_CLIENT:
		TRACE_DBG_L("BLE_P_EVT_DISCONNECT_CLIENT\n");
		m_tEvent = BLE_P_EVT_NONE;  // clear the last event

		// is a client connected?
		if (m_tState == BLE_P_STATE_CONNECTED) {
			// client connected, send pending data if any
			m_tState = BLE_P_STATE_SEND_PENDING_DATA_BEFORE_DISCONNECTION;
			TRACE_DBG_L("BLE_P_STATE_SEND_PENDING_DATA_BEFORE_DISCONNECTION\n");
		} else {
			// no client connected, disable ble now
			disable_softdevice();
			// make sure the BLE tx queue is empty
			ble_peripheral_clear_queue();
#ifdef CONF_BLE_RESET_ON_CONNECTION_PROBLEM
			// unregister the no connection timeout handler as never connected
			FUNR_unregister(ble_peripheral_no_connection_timeout_handler);
#endif
			m_tState = BLE_P_STATE_DISABLED;
			TRACE_DBG_L("[%s:%u] BLE_P_STATE_DISABLED\n", __FILENAME__, __LINE__);
		}
		break;

		/* event from the peripheral advertisement module when client is disconnected */
	case BLE_P_EVT_BLE_GAP_EVT_DISCONNECTED:
		if (m_tState == BLE_P_STATE_CONNECTED) {
			disable_softdevice();        // disable the softdevice now
			ble_peripheral_clear_queue(); // make sure the BLE tx queue is empty
			m_tState = BLE_P_STATE_DISABLED;
			m_tEvent = BLE_P_EVT_NONE;  // clear the last event
			TRACE_DBG_L("[%s:%u] BLE_P_STATE_DISABLED\n", __FILENAME__, __LINE__);
		} else {
			// It must be advertising and a connection process had been interrupted
			// The softdevice will crash and block right now
			//NVIC_SystemReset();
		}
		break;

	default:
		TRACE_DBG_H("Warning: BLE event is not defined!\n");
	}
}

/*-------------------------------------------------------------------------*/
/**@brief Sends data on BLE.
 *
 */
uint32_t ble_peripheral_send_data(void) {
	static uint8_t* pu8_msg = NULL;
	static uint16_t u16_left_bytes = 0;
	uint32_t err_code = NRF_SUCCESS;

	if (ble_peripheral_get_conn_handle() != BLE_CONN_HANDLE_INVALID
			&& ble_peripheral_is_notification_enabled()
			&& (queue_peek(BLE_getTxQueue(0)) != NULL)) {
		// Valid connection handle available
		// Check if there is something and send as much data as available and possible
		// Possible only if m_u8_tx_packet_count > 0 ==> BLE TX buffer free
		while ((u16_left_bytes > 0 || queue_peek(BLE_getTxQueue(0)) != NULL)
				&& (m_u8_tx_packet_count > 0)) {
			uint16_t bytes;
			uint8_t* pu8_actual;

			//todo: get current MTU length.

			if (u16_left_bytes == 0) {
				pu8_msg = queue_pop(BLE_getTxQueue(0));
				u16_left_bytes = queue_get_object_length(pu8_msg);
#ifdef DBG_BLE_PERIPHERAL_TX_DATA
				TRACE_DBG_L("New BLE message/frame to send:\n");
				for (int i = 0; i < u16_left_bytes; i++)
				{
					TRACE_DBG_L("%02x ", pu8_msg[i]);
				}
				TRACE_DBG_L("\n");
#endif // DBG_BLE_PERIPHERAL_TX_DATA
			}
			// Calculate copy length
			if (u16_left_bytes <= m_ble_nus_max_data_len) {
				bytes = u16_left_bytes;
				u16_left_bytes = 0;
				pu8_actual = pu8_msg;
			} else {
				bytes = m_ble_nus_max_data_len;
				u16_left_bytes -= m_ble_nus_max_data_len;
				pu8_actual = &pu8_msg[queue_get_object_length(pu8_msg)
						- (bytes + u16_left_bytes)];
			}

			if (m_u8_mode == 0) {
				err_code = ble_nus_string_send(&m_nus, pu8_actual, bytes);
			} else if (m_u8_mode == 1) {
				// DEPRECATED code, NOT  tested for huge messages
				static uint8_t u8_fnum = 0;
				uint8_t au8_msg[NRF_BLE_GATT_MAX_MTU_SIZE - OPCODE_HANDLE_LENGTH];
				if (bytes >= m_ble_nus_max_data_len) {
					bytes--;
					u16_left_bytes++;
				}
				memcpy(&au8_msg[1], pu8_actual, bytes);
				au8_msg[0] = u8_fnum++; // set frame number
				err_code = ble_nus_string_send(&m_nus, au8_msg,
						queue_get_object_length(pu8_msg) + 1);
			}
			if (err_code == NRF_SUCCESS) {
				// Added data to BLE TX buffer, decrease m_u8_tx_packet_count to reflect less free BLE TX buffer
				m_u8_tx_packet_count--;
#ifdef DBG_BLE_P_TRACE_FREE_TX_PACKETS
				TRACE_DBG_L("BLE free tx packets: %u\n", m_u8_tx_packet_count);
#endif
			}
			if (u16_left_bytes == 0) {
				queue_release_object_memory(pu8_msg);
			}
			RETURN_ON_NRF_ERROR(err_code);
		}
	}

	return err_code;
}

/*-------------------------------------------------------------------------*/
/**@brief Function for initializing services that will be used by the application.
 */
static uint32_t ble_peripheral_services_init(uint16_t u16_service_uuid) {
	uint32_t err_code = NRF_SUCCESS;
	ble_nus_init_t nus_init;

	memset(&nus_init, 0, sizeof(nus_init));

	nus_init.data_handler = ble_peripheral_nus_data_handler;

	err_code = ble_nus_init(&m_nus, &nus_init, u16_service_uuid);
	RETURN_ON_NRF_ERROR(err_code);

	return err_code;
}

/*--------------------------------------------------------------------------*/

/**@brief Function for handling events from the GATT library. */
static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt,
		const nrf_ble_gatt_evt_t * p_evt) {
	if ((m_conn_handle == p_evt->conn_handle)
			&& (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)) {
		m_ble_nus_max_data_len = p_evt->params.att_mtu_effective
				- OPCODE_HANDLE_LENGTH;
		TRACE_DBG_L("MAX data length is set to 0x%X(%d)\n",
				m_ble_nus_max_data_len, m_ble_nus_max_data_len);
	}
	//TRACE_DBG_L("ATT MTU exchange completed. central %d peripheral %d\n",
	//            p_gatt->att_mtu_desired_central, p_gatt->att_mtu_desired_periph);
}

/*--------------------------------------------------------------------------*/

/**@brief Function for initializing the GATT library. */
static uint32_t gatt_init(void) {
	ret_code_t err_code;

	err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
	RETURN_ON_NRF_ERROR(err_code);

	err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt,
	NRF_BLE_GATT_MAX_MTU_SIZE);
	RETURN_ON_NRF_ERROR(err_code);

	return err_code;
}

/*--------------------------------------------------------------------------+
 |  functions                                                                |
 +--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/

uint8_t ble_peripheral_GetBLEtxCnt(void) {
	return m_u8_tx_packet_count;
}

/*-------------------------------------------------------------------------*/

void ble_peripheral_RstBLEtxCnt(void) {
	m_u8_tx_packet_count = MAX_TX_PACKETS;
}

/*-------------------------------------------------------------------------*/

bool ble_peripheral_is_notification_enabled(void) {
	return (m_nus.is_notification_enabled == true);
}

/*-------------------------------------------------------------------------*/

void ble_peripheral(void) {
	uint32_t err_code;

	ble_peripheral_process_app_events();

	switch (m_tState) {
	case BLE_P_STATE_DISABLED:
		// it's not allowed to send messages to BLE while disabled
		ble_peripheral_clear_queue();
		break;

	case BLE_P_STATE_ADVERTISING:
		if (ble_peripheral_get_conn_handle() != BLE_CONN_HANDLE_INVALID) {
			// make sure notification is enabled before switching to connected state
			if (m_nus.is_notification_enabled == true) {
				m_tState = BLE_P_STATE_CONNECTED;
				// Connection established, reset m_u8_tx_packet_count to reflect max available BLE TX buffer size
				m_u8_tx_packet_count = MAX_TX_PACKETS;
#ifdef CONF_BLE_RESET_ON_CONNECTION_PROBLEM
				// unregister the no connection timeout handler at successful connection
				FUNR_unregister(ble_peripheral_no_connection_timeout_handler);
#endif
			}
		}
		break;

	case BLE_P_STATE_CONNECTED:
		err_code = ble_peripheral_send_data();
		if (err_code != NRF_SUCCESS) {
			disable_softdevice();
			m_tState = BLE_P_STATE_DISABLED;
			TRACE_DBG_L("[%s:%u] BLE_P_STATE_DISABLED\n", __FILENAME__, __LINE__);
		}
		break;

	case BLE_P_STATE_SEND_PENDING_DATA_BEFORE_DISCONNECTION:
		// If we have any data into the queue send it before disconnection
		if (queue_peek(BLE_getTxQueue(0)) != NULL) {
			err_code = ble_peripheral_send_data();
			if (err_code != NRF_SUCCESS) {
				disable_softdevice();
				m_tState = BLE_P_STATE_DISABLED;
				TRACE_DBG_L("[%s:%u] BLE_P_STATE_DISABLED\n", __FILENAME__, __LINE__);
			}
		}
		// SD BLE TX buffer is empty therefore close connection
		else if (MAX_TX_PACKETS == m_u8_tx_packet_count) {
			// disconnect procedure
			TRACE_DBG_L("Disconnecting the client\n");
			err_code = ble_peripheral_disconnect();
			APP_ERROR_CHECK(err_code);
			if (err_code == NRF_SUCCESS) {
				TRACE_DBG_L("Client disconnected\n");
			} else {
				TRACE_DBG_L("Warning: Client disconnection failed\n");
			}TRACE_DBG_L("BLE_P_STATE_WAITING_FOR_DISCONNECTION\n");
			m_tState = BLE_P_STATE_WAITING_FOR_DISCONNECTION;
		}
		break;

	case BLE_P_STATE_WAITING_FOR_DISCONNECTION:
		// nop, just wait for disconnection event
		break;

	default:
		TRACE_DBG_H("ERROR: unknown BLE state!\n");
		m_tState = BLE_P_STATE_DISABLED;
		TRACE_DBG_H("[%s:%u] BLE_P_STATE_DISABLED\n", __FILENAME__, __LINE__);
	}
}

/*-------------------------------------------------------------------------*/

uint32_t ble_peripheral_init(int8_t i8_tx_power,
		ble_adv_mode_t advertising_mode, uint8_t* pau8_dev_name,
		uint16_t u16_dev_name_len, ble_uuid_t* p_ble_uuid,
		ble_advdata_manuf_data_t* p_manuf_specific_data,
		const uint8_t au8_irk[BLE_GAP_SEC_KEY_LEN], uint8_t u8_mode,
		ble_evt_handler_t evt_handler) {
	uint32_t err_code = NRF_SUCCESS;

	// parameter check
	if (u16_dev_name_len > BLE_GAP_DEVNAME_MAX_LEN) {
		return NRF_ERROR_INVALID_PARAM;
	}
	m_u8_mode = u8_mode;
	m_evt_handler = evt_handler;

	// initialze the softdevice / BLE stack
	err_code = ble_peripheral_ble_stack_init();	// initializing the SoftDevice / BLE stack
	RETURN_ON_NRF_ERROR(err_code);
#ifdef CONF_BLE_PERIPHERAL_TRACE_MAC_ADDRESS_AT_INIT
	err_code = ble_peripheral_trace_mac_address();
	RETURN_ON_NRF_ERROR(err_code);
#endif
	// initialze profile services
	// Generic Access (Profile) service
	err_code = ble_peripheral_gap_params_init(pau8_dev_name, u16_dev_name_len);
	RETURN_ON_NRF_ERROR(err_code);
	// Generic ATTribut service
	// Custom UART service
	err_code = gatt_init();
	RETURN_ON_NRF_ERROR(err_code);

	err_code = ble_peripheral_services_init(p_ble_uuid->uuid);
	RETURN_ON_NRF_ERROR(err_code);
	err_code = ble_peripheral_advertising_init(i8_tx_power,
			p_manuf_specific_data, p_ble_uuid);
	RETURN_ON_NRF_ERROR(err_code);

	// initialize the connection
	err_code = ble_peripheral_conn_params_init();
	TRACE_DBG_L("ble_peripheral_conn_params_init() stop %d\n", err_code);
	RETURN_ON_NRF_ERROR(err_code);

	err_code = ble_peripheral_init_privacy(au8_irk);
	TRACE_DBG_L("ble_peripheral_init_privacy() stop %d\n", err_code);
	RETURN_ON_NRF_ERROR(err_code);

	err_code = sd_ble_gap_tx_power_set(i8_tx_power);
	APP_ERROR_CHECK(err_code);

	err_code = ble_advertising_start(advertising_mode);
	TRACE_DBG_L("ble_advertising_start() stop %d\n", err_code);
	RETURN_ON_NRF_ERROR(err_code);

	m_tState = BLE_P_STATE_ADVERTISING;
	TRACE_DBG_L("BLE_P_STATE_ADVERTISING\n");
#ifdef CONF_BLE_RESET_ON_CONNECTION_PROBLEM
	// register a timeout function to reset the device in case of no BLE connection
	FUNR_register(ble_peripheral_no_connection_timeout_handler, BLE_NO_CONN_TIMEOUT_MS);
#endif

	return err_code;
}

/*-------------------------------------------------------------------------*/

uint16_t ble_peripheral_get_conn_handle(void) {
	return m_conn_handle;
}

/*-------------------------------------------------------------------------*/

eBLE_P_State_t ble_peripheral_get_state(void) {
	return m_tState;
}

/*-------------------------------------------------------------------------*/

void ble_peripheral_set_event(eBLE_P_Event_t tEvent) {
	if (m_tEvent == BLE_P_EVT_NONE) {
		m_tEvent = tEvent;
		//TRACE_DBG_L("BLE handler event: %u\n", m_tEvent);
	} else {
		TRACE_DBG_L("Warning: An event is already pending! Event not sent to BLE handler.\n");
	}
}

/*-------------------------------------------------------------------------*/

uint32_t ble_peripheral_trace_mac_address(void) {
	ble_gap_addr_t t_addr;
	uint32_t err_code = ERR_OK;

	err_code = sd_ble_gap_addr_get(&t_addr);
	RETURN_ON_NRF_ERROR(err_code);TRACE_DBG_L("MAC address: %02x:%02x:%02x:%02x:%02x:%02x // LSB format\r\n",
			t_addr.addr[0], t_addr.addr[1], t_addr.addr[2],
			t_addr.addr[3], t_addr.addr[4], t_addr.addr[5]);

	return err_code;
}

/*-------------------------------------------------------------------------*/

uint16_t ble_peripheral_get_data_length(void) {
	return m_ble_nus_max_data_len;
}
