/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: Generic Library
 *
 *     Module: Error
 *      State: reviewed
 * Originator: E. Hirt
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/error/error.c $
 *  $Revision: 25035 $
 *      $Date: 2018-06-13 13:00:25 +0200 (Wed, 13 Jun 2018) $
 *    $Author: Schwinghammer $
 *
 *  Developed by Art of Technology AG, 2009
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/
#include "error.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stddef.h>

/* headers from other modules ---------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  local constants and macros                                              |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  module-global types                                                     |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  module-global variables                                                 |
 +--------------------------------------------------------------------------*/

/**
 * Context for maximum assert error
 */
static ERR_ErrorInfo ASSERT_ERROR =
{
    .moduleNumber = ERR_MOD_ERROR,
    .errorClass = ERR_CLASS_ERROR,
    .errorNumber = 1,
    .lineNumber = 0,
    ERRORSTRING("assert")
};

ERR_CallbackType m_callback = NULL;    ///< application callback, NULL to not in use

/*--------------------------------------------------------------------------+
 |  declaration of local functions (helper functions)                       |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  functions                                                               |
 +--------------------------------------------------------------------------*/

void ERR_init(ERR_CallbackType function)
{
    m_callback = function; 
}

/*-------------------------------------------------------------------------*/
void ERR_setErrorParam(const ERR_ErrorInfo* pInfo, uint16_t lineNumber, ERR_pErrorInfo pError,
        uint32_t u32errParam1, uint32_t u32errParam2)
{
    ERR_ErrorInfo sInfo; ///< ensure writeable error structure

    if (pError == NULL)
    {
        // no external reporting, use internal structure
        pError = &sInfo;
    }

    pError->lineNumber = lineNumber;
    if (pInfo != NULL)
    {
        pError->moduleNumber = pInfo->moduleNumber;
        pError->errorClass   = pInfo->errorClass;
        pError->errorNumber  = pInfo->errorNumber;
        pError->errorString  = pInfo->errorString;
    }
    else
    {
        pError->moduleNumber = 0;
        pError->errorClass   = ERR_CLASS_NOERROR;
        pError->errorNumber  = 0;
        pError->errorString  = NULL;
    }

    // execute application callback if available
    if (m_callback != NULL) 
    {
        m_callback(pError, u32errParam1, u32errParam2);
    }
}

/*-------------------------------------------------------------------------*/
void ERR_assert(bool condition, uint16_t lineNumber, char* filename)
{
    if (condition)
    {
        if (ASSERT_ERROR.errorString != NULL)
        {
            ASSERT_ERROR.errorString = filename;
        }
        ERR_setErrorParam(&ASSERT_ERROR, __LINE__, NULL, lineNumber, 0);
        while (1)
        {
            // intentionally endless loop
        };
    }
}
/*-------------------------------------------------------------------------*/
// eof
