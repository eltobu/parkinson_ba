/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: Generic Library
 *
 *     Module: Error
 *      State: reviewed
 * Originator: Hirt
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/error/error.h $
 *  $Revision: 25047 $
 *      $Date: 2018-06-14 12:28:07 +0200 (Thu, 14 Jun 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2009
 *
 ****************************************************************************
 */
/**
 * @file
 * Module ERR: Error
 *
 * This module implements the general error handling
 *
 *
 ****************************************************************************
 */

#ifndef ERROR_H_
#define ERROR_H_

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

/* headers from other modules ---------------------------------------------*/
#include "module_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
 |  check configuration                                                     |
 +--------------------------------------------------------------------------*/
#ifndef  CFG_YES
    #warning "Warning: CFG_YES is not defined!"
#endif
#ifndef  CFG_NO
    #warning "Warning: CFG_NO is not defined!"
#endif

/*--------------------------------------------------------------------------+
 |  global constants                                                        |
 +--------------------------------------------------------------------------*/
#ifndef CFG_ERR_USE_ERRORSTRING
    // ensure backward (default) behavior
    // Define CFG_ERR_USE_ERRORSTRING CFG_NO
    // if the errorstring shall be disabled to save memory on small processors.
    #define CFG_ERR_USE_ERRORSTRING CFG_YES
#endif

#if (CFG_ERR_USE_ERRORSTRING == CFG_NO)
    #define ERRORSTRING(x) .errorString = NULL
#else
    #define ERRORSTRING(x) .errorString = x
#endif

/*--------------------------------------------------------------------------+
 |  global types                                                             |
 +--------------------------------------------------------------------------*/

/**
 status type, used for return success or error
 */
typedef enum tagERR_StatusType
{
    ERR_OK       = 0x00, ///< return value for ok
    ERR_ERROR    = 0x01, ///< return value for any error. Check error Module

    /*
     * Following errors are intended to inform the next level only.
     * If transferred to a higher level they should be changed to ERR_ERROR
     * posix like errors
     */
    ERR_AGAIN    = 0x02, ///< Resource temporarily unavailable such as Buffer not filled or consumed -> retry
    ERR_BUSY     = 0x03, ///< Device or resource busy -> resolve conflict and retry after that
    ERR_NOMEM    = 0x04, ///< Not enough space
    ERR_TIMEDOUT = 0x05, ///< Connection timed out
    ERR_NOSYS    = 0x06, ///< Function not implemented (NO SUCH SYSTEM CALL)
    /**
     * Bad protocol exchange (might be only a warning, depending on module)
     * Return when an invalid packet has been received (and detected) during a communication.
     * The error is not fatal (ie the protocol stack doesn't lock up) and can be recovered,
     * but the caller should still be informed of the issue.
     */
    ERR_BADE     = 0x07,
    ERR_NAK      = 0x08, ///< Message not acknowledged
} ERR_StatusType;

/*--------------------------------------------------------------------------*/

/**
 Module Numbers of SAM7 Modules
 Never ever change or remove an existing number
 For private modules add enum to board.h starting at ERR_MOD_PRIVATE_START.
 */
typedef enum tagERR_ModuleNumbers
{
    ERR_MOD_ERROR               = 0x1,
    ERR_MOD_ADC_DRV             = 0x2,
    ERR_MOD_DISPLAY_128_64      = 0x3,
    ERR_MOD_DISPLAYWX12863_DRV  = 0x4,
    ERR_MOD_SAM7FLASH           = 0x5, // obsolete
    ERR_MOD_INTERNAL_FLASH      = 0x5, // internal flash of SAM and NRF
    ERR_MOD_INTERRUPT           = 0x6,
    ERR_MOD_ADC_LTC244X         = 0x7,
    ERR_MOD_PIO_INTERRUPT       = 0x8,
    ERR_MOD_PWM                 = 0x9,
    ERR_MOD_ACC_SCA3000         = 0xA,
    ERR_MOD_SPI                 = 0xB,
    ERR_MOD_CLOCK               = 0xC,
    ERR_MOD_HAL_TIMER           = 0xD,
    ERR_MOD_TRACE               = 0xE,
    ERR_MOD_SERIAL              = 0xF,
    ERR_MOD_SDCARD_DATETIME     = 0x10,
    ERR_MOD_FILE_SYS            = 0x11,
    ERR_MOD_SD_SPI_CMD          = 0x12,
    ERR_MOD_HS_SDCMD            = 0x12, // obsolete name for sd_spi_cmd
    ERR_MOD_MC                  = 0x13,
    ERR_MOD_CRC                 = 0x14,
    ERR_MOD_RB                  = 0x15,
    ERR_MOD_TIMER               = 0x16,
    ERR_MOD_SCP1000             = 0x17,
    ERR_MOD_TWD                 = 0x18,
    ERR_MOD_AOTPROT_PROTOCOL    = 0x19,
    ERR_MOD_AOTPROT_PARSER      = 0x1A,
    ERR_MOD_TWI                 = 0x1B,
    ERR_MOD_AOTPROT_CONTROL     = 0x1C,
    ERR_MOD_MT9P001             = 0x1D,
    ERR_MOD_SDNATHAL            = 0x1E,
    ERR_MOD_SDNAT               = 0x1F,
    ERR_MOD_KEY                 = 0x20,
    ERR_MOD_SCA8x0              = 0x21,
    ERR_MOD_SHTXX               = 0x22,
    ERR_MOD_SAOCS               = 0x23,
    ERR_MOD_AOTPROT_COMMAND     = 0x24,
    ERR_MOD_HANDGRIP            = 0x25,
    ERR_MOD_MS5607              = 0x26, //< pressure/temperature sensor
    ERR_MOD_AT45DBXXD			= 0x27,	//< AT45DBxxD Serial Flash
    ERR_MOD_AT45DBXXD_TEST		= 0x28,	//< Module test of serial data flash
    ERR_MOD_UBX					= 0x29,	//< u-blox GPS receiver
    ERR_MOD_WD                  = 0x2A, //< watchdog
    ERR_MOD_OW                  = 0x2B, //< Dallas 1 wire
    ERR_MOD_DS2762              = 0x2C, //< LiPo battery protection (Dallas1W) share ID with TWI version
    ERR_MOD_DS2764              = 0x2C, //< LiPo battery protection (TWI) share ID with Dallas1W version
    ERR_MOD_PCD                 = 0x2D, //< PCD: RFID reader chips (MRFC522)
    ERR_MOD_PICC                = 0x2E, //< PICC: RFID card (Mifare Desfire)
    ERR_MOD_RFID                = 0x2F, //< RFID: subsystem
    ERR_MOD_MCP                 = 0x30, //< I2C - GPIO modul MCP23008
    ERR_MOD_PCKOUT				= 0x31,	//< PCK output module
    ERR_MOD_OV_COMMON			= 0x32,	//< common module for Omnivision Camera
    ERR_MOD_OV_CAPTURE			= 0x33,	//< capture module for Omnivision Camera
    ERR_MOD_UART				= 0x34,	//< UART module
    ERR_MOD_DAC_DRV				= 0x35,	//< DAC hal module
    ERR_MOD_GSM                 = 0x36, //< GSM LISA module
    ERR_MOD_RV3029B             = 0x37, //< Real Time clock RV3029B with TWI
    ERR_MOD_US                  = 0x38, //< USART module
    ERR_MOD_ADC_LTC2499         = 0x39, //< LTC2499 ADC converter with TWI
    ERR_MOD_CC3000              = 0x3A, //< CC3000 WiFi module
    ERR_MOD_EM3027              = 0x3B, //< EM3027 Real time clock module
	ERR_MOD_RTC	                = 0x3C, //< RTC Real time clock module - could by EM3027 or PCF85063
    ERR_MOD_DDC118              = 0x3D, //< DDC118 current-input ADC
    ERR_MOD_SCHEDULER           = 0x3E, //< Scheduler, Function Run
    ERR_MOD_MTD                 = 0x3F, //< MTD415T temperature controller
    ERR_MOD_POWER               = 0x40, //< Power module
    ERR_MOD_LOG                 = 0x41, //< Generic log module
    ERR_MOD_ATCA                = 0x42, //< Crypto Authentication module
	ERR_MOD_PIO                 = 0x43, //< PIO module
    ERR_MOD_IND_LDCXXXX         = 0x44, //< Inductance-to-Digital Converter (LDC1100)

    // USB range
    ERR_MOD_USB_HAL             = 0x100,
    ERR_MOD_USB_CH9             = 0x101,
    // USB Classes
    ERR_MOD_USB_VCP             = 0x110,
	ERR_MOD_USB_HID				= 0x111,
    ERR_MOD_PRIVATE_START = 0x40000000, // start value for private modules. Do not use highest bit
} ERR_ModuleNumbers;

/**
 Error classification enumerator
 For private error classes add enum to board.h starting at ERR_CLASS_PRIVATE_START.
 Copy this definition. Do not include this header file in board.h
 */
typedef enum tagERR_ErrorClass
{
    ERR_CLASS_NOERROR       = 0x0, ///< no error
    ERR_CLASS_ERROR         = 0x1, ///< general error
    ERR_CLASS_WARNING       = 0x2, ///< warning
    ERR_CLASS_FATAL         = 0x3, ///< fatal error
    ERR_CLASS_PRIVATE_START = 0x8000, ///< start value for private error classes
} ERR_ErrorClass;

/*--------------------------------------------------------------------------*/

/**
 Structure to handle error information.
 For logging usually parameters are added!
 */
typedef struct tagERR_ErrorInfo
{
    uint32_t moduleNumber;///< module identifier: @code ERR_ModuleNumbers @endcode
    char*    errorString; ///< human readable error description
    uint16_t errorClass;  ///< error classification: @code ERR_ErrorClass @endcode
    uint16_t errorNumber; ///< within Module
    uint16_t lineNumber;  ///< source code line number
}*ERR_pErrorInfo, ERR_ErrorInfo;

/**
 application callback for error module
 */
typedef void (*ERR_CallbackType)(const ERR_pErrorInfo pError, uint32_t u32errParam1,
        uint32_t u32errParam2);

/*--------------------------------------------------------------------------+
 |  macros                                                                  |
 +--------------------------------------------------------------------------*/

/**
 * MACRO: Simple error handling to increase code readablity
 * Just calls the given function and - in case of error - leaves the _calling_ function
 * with ERR_ERROR
 *
 * @param AOT_Function: complete function including parameters
 */
#define RETURN_ON_ERROR(AOT_Function) \
    if (AOT_Function == ERR_ERROR) \
    { \
        return ERR_ERROR; \
    }

/** Terminate the calling function in case the driver function fails
 * MACRO: Simple error handling to increase code readablity
 * Just calls the given function and -
 * in case of error - terminates the _calling_ function
 *
 * @param AOT_Function: complete function including parameters
 */
#define STOP_ON_ERROR(AOT_Function) \
    if (AOT_Function == ERR_ERROR) \
    { \
        return; \
    }

/** Break the calling loop in case the driver function fails
 * MACRO: Simple error handling to increase code readablity
 * Just calls the given function and -
 * in case of error - breaks the _calling_ loop
 *
 * @param AOT_Function: complete function including parameters
 */
#define BREAK_ON_ERROR(AOT_Function) \
    if (AOT_Function == ERR_ERROR) \
    { \
        break; \
    }

/** Continues the calling loop in case the driver function fails
 * MACRO: Simple error handling to increase code readablity
 * Just calls the given function and -
 * in case of error - continues the _calling_ loop
 *
 * @param AOT_Function: complete function including parameters
 */
#define CONTINUE_ON_ERROR(AOT_Function) \
    if (AOT_Function == ERR_ERROR) \
    { \
        continue; \
    }

/*-------------------------------------------------------------------------*/

/**
 * MACRO: Smarter error handling to increase code readablity
 * Just calls the given function and - in case of error - first calls the cleanup
 * function, then leaves the _calling_ function with ERR_ERROR
 *
 * @param AOT_Function: complete function including parameters
 * @param AOT_Err_Function: cleanup function including parameters
 */
#define RETURN_ON_ERROR_CLEAN(AOT_Function, AOT_Err_Function) \
    if (AOT_Function == ERR_ERROR) \
    { \
        AOT_Err_Function; \
        return ERR_ERROR; \
    }

/*--------------------------------------------------------------------------+
 |  function prototypes for C                                                |
 +--------------------------------------------------------------------------*/

/**
 Initializes the error module

 @param function: pointer to application error callback function, NULL for not in use
 */
void ERR_init(ERR_CallbackType function);
/*-------------------------------------------------------------------------*/

/**
 stores the error to be requested from the calling or higher level module

 @param pInfo: pointer to error stucture (from const)
 @param lineNumber: source code line number
 @param pError: pointer to error stucture (output)
 @param u32errParam1: optional parameter for additional information
 @param u32errParam2: optional parameter for additional information
 */
void ERR_setErrorParam(const ERR_ErrorInfo* pInfo, uint16_t lineNumber, ERR_pErrorInfo pError,
        uint32_t u32errParam1, uint32_t u32errParam2);
/*-------------------------------------------------------------------------*/

/**
 assert function. program can be halted, so that watchdog can reboot system.

 @param condition: halt program when true
 @param lineNumber: source code line number
 @param filename: source code file
 */
void ERR_assert(bool condition, uint16_t lineNumber, char* filename);
/*-------------------------------------------------------------------------*/

#ifdef  __cplusplus
}
#endif

#endif // ERROR_H_
/* eof */

