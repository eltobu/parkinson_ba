/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: PWM driver
 *      State: reviewed
 * Originator: M.Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/pwm/pwm.h $
 *  $Revision: 24246 $
 *      $Date: 2018-04-23 12:22:42 +0200 (Mon, 23 Apr 2018) $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Module PWM:
 *
 * @brief This module contains functions to control the PWM module.
 *
 * INFO:
 * The PWM in sdk_config.h must not be enabled.
 * Best usage: set "PWM_ENABLED 0". In this way has the SDK no code for PWM.
 *
 ****************************************************************************
 */

#ifndef PWM_H
#define PWM_H

/* Includes needed by this header  ----------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "error.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
 |  global defines                                                           |
 +--------------------------------------------------------------------------*/

#define PWM_DUTY_CYCLE_MAX 1000 ///< Per mill high

/*--------------------------------------------------------------------------+
 |  global types                                                             |
 +--------------------------------------------------------------------------*/

/**
 * Context definition to setup all the required hardware
 * Each interface has to manage 4 channels.
 * Only 1 context makes sense for one interface.
 */
typedef struct tagPWM_Context {
	uint8_t u8Interface;   ///< [0, 1, 2] 3 PWMs a 4 channels are available
	uint32_t u32Frequency;  ///< PWM signal frequency.
	uint8_t u8Pin1;        ///< [0..31] Pin number for channel 1
	uint8_t u8Pin2;        ///< [0..31] Pin number for channel 2
	uint8_t u8Pin3;        ///< [0..31] Pin number for channel 3
	uint8_t u8Pin4;        ///< [0..31] Pin number for channel 4
	bool bPin1Enabled;  ///< true when channel 1 is used, false = disabled
	bool bPin2Enabled;  ///< true when channel 2 is used, false = disabled
	bool bPin3Enabled;  ///< true when channel 3 is used, false = disabled
	bool bPin4Enabled;  ///< true when channel 4 is used, false = disabled
	void *pInternal;    ///< Driver internal use only
} PWM_Context;

/*--------------------------------------------------------------------------+
 |  function prototypes for C                                                |
 +--------------------------------------------------------------------------*/

/**
 * @brief Function to initialize the PWM module
 *
 * @details This function will set up all the necessary parameters
 *          The PWM duty cycle is set to zero
 *          The active pins are LOW
 *          The interface is stopped
 *
 * Code samples:
 * static PWM_Context m_Context =
 * {
 *     .u8Interface  = 0,
 *     .u32Frequency = BRD_MOTOR_PWM_FREQUENCY_HZ,
 *     .u8Pin1       = BRD_MOTOR_PWM_1,
 *     .u8Pin2       = BRD_MOTOR_PWM_2,
 *     .u8Pin3       = 0,
 *     .u8Pin4       = 0,
 *     .bPin1Enabled = true,
 *     .bPin2Enabled = true,
 *     .bPin3Enabled = false,
 *     .bPin4Enabled = false,
 * };
 * PWM_init(&m_Context, 0, NULL);
 * Status = PWM_generate(&m_Context, 100, 900, 0, 0);
 * PWM_start(&m_Context);
 *
 * @param   pContext    Pointer to the PWM context structure
 * @param   u32MCK_Freq Master Clock, UNUSED, for interface compatibility
 * @param   pError      Pointer to error structure, most likely NULL
 *
 * @retval              ERR_OK if operation was successful
 */
uint32_t PWM_init(PWM_Context *pContext, uint32_t u32MCK_Freq,
		ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function to de-initialize the PWM module
 *
 * @details	sets pins to default configuration
 *
 * @param   pContext    Pointer to the PWM context structure
 */
void PWM_deinit(PWM_Context *pContext);

/*-------------------------------------------------------------------------*/

/**
 * @brief It initializes the duty cycle for the only enabled channel.
 *
 * @details This interface is for compatibility reason
 *          It will be the first enabled pin.
 *          It must be started before getting active.
 *          If the PWM is already running,
 *          then is the new value loaded with the next cycle.
 *
 * @param pContext        Pointer to the PWM context structure
 * @param u16dutyCycle    Positive duty cycle. Values from 0..1000 "per mill"
 * @param pError          Pointer to error structure, most likely NULL
 *
 * @retval                ERR_OK if operation was successful
 */
uint32_t PWM_generate(PWM_Context *pContext, uint16_t u16dutyCycle,
		ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @brief It initializes the duty cycle for the specified channel.
 *
 * @details It must be started before getting active.
 *          If the PWM is already running,
 *          then is the new value loaded with the next cycle.
 *          Inactive channels have no effect.
 *          All active pins must be changed at the same time to be glitch free
 *
 * @param pContext        Pointer to the PWM context structure
 * @param u8channel       Number of PWM channel (0-indexed)
 * @param u16dutyCycle    Positive duty cycle. Values from 0..1000 "per mill"
 * @param pError          Pointer to error structure, most likely NULL
 *
 * @retval                ERR_OK if operation was successful
 */
uint32_t PWM_generateOne(PWM_Context *pContext, uint8_t u8channel,
		uint16_t u16dutyCycle, ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @brief It initializes the duty cycle for each channel.
 *
 * @details It must be started before getting active.
 *          If the PWM is already running,
 *          then is the new value loaded with the next cycle.
 *          Inactive channels have no effect.
 *          All active pins must be changed at the same time to be glitch free
 *
 * @param pContext          Pointer to the PWM context structure
 * @param u16dutyCycle1..4  Positive duty cycle. Values from 0..1000 "per mill"
 * @param pError            Pointer to error structure, most likely NULL
 *
 * @retval                  ERR_OK if operation was successful
 */
uint32_t PWM_generateAll(PWM_Context *pContext, uint16_t u16dutyCycle1,
		uint16_t u16dutyCycle2, uint16_t u16dutyCycle3, uint16_t u16dutyCycle4,
		ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @brief Starts a previous initializes PWM interface.
 *
 * Code samples:
 * Status = PWM_generate(&m_Context, 100, 900, 0, 0);
 * PWM_start(&m_Context);
 *
 * @param pContext          Pointer to the PWM context structure
 */
void PWM_start(PWM_Context *pContext);

/*-------------------------------------------------------------------------*/

/**
 * @brief Stops a running PWM interface.
 *
 * Code samples:
 * PWM_stop(&m_Context);  // Outputs go low
 * Status = PWM_generate(&m_Context, 123, 456, 0, 0); // Outputs stay low
 *
 * @param pContext          Pointer to the PWM context structure
 */
void PWM_stop(PWM_Context *pContext);

/*-------------------------------------------------------------------------*/

/**
 * @brief Module test, only active during developing
 */
void PWM_test(void);

/*-------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // PWM_H
