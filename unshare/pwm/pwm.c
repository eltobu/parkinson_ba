/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: PWM driver
 *      State: reviewed
 * Originator: M.Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/pwm/pwm.c $
 *  $Revision: 24246 $
 *      $Date: 2018-04-23 12:22:42 +0200 (Mon, 23 Apr 2018) $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "pwm.h"
#include <stddef.h>
#include "memorymanagement.h"
#include "nrf.h"
#include "trace.h"


// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_PWM
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_PWM undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_PWM
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/**
 * Range error occurred, Parameters, Context
 */
const ERR_ErrorInfo PWM_RANGE_ERROR = {
        .moduleNumber = ERR_MOD_PWM,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 1,
        .lineNumber = 0,
        ERRORSTRING("PWM Range Error, check context settings")
};

/**
 * Range error occurred, Parameters, Context
 */
const ERR_ErrorInfo PWM_CYCLE_ERROR = {
        .moduleNumber = ERR_MOD_PWM,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 2,
        .lineNumber = 0,
        ERRORSTRING("PWM Duty Cycle out of range")
};

#define PWMS_PER_DEVICE 4           ///< Synchronous pwm's
#define HIGHEST_PIN     31          ///< Pins 0..31 (0x1F) can be used
#define PWM_CLK         16000000    ///< The clock going to pre-scaler

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/**
 * PWM state of the configured context
 */
typedef enum tag_PWM_State
{
    PWM_Stopped,     ///< Outputs active and low
    PWM_Running,     ///< Outputs showing the duty cycle
} State_t;

/**
 * This is the store for every active context
 * This structure manages the behavior of the PWM.
 */
typedef struct tagPWM_Instance
{
    uint16_t u16Sequence[PWMS_PER_DEVICE];  ///< Duty cycles for each channel.
    State_t  PWM_State;       ///< Reflects the state of this instance
    NRF_PWM_Type *pPWMx;      ///< Peripheral interface
    float fStep;              ///< Increment per mill
} Instance_t;


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  local helper functions                                                   |
+--------------------------------------------------------------------------*/

static uint16_t DutyCycle2Sequence(PWM_Context *pContext, uint16_t u16DutyCycle)
{
    Instance_t *pInstance = pContext->pInternal;
    uint32_t   Sequence;
    uint32_t   Invert;
    uint32_t   Top = pInstance->pPWMx->COUNTERTOP;

    // Invert the duty cycle 10% would produce 90% High
    Invert = PWM_DUTY_CYCLE_MAX - u16DutyCycle;
    Sequence = (uint32_t)((float)Invert * pInstance->fStep);
    if (Sequence > Top)
    {
        Sequence = Top;
    }
    // Check if minimum duty cycle really shows one
    if ((Sequence == Top) && (u16DutyCycle > 0))
    {
        Sequence = Top - 1;
    }
    // Check if maximum duty cycle really shows one
    if ((Sequence == 0) && (u16DutyCycle < PWM_DUTY_CYCLE_MAX))
    {
        Sequence = 1; // Prevent rounding problem
    }
    TRACE_DBG_L("PWM_ Top: %d, Cyc %d, Seq %d\n", Top, u16DutyCycle, Sequence);

    return ((uint16_t)Sequence);

} // End of DutyCycle2Sequence


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

uint32_t PWM_init(PWM_Context *pContext,
                  uint32_t u32MCK_Freq, ERR_pErrorInfo pError)
{
    uint32_t   Status = ERR_OK;
    uint32_t   Scaler;
    uint32_t   Index;
    Instance_t *pInstance; ///< For easy access only

    // Helper for loop initialization
    bool       Enabled[PWMS_PER_DEVICE];
    uint8_t    Pins[PWMS_PER_DEVICE];
    uint8_t    Prescalers[8];

    // Check the input context settings first
    if (pContext->u8Interface > 2)
    {
        Status = ERR_ERROR;  // PWM0..PWM2 are possible
    }
    if ((pContext->bPin1Enabled && pContext->u8Pin1 > HIGHEST_PIN)
    ||  (pContext->bPin2Enabled && pContext->u8Pin2 > HIGHEST_PIN)
    ||  (pContext->bPin3Enabled && pContext->u8Pin3 > HIGHEST_PIN)
    ||  (pContext->bPin4Enabled && pContext->u8Pin4 > HIGHEST_PIN))
    {
        Status = ERR_ERROR;  // Pins 0..31 are possible
    }
    if (! (pContext->bPin1Enabled || pContext->bPin2Enabled ||
           pContext->bPin3Enabled || pContext->bPin4Enabled))
    {
        Status = ERR_ERROR;  // At leased one channel must be enabled
    }
    if ((pContext->u32Frequency > 1000000) || (pContext->u32Frequency < 4))
    {
        Status = ERR_ERROR;  // At 16 MHz PWM clock !!?
    }
    if (Status != ERR_OK)
    {
        ERR_setErrorParam(&PWM_RANGE_ERROR, __LINE__, pError,
                pContext->u8Interface, pContext->u32Frequency);
        return (Status);
    }

    // Prepare the used memory
#if (CFG_MM_SMALL_BLOCK_HEADER_MODE == 1)
            pContext->pInternal = MM_malloc(sizeof(Instance_t));
#endif
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
            pContext->pInternal = MM_malloc(sizeof(Instance_t), pContext->pInternal, "PWMM");
#endif

    //pContext->pInternal = MM_malloc(sizeof(Instance_t));

    pInstance = pContext->pInternal;
    switch (pContext->u8Interface)
    {
    case 0:
        pInstance->pPWMx = NRF_PWM0;
        break;
    case 1:
        pInstance->pPWMx = NRF_PWM1;
        break;
    default:
        pInstance->pPWMx = NRF_PWM2;
    }

    // Prepare for easy loop handling
    Enabled[0] = pContext->bPin1Enabled;
    Enabled[1] = pContext->bPin2Enabled;
    Enabled[2] = pContext->bPin3Enabled;
    Enabled[3] = pContext->bPin4Enabled;
    Pins[0]    = pContext->u8Pin1;
    Pins[1]    = pContext->u8Pin2;
    Pins[2]    = pContext->u8Pin3;
    Pins[3]    = pContext->u8Pin4;
    Prescalers[0] = PWM_PRESCALER_PRESCALER_DIV_1;
    Prescalers[1] = PWM_PRESCALER_PRESCALER_DIV_2;
    Prescalers[2] = PWM_PRESCALER_PRESCALER_DIV_4;
    Prescalers[3] = PWM_PRESCALER_PRESCALER_DIV_8;
    Prescalers[4] = PWM_PRESCALER_PRESCALER_DIV_16;
    Prescalers[5] = PWM_PRESCALER_PRESCALER_DIV_32;
    Prescalers[6] = PWM_PRESCALER_PRESCALER_DIV_64;
    Prescalers[7] = PWM_PRESCALER_PRESCALER_DIV_128;

    for (int i = 0; i < PWMS_PER_DEVICE; i++)
    {
        // Setup pins
        if (Enabled[i])
        {
            pInstance->pPWMx->PSEL.OUT[i] = (Pins[i] << PWM_PSEL_OUT_PIN_Pos) |
                      (PWM_PSEL_OUT_CONNECT_Connected << PWM_PSEL_OUT_CONNECT_Pos);
        }
        else
        {
            pInstance->pPWMx->PSEL.OUT[i] = PWM_PSEL_OUT_CONNECT_Disconnected
                                         << PWM_PSEL_OUT_CONNECT_Pos;
        }
    }
    // Setup the rest of the device instance
    pInstance->pPWMx->ENABLE = (PWM_ENABLE_ENABLE_Enabled << PWM_ENABLE_ENABLE_Pos);

    // UP mode
    pInstance->pPWMx->MODE = (PWM_MODE_UPDOWN_Up << PWM_MODE_UPDOWN_Pos);

    // Get prescaler from the 16 MHz PWM-CLK and the desired frequency
    Scaler = PWM_CLK / (pContext->u32Frequency * PWM_DUTY_CYCLE_MAX);
    Index = 0;
    while (Scaler > 0)
    {
        Scaler >>= 1;
        if (Scaler > 0)
        {
            Index++;
        }
    }
    if (Index > 7)
    {
        Index = 7;
    }
    pInstance->pPWMx->PRESCALER = (Prescalers[Index] << PWM_PRESCALER_PRESCALER_Pos);

    // UpCounter = f_clk / Prescaler / f_pwm
    pInstance->pPWMx->COUNTERTOP = (((PWM_CLK / (1 << Index)) / pContext->u32Frequency)
                                 << PWM_COUNTERTOP_COUNTERTOP_Pos);

    pInstance->fStep = (float)pInstance->pPWMx->COUNTERTOP / (float) PWM_DUTY_CYCLE_MAX;
    for (int i = 0; i < PWMS_PER_DEVICE; i++)
    {
        pInstance->u16Sequence[i] = pInstance->pPWMx->COUNTERTOP; // Is output LOW
    }

    pInstance->pPWMx->LOOP = (PWM_LOOP_CNT_Disabled << PWM_LOOP_CNT_Pos);

    pInstance->pPWMx->DECODER = (PWM_DECODER_LOAD_Individual << PWM_DECODER_LOAD_Pos)
                              | (PWM_DECODER_MODE_RefreshCount << PWM_DECODER_MODE_Pos);

    pInstance->pPWMx->SEQ[0].PTR = ((uint32_t)(pInstance->u16Sequence) << PWM_SEQ_PTR_PTR_Pos);

    pInstance->pPWMx->SEQ[0].CNT = ((sizeof(pInstance->u16Sequence) / sizeof(uint16_t))
                                 << PWM_SEQ_CNT_CNT_Pos);

    pInstance->pPWMx->SEQ[0].REFRESH = 0;
    pInstance->pPWMx->SEQ[0].ENDDELAY = 0;
    pInstance->pPWMx->TASKS_SEQSTART[0] = 0; // Do NOT start now

    PWM_stop(pContext);

    TRACE_DBG_L("PWM_init() Fpwm = %d, UP: %d, Prescaler %d, mStep %d\n",
                pContext->u32Frequency, pInstance->pPWMx->COUNTERTOP, 1 << Index,
                (int)(pInstance->fStep * 1000.0));

	return (ERR_OK);

} // End of PWM_init

/*-------------------------------------------------------------------------*/

void PWM_deinit(PWM_Context *pContext)
{
    Instance_t *pInstance = pContext->pInternal;

    pInstance->pPWMx->TASKS_STOP = 1;
    pInstance->pPWMx->ENABLE = (PWM_ENABLE_ENABLE_Disabled << PWM_ENABLE_ENABLE_Pos);

    for (int i = 0; i < PWMS_PER_DEVICE; i++)
    {
        pInstance->pPWMx->PSEL.OUT[i] = PWM_PSEL_OUT_CONNECT_Disconnected
                                     << PWM_PSEL_OUT_CONNECT_Pos;
    }
    MM_free(pContext->pInternal);

} // End of PWM_deinit

/*-------------------------------------------------------------------------*/

uint32_t PWM_generate(PWM_Context *pContext, uint16_t u16dutyCycle,
                      ERR_pErrorInfo pError)
{
    uint16_t   Sequence;
    Instance_t *pInstance = pContext->pInternal;

    // Validate parameters
    if (u16dutyCycle > PWM_DUTY_CYCLE_MAX)
    {
        ERR_setErrorParam(&PWM_CYCLE_ERROR, __LINE__, pError, 0, u16dutyCycle);
        return (ERR_ERROR);  // Duty cycle 0..1000 are possible
    }
    Sequence = DutyCycle2Sequence(pContext, u16dutyCycle);

    // Find the channel to be changed
    if (pContext->bPin1Enabled)
    {
        pInstance->u16Sequence[0] = Sequence;
    }
    else if (pContext->bPin2Enabled)
    {
        pInstance->u16Sequence[1] = Sequence;
    }
    else if (pContext->bPin3Enabled)
    {
        pInstance->u16Sequence[2] = Sequence;
    }
    else if (pContext->bPin4Enabled)
    {
        pInstance->u16Sequence[3] = Sequence;
    }
    // Re-trigger in case PWM is already running
    if (pInstance->PWM_State == PWM_Running)
    {
        PWM_start(pContext);
    }
    return (ERR_OK);

} // End of PWM_generate

uint32_t PWM_generateOne(PWM_Context *pContext, uint8_t u8channel,
		uint16_t u16dutyCycle, ERR_pErrorInfo pError) {
	Instance_t *pInstance = pContext->pInternal;

	switch (u8channel) {
	case 0:
		// Validate parameters
		if ((pContext->bPin1Enabled && u16dutyCycle > PWM_DUTY_CYCLE_MAX))
		{
			ERR_setErrorParam(&PWM_CYCLE_ERROR, __LINE__, pError,
					u8channel, u16dutyCycle);
			return (ERR_ERROR);  // Duty cycle 0..1000 are possible
		}

		// Set the new duty cycles as sequence
		if (pContext->bPin1Enabled)
		{
			pInstance->u16Sequence[0] = DutyCycle2Sequence(pContext, u16dutyCycle);
		}
		break;
	case 1:
		// Validate parameters
		if ((pContext->bPin2Enabled && u16dutyCycle > PWM_DUTY_CYCLE_MAX))
		{
			ERR_setErrorParam(&PWM_CYCLE_ERROR, __LINE__, pError,
					u8channel, u16dutyCycle);
			return (ERR_ERROR);  // Duty cycle 0..1000 are possible
		}

		// Set the new duty cycles as sequence
		if (pContext->bPin2Enabled)
		{
			pInstance->u16Sequence[1] = DutyCycle2Sequence(pContext, u16dutyCycle);
		}
		break;
	case 2:
		// Validate parameters
		if ((pContext->bPin3Enabled && u16dutyCycle > PWM_DUTY_CYCLE_MAX))
		{
			ERR_setErrorParam(&PWM_CYCLE_ERROR, __LINE__, pError,
					u8channel, u16dutyCycle);
			return (ERR_ERROR);  // Duty cycle 0..1000 are possible
		}

		// Set the new duty cycles as sequence
		if (pContext->bPin3Enabled)
		{
			pInstance->u16Sequence[2] = DutyCycle2Sequence(pContext, u16dutyCycle);
		}
		break;
	case 3:
		// Validate parameters
		if ((pContext->bPin4Enabled && u16dutyCycle > PWM_DUTY_CYCLE_MAX))
		{
			ERR_setErrorParam(&PWM_CYCLE_ERROR, __LINE__, pError,
					u8channel, u16dutyCycle);
			return (ERR_ERROR);  // Duty cycle 0..1000 are possible
		}

		// Set the new duty cycles as sequence
		if (pContext->bPin4Enabled)
		{
			pInstance->u16Sequence[3] = DutyCycle2Sequence(pContext, u16dutyCycle);
		}
		break;
	default:
		ERR_setErrorParam(&PWM_CYCLE_ERROR, __LINE__, pError,
				u8channel, u16dutyCycle);
		return (ERR_ERROR); // Unknown channel
	}

	// Re-trigger in case PWM is already running
	if (pInstance->PWM_State == PWM_Running)
	{
		PWM_start(pContext);
	}
	return (ERR_OK);

} // End of PWM_generateOne

/*-------------------------------------------------------------------------*/

uint32_t PWM_generateAll(PWM_Context *pContext,
                         uint16_t u16dutyCycle1, uint16_t u16dutyCycle2,
                         uint16_t u16dutyCycle3, uint16_t u16dutyCycle4,
                         ERR_pErrorInfo pError)
{
    Instance_t *pInstance = pContext->pInternal;

    // Validate parameters
    if ((pContext->bPin1Enabled && u16dutyCycle1 > PWM_DUTY_CYCLE_MAX)
    ||  (pContext->bPin2Enabled && u16dutyCycle2 > PWM_DUTY_CYCLE_MAX)
    ||  (pContext->bPin3Enabled && u16dutyCycle3 > PWM_DUTY_CYCLE_MAX)
    ||  (pContext->bPin4Enabled && u16dutyCycle4 > PWM_DUTY_CYCLE_MAX))
    {
        ERR_setErrorParam(&PWM_CYCLE_ERROR, __LINE__, pError,
                u16dutyCycle1, u16dutyCycle2);
        return (ERR_ERROR);  // Duty cycle 0..1000 are possible
    }

    // Set the new duty cycles as sequence
    if (pContext->bPin1Enabled)
    {
        pInstance->u16Sequence[0] = DutyCycle2Sequence(pContext, u16dutyCycle1);
    }
    if (pContext->bPin2Enabled)
    {
        pInstance->u16Sequence[1] = DutyCycle2Sequence(pContext, u16dutyCycle2);
    }
    if (pContext->bPin3Enabled)
    {
        pInstance->u16Sequence[2] = DutyCycle2Sequence(pContext, u16dutyCycle3);
    }
    if (pContext->bPin4Enabled)
    {
        pInstance->u16Sequence[3] = DutyCycle2Sequence(pContext, u16dutyCycle4);
    }
    // Re-trigger in case PWM is already running
    if (pInstance->PWM_State == PWM_Running)
    {
        PWM_start(pContext);
    }
    return (ERR_OK);

} // End of PWM_generateAll

/*-------------------------------------------------------------------------*/

void PWM_start(PWM_Context *pContext)
{
    Instance_t *pInstance = pContext->pInternal;

    pInstance->pPWMx->TASKS_SEQSTART[0] = 1;
    pInstance->PWM_State = PWM_Running;

} // End of PWM_stop

/*-------------------------------------------------------------------------*/

void PWM_stop(PWM_Context *pContext)
{
    Instance_t *pInstance = pContext->pInternal;

    pInstance->pPWMx->TASKS_STOP = 1;
    pInstance->PWM_State = PWM_Stopped;

} // End of PWM_stop

/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/

#if 0
#include "function_run.h"
// Precondition for the module test: No other module uses the PWM
// The pins enabled for another PWM module cannot be used twice.
static PWM_Context m_pwm =
{
    .u8Interface  = 1,
    .u32Frequency = 10000,
    .u8Pin1       = 22,
    .u8Pin2       = 23,
    .u8Pin3       = 0,
    .u8Pin4       = 0,
    .bPin1Enabled = true,
    .bPin2Enabled = true,
    .bPin3Enabled = false,
    .bPin4Enabled = false,
};
static uint16_t DutyCycle1;
static uint16_t DutyCycle2;

static void PwmUpdate(void)
{
    uint32_t Status = ERR_OK;

    DutyCycle1 += 50;
    if (DutyCycle1 > PWM_DUTY_CYCLE_MAX)
    {
        DutyCycle1 = 0;
    }
    DutyCycle2 += 13;
    if (DutyCycle2 > PWM_DUTY_CYCLE_MAX)
    {
        DutyCycle2 = 0;
    }
    Status = PWM_generate(&m_pwm, DutyCycle1, NULL);
    //Status = PWM_generateAll(&m_pwm, DutyCycle1, DutyCycle2, 0, 0, NULL);
    if (Status != ERR_OK)
    {
        TRACE_DBG_H("Stat-%d, PwmUpdate ERROR\n", Status);
    }
    FUNR_register(PwmUpdate, 100);
}

/*-------------------------------------------------------------------------*/

static void PwmStop(void)
{
    FUNR_unregister(PwmUpdate);
    PWM_stop(&m_pwm);
    PWM_deinit(&m_pwm);
}

/*-------------------------------------------------------------------------*/

void PWM_test(void)
{
    uint32_t Status = ERR_OK;

    Status = PWM_init(&m_pwm, 0, NULL);
    TRACE_DBG_H("Stat-%d, PWM_init\n", Status);

    DutyCycle1 = 1;
    DutyCycle2 = 999;
    Status = PWM_generateAll(&m_pwm, DutyCycle1, DutyCycle2, 0, 0, NULL);
    TRACE_DBG_H("Stat-%d, PWM_generate\n", Status);

    PWM_start(&m_pwm);

    FUNR_register(PwmUpdate, 100);
    FUNR_register(PwmStop, 5100);

} // End of PWM_test
#endif

/*-------------------------------------------------------------------------*/
