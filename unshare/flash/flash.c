/* ***************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: Flash driver
 *      State: Not formally tested
 * Originator: Iliev
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/flash/flash.c $
 *  $Revision: 25033 $
 *      $Date: 2018-06-13 12:13:39 +0200 (Wed, 13 Jun 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/**
 * @file flash.c
 * @brief Detailed description in header file
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "flash.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stdio.h>

/* headers from other modules ---------------------------------------------*/
#include "board.h"
#include "module_config.h"
#include "trace.h"
#include "softdevice_handler.h"
#include "nrf_nvmc.h"   // NRF Flash Driver
#include "fstorage.h"   // Softdevice Flash Driver


/**
 * Error definition: Source and/or Destination address not aligned
 */
const ERR_ErrorInfo FLASH_ALIGNERROR = {
        .moduleNumber = ERR_MOD_INTERNAL_FLASH,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 1,
        .lineNumber = 0,
        ERRORSTRING("Source and/or Destination address not aligned")
};

/**
 * Error definition: Source address not in range
 */
const ERR_ErrorInfo FLASH_SOURCEERROR = {
        .moduleNumber = ERR_MOD_INTERNAL_FLASH,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 2,
        .lineNumber = 0,
        ERRORSTRING("Source address not in range")
};

/**
 * Error definition: Destination address not in range
 */
const ERR_ErrorInfo FLASH_DESTINATIONERROR = {
        .moduleNumber = ERR_MOD_INTERNAL_FLASH,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 3,
        .lineNumber = 0,
        ERRORSTRING("Destination address not in range")
};

/**
 * Error definition: Destination address write not allowed
 */
const ERR_ErrorInfo FLASH_LOCKERROR = {
        .moduleNumber = ERR_MOD_INTERNAL_FLASH,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 4,
        .lineNumber = 0,
        ERRORSTRING("Destination address write not allowed")
};

/**
 * Error definition: Timeout or default fault
 */
const ERR_ErrorInfo FLASH_TIMEOUT = {
        .moduleNumber = ERR_MOD_INTERNAL_FLASH,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 5,
        .lineNumber = 0,
        ERRORSTRING("Operation timed out")
};

/**
 * Error definition: Wrong parameter
 */
const ERR_ErrorInfo FLASH_PARAMETERERROR = {
        .moduleNumber = ERR_MOD_INTERNAL_FLASH,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 6,
        .lineNumber = 0,
        ERRORSTRING("Wrong parameter")
};

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_FLASH
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_FLASH undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_FLASH
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/** "Return" values */
typedef enum tagFLASH_CmdStatus
{
    FLASH_CMD_OK,               ///< flash task sucessfully done
    FLASH_CMD_BAD_PAGEMASK,     ///< flash task aborted due to bad mask
    FLASH_CMD_UNLOCK_TIMEOUT,   ///< unlock flash task timed out
    FLASH_CMD_LOCK_TIMEOUT,     ///< lock flash task timed out
    FLASH_CMD_PROGRAM_TIMEOUT,  ///< program flash task timed out
    FLASH_CMD_SSB_TIMEOUT,      ///< ssb /GPNVM bit 0 flash task timed out
} FLASH_CmdStatus;

/*--------------------------------------------------------------------------+
 |  module variables                                                         |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  foreign module registrations                                             |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  local functions (helper functions)                                       |
+--------------------------------------------------------------------------*/

#if (CFG_FLASH_DISABLE_ERRATA_CHECK == CFG_NO)
/**
 * Check if memory map basically makes sense
 *
 * @param none
 */
static void errataIssuesCheck(ERR_pErrorInfo pError)
{
// disable the errata check in module_config.h if all issues are OK!
#warning "Errata issues check enabled!"

    // Check if memory area is multiple of pagesize and

    // Check if BRD_FLASH_SIZE is multiple of page size
    if ( BRD_FLASH_SIZE % BRD_FLASH_PAGESIZE != 0 )
    {
        TRACE_DBG_L("errataIssuesCheck: Board flash size not a multiple of page size!\r\n");
        ERR_setErrorParam(&FLASH_ALIGNERROR, __LINE__, pError, (uint32_t) BRD_FLASH_SIZE, (uint32_t) BRD_FLASH_PAGESIZE);
    }

    // Check if BRD_FLASH_PAGESIZE_DW is multiple of page size
    if ( BRD_FLASH_PAGESIZE % BRD_FLASH_PAGESIZE_DW != 0 )
    {
        TRACE_DBG_L("errataIssuesCheck: Page size not a multiple of double word size!\r\n");
        ERR_setErrorParam(&FLASH_ALIGNERROR, __LINE__, pError, (uint32_t) BRD_FLASH_PAGESIZE, (uint32_t) BRD_FLASH_PAGESIZE_DW);
    }

}// End of errataIssuesCheck
#endif // CFG_FLASH_DISABLE_ERRATA_CHECK

/*--------------------------------------------------------------------------*/

/**
 * @fn fstorage_Wait(void)
 *
 * @brief Waits until the SoftDevice has finished writing to flash.
 *
 * @return void
 */
static void fstorage_Wait(void)
{
    while (! fs_queue_is_empty())
    {
        APP_ERROR_CHECK (sd_app_evt_wait());
    }
} // End of fstorage_Wait
/*--------------------------------------------------------------------------*/

/**
 * @fn FLASH_CmdStatus FLASH_init (FLASH_Context *pFlash, ERR_pErrorInfo pError)
 *
 * @brief Initialize the flash module
 *
 * @pre                     1. Flash areas must be predefined during compile time
 *                             by using FS_REGISTER_AOT() macro before calling this function.
 *                          2. Context must be initialized
 * @post                    Call FLASH_close() after all write / erase / read operations are done
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
static FLASH_CmdStatus FLASH_open (FLASH_Context *pFlash, ERR_pErrorInfo pError)
{
    uint8_t u8sdEnabled;  // Status variable of softdevice
    fs_ret_t err_code; // Return variable of fstorage

#if (CFG_FLASH_DISABLE_ERRATA_CHECK == CFG_NO)
    // Perform memory sanity check. This should be disabled for production
    errataIssuesCheck(pError);
#endif

    // FLASH with NRF how to:
    // 1. Use FS_REGISTER_CFG to register flash region/s
    // 2. Check if Softdevice is active (FLASH_checkActiveSoftDevice()) and store status in m_SD_enabled
    // 3. Work with flash (NVMC if SD not enabled or fs_write() if SD enabled)

    // Check if softdevice is enabled and store status in ->sFstatus.bSDstatus
    sd_softdevice_is_enabled(&u8sdEnabled);

    if (u8sdEnabled && !pFlash->sFstatus.bSDstatus)
    {
        err_code = fs_init(); // Init SD flash handling
        if (err_code != FS_SUCCESS)
        {
            TRACE_DBG_M("fs_init failed %d\n", (int)err_code);
            return (FLASH_CMD_PROGRAM_TIMEOUT);
        }

        err_code = softdevice_sys_evt_handler_set(fs_sys_event_handler); // set the event handler
        if (err_code != NRF_SUCCESS)
        {
            TRACE_DBG_M("ERR softdevice_sys_evt_handler_set: %d\n", (int)err_code);
            return (FLASH_CMD_PROGRAM_TIMEOUT);
        }

        // Set SD status flag in context
        pFlash->sFstatus.bSDstatus = true;
    }

    return (FLASH_CMD_OK);

} // End of FLASH_open
/*-------------------------------------------------------------------------*/

/**
 * @fn FLASH_CmdStatus FLASH_readBytes (const void *pDest, const void *pSource,
 *                                      uint32_t u32Bytes, ERR_pErrorInfo pError)
 *
 * @brief It reads bytes stored in flash to a destination.
 *
 * @pre                     FLASH_open() must be called before calling this function.
 * @post                    Call FLASH_close() after all write / erase / read operations are done
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pDest             The area, where the data is programmed to.
 * @param pSource           The area, where the data is read from.
 * @param u32Bytes          Amount of bytes to read.
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
static FLASH_CmdStatus FLASH_readBytes (FLASH_Context *pFlash, const void *pDest, const void *pSource,
                                        uint32_t u32Bytes, ERR_pErrorInfo pError)
{
    uint32_t u32i = 0;
    uint8_t *pu8Dest;
    uint8_t *pu8Source;

    // Check: Read bytes should not exceed flash size
    if ( ((uint32_t)pSource < BRD_FLASH_BASE) || ( (uint32_t)pSource > (BRD_FLASH_BASE + BRD_FLASH_SIZE - u32Bytes - 1)) )
    {
        ERR_setErrorParam(&FLASH_DESTINATIONERROR, __LINE__, pError, (uint32_t) pSource, u32Bytes);
        return (FLASH_CMD_BAD_PAGEMASK);
    }

    TRACE_DBG_M("Read Flash from 0x%06X to 0x%06X\r\n", (uint32_t) pSource, (uint32_t) pSource + u32Bytes - 1);

    pu8Dest   = (uint8_t*)pDest;
    pu8Source = (uint8_t*)pSource;

    for (u32i = 0; u32i < u32Bytes; u32i++)
    {
        *pu8Dest++ = *pu8Source++;
    }

    // If we get here programming was successful
    return (FLASH_CMD_OK);
} // End of FLASH_Write_Words
/*-------------------------------------------------------------------------*/

/**
 * @fn FLASH_CmdStatus FLASH_writeWords (FLASH_Context *pFlash, const void *pDest, const void *pSource,
 *                                       uint32_t u32Words, ERR_pErrorInfo pError)
 *
 * @brief It writes bytes from a general data store into the flash.
 *
 * @pre                     1. FLASH_open() must be called before calling this function.
 *                          2. Erase has to be done manually before write!
 * @post                    Call FLASH_close() after all write / erase / read operations are done
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pDest             The area, where the data is programmed to. Must be word aligned.
 * @param pSource           The area, where the data is read from.
 * @param u32Words          Amount of words to write.
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
static FLASH_CmdStatus FLASH_writeWords (FLASH_Context *pFlash, const void *pDest, const void *pSource,
                                         uint32_t u32Words, ERR_pErrorInfo pError)
{
    fs_ret_t eRet;      // Return variable of fstorage
    uint8_t u8sdEnabled;  // Status variable of softdevice
    FLASH_CmdStatus eStatus; // Return variable of flash function

    // Check: Written words should not exceed flash size
    if ( ((uint32_t)pDest < BRD_FLASH_BASE) || ( (uint32_t)pDest > (BRD_FLASH_BASE + BRD_FLASH_SIZE - (u32Words << 2) - 1)) )
    {
        ERR_setErrorParam(&FLASH_DESTINATIONERROR, __LINE__, pError, (uint32_t) pDest, (uint32_t) (BRD_FLASH_BASE + BRD_FLASH_SIZE - (u32Words << 2) - 1));
        return (FLASH_CMD_BAD_PAGEMASK);
    }

    TRACE_DBG_M("Write Flash from 0x%06X to 0x%06X\r\n", (uint32_t) pDest, (uint32_t) pDest + (u32Words << 2) - 1);

    // Check if softdevice is enabled
    sd_softdevice_is_enabled(&u8sdEnabled);

    // We first check the "global" softdevice status to make sure we initialize fstorage before using it.
    // E.g. the softdevice was started after the module initialization.
    // The module context flag is used to keep track if fstorage was initialized before.
    // It's only necessary to initialize fstorage once for a registered flash area.
    // This is done in this way to keep the initialization overhead small (performance)

    // Depending on SD status use fstorage or NVMC
    if (u8sdEnabled)
    {
        // Check if context SD status is as well true (fs_init() was performed before
        // else initialize module. This makes the module a bit more user friendly.
        if (!pFlash->sFstatus.bSDstatus)
        {
            // Check if fstorage was initilized before, else initialize
            eStatus = FLASH_open(pFlash, pError);

            // Check if FLASH_open was successful
            if (eStatus != FLASH_CMD_OK)
            {
                TRACE_DBG_M("Call to FLASH_open() failed: %d\n", (int)eStatus);
                return (eStatus);
            }
        }

        // We need to know in what memory area we are going to write to (only if softdevice is active)
        // NVMC does not require this. Therefore we use the pointer to the fstorage context

        // Perform the write
        eRet = fs_store(pFlash->pFstorage, (uint32_t *) pDest, (uint32_t *) pSource, (uint16_t) u32Words, NULL);

        // Check if fs_store was successful
        if (eRet != FS_SUCCESS)
        {
            TRACE_DBG_M("Call to fs_write failed: %d\n", (int)eRet);
            return (FLASH_CMD_PROGRAM_TIMEOUT);
        }

        // Busy wait until job is done
        fstorage_Wait();
    }
    else
    {
        // Write words to flash
        nrf_nvmc_write_words ((uint32_t) pDest, (uint32_t *) pSource, u32Words);
    }

    // If we get here programming was successful
    return (FLASH_CMD_OK);
} // End of FLASH_Write_Words
/*-------------------------------------------------------------------------*/

/**
 * @fn FLASH_CmdStatus FLASH_erasePages (FLASH_Context *pFlash, const void *pDest, uint32_t u32Pages,
 *                                       ERR_pErrorInfo pError)
 *
 * @brief Erase "BRD_FLASH_PAGESIZE" large block
 *
 * @pre                     FLASH_open() must be called before calling this function.
 * @post                    Call FLASH_close() after all write / erase / read operations are done
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pDst              Start of area to erase. Must be page aligned.
 * @param u32Pages          Amount of pages to erase.
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
static FLASH_CmdStatus FLASH_erasePages (FLASH_Context *pFlash, const void *pDest, uint32_t u32Pages,
                                         ERR_pErrorInfo pError)
{
    uint32_t u32p = 0; // Loop variable for pages
    uint8_t u8sdEnabled;  // Status variable of softdevice
    fs_ret_t eRet;   // Variable FS error
    FLASH_CmdStatus eStatus; // Return variable of flash function

    // Check: Erased pages should not exceed flash size
    if ( ((uint32_t)pDest < BRD_FLASH_BASE) || ( (uint32_t)pDest > (BRD_FLASH_BASE + BRD_FLASH_SIZE - u32Pages * BRD_FLASH_PAGESIZE)) )
    {
        ERR_setErrorParam(&FLASH_DESTINATIONERROR, __LINE__, pError, (uint32_t) pDest, u32Pages * BRD_FLASH_PAGESIZE);
        return (FLASH_CMD_BAD_PAGEMASK);
    }

    TRACE_DBG_M("Erase Flash from 0x%06X to 0x%06X\r\n",
                (uint32_t) pDest,
                (uint32_t) pDest + u32Pages * BRD_FLASH_PAGESIZE - 1);

    // Get the actual Softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);

    // We first check the "global" softdevice status to make sure we initialize fstorage before using it.
    // E.g. the softdevice was started after the module initialization.
    // The module context flag is used to keep track if fstorage was initialized before.
    // It's only necessary to initialize fstorage once for a registered flash area.
    // This is done in this way to keep the initialization overhead small (performance)

    // Depending on SD status use fstorage or NVMC
    if (u8sdEnabled)
    {
        // Check if context SD status is as well true (fs_init() was performed before
        // else initialize module. This makes the module a bit more user friendly.
        if (!pFlash->sFstatus.bSDstatus)
        {
            // Check if fstorage was initilized before, else initialize
            eStatus = FLASH_open(pFlash, pError);

            // Check if FLASH_open was successful
            if (eStatus != FLASH_CMD_OK)
            {
                TRACE_DBG_M("Call to FLASH_open() failed: %d\n", (int)eStatus);
                return (eStatus);
            }
        }

        // Use the soft device flash driver to erase the flash
        eRet = fs_erase(pFlash->pFstorage, (uint32_t *) pDest, (uint16_t) u32Pages, NULL);

        // Check if fs_erase was successful
        if (eRet != FS_SUCCESS)
        {
            TRACE_DBG_M("Call to fs_erase failed: %d\n", (int)eRet);
            return (FLASH_CMD_PROGRAM_TIMEOUT);
        }

        // Busy wait until page is erased
        fstorage_Wait();
    }
    else
    {
        // Loop trough pages
        for (u32p = 0; u32p < u32Pages; u32p++)
        {
            // Use the NVMC flash driver to erase the flash}
            nrf_nvmc_page_erase((uint32_t) pDest + (u32p * BRD_FLASH_PAGESIZE));
        }
    }

    // If we get here programming was successful
    return (FLASH_CMD_OK);

} // End of FLASH_erasePages
/*-------------------------------------------------------------------------*/

/**
 * @fn FLASH_CmdStatus FLASH_close (FLASH_Context *pFlash, ERR_pErrorInfo pError)
 *
 * @brief Deinitialize the flash module
 *
 * @pre                     FLASH_open() must be called before calling this function.
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
static FLASH_CmdStatus FLASH_close(FLASH_Context *pFlash, ERR_pErrorInfo pError)
{
    // close flash driver
    return (FLASH_CMD_OK);

} // End of FLASH_close
/*-------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

// Initialize the flash module
ERR_StatusType FLASH_init (FLASH_Context *pFlash, ERR_pErrorInfo pError)
{
    FLASH_CmdStatus eStatus = FLASH_CMD_OK;    ///< status of program/unlock command

    TRACE_DBG_L("FLASH_init: Enter.\r\n");

    // Perform basic sanity checks

    // Make sure user set a plausible address range
    // End address should be higher Start address
    if ( pFlash->u32EndAddr < pFlash->u32StartAddr )
    {
        ERR_setErrorParam(&FLASH_PARAMETERERROR, __LINE__, pError, pFlash->u32StartAddr, pFlash->u32EndAddr);
        TRACE_DBG_M("FLASH_init: Defined flash area not valid\r\n");
        return ERR_ERROR;
    }

    // Reset SD status flag in context. It will be set according during init
    pFlash->sFstatus.bSDstatus = false;

    // Initialize the flash interface
    eStatus = FLASH_open(pFlash, pError);

    // check result
    switch (eStatus)
    {
        case FLASH_CMD_OK:
            break;

        case FLASH_CMD_BAD_PAGEMASK:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x00, (uint32_t) FLASH_CMD_BAD_PAGEMASK);
            return ERR_ERROR;

        case FLASH_CMD_PROGRAM_TIMEOUT:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x00, (uint32_t) FLASH_CMD_PROGRAM_TIMEOUT);
            return ERR_ERROR;

        default:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x00, (uint32_t) ERR_ERROR);
            return ERR_ERROR;
//          break;    // KEIL complains
    }

    // Flash was initialized, set status flag in context
    pFlash->sFstatus.bFinit = true;

    TRACE_DBG_L("FLASH_init: Exit.\r\n");
    return ERR_OK;
} // End of FLASH_init
/*-------------------------------------------------------------------------*/

// Write data into the flash
ERR_StatusType FLASH_write (FLASH_Context *pFlash, const void *pDest, const void *pSource,
                            uint32_t u32Len, ERR_pErrorInfo pError)
{
    FLASH_CmdStatus eStatus = FLASH_CMD_OK;    ///< status of program/unlock command

    TRACE_DBG_L("FLASH_write: Enter.\r\n");

    // Verify u32Len argument is valid (>0) and doesn't exceed defined address range
    if ((u32Len < 1) || (u32Len > pFlash->u32EndAddr - pFlash->u32StartAddr + 1))
    {
        ERR_setErrorParam(&FLASH_ALIGNERROR, __LINE__, pError, (uint32_t) pDest, u32Len);
        TRACE_DBG_M("FLASH_write: Data length not valid\r\n");
        return ERR_ERROR;
    }

    // Verify Flash module was initialized
    if (!pFlash->sFstatus.bFinit)
    {
        ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x02, (uint32_t) pFlash->sFstatus.bFinit);
        TRACE_DBG_M("FLASH_write: Flash module isn't initialized\r\n");
        return ERR_ERROR;
    }

    // Perform basic sanity checks

    // Make sure we are within memory area as set in context and written bytes don't exceed this area
    // Dest. address should be higher or equal Start addr.
    // Dest. address should be lower or equal End addr. minus length of bytes
    if ( ((uint32_t)pDest < pFlash->u32StartAddr) || ((uint32_t)pDest > (pFlash->u32EndAddr - u32Len + 1)) ||
         (pFlash->u32EndAddr < (pFlash->u32StartAddr + u32Len - 1)) )
    {
        ERR_setErrorParam(&FLASH_PARAMETERERROR, __LINE__, pError, u32Len, (uint32_t) pDest);
        TRACE_DBG_M("FLASH_write: Outside defined flash area\r\n");
        return ERR_ERROR;
    }

    // We can only write a multiple of a word. Make sure this is true
    if ( ((u32Len & 0x00000003) != 0x00000000) || (((uint32_t)pDest & 0x00000003) != 0x00000000) )
    {
        ERR_setErrorParam(&FLASH_PARAMETERERROR, __LINE__, pError, u32Len, (uint32_t) pDest);
        TRACE_DBG_M("FLASH_write: Not a multiple of a word or not word aligned\r\n");
        return ERR_ERROR;
    }

    // Perform write action
    eStatus = FLASH_writeWords (pFlash, pDest, pSource, (uint32_t) (u32Len >> 2), pError);

    // check result
    switch (eStatus)
    {
        case FLASH_CMD_OK:
            break;

        case FLASH_CMD_BAD_PAGEMASK:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pDest, (uint32_t) FLASH_CMD_BAD_PAGEMASK);
            return ERR_ERROR;

        case FLASH_CMD_PROGRAM_TIMEOUT:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pDest, (uint32_t) FLASH_CMD_PROGRAM_TIMEOUT);
            return ERR_ERROR;

        default:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pDest, (uint32_t) ERR_ERROR);
            return ERR_ERROR;
//          break;    // KEIL complains
    }

    TRACE_DBG_L("FLASH_write: Exit.\r\n");
    return ERR_OK;

} // End of FLASH_write
/*-------------------------------------------------------------------------*/

// Read data from flash
ERR_StatusType FLASH_read (FLASH_Context *pFlash, const void *pDest, const void *pSource,
                           uint32_t u32Len, ERR_pErrorInfo pError)
{
    FLASH_CmdStatus eStatus = FLASH_CMD_OK;    ///< status of program/unlock command

    TRACE_DBG_L("FLASH_read: Enter.\r\n");

    // Verify u32Len argument is valid (>0) and doesn't exceed defined address range
    if ((u32Len < 1) || (u32Len > pFlash->u32EndAddr - pFlash->u32StartAddr + 1))
    {
        ERR_setErrorParam(&FLASH_ALIGNERROR, __LINE__, pError, (uint32_t) pSource, u32Len);
        TRACE_DBG_M("FLASH_read: data length can't be zero\r\n");
        return ERR_ERROR;
    }

    // Verify Flash module was initialized
    if (!pFlash->sFstatus.bFinit)
    {
        ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x03, (uint32_t) pFlash->sFstatus.bFinit);
        TRACE_DBG_M("FLASH_read: Flash module isn't initialized\r\n");
        return ERR_ERROR;
    }

    // Perform basic sanity checks

    // Make sure we are within memory area as set in context and written bytes don't exceed this area
    // Dest. address should be higher or equal Start addr.
    // Dest. address should be lower or equal End addr. minus length of bytes
    if ( ((uint32_t)pSource < pFlash->u32StartAddr) || ((uint32_t)pSource > (pFlash->u32EndAddr - u32Len + 1)) ||
         (pFlash->u32EndAddr < (pFlash->u32StartAddr + u32Len - 1)) )
    {
        ERR_setErrorParam(&FLASH_PARAMETERERROR, __LINE__, pError, u32Len, (uint32_t) pSource);
        TRACE_DBG_M("FLASH_read: Outside defined flash area\r\n");
        return ERR_ERROR;
    }

    // Perform reading
    eStatus = FLASH_readBytes (pFlash, pDest, pSource, u32Len, pError);

    // check result
    switch (eStatus)
    {
        case FLASH_CMD_OK:
            break;

        case FLASH_CMD_BAD_PAGEMASK:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pSource, (uint32_t) FLASH_CMD_BAD_PAGEMASK);
            return ERR_ERROR;

        case FLASH_CMD_PROGRAM_TIMEOUT:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pSource, (uint32_t) FLASH_CMD_PROGRAM_TIMEOUT);
            return ERR_ERROR;

        default:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pSource, (uint32_t) ERR_ERROR);
            return ERR_ERROR;
//          break;    // KEIL complains
    }

    TRACE_DBG_L("FLASH_read: Exit.\r\n");
    return ERR_OK;

} // End of FLASH_write
/*-------------------------------------------------------------------------*/

// Erase flash sectors
ERR_StatusType FLASH_erase (FLASH_Context *pFlash, const void *pDest,
                            uint32_t u32Len, ERR_pErrorInfo pError)
{
    FLASH_CmdStatus eStatus = FLASH_CMD_OK;    ///< status of program/unlock command

    TRACE_DBG_L("FLASH_erase: Enter.\r\n");

    // Verify u32Len argument is valid (>0) and doesn't exceed defined address range
    if ((u32Len < 1) || (u32Len > pFlash->u32EndAddr - pFlash->u32StartAddr + 1))
    {
        ERR_setErrorParam(&FLASH_ALIGNERROR, __LINE__, pError, (uint32_t) pDest, u32Len);
        TRACE_DBG_M("FLASH_erase: data length can't be zero\r\n");
        return ERR_ERROR;
    }

    // Verify Flash module was initialized
    if (!pFlash->sFstatus.bFinit)
    {
        ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x04, (uint32_t) pFlash->sFstatus.bFinit);
        TRACE_DBG_M("FLASH_erase: Flash module isn't initialized\r\n");
        return ERR_ERROR;
    }

    // Perform basic sanity checks

    // Make sure we are within memory area as set in context and written bytes don't exceed this area
    // Dest. address should be higher or equal Start addr.
    // Dest. address should be lower or equal End addr. minus length of bytes
    //                                                   0x74000
    if ( ((uint32_t)pDest < pFlash->u32StartAddr) || ((uint32_t)pDest > (pFlash->u32EndAddr - u32Len + 1)) ||
         (pFlash->u32EndAddr < (pFlash->u32StartAddr + u32Len - 1)) )
    {
        ERR_setErrorParam(&FLASH_PARAMETERERROR, __LINE__, pError, u32Len, (uint32_t) pDest);
        TRACE_DBG_M("FLASH_erase: Outside defined flash area\r\n");
        return ERR_ERROR;
    }

    // Make sure data length is a multiple of a page and start address is page aligned
    if ((u32Len % BRD_FLASH_PAGESIZE != 0) || ((uint32_t)pDest  % BRD_FLASH_PAGESIZE != 0))
    {
        ERR_setErrorParam(&FLASH_PARAMETERERROR, __LINE__, pError, u32Len, (uint32_t) pFlash->sFstatus.bFinit);
        TRACE_DBG_M("FLASH_erase: Not a multiple of a page or not page aligned\r\n");
        return ERR_ERROR;
    }

    // Perform erase action
    eStatus = FLASH_erasePages(pFlash, pDest, (uint32_t)(u32Len / BRD_FLASH_PAGESIZE), pError); // Erase flash pages

    // check result
    switch (eStatus)
    {
        case FLASH_CMD_OK:
            break;

        case FLASH_CMD_BAD_PAGEMASK:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pDest, (uint32_t) FLASH_CMD_BAD_PAGEMASK);
            return ERR_ERROR;

        case FLASH_CMD_PROGRAM_TIMEOUT:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pDest, (uint32_t) FLASH_CMD_PROGRAM_TIMEOUT);
            return ERR_ERROR;

        default:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) pDest, (uint32_t) ERR_ERROR);
            return ERR_ERROR;
//          break;    // KEIL complains
    }

    TRACE_DBG_L("FLASH_erase: Exit.\r\n");
    return ERR_OK;

} // End of FLASH_write
/*-------------------------------------------------------------------------*/

// Terminate the flash module
ERR_StatusType FLASH_terminate (FLASH_Context *pFlash, ERR_pErrorInfo pError)
{
    FLASH_CmdStatus eStatus = FLASH_CMD_OK;    ///< status of program/unlock command

    TRACE_DBG_L("FLASH_terminate: Enter.\r\n");

    // Flash was deinitialized, set status to context
    pFlash->sFstatus.bFinit = false;

    // Deinitialize the flash interface
    eStatus = FLASH_close(pFlash, pError);

    // check result
    switch (eStatus)
    {
        case FLASH_CMD_OK:
            break;

        case FLASH_CMD_BAD_PAGEMASK:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x00, (uint32_t) FLASH_CMD_BAD_PAGEMASK);
            return ERR_ERROR;

        case FLASH_CMD_PROGRAM_TIMEOUT:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x00, (uint32_t) FLASH_CMD_PROGRAM_TIMEOUT);
            return ERR_ERROR;

        default:
            ERR_setErrorParam(&FLASH_TIMEOUT, __LINE__, pError, (uint32_t) 0x00, (uint32_t) ERR_ERROR);
            return ERR_ERROR;
//          break;    // KEIL complains
    }

    TRACE_DBG_L("FLASH_terminate: Exit.\r\n");

    return ERR_OK;
} // End of FLASH_terminate
/*-------------------------------------------------------------------------*/

// Callback on flash job done. Used by NRF fstorage
void fstorage_EventHandler(fs_evt_t const * const Event, fs_ret_t Result)
{
    if (Result != FS_SUCCESS)
    {
        TRACE_DBG_M("ERROR fstorage: %d\n", (int)Result);
    }
} // End of fstorage_EventHandler
/*-------------------------------------------------------------------------*/

// Flash module test preprocessor switch
#if (CFG_FLASH_HAL_TEST == CFG_YES)
// Controlled from module_config.h

#warning "Flash module test enabled!"

// Configure which tests to run by enabling this defines
#define FLASH_HAL_TEST_SANITY1 CFG_NO // Sanity check and error handling (Test should fail)
#define FLASH_HAL_TEST_SANITY2 CFG_NO // Write, Erase check for multiple of word / page (Test should fail)
#define FLASH_HAL_TEST_EWRER_SD_ADDR0 CFG_NO // Erase, Write, Read, Erase, Read at the very beginning of address range (SD on)
#define FLASH_HAL_TEST_EWRER_SD_ADDRFF CFG_YES // Erase, Write, Read, Erase, Read at the very end of address range (SD on)
#define FLASH_HAL_TEST_EWRER_nSD_ADDRFF CFG_YES // Erase, Write, Read, Erase, Read at the very end of address range (SD off)
#define FLASH_HAL_TEST_EWRER_nSD_ADDRFF_G CFG_NO // Erase, Write, Read, Erase, Read at the very end of address range with context from board.c (SD off)

/**
 * @fn FLASH_test(void)
 *
 * @brief Routine to test the implemented flash functions and boundaries
 *
 * @pre 1. Softdevice must be activated before calling this function
 *      2. Flash areas must be predefined during compile time
 *         by using FS_REGISTER_AOT() macro before calling this function.
 */
void FLASH_test(void)
{
    ERR_StatusType eStatus; // Status holder variable
    uint8_t u8sdEnabled;  // status variable for softdevice
    // uint32_t err_code; // Status holder variable fstorage

    FLASH_Context sContext; // Flash context variable

    /*
     * E.g. in board.h
     * FS_REGISTER_AOT(User_AppUpload, BRD_FLASH_APP_UPDATE_SOURCE_SIZE,
     *                 BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
     *                 BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE, 0xFE);
     */
    // Set the pointer to the fstorage configuration as initialized during compile time in board.h
    sContext.pFstorage = &User_AppUpload;

    uint32_t aTest[4] = { 0x01010101, 0x02020202, 0x03030303, 0x04040404 }; // Test data source
    uint32_t bTest[4] = {0}; // Temporary data storage

    // ** START TEST **

    /*
     * Standard test to write data at the very end of a page
     */
    void FLASH_test_EWRERe (FLASH_Context *pFlash)
    {
        // Check status of softdevice
        sd_softdevice_is_enabled(&u8sdEnabled);

        //Test FLASH_init()...should not fail
        eStatus = FLASH_init(pFlash, NULL);

        // Print Softdevice status
        TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, pFlash->sFstatus.bSDstatus);

        if (eStatus != ERR_OK)
        {
            TRACE_DBG_L("Flash_Test: FLASH_init(): NOK: 0x%X\r\n", eStatus);
        }
        else
        {
            TRACE_DBG_L("Flash_Test: FLASH_init(): OK\r\n");
        }

        // Flash_Test: Test FLASH_erase()
        eStatus = FLASH_erase(pFlash, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - BRD_FLASH_PAGESIZE),
                              BRD_FLASH_PAGESIZE, NULL);

        if (eStatus != ERR_OK)
        {
            TRACE_DBG_L("Flash_Test: FLASH_erase() NOK: 0x%X\r\n", eStatus);
        }
        else
        {
            TRACE_DBG_L("Flash_Test: FLASH_erase(): OK\r\n");
        }

        // Flash_Test: Test FLASH_write()
        eStatus = FLASH_write(pFlash, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - (uint32_t) sizeof(aTest)),
                              &aTest, (uint32_t) sizeof(aTest), NULL);

        if (eStatus != ERR_OK)
        {
            TRACE_DBG_L("Flash_Test: FLASH_write(): NOK: 0x%X\r\n", eStatus);
        }
        else
        {
            TRACE_DBG_L("Flash_Test: FLASH_write(): OK\r\n");
        }

        // Flash_Test: Test FLASH_read()
        eStatus = FLASH_read(pFlash, &bTest, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - (uint32_t) sizeof(aTest)),
                             (uint32_t) sizeof(aTest), NULL);

        if (eStatus != ERR_OK)
        {
            TRACE_DBG_L("Flash_Test: FLASH_read(): NOK: 0x%X\r\n", eStatus);
        }
        else
        {
            TRACE_DBG_L("Flash_Test: FLASH_read(): OK\r\n");
        }

        TRACE_DBG_L("Data read: 0x%06X, 0x%06X, 0x%06X, 0x%06X\r\n", bTest[0], bTest[1], bTest[2], bTest[3]);

        // Flash_Test: Test FLASH_erase()
        eStatus = FLASH_erase(pFlash, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - BRD_FLASH_PAGESIZE),
                              BRD_FLASH_PAGESIZE, NULL);

        if (eStatus != ERR_OK)
        {
            TRACE_DBG_L("Flash_Test: FLASH_erase() NOK: 0x%X\r\n", eStatus);
        }
        else
        {
            TRACE_DBG_L("Flash_Test: FLASH_erase(): OK\r\n");
        }

        // Flash_Test: Test FLASH_read()
        eStatus = FLASH_read(pFlash, &bTest, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - (uint32_t) sizeof(aTest)),
                             (uint32_t) sizeof(aTest), NULL);

        if (eStatus != ERR_OK)
        {
            TRACE_DBG_L("Flash_Test: FLASH_read(): NOK: 0x%X\r\n", eStatus);
        }
        else
        {
            TRACE_DBG_L("Flash_Test: FLASH_read(): OK\r\n");
        }

        TRACE_DBG_L("Data read: 0x%06X, 0x%06X, 0x%06X, 0x%06X\r\n", bTest[0], bTest[1], bTest[2], bTest[3]);

        // Check status of softdevice
        sd_softdevice_is_enabled(&u8sdEnabled);
        // Print Softdevice status
        TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, pFlash->sFstatus.bSDstatus);

    } // End FLASH_test_EWRERe

#if (FLASH_HAL_TEST_SANITY1 == CFG_YES)
    /*
     * Sanity Check and error handling
     * @pre This test requires to have a local context with empty address range
     */

    // Check softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);

    // Test FLASH_init()
    eStatus = FLASH_init(&sContext, NULL);

    // Print Softdevice status
    TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, sContext.sFstatus.bSDstatus);

    // ** Missing Start and End Address in context  ** //
    TRACE_DBG_L("Start and End Address not set in context...\r\n");
    TRACE_DBG_L("Flash_Test area: 0x%06X to 0x%06X\r\n", sContext.u32StartAddr, sContext.u32EndAddr);


    // This should not fail, no sanity check implemented
    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_init(): OK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_init(): NOK\r\n");
    }

    // Test FLASH_erase()...should fail
    eStatus = FLASH_erase(&sContext, (uint32_t *)BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS, BRD_FLASH_PAGESIZE, NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase() OK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase(): NOK\r\n");
    }

    // Test FLASH_write()...should fail
    eStatus = FLASH_write(&sContext, (uint32_t *) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
                          &aTest, (uint32_t) sizeof(aTest), NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_write(): OK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_write(): NOK\r\n");
    }

    //Test FLASH_read()...should fail
    eStatus = FLASH_read(&sContext, (uint32_t *) &bTest, (uint32_t *) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
                         (uint32_t) sizeof(aTest), NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_read(): OK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_read(): NOK\r\n");
    }

    // Check softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);
    // Print Softdevice status
    TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, sContext.sFstatus.bSDstatus);
#endif

#if (FLASH_HAL_TEST_SANITY2 == CFG_YES)
    /*
     * Write, Erase check for multiple of word / page
     */

    // Set the memory area where we will perform the tests
    sContext.u32StartAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS;
    sContext.u32EndAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 1;
    TRACE_DBG_L("Flash_Test set area: 0x%06X to 0x%06X\r\n", sContext.u32StartAddr, sContext.u32EndAddr);

    // Check softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);

    //Test FLASH_init()...should not fail
    eStatus = FLASH_init(&sContext, NULL);

    // Print Softdevice status
    TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, sContext.sFstatus.bSDstatus);
    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_init(): NOK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_init(): OK\r\n");
    }

    // Flash_Test: Test FLASH_erase() not page aligned
    eStatus = FLASH_erase(&sContext, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + 1),
                          BRD_FLASH_PAGESIZE, NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase() OK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase(): NOK\r\n");
    }

    // Flash_Test: Test FLASH_erase() write size page + 1
    eStatus = FLASH_erase(&sContext, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS),
                          BRD_FLASH_PAGESIZE + 1, NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase() OK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase(): NOK\r\n");
    }

    // Flash_Test: Test FLASH_write() destination not word aligned
    eStatus = FLASH_write(&sContext, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + 1),
                          (uint32_t *) &aTest, (uint32_t) sizeof(aTest), NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_write(): OK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_write(): NOK\r\n");
    }

    // Flash_Test: Test FLASH_write() size not multiple of a word
    eStatus = FLASH_write(&sContext, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS),
                          (uint32_t *) &aTest, (uint32_t) sizeof(aTest) + 1, NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_write(): OK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_write(): NOK\r\n");
    }

    // Check softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);
    // Print Softdevice status
    TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, sContext.sFstatus.bSDstatus);
#endif

#if (FLASH_HAL_TEST_EWRER_SD_ADDR0 == CFG_YES)
    /*
     * Erase, Write, Read, Erase, Read beginning of address range
     */

    // Set the memory area where we will perform the tests
    sContext.u32StartAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS;
    sContext.u32EndAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 1;
    TRACE_DBG_L("Flash_Test set area: 0x%06X to 0x%06X\r\n", sContext.u32StartAddr, sContext.u32EndAddr);

    // Check softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);

    //Test FLASH_Init()...should not fail
    eStatus = FLASH_init(&sContext, NULL);

    // Print Softdevice status
    TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, sContext.sFstatus.bSDstatus);
    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_init(): NOK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_init(): OK\r\n");
    }

    // Flash_Test: Test FLASH_erase()
    eStatus = FLASH_erase(&sContext, (uint32_t *)BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS, BRD_FLASH_PAGESIZE, NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase() NOK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase(): OK\r\n");
    }

    // Flash_Test: Test FLASH_write()
    eStatus = FLASH_write(&sContext, (uint32_t *) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
                          &aTest, (uint32_t) sizeof(aTest), NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_write(): NOK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_write(): OK\r\n");
    }

    // Flash_Test: Test FLASH_read()
    eStatus = FLASH_read(&sContext, &bTest, (uint32_t *) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
                         (uint32_t) sizeof(aTest), NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_read(): NOK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_read(): OK\r\n");
    }

    TRACE_DBG_L("Data read: 0x%06X, 0x%06X, 0x%06X, 0x%06X\r\n", bTest[0], bTest[1], bTest[2], bTest[3]);

    // Flash_Test: Test FLASH_erase()
    eStatus = FLASH_erase(&sContext, (uint32_t *)BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS, BRD_FLASH_PAGESIZE, NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase() NOK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_erase(): OK\r\n");
    }

    // Flash_Test: Test FLASH_read()
    eStatus = FLASH_read(&sContext, (uint32_t *) &bTest, (uint32_t *) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
                         (uint32_t) sizeof(aTest), NULL);

    if (eStatus != ERR_OK)
    {
        TRACE_DBG_L("Flash_Test: FLASH_read(): NOK: 0x%X\r\n", eStatus);
    }
    else
    {
        TRACE_DBG_L("Flash_Test: FLASH_read(): OK\r\n");
    }

    TRACE_DBG_L("Data read: 0x%06X, 0x%06X, 0x%06X, 0x%06X\r\n", bTest[0], bTest[1], bTest[2], bTest[3]);

    // Check softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);
    // Print Softdevice status
    TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, sContext.sFstatus.bSDstatus);
#endif

#if (FLASH_HAL_TEST_EWRER_SD_ADDRFF == CFG_YES)
    /*
     * Erase, Write, Read, Erase, Read at the end of address range
     */

    // Set the memory area where we will perform the tests
    sContext.u32StartAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS;
    sContext.u32EndAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 1;
    TRACE_DBG_L("Flash_Test set area: 0x%06X to 0x%06X\r\n", sContext.u32StartAddr, sContext.u32EndAddr);

    // Perform standard test at very end of a page
    FLASH_test_EWRERe(&sContext);
#endif

#if (FLASH_HAL_TEST_EWRER_nSD_ADDRFF == CFG_YES)

    /*
     * Erase, Write, Read, Erase, Read at the end of address range (SD off)
     */

    // Check softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);
    // Print Softdevice status
    TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, sContext.sFstatus.bSDstatus);

    // *** Disable softdevice ***
    sd_softdevice_disable(); // retval ::NRF_SUCCESS
    TRACE_DBG_L("Disable SD...\r\n");

    // Set the memory area where we will perform the tests
    sContext.u32StartAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS;
    sContext.u32EndAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 1;
    TRACE_DBG_L("Flash_Test set area: 0x%06X to 0x%06X\r\n", sContext.u32StartAddr, sContext.u32EndAddr);

    // Perform standard test at very end of a page
    FLASH_test_EWRERe(&sContext);
#endif

#if (FLASH_HAL_TEST_EWRER_nSD_ADDRFF_G == CFG_YES)
    /*
     * Erase, Write, Read, Erase, Read at the end of address range with context from board.c (SD off)
     */

    extern FLASH_Context BRD_FLASH_APPUPLOAD; // defined in board.c

    // Check softdevice status
    sd_softdevice_is_enabled(&u8sdEnabled);
    // Print Softdevice status
    TRACE_DBG_L("SD status G: %d ,M: %d\r\n", u8sdEnabled, sContext.sFstatus.bSDstatus);

    // *** Disable softdevice ***
    sd_softdevice_disable(); // retval ::NRF_SUCCESS
    TRACE_DBG_L("Disable SD...\r\n");

    // Perform standard test at very end of a page
    FLASH_test_EWRERe(&BRD_FLASH_APPUPLOAD);
#endif

    FLASH_terminate(&sContext, NULL);
} // End of FLASH_Test
/*-------------------------------------------------------------------------*/
#endif
