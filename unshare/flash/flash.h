/* ***************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: Flash driver
 *      State: Not formally tested
 * Originator: Iliev
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/flash/flash.h $
 *  $Revision: 23665 $
 *      $Date: 2018-03-19 17:55:16 +0100 (Mon, 19 Mar 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */
/**
 * @file flash.h
 *
 * @brief This module contains functions to control the Flash module.
 *
 * @pre Define flash area within board.h
 *      Create context within board.c
 *
 * Details on how to use this module:
 *
 * Within board.h basic flash properties must be defined and required user flash area:
 *
 * @code
 * #define BRD_FLASH_BASE          0x00000000
 * #define BRD_FLASH_SIZE          0x00080000 // 512 kByte
 * #define BRD_FLASH_PAGESIZE      0x1000 // 4096 Byte
 * #define BRD_FLASH_PAGESIZE_DW  (BRD_FLASH_PAGESIZE / 4)
 *
 *
 * #define BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS   0x0004A000
 * #define BRD_FLASH_APP_UPDATE_SOURCE_SIZE      0x0002B000
 * @endcode
 *
 * Within board.h register user area (name: User_AppUpload) with macro
 *
 * @code
 * FS_REGISTER_AOT(User_AppUpload, BRD_FLASH_APP_UPDATE_SOURCE_SIZE,
 *                 BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
 *                 BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE, 0xFE);
 * @endcode
 *
 * Within board.c define context:
 *
 * @code
 * FLASH_Context BRD_FLASH_APPUPLOAD =
 * {
 *     .u32StartAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
 *     .u32EndAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 1,
 *     .pFstorage = &User_AppUpload,
 * };
 * @endcode
 *
 * Within module_config.h one can enable errata checks and module tests
 * (Module tests are furthermore controller within flash.c, where one can enable/disable specific tests)
 *
 * @code
 * #define CFG_FLASH_DISABLE_ERRATA_CHECK      CFG_YES  ///<configuration is known good
 * #define CFG_FLASH_HAL_TEST                  CFG_YES  ///<enable flash HAL test code
 * @endcode
 *
 * Within your application file:
 *
 * @code
 * #include "board.h"
 *
 * extern FLASH_Context BRD_FLASH_APPUPLOAD; // defined in board.c
 * @endcode
 *
 * Within your function:
 *
 * @code
 * ERR_StatusType eStatus; // Status holder variable for return
 * @endcode
 *
 * (ADVANCED Start) Possibility of using a local context:
 * @code
 * FLASH_Context sContext; // Flash context variable
 * sContext.pFstorage = &User_AppUpload; // Set the pointer to the fstorage configuration as initialized during compile time
 * sContext.u32StartAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS;
 * sContext.u32EndAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 1;
 * @endcode
 * (ADVANCED End)
 *
 * To round out our example:
 * @code
 * uint32_t aTest[4] = { 0x01010101, 0x02020202, 0x03030303, 0x04040404 }; // Test data source
 * uint32_t bTest[4] = {0}; // Temporary data storage
 * @endcode
 *
 * Open the flash area to work with:
 * @code
 * eStatus = FLASH_open(&BRD_FLASH_APPUPLOAD, NULL);
 * if (eStatus != ERR_OK)
 * {
 *     // Do something with the error
 * }
 * @endcode
 *
 * Erase a flash page at the end of defined flash area:
 * @code
 * eStatus = FLASH_erase(&BRD_FLASH_APPUPLOAD, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - BRD_FLASH_PAGESIZE),
 *                       BRD_FLASH_PAGESIZE, NULL);
 * if (eStatus != ERR_OK)
 * {
 *     // Do something with the error
 * }
 * @endcode
 *
 * Write some test data at the end of defined flash area:
 * @code
 * eStatus = FLASH_write(&BRD_FLASH_APPUPLOAD, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 0x10),
 *                       &aTest, 0x10, NULL);
 * if (eStatus != ERR_OK)
 * {
 *     // Do something with the error
 * }
 * @endcode
 *
 * Read data we just wrote:
 * @code
 * eStatus = FLASH_read(&BRD_FLASH_APPUPLOAD, &bTest, (uint32_t *) (BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 0x10),
 *                      0x10, NULL);
 * if (eStatus != ERR_OK)
 * {
 *     // Do something with the error
 * }
 * @endcode
 *
 * -> Do something with the data in "bTest"
 * -> Perform further actions with the flash within allocated memory area
 *
 * Terminate flash after all operations are performed:
 * @code
 * FLASH_terminate(&BRD_FLASH_APPUPLOAD, NULL);
 * if (eStatus != ERR_OK)
 * {
 *     // Do something with the error
 * }
 * @endcode
 *
 ****************************************************************************
 */

#ifndef FLASH_H_
#define FLASH_H_

/* Includes needed by this header -----------------------------------------*/
#include <stdint.h>
#include "error.h"
#include "softdevice_handler.h"
#include "nrf_nvmc.h"   // NRF Flash Driver
#include "fstorage.h"   // Softdevice Flash Driver

#ifdef __cplusplus
extern "C"
{
#endif

/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/

 /**
  * @struct tagFLASH_Status
  *
  * @brief Flash status flags
  */
typedef struct tagFLASH_Status
{
    bool bFinit;        // Flash module initialized
    bool bSDstatus;     // Status of SD
} FLASH_Status;

/**
 * @struct tagFLASH_Context
 *
 * @brief Context structure used within FLASH module
 */
typedef struct tagFLASH_Context
{
    FLASH_Status sFstatus;      // Status flags of flash module
    uint32_t u32StartAddr;      // Start Address (e.g. 0x00000000)
    uint32_t u32EndAddr;        // End Address (e.g. 0x00000FFF for 1 Page a 0x1000 Byte)
    fs_config_t *pFstorage;     // Pointer to working flash user configuration (cfgName used in FS_REGISTER_AOT() macro)
} FLASH_Context;

/*--------------------------------------------------------------------------+
|  global processor specific macros                                         |
+--------------------------------------------------------------------------*/

/**
 * @def FS_REGISTER_AOT(cfgName, fsSize, fsStartAddr, fsEndAddr, fsPrio)
 *
 * @brief Automatically creates fstorage structs. To be used within board.h
 *
 * @param cfgName:          Name of the configuration variable
 * @param fsSize:           The number of flash pages within this flash area
 * @param fsStartAddr:      Start address of flash area
 * @param fsEndAddr:        End address of flash area
 * @param fsPrio:           The priority with which fstorage will process storage tasks.
 *                          The highest priority is reserved (0xFF). Must be unique among configurations.
 *
 */
#define FS_REGISTER_AOT(cfgName, fsSize, fsStartAddr, fsEndAddr, fsPrio) \
FS_REGISTER_CFG(fs_config_t cfgName) = \
{ \
    .callback     = fstorage_EventHandler, \
    .num_pages    = (uint8_t)(fsSize / BRD_FLASH_PAGESIZE), \
    .p_start_addr = (uint32_t*)fsStartAddr, \
    .p_end_addr   = (uint32_t*)fsEndAddr, \
    .priority     = fsPrio \
};

/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/

/**
 * @fn fstorage_EventHandler(fs_evt_t const * const Event, fs_ret_t Result)
 *
 * @brief Callback on flash job done. Used internally within flash module.
 *
 * @param Event:            Pointer to event
 * @param Result:           Pointer to result
 */
void fstorage_EventHandler(fs_evt_t const * const Event, fs_ret_t Result);

/**
 * @fn FLASH_init (FLASH_Context *pFlash, ERR_pErrorInfo pError)
 *
 * @brief Initialize the flash module
 *
 * @pre                     1. Flash areas must be predefined during compile time
 *                             by using FS_REGISTER_AOT() macro before calling this function.
 *                          2. Context must be initialized
 * @post                    Call FLASH_close() after all write / erase / read operations are done
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
ERR_StatusType FLASH_init (FLASH_Context *pFlash, ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @fn FLASH_write (FLASH_Context *pFlash, const void *pDest, const void *pSource,
 *                  uint32_t u32Len, ERR_pErrorInfo pError)
 *
 * @brief Write bytes from a general data store into the flash.
 *
 * @pre                     1. FLASH_open() must be called before calling this function.
 *                          2. Erase has to be done manually before write!
 * @post                    Call FLASH_close() after all write / erase / read operations are done
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pDest             The area, where the data is programmed to. Must be word aligned.
 * @param pSource           The area, where the data is read from.
 * @param u32Len            Amount of bytes to write. Must be multiple of a word.
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
ERR_StatusType FLASH_write (FLASH_Context *pFlash, const void *pDest, const void *pSource,
                            uint32_t u32Len, ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @fn FLASH_read (FLASH_Context *pFlash, const void *pDest, const void *pSource,
 *                 uint32_t u32Len, ERR_pErrorInfo pError);
 *
 * @brief Read bytes from flash into a general data store
 *
 * @pre                     FLASH_open() must be called before calling this function
 * @post                    Call FLASH_close() after all write / erase / read operations are done
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pDest             The area, where the data is programmed to.
 * @param pSource           The area, where the data is read from.
 * @param u32Len            Amount of bytes to read.
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
ERR_StatusType FLASH_read (FLASH_Context *pFlash, const void *pDest, const void *pSource,
                           uint32_t u32Len, ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @fn FLASH_erase (FLASH_Context *pFlash, const void *pDst,
 *                  uint32_t u32Len, ERR_pErrorInfo pError)
 *
 * @brief Erase flash area
 *
 * @pre                     FLASH_open() must be called before calling this function
 * @post                    Call FLASH_close() after all write / erase / read operations are done
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pDst              Start of area to erase. Must be page aligned.
 * @param u32Len            Amount of bytes to erase. Must be multiple of a page.
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
ERR_StatusType FLASH_erase (FLASH_Context *pFlash, const void *pDst,
                            uint32_t u32Len, ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

/**
 * @fn FLASH_terminate (FLASH_Context *pFlash, ERR_pErrorInfo pError)
 *
 * @brief Terminate the flash module
 *
 * @pre                     FLASH_open() must be called before calling this function
 *
 * @param pFlash            Pointer to Context of flash module
 * @param pError            Pointer to error structure (or NULL)
 *
 * @return tagERR_StatusType:   ERR_OK, ERR_ERROR
 */
ERR_StatusType FLASH_terminate (FLASH_Context *pFlash, ERR_pErrorInfo pError);

/*-------------------------------------------------------------------------*/

#if (CFG_FLASH_HAL_TEST == CFG_YES)
/**
 * @fn FLASH_test(void)
 *
 * @brief Module test
 *
 * NOTE: It must be internally enabled to have the code active.
 */
void FLASH_test(void);

/*-------------------------------------------------------------------------*/
#endif

#ifdef  __cplusplus
}
#endif

#endif  // FLASH_H_
