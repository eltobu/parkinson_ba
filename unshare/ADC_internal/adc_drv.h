/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: ADC driver
 *      State: reviewed
 * Originator: Temnitzer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/ADC_internal/adc_drv.h $
 *  $Revision: 21973 $
 *      $Date: 2017-12-14 08:26:54 +0100 (Thu, 14 Dec 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Module adc_drv: ADC driver
 *
 * This module implements the CPU internal ADC.
 *
 * support 8bit/10bit and 10bit/12bit, sw/ext/hal timer trigger,
 * single (repeated single), mean measurements on multiple channels
 * infinite or counted number of measurements to ringbuffer on multiple channels
 *
 * Depending on the hardware the module is compiled for the older 8/10bit ADC
 * or the newer 10/12bit ADC. The AT91SAM3U has both hardware versions included,
 * in this case the module is using the 8/10bit ADC.
 *
 * Oversampling works only when just 1 channel is active.
 * Otherwise is the ADC oversampling over all channels
 *
 * ATTENTION:
 * It is important that the softdevice initializes the clock, ...
 * It should be done at the very beginning in the main init zone.
 * sd_softdevice_disable() can be called after SOFTDEVICE_HANDLER_INIT().
 *
 ****************************************************************************
 */

#ifndef ADC_DRV_H_
#define ADC_DRV_H_

/* Includes needed by this header -----------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf_drv_saadc.h"
#include "error.h"
#include "rb.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/

#define AD_MAX_SAMPLE_FREQ   0  ///< sample as fast as possible instead of HAL timer
#define AD_INFINITE_SAMPLING 0  ///< sample until stopped by application
#define AD_EXT_TRIG         -1  ///< set BRD_TIMER_ADC to AD_EXT_TRIG for hw trigger on external pin
#define AD_CHANNELS          8  ///< number of ADC channels

/**
 * defines generated output
 */
typedef enum tagAD_OutputMode
{
    AD_OM_VALUE_2_ARRAY     = 0x1,  ///< output one value per channel to channel array. Must read after every shot
    AD_OM_MEAN_2_ARRAY      = 0x2,  ///< output mean value to channel array. Shot count is nr of values for mean
    AD_OM_VALUE_2_RING_BUFF = 0x3,  ///< output value to ring buffer. Provide Meas1 for all channels followed by Meas2 for all channels and so on, may treat as 2d array
    AD_OM_MOVING_AVG_2_ARRAY= 0x4,  ///< output moving average value to array. One entry per channel, need AD_getMovingAverage() to get correct value
} AD_OutputMode;

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/
/**
 * callback function type, used by this module to inform client about end of conversion
 */
typedef void (*AD_callbackFunction)(ERR_pErrorInfo pError);

/*--------------------------------------------------------------------------+
 |  module global variables declarations                                     |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  function prototypes for C                                                |
 +--------------------------------------------------------------------------*/

/**
 * Initialize the internal ADC
 *
 * @pre The timer module must be initialized, we use one MID timer for safety reasons
 * Other CPUs used u32clockFrequency: clock frequency of MCK during sampling
 *
 * @param i8Resolution : number of bits of resolution. Currently supported 8,10,12,14
 * @param[out] pError  : where to write error information
 *
 * @return status   ERR_OK, ERR_ERROR
 */
uint32_t AD_init(uint8_t u8Resolution, ERR_ErrorInfo *pError);

/*-------------------------------------------------------------------------*/

/**
 * Function to de-initialize the saadc module
 *
 * @details closes ADC module and sets pin to default confiq.
 *          All channels are closed and pins are set to initial state.
 */
void AD_deinit(void);

/*-------------------------------------------------------------------------*/

/**
 * Prepare a task for the internal ADC
 *
 * NOTE: Moving Average: use AD_getMovingAverage() to receive a result.
 *
 * @pre must call AD_init() at least once
 *
 * @param eOutputMode           : kind of output to produce @code AD_OutputMode @endcode
 *                                - AD_OM_MOVING_AVG_2_ARRAY runs forever
 * @param pChannelConfigs       : Pointer to a channel configuration struct or NULL for not used
 * @param u16shotCount          : number of shots until end of task, or use AD_INFINITE_SAMPLING
 * @param u32sampleFreq         : configure sample frequency.
 *                                - AD_MAX_SAMPLE_FREQ: sample as fast as possible
 *                                  Blocking = Nothing_else_than_Interrupt. Control via callback
 *                                - freq in [Hz]:
 * @param pCallback             : function to call when task has finished (may be NULL)
 *                                ATTENTION: runs in interrupt context for speed reasons!
 * @param[out] pi32value        : pointer to output array, size of active channels
 * @param[out] pRingBuff        : pointer to ringbuffer for output.
 *                                Moving Average: ring buffer size must be EXACT the nr
 *                                of channels multiplied by the shotCount (average size),
 *                                otherwise unpredictable results
 * @param[out] pError           : pointer to optional status/error information
 *
 * @return status   ERR_OK, ERR_ERROR
 */
uint32_t AD_taskConfigure(AD_OutputMode eOutputMode,
                          nrf_saadc_channel_config_t *pChannelConfigs[AD_CHANNELS],
                          uint16_t      u16shotCount,
                          uint32_t      u32sampleFreq,
                          AD_callbackFunction pCallback,
                          int32_t       *pi32value,
                          RB_RingBuf    *pRingBuff,
                          ERR_ErrorInfo *pError);

/*-------------------------------------------------------------------------*/

/**
 * start the ad conversion for the selected channels and wait for the results
 *
 * @param[out]   au32Values     pointer to data array (size must match the number of channels selected)
 * @param pChannelConfigs       Pointer to a channel configuration struct or NULL for not used
 * @param[out] pError           where to write error information
 * @return                      status   ERR_OK, ERR_ERROR
 */
uint32_t AD_singleShotBlocking(int32_t  ai32Values[],
                               nrf_saadc_channel_config_t *pChannelConfigs[AD_CHANNELS],
                               ERR_ErrorInfo *pError);

/*-------------------------------------------------------------------------*/

/**
 * start the configured task
 *
 * @pre                 call AD_taskConfigure() first
 * @param[out] pErrorInfo   where to write error information
 * @return              status   ERR_OK, ERR_ERROR
 */
uint32_t AD_startTask(ERR_ErrorInfo* pErrorInfo);

/*-------------------------------------------------------------------------*/

/**
 * start the configured task, blocks until measurement has been completed
 *
 * @pre                 call AD_taskConfigure() first
 *                      AD_taskConfigure parameter "pCallback" cannot be used
 *
 * @param[out] pError   where to write error information
 * @return              status   ERR_OK, ERR_ERROR
 */
uint32_t AD_startTaskBlocking(ERR_ErrorInfo *pError);

/*-------------------------------------------------------------------------*/

/**
 * stop the current active task
 */
void AD_stopTask(void);

/*-------------------------------------------------------------------------*/

/**
 * Value readout for moving average
 *
 * NOTE: All other output modes shall access their data directly (array/ringbuffer)
 *       recommended to use the callback.
 *
 * @param u16channel   : [0..7]single channel for readout, must be in the programmed
 *                       channel list see @code AD_taskConfigure @endcode
 * @param[out] i32value: pointer to store measured value
 * @param[out] pError  : where to write error information
 * @return             : status   ERR_OK, ERR_ERROR
 */
uint32_t AD_getMovingAverage(uint16_t u16channel, int32_t *i32value, ERR_ErrorInfo *pError);

/*-------------------------------------------------------------------------*/

/**
 * Check if internal AD state is idle
 * NOTE: Infinite tasks are never idle while running. If AD_isIdle returns
 * IDLE for an infinite task, it has stopped due to error (rb full/timeout)
 *
 * @return  true when AD is idle
 */
bool AD_isIdle(void);

/*-------------------------------------------------------------------------*/

/**
 * Module test
 * NOTE: It must be internally enabled to have the code active.
 */
void AD_test(void);

/*-------------------------------------------------------------------------*/

#ifdef  __cplusplus
}
#endif

#endif // ADC_DRV_H_
