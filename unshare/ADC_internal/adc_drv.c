/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: ADC driver
 *      State: reviewed
 * Originator: Temnitzer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/ADC_internal/adc_drv.c $
 *  $Revision: 24623 $
 *      $Date: 2018-05-15 11:32:22 +0200 (Tue, 15 May 2018) $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "adc_drv.h"
#include "board.h"
#include "nrf_drv_saadc.h"
#include "nrf.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_ppi.h"
#include "function_run.h"
#include "trace.h"
#include "system_time.h"

/* -------------------------------------------------------------------------- */

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_ADC_DRIVER
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_ADC_DRIVER undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_ADC_DRIVER
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*
 * Definitions of warnings and errors below.
 * See SDS (sections ADC and Error) for design details.
 * Note: The error numbers are the same (whenever possible) for
 *       different ADC types (internal, LT244x, ...)
 */

/**
 * timeout occurred
 */
const ERR_ErrorInfo AD_TIMEOUTERROR = {
        .moduleNumber = ERR_MOD_ADC_DRV,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 1,
        .lineNumber = 0,
        ERRORSTRING("Timeout waiting for SAADC Event")
};

/**
 * ADC is already in use
 */
const ERR_ErrorInfo AD_INUSEERROR = {
        .moduleNumber = ERR_MOD_ADC_DRV,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 2,
        .lineNumber = 0,
        ERRORSTRING("AD: in use")
};

/**
 * no channel selected
 */
const ERR_ErrorInfo AD_NO_CHANNEL_ERROR = {
        .moduleNumber = ERR_MOD_ADC_DRV,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 3,
        .lineNumber = 0,
        ERRORSTRING("No channel selected")
};

/**
 * ADC NRF driver error
 */
const ERR_ErrorInfo AD_NRF_ERROR = {
        .moduleNumber = ERR_MOD_ADC_DRV,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 4,
        .lineNumber = 0,
        ERRORSTRING("AD: NRF driver error")
};

/**
 * rinbuffer overflow
 */
const ERR_ErrorInfo AD_RINGFULL = {
        .moduleNumber = ERR_MOD_ADC_DRV,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 5,
        .lineNumber = 0,
        ERRORSTRING("Ringbuffer overflow")
};

/**
 * invalid arguments in function call
 */
const ERR_ErrorInfo AD_INVALDARG = {
        .moduleNumber = ERR_MOD_ADC_DRV,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 6,
        .lineNumber = 0,
        ERRORSTRING("AD: invalid arguments")
};

/**
 * WARNING: ad is stopped when reading values
 */
const ERR_ErrorInfo AD_STOPPED = {
        .moduleNumber = ERR_MOD_ADC_DRV,
        .errorClass = ERR_CLASS_WARNING,
        .errorNumber = 7,
        .lineNumber = 0,
        ERRORSTRING("AD: idle")
};

/**
 * WARNING: ad clock frequency has been adjusted to feasible value
 */
const ERR_ErrorInfo AD_FREQ = {
        .moduleNumber = ERR_MOD_ADC_DRV,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 8,
        .lineNumber = 0,
        ERRORSTRING("AD: ADC frequency too low")
};

/**
 * Timer definition who triggers the ADC sampling frequency
 */
#ifndef BRD_ADC_DRIVER_CLOCK
#define BRD_ADC_DRIVER_CLOCK    2
#warning BRD_ADC_DRIVER_CLOCK should be defined in board.h
#endif
static const nrf_drv_timer_t m_Timer = NRF_DRV_TIMER_INSTANCE(BRD_ADC_DRIVER_CLOCK);

#define AD_TIMEOUT  500 ///< [ms] max timeout for all channels

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/**
 * internal status
 */
typedef enum tagAD_Status{
    AD_STATUS_IDLE      = 0x00,   ///< ADC is in idle state
    AD_STATUS_CONVERT   = 0x01,   ///< ADC is converting
    AD_STATUS_NO_INIT   = 0x02,   ///< ADC not initialized
    AD_STATUS_CALIBRATE = 0x03,   ///< ADC running calibration
} AD_Status;

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/

static nrf_saadc_channel_config_t *m_AD_pChannelConfigs[AD_CHANNELS]; ///< NULL=inactive, else ptr to config
static volatile AD_Status  m_AD_status = AD_STATUS_NO_INIT; ///< module status, volatile needed to leave blocking calls (AD_startTaskBlocking)
static nrf_saadc_value_t   m_AD_BufferPool[2][AD_CHANNELS];    ///< Pool for ADC conversion results
static int32_t             *m_AD_pi32Value;  ///< array used to store result
static RB_RingBuf          *m_AD_pRingBuf;   ///< ringbuffer used to store result
static AD_callbackFunction m_AD_pCallback;   ///< callback procedure depending on usage
static ERR_ErrorInfo       m_AD_errorInfo;   ///< errorInfo Feedback for timeouts
static AD_OutputMode       m_AD_eOutputMode; ///< output mode (array, ring buffer etc.)
static uint16_t m_ADC_u16ChannelCount = 0;   ///< Count of assigned channels

static uint8_t         m_AD_u8Resolution;     ///< ADC resolution set at init
static uint16_t        m_AD_u16shotCount;     ///< total number of samples to perform
static uint16_t        m_AD_u16shotCountdown; ///< current number of samples to perform, stops sampling when counter reaches zero
static uint32_t        m_AD_u32sampleFreq;    ///< [Hz] sampling freq
static nrf_ppi_channel_t     m_ppi_channel;   ///< Programmable Peripheral Interconnect ADC


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/

/**
 * Timeout procedure
 */
static void AD_timeOut(void)
{
    AD_stopTask();  //< stop adc

    ERR_setErrorParam(&AD_TIMEOUTERROR, __LINE__, &m_AD_errorInfo, 0, 0);

    if(m_AD_pCallback != NULL)
    {
        m_AD_pCallback(&m_AD_errorInfo);
    }
} // End of AD_timeOut

/* -------------------------------------------------------------------------- */

/**
 * The external data buffers get cleared (set to 0)
 *
 * @return Buffers erased for the number of channels.
 */
static uint16_t clearBuffers(void)
{
    uint16_t Channels = 0;
    if (m_AD_pi32Value != NULL)
    {
        for (int i = 0; i < AD_CHANNELS; i++)
        {
            if (m_AD_pChannelConfigs[i] != NULL)
            {
                m_AD_pi32Value[Channels] = 0;
                Channels++;
            }
        }
    }
    // Initialize ring buffer (if required)
    if (m_AD_pRingBuf != NULL)
    {
        RB_purge(m_AD_pRingBuf);
    }
    return (Channels);

} // End of clearBuffers

/* -------------------------------------------------------------------------- */

/**
 * Clear internal error
 */
static void clearInternalError(void)
{
    m_AD_errorInfo.moduleNumber = 0;
    m_AD_errorInfo.errorClass   = ERR_CLASS_NOERROR;
    m_AD_errorInfo.errorNumber  = 0;
    m_AD_errorInfo.errorString  = NULL;
    m_AD_errorInfo.lineNumber   = 0;

} // End of clearInternalError

/* -------------------------------------------------------------------------- */

/**
 * Mandatory handler, which does nothing
 */
static void TimerHandler(nrf_timer_event_t EventType, void* pContext)
{
    (void) EventType;
    (void) pContext;
} // End of TimerHandler

/* -------------------------------------------------------------------------- */

/**
 * Sets an internal timer which triggers sampling start again
 */
static void startSamplingTimer(void)
{
    ret_code_t NrfStatus;
    uint32_t u32Ticks;
    uint32_t u32EventAddr;
    uint32_t u32TaskAddr;

    NrfStatus = nrf_drv_ppi_init();
    if (NrfStatus != NRF_SUCCESS && NrfStatus != NRF_ERROR_MODULE_ALREADY_INITIALIZED)
    {
        APP_ERROR_CHECK(NrfStatus);
    }

    // frequency=16MHz, mode=Timer, bit_width=16Bit, interrupt_priority=7
    nrf_drv_timer_config_t TimerConfig = NRF_DRV_TIMER_DEFAULT_CONFIG;
    TimerConfig.bit_width = NRF_TIMER_BIT_WIDTH_32;
    NrfStatus = nrf_drv_timer_init(&m_Timer, &TimerConfig, TimerHandler);
    TRACE_DBG_L("Stat %d, ADC nrf_drv_timer_init inst %d, channel %d\n",
            NrfStatus, m_Timer.instance_id, m_Timer.cc_channel_count);
    APP_ERROR_CHECK(NrfStatus);

    // Get the number of ticks out of the frequency
    if (m_AD_u32sampleFreq > 100)
    {
        u32Ticks = nrf_drv_timer_us_to_ticks(&m_Timer, 1000000 / m_AD_u32sampleFreq);
    }
    else
    {
        u32Ticks = nrf_drv_timer_ms_to_ticks(&m_Timer, 1000 / m_AD_u32sampleFreq);
    }
    nrf_drv_timer_extended_compare(&m_Timer,
                                   NRF_TIMER_CC_CHANNEL0,
                                   u32Ticks,
                                   NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
                                   false);
    nrf_drv_timer_enable(&m_Timer);

    u32EventAddr = nrf_drv_timer_compare_event_address_get(&m_Timer,
                                                           NRF_TIMER_CC_CHANNEL0);
    u32TaskAddr = nrf_drv_saadc_sample_task_get();

    // Setup ppi channel so that timer compare event is triggering sample task in SAADC
    NrfStatus = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
    APP_ERROR_CHECK(NrfStatus);

    NrfStatus = nrf_drv_ppi_channel_assign(m_ppi_channel,
                                           u32EventAddr,
                                           u32TaskAddr);
    APP_ERROR_CHECK(NrfStatus);

    // Enable sampling
    NrfStatus = nrf_drv_ppi_channel_enable(m_ppi_channel);
    APP_ERROR_CHECK(NrfStatus);

} // End of startSamplingTimer

/* -------------------------------------------------------------------------- */

/**
 * @brief SAADC callback function
 *
 * @details This function is executed on SAADC events
 *
 * @param   pEvent  Pointer to the NRF ADC event
 */
static void ADC_callback(nrf_drv_saadc_evt_t const * pEvent)
{
    ret_code_t NrfStatus;
    nrf_saadc_value_t CurrentSample;

    /* conversion done event */
    switch (pEvent->type)
    {
    case NRF_DRV_SAADC_EVT_DONE:
        if (AD_isIdle())
        {
            return; // AD_stopTask() must have been called before
        }

        if (m_AD_u16shotCountdown > 0) // else: maybe AD_INFINITE_SAMPLING
        {
            m_AD_u16shotCountdown--;
        }
        if ((m_AD_u16shotCountdown > 0) || (m_AD_u16shotCount == AD_INFINITE_SAMPLING))
        {
            // Put buffer to the sample queue again
            NrfStatus = nrf_drv_saadc_buffer_convert(pEvent->data.done.p_buffer,
                                                     m_ADC_u16ChannelCount);
            APP_ERROR_CHECK(NrfStatus);

            if (m_AD_u32sampleFreq == AD_MAX_SAMPLE_FREQ)
            {
                // Restart sampling again, blocks in interrupt until job done
                NrfStatus = nrf_drv_saadc_sample();
                APP_ERROR_CHECK(NrfStatus);
            } // else re-trigger via timer
        }

        for (int i = 0; i < m_ADC_u16ChannelCount; i++)
        {
            CurrentSample = pEvent->data.done.p_buffer[i];
            switch(m_AD_eOutputMode)
            {
                case AD_OM_VALUE_2_ARRAY:
                    m_AD_pi32Value[i] = CurrentSample;
                    break;

                case AD_OM_MEAN_2_ARRAY:
                    m_AD_pi32Value[i] += CurrentSample;
                    break;

                case AD_OM_MOVING_AVG_2_ARRAY:
                    m_AD_u16shotCountdown = 42; // force infinite sampling by setting value > 1

                    if (RB_getFreeQty(m_AD_pRingBuf) == 0) // ring buffer full (no overflow)?
                    {
                        nrf_saadc_value_t  i16dropSample; //< only needed for moving average
                        // rb is filled, we must consume the oldest value
                        RB_get(m_AD_pRingBuf, (uint8_t*)&i16dropSample);
                        m_AD_pi32Value[i] -= i16dropSample;
                    }
                    m_AD_pi32Value[i] += CurrentSample;
                    // intended fallthru

                case AD_OM_VALUE_2_RING_BUFF:
                    if (! RB_put(m_AD_pRingBuf, (uint8_t*) (&CurrentSample)))
                    {
                        // Ringbuffer full?
                        ERR_setErrorParam(&AD_RINGFULL, __LINE__, &m_AD_errorInfo, 0, 0);
                        AD_stopTask();
                    }
                    break;

                default:
                    ERR_setErrorParam(&AD_INVALDARG, __LINE__, &m_AD_errorInfo, 1, m_AD_eOutputMode);
                    break;
            } // switch Output-Mode
        }
        break; // Event done

    case NRF_DRV_SAADC_EVT_LIMIT:
        TRACE_DBG_L("ADC LIMIT event !?\n");
        return;

    case NRF_DRV_SAADC_EVT_CALIBRATEDONE:
        m_AD_status = AD_STATUS_IDLE;
        TRACE_DBG_L("ADC Calibration done\n");
        return;

    default:
        TRACE_DBG_H("ADC not handled event %d\n", pEvent->type);
    }

    // More shots required?
    if ( (m_AD_u16shotCountdown > 0) || (m_AD_u16shotCount == AD_INFINITE_SAMPLING) )
    {
        FUNR_register(AD_timeOut, AD_TIMEOUT); // Re-trigger timeout for new shot
        if (m_AD_u32sampleFreq == AD_MAX_SAMPLE_FREQ)   // no sampling freq. specified (no hal timer required)?
        {
            // Restart AD conversion (not needed for HAL timer)
            // NOT IMPLEMENTED YET
        }

        if (m_AD_pCallback != NULL)
        {
            if ( (m_AD_eOutputMode == AD_OM_VALUE_2_ARRAY) || (m_AD_eOutputMode == AD_OM_MOVING_AVG_2_ARRAY) )
            {
                // single value requires callback after each conversion otherwise data loss
                if (m_AD_errorInfo.errorClass == ERR_CLASS_ERROR)
                {
                    m_AD_pCallback(&m_AD_errorInfo);
                }
                else
                {
                    m_AD_pCallback(NULL);
                }
            }
        }
    }
    else // no more shots required
    {
        // Task done, prepare for next shot
        FUNR_unregister(AD_timeOut);

        if (m_AD_u32sampleFreq != AD_MAX_SAMPLE_FREQ)
        {
            // Stop AD conversion (only for HAL timer)
            // NOT IMPLEMENTED YET
        }

        if (m_AD_eOutputMode == AD_OM_MEAN_2_ARRAY) // mean value required?
        {
            for (int i = 0; i < m_ADC_u16ChannelCount; i++)
            {
                m_AD_pi32Value[i] /= m_AD_u16shotCount;
            }
        }

        // Data is ready for application processing
        if (m_AD_pCallback != NULL)
        {
            if (m_AD_errorInfo.errorClass == ERR_CLASS_ERROR)
            {
                m_AD_pCallback(&m_AD_errorInfo);
            }
            else
            {
                m_AD_pCallback(NULL);
            }
        }
        m_AD_u16shotCountdown = m_AD_u16shotCount;  // Reconfigure task
        m_AD_status = AD_STATUS_IDLE;               // AD is idle again
    }
} // End of ADC_callback


/*--------------------------------------------------------------------------+
 |  functions                                                                |
 +--------------------------------------------------------------------------*/

uint32_t AD_init(uint8_t u8Resolution, ERR_ErrorInfo *pError)
{
    // Oversampling, Interrupt priority and Low power mode is defined in sdk_config.h
    nrf_drv_saadc_config_t Config = NRF_DRV_SAADC_DEFAULT_CONFIG; // Read sdk_config
    ret_code_t NrfStatus;

    if (m_AD_status != AD_STATUS_NO_INIT)
    {
        ERR_setErrorParam(&AD_INUSEERROR, __LINE__, pError, m_AD_status, (uint32_t)u8Resolution);
        return (ERR_ERROR);
    }
    // Prepare internal configuration
    switch (u8Resolution)
    {
    case 8:
        Config.resolution = SAADC_RESOLUTION_VAL_8bit;
        break;
    case 10:
        Config.resolution = SAADC_RESOLUTION_VAL_10bit;
        break;
    case 12:
        Config.resolution = SAADC_RESOLUTION_VAL_12bit;
        break;
    case 14:
        Config.resolution = SAADC_RESOLUTION_VAL_14bit;
        break;
    default:
        TRACE_DBG_H("ADC wrong resolution %d\n", u8Resolution);
        ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, 2, u8Resolution);
        return (ERR_ERROR); // Illegal parameters
    }

    /* Initialize SADC module */
    NrfStatus = nrf_drv_saadc_init(&Config, ADC_callback);
    if(NrfStatus != NRF_SUCCESS)
    {
        ERR_setErrorParam(&AD_NRF_ERROR, __LINE__, pError, NrfStatus,
                          (uint32_t)u8Resolution);
        if (NrfStatus == NRF_ERROR_INVALID_STATE)
        {
            return (ERR_BUSY);
        }
        else
        {
            return (ERR_ERROR);
        }
    }

    // Init some other stuff
    m_AD_pRingBuf  = NULL;
    m_AD_pi32Value = NULL;
    m_AD_u8Resolution = u8Resolution;
    clearInternalError();

    m_AD_status = AD_STATUS_IDLE;

    /* todo check adc calibration issue.
     * adc calibration is inconsistent with variations
     * of up to +/- 5 lsb offset @ 12bit
     */
#if 0
    uint64_t u64_Timeout;

    // Do the offset calibration and wait until done
    m_AD_status = AD_STATUS_CALIBRATE;
    NrfStatus = nrf_drv_saadc_calibrate_offset();
    if (NrfStatus != NRF_SUCCESS)
    {
        ERR_setErrorParam(&AD_NRF_ERROR, __LINE__, pError, NrfStatus,
                          (uint32_t)u8Resolution);
        return (ERR_ERROR);
    }

    // Wait until calibration is done
    u64_Timeout = TIME_setTimeout(20); // [ms]
    while((! AD_isIdle()) && (!TIME_isTimeout(u64_Timeout)));
    if (! AD_isIdle())
    {
        // The calibration has not been performed
        ERR_setErrorParam(&AD_NRF_ERROR, __LINE__, pError, NrfStatus,
                          (uint32_t)u8Resolution);
        return (ERR_ERROR);
    }
#endif
    return (ERR_OK);

} // End of AD_init

/* -------------------------------------------------------------------------- */

void AD_deinit(void)
{
    if (m_AD_status != AD_STATUS_NO_INIT)
    {
        AD_stopTask();
        nrf_drv_saadc_uninit();
    }
    m_AD_status = AD_STATUS_NO_INIT;
    m_ADC_u16ChannelCount = 0;

} // End of AD_deinit

/* -------------------------------------------------------------------------- */

uint32_t AD_taskConfigure(AD_OutputMode   eOutputMode,
                          nrf_saadc_channel_config_t *pChannelConfigs[AD_CHANNELS],
                          uint16_t        u16shotCount,
                          uint32_t        u32sampleFreq,
                          AD_callbackFunction pCallback,
                          int32_t         *pi32value,
                          RB_RingBuf      *pRingBuff,
                          ERR_ErrorInfo   *pError)
{
    ret_code_t NrfStatus;

    // Prevent from deadlock! return error if not idle
    if (m_AD_status != AD_STATUS_IDLE)
    {
        ERR_setErrorParam(&AD_INUSEERROR, __LINE__, pError, m_AD_status, (uint32_t)AD_taskConfigure);
        return (ERR_ERROR);
    }

    // Deactivate all ADC channels and copy channel configuration
    for (int i = 0; i < AD_CHANNELS; i++)
    {
        NrfStatus = nrf_drv_saadc_channel_uninit(i);
        APP_ERROR_CHECK(NrfStatus);
        if (NrfStatus != NRF_SUCCESS)
        {
            ERR_setErrorParam(&AD_INUSEERROR, __LINE__, pError, i, NrfStatus);
            return (ERR_ERROR);
        }
        m_AD_pChannelConfigs[i] = pChannelConfigs[i];
    }

    // Store configuration for later use
    m_AD_eOutputMode      = eOutputMode;
    m_AD_u32sampleFreq    = u32sampleFreq;
    m_AD_u16shotCount     = u16shotCount;
    m_AD_pCallback        = pCallback;
    m_AD_pi32Value        = pi32value;
    m_AD_pRingBuf         = pRingBuff;
    m_AD_u16shotCountdown = u16shotCount;

    m_ADC_u16ChannelCount = clearBuffers();
    if (m_ADC_u16ChannelCount == 0)
    {
        // No channels configured
        ERR_setErrorParam(&AD_NO_CHANNEL_ERROR, __LINE__, pError, 0, 0);
        return (ERR_ERROR);
    }

    // No array available but output to array?
    if ( (m_AD_pi32Value == NULL) &&
         ((eOutputMode == AD_OM_VALUE_2_ARRAY) ||
          (eOutputMode == AD_OM_MEAN_2_ARRAY)  ||
          (eOutputMode == AD_OM_MOVING_AVG_2_ARRAY)) )
    {
        ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, 3, 1);
        return (ERR_ERROR);
    }

    // Mean / moving average over infinite/too big number of samples?
    if ( (eOutputMode == AD_OM_MEAN_2_ARRAY) ||
         (eOutputMode == AD_OM_MOVING_AVG_2_ARRAY) )
    {
        uint16_t u16Tmp;   ///< Temporary register

        // Calculate max number of shots (avoid internal overrun)
        u16Tmp = (sizeof(pi32value[0]) << 3);     //< nr of bits in output variable
        // Subtract the number of bits for one conversion to get the max shot count
        u16Tmp -= m_AD_u8Resolution;

        if ( (u16shotCount == AD_INFINITE_SAMPLING) || (u16shotCount > (1 << u16Tmp)) )
        {
            ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, 1 << u16Tmp, u16shotCount);
            return (ERR_ERROR);
        }
    }

    // No ring buffer available but output to ring buffer?
    if ( (m_AD_pRingBuf == NULL) &&
         ((eOutputMode == AD_OM_VALUE_2_RING_BUFF) ||
          (eOutputMode == AD_OM_MOVING_AVG_2_ARRAY)) )
    {
        ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, 3, 2);
        return (ERR_ERROR);
    }

    // Configure all active channels
    for (uint8_t i = 0; i < AD_CHANNELS; i++)
    {
        if (m_AD_pChannelConfigs[i] != NULL)
        {
            NrfStatus = nrf_drv_saadc_channel_init(i, m_AD_pChannelConfigs[i]);
            APP_ERROR_CHECK(NrfStatus);
            if (NrfStatus != NRF_SUCCESS)
            {
                ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, i, NrfStatus);
                return (ERR_ERROR);
            }
        }
    }
    return (ERR_OK);

} // End of AD_taskConfigure

/* -------------------------------------------------------------------------- */

uint32_t AD_startTask(ERR_ErrorInfo* pErrorInfo)
{
    ret_code_t NrfStatus; ///< Status code from NRF library

    // Prevent from deadlock! return error if not idle
    if (m_AD_status != AD_STATUS_IDLE)
    {
        ERR_setErrorParam(&AD_INUSEERROR, __LINE__, pErrorInfo, m_AD_status, (uint32_t)AD_startTask);
        return (ERR_ERROR);
    }

    m_AD_status = AD_STATUS_CONVERT;

    // destroy all old (out dated) data
    clearBuffers();
    clearInternalError();

    m_AD_u16shotCountdown = m_AD_u16shotCount;

    FUNR_register(AD_timeOut, AD_TIMEOUT); //< Set timeout

    // Enable ADC Interrupt. Configured in AD_init and AD_taskConfigure
    NrfStatus = nrf_drv_saadc_buffer_convert(m_AD_BufferPool[0], m_ADC_u16ChannelCount);
    TRACE_DBG_L("NrfStatus %d, nrf_drv_saadc_buffer_convert\n", NrfStatus);
    APP_ERROR_CHECK(NrfStatus);
    if (NrfStatus != NRF_SUCCESS)
    {
        ERR_setErrorParam(&AD_NRF_ERROR, __LINE__, pErrorInfo, NrfStatus,
                          (uint32_t)m_ADC_u16ChannelCount);
        return (ERR_ERROR);
    }
    /* Trigger sampling task */
    if (m_AD_u32sampleFreq == AD_MAX_SAMPLE_FREQ)
    {
        NrfStatus = nrf_drv_saadc_sample();
        TRACE_DBG_L("NrfStatus %d, nrf_drv_saadc_sample\n", NrfStatus);
    }
    else
    {
        // Enable double buffering
        NrfStatus = nrf_drv_saadc_buffer_convert(m_AD_BufferPool[1], m_ADC_u16ChannelCount);
        TRACE_DBG_L("NrfStatus %d, nrf_drv_saadc_buffer_convert\n", NrfStatus);
        APP_ERROR_CHECK(NrfStatus);
        if (NrfStatus != NRF_SUCCESS)
        {
            ERR_setErrorParam(&AD_NRF_ERROR, __LINE__, pErrorInfo, NrfStatus,
                              (uint32_t)m_ADC_u16ChannelCount);
            return (ERR_ERROR);
        }
        startSamplingTimer(); // Sampling start via timer
    }
    APP_ERROR_CHECK(NrfStatus);
    if (NrfStatus != NRF_SUCCESS)
    {
        ERR_setErrorParam(&AD_NRF_ERROR, __LINE__, pErrorInfo, NrfStatus, m_AD_u32sampleFreq);
        return (ERR_ERROR);
    }
   return (ERR_OK);

} // End of AD_startTask

/*-------------------------------------------------------------------------*/

uint32_t AD_startTaskBlocking(ERR_ErrorInfo *pError)
{
    ERR_StatusType eStatus = ERR_OK;    ///< status, default = no error

    eStatus = AD_startTask(pError);     //< start task
    if (eStatus != ERR_OK)
    {
        return eStatus;
    }

    while ( m_AD_status == AD_STATUS_CONVERT )
    {
        // wait until conversion has finished
        FUNR_runFunctions(NULL);  //< Ensure that the timeout timer can finish
    }

    // report internal error (e.g. timeout) if applicable
    if (pError != NULL)
    {
    	*pError = m_AD_errorInfo;
    }
    if (m_AD_errorInfo.errorClass == ERR_CLASS_ERROR)
    {
        eStatus = ERR_ERROR;
    }
    return (eStatus);

} // End of AD_startTaskBlocking

/*-------------------------------------------------------------------------*/

uint32_t AD_singleShotBlocking(int32_t  ai32Values[],
                               nrf_saadc_channel_config_t *pChannelConfigs[AD_CHANNELS],
                               ERR_ErrorInfo *pError)
{
    ERR_StatusType eStatus;

    // Prevent from deadlock! return error if not idle
    if (m_AD_status != AD_STATUS_IDLE)
    {
        ERR_setErrorParam(&AD_INUSEERROR, __LINE__, pError, m_AD_status, (uint32_t)AD_singleShotBlocking);
        return (ERR_ERROR);
    }

    eStatus = AD_taskConfigure( AD_OM_VALUE_2_ARRAY,
                                pChannelConfigs,
                                1,                  //< u16shotCount
                                AD_MAX_SAMPLE_FREQ, //< u32sampleFreq
                                NULL,               //< pCallback
                                ai32Values,
                                NULL,               //< pRingBuff,
                                pError);

    if (eStatus == ERR_OK) // at least one channel was selected?
    {
        eStatus = AD_startTaskBlocking(pError);
    }
    return (eStatus);

} // End of AD_singleShotBlocking

/*-------------------------------------------------------------------------*/

void AD_stopTask(void)
{
    ret_code_t NrfStatus; ///< Status code from NRF library

    TRACE_DBG_L("AD_stopTask(), m_AD_status %d\n", m_AD_status);
    if (m_AD_status != AD_STATUS_CONVERT)
    {
        return; // Already stopped, nothing to do
    }
    // module cleanup
    m_AD_u16shotCountdown = m_AD_u16shotCount;
    m_AD_status = AD_STATUS_IDLE;

    // Abort ongoing and buffered conversions. Done-Event will be shot when busy.
    nrf_drv_saadc_abort();
    nrf_drv_timer_uninit(&m_Timer);

    // Programmable Peripheral Interconnect disconnect
    NrfStatus = nrf_drv_ppi_channel_disable(m_ppi_channel);
    TRACE_DBG_L("NrfStatus %d, nrf_drv_ppi_channel_disable\n", NrfStatus);
    APP_ERROR_CHECK(NrfStatus);
    NrfStatus = nrf_drv_ppi_channel_free(m_ppi_channel);
    APP_ERROR_CHECK(NrfStatus);

    // Stop the task & cleanup
    FUNR_unregister(AD_timeOut); //< Avoid false timeout

} // End of AD_stopTask

/*-------------------------------------------------------------------------*/

uint32_t AD_getMovingAverage(uint16_t u16channel, int32_t *i32value, ERR_ErrorInfo *pError)
{
    uint16_t u16tmp;              ///< calculation helper: 1) channel search, 2: available buffer elements
    uint8_t  u8arrayPos = 0;      ///< array position of selected channel
    uint8_t  u8channels = 0;      ///< number of channels, should be 1
    uint8_t  u8chCtr;             ///< channel loop counter
    bool     bFound = false;      ///< true when at least on channel within programmed task list

    if (m_AD_eOutputMode != AD_OM_MOVING_AVG_2_ARRAY)
    {
        // invalid output mode for this function
        ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, 1, m_AD_eOutputMode);
        return (ERR_ERROR);
    }

    if (AD_isIdle())
    {
        // Warning, application may have stopped acquisition and wants to read the last value
        ERR_setErrorParam(&AD_STOPPED, __LINE__, pError, m_AD_status, u16channel);
        return (ERR_NOSYS);
    }

    if (m_AD_pChannelConfigs[u16channel] == NULL)
    {
        // desired channel not in task list
        ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, 7, u16channel);
        return (ERR_ERROR);
    }

    /* It's already checked that the requested channel is available
     * now verify that only one channel is requested and
     * derive the array position of the data
     */
    for(u8chCtr = 0; u8chCtr < AD_CHANNELS; u8chCtr++)
    {
        if (m_AD_pChannelConfigs[u8chCtr] != NULL) // channel part of channel list?
        {
            u8channels++;
            if (u16channel != u8chCtr)    // channel also requested?
            {
                if (!bFound)
                {
                    // no, requested channel is later in the data array
                    u8arrayPos++;
                }
                // no else path, the channel is already found
            }
            else
            {
                // requested channel found
                if (!bFound) // only one channel requested?
                {
                    // it's the first one
                    bFound = true;
                }
                else
                {
                    // more than one requested => error!
                    ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, u8arrayPos, u16channel);
                    return (ERR_ERROR);
                }
            }
        }
    }   // end for

    u16tmp = RB_getQty(m_AD_pRingBuf); //< number of available rb elements
    if (u16tmp >= u8channels)
    {
        // scaling required
        *i32value = (m_AD_pi32Value[u8arrayPos]  //< sum from ISR
             / (RB_getQty(m_AD_pRingBuf) / u8channels) ); //< divided by nr of samples for this channel
    }
    else
    {
        // no data available
        *i32value = 0;
        ERR_setErrorParam(&AD_INVALDARG, __LINE__, pError, 3, 2);
        return (ERR_AGAIN);
    }

    TRACE_DBG_L("AD_getMovingAverage channel 0x%X, array pos %d, value %d\n",
                 u16channel, u8arrayPos, *i32value);
    return (ERR_OK);

} // End of AD_getMovingAverage

/*-------------------------------------------------------------------------*/

bool AD_isIdle(void)
{
    return (m_AD_status == AD_STATUS_IDLE);

} // End of AD_isIdle

/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/

#if 0
// This test kills all other internal ADC setups.
#define AD_ADC_CONFIG_BAT   \
{                           \
    .resistor_p = NRF_SAADC_RESISTOR_DISABLED,   /*//< disable resistors */\
    .resistor_n = NRF_SAADC_RESISTOR_DISABLED,   /*//< disable resistors */\
    .gain       = NRF_SAADC_GAIN1_6,             /*//< gain factor */ \
    .reference  = NRF_SAADC_REFERENCE_INTERNAL,  /*//< reference voltage */ \
    .acq_time   = NRF_SAADC_ACQTIME_10US,        /*//< Acquisition time in us */ \
    .mode       = NRF_SAADC_MODE_SINGLE_ENDED,   /*//< single ended measurement mode */ \
    .burst      = NRF_SAADC_BURST_DISABLED,      /*//< No burst, no oversampling */ \
    .pin_p      = NRF_SAADC_INPUT_VDD,           /*//< input channel, internal VDD */ \
    .pin_n      = NRF_SAADC_INPUT_DISABLED       /*//< negative input channel */ \
}
#define AD_ADC_CONFIG_DIFF  \
{                           \
    .resistor_p = NRF_SAADC_RESISTOR_PULLDOWN,  /*//< disable resistors */\
    .resistor_n = NRF_SAADC_RESISTOR_PULLDOWN,  /*//< disable resistors */\
    .gain       = NRF_SAADC_GAIN4,              /*//< gain factor */ \
    .reference  = NRF_SAADC_REFERENCE_INTERNAL, /*//< reference voltage */ \
    .acq_time   = NRF_SAADC_ACQTIME_3US,        /*//< Acquisition time in us */ \
    .mode       = NRF_SAADC_MODE_DIFFERENTIAL,  /*//< single ended measurement mode */ \
    .burst      = NRF_SAADC_BURST_DISABLED,     /*//< No burst, no oversampling */ \
    .pin_p      = NRF_SAADC_INPUT_AIN1,         /*//< positive input channel */\
    .pin_n      = NRF_SAADC_INPUT_AIN2          /*//< negative input channel */ \
}
#define MOVING_AVR_SIZE 5
static nrf_saadc_channel_config_t m_AD_adcConfigBat  = AD_ADC_CONFIG_BAT;
static nrf_saadc_channel_config_t m_AD_adcConfigDiff = AD_ADC_CONFIG_DIFF;
static nrf_saadc_channel_config_t *m_Configs[AD_CHANNELS];
static int32_t m_ai32_Values[AD_CHANNELS];
static uint8_t m_RB_Buffer[AD_CHANNELS * MOVING_AVR_SIZE * sizeof(int32_t)];
static RB_RingBuf m_RingBuffer;
static uint32_t StopAfter;

/*-------------------------------------------------------------------------*/
#define MAX_READOUT 15

static void ADC_Callback(ERR_pErrorInfo pError)
{
    uint32_t Status;
    int32_t i32value = 0;

    //for (int i = 0; i < AD_CHANNELS; i++)
    for (int i = 0; i < 2; i++)
    {
        if (StopAfter <= MAX_READOUT)
        {
            uint32_t Channel;
            if (i == 0) Channel = 4;
            else        Channel = 7;
            Status = AD_getMovingAverage(Channel, &i32value, NULL);
            if (Status == ERR_OK)
            {
                TRACE_DBG_L("0x%04X, 0x%04X / ", m_ai32_Values[i], i32value);
            }
            else
            {
                TRACE_DBG_L("AD_getMovingAverage ERROR %d", Status);
            }
        }
        else
        {
            TRACE_DBG_L("%d: 0x%04X ", i, m_ai32_Values[i]);
        }
    }
    if (pError != NULL)
    {
        TRACE_DBG_L(" ERR=0x%08X", pError);
    }
    TRACE_DBG_L("\n");

    StopAfter++;
    if (StopAfter > MAX_READOUT)
    {
        AD_stopTask();
        TRACE_DBG_L("AD_stopTask() done in test ADC_Callback\n");
    }
} // End of ADC_Callback

/*-------------------------------------------------------------------------*/

void AD_test(void)
{
    uint32_t Status;
    uint32_t Count;

    TRACE_DBG_H("AD_test()\n");
    for (int i = 0; i < AD_CHANNELS; i++)
    {
        m_Configs[i] = NULL;
        m_ai32_Values[i]  = 0xDEAD;
    }
    m_Configs[4] = &m_AD_adcConfigBat;
    m_Configs[7] = &m_AD_adcConfigDiff;

    Status = AD_init(14, NULL);
    TRACE_DBG_L("Stat %d, AD_init(14), Idle-%d\n", Status, AD_isIdle());
    if (Status != ERR_OK)
    {
        // Another application has already setup the internal ADC
        m_AD_status = AD_STATUS_IDLE; // Show dummy activity
        AD_deinit();
        TRACE_DBG_L("Stat %d, AD_init > AD_deinit\n", Status);
        Status = AD_init(12, NULL);
        TRACE_DBG_L("Stat %d, AD_init(12), Idle-%d\n", Status, AD_isIdle());
    }
#if 1
    // Single measurement
    for (int i = 0; i < 2; i++)
    {
        Status = AD_singleShotBlocking(m_ai32_Values, m_Configs, NULL);
        TRACE_DBG_L("Stat %d, AD_singleShotBlocking 0x%04X 0x%04X 0x%04X\n",
                    Status, m_ai32_Values[0], m_ai32_Values[1], (uint16_t)m_ai32_Values[2]);
    }

    // Collect data via callback
    Status = AD_taskConfigure(AD_OM_VALUE_2_ARRAY, m_Configs, 1, AD_MAX_SAMPLE_FREQ,
                              ADC_Callback, m_ai32_Values, NULL, NULL);
    TRACE_DBG_L("Stat %d, AD_taskConfigure to array\n", Status);

    Count = 0;
    StopAfter = 1000;
    Status = AD_startTask(NULL);
    TRACE_DBG_L("Stat %d, AD_startTask single\n", Status);
    while(! AD_isIdle())
    {
        Count++;
    }
    TRACE_DBG_L("Stat %d, AD_startTask after %d, Idle-%d\n", Status, Count, (int)AD_isIdle());
    AD_stopTask(); // Should have no effect since the task has finished already
#endif
#if 1
    AD_deinit();

    // Collect data via array using ring buffer and moving average
    // Needs a low resolution that moving average can be setup
    RB_init(&m_RingBuffer, m_RB_Buffer, 2*MOVING_AVR_SIZE, sizeof(int32_t));
    Status = AD_init(12, NULL);
    TRACE_DBG_L("Stat %d, AD_init(12)\n", Status);
    Status = AD_taskConfigure(AD_OM_MOVING_AVG_2_ARRAY, m_Configs,
                              MOVING_AVR_SIZE, 1000,
                              ADC_Callback, m_ai32_Values, &m_RingBuffer, NULL);
    TRACE_DBG_L("Stat %d, AD_taskConfigure to array moving average\n", Status);

    Count = 0;
    StopAfter = 0;
    Status = AD_startTask(NULL);
    TRACE_DBG_L("Stat %d, AD_startTask RB\n", Status);
    while(! AD_isIdle())
    {
        Count++;
    }
    TRACE_DBG_L("Stat %d, AD_startTask after %d counts, Idle-%d\n", Status, Count, (int)AD_isIdle());

    AD_stopTask(); // Should have no effect since the task has finished already
#endif
    AD_deinit();

} // End of AD_test
#endif

/*-------------------------------------------------------------------------*/
