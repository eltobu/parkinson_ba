/****************************************************************************
 *
 *   Customer: AoT
 *   Project#: 00-00
 *       Name: AOT_SW
 *
 *     Module: trace_dbg_late_include.h
 *      State: ready for use
 * Originator: Klaus Ruzicka
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/trace/trace_dbg_late_include.h $
 *  $Revision: 22815 $
 *      $Date: 2018-01-29 11:32:42 +0100 (Mon, 29 Jan 2018) $
 *    $Author: Schwinghammer $
 *
 *  Developed by Art of Technology AG, 2011
 *
 ****************************************************************************
 */
/*  Purpose: Helper for module level configurable debugging
 *
 *	Usage: add this include in your .c file after defining LOCAL_DBG level
 *        see example below:

	/module/.c
	#include "module_config.h"
	
	...
	
	// *************************************************************
	// * debugging and check configuration
	// *************************************************************
	#undef LOCAL_DBG
	//@TODO: replace /module/ by module name for 2 times
	#ifndef CFG_DBG_/module/
	    #define LOCAL_DBG TR_DBG_NO
	    #warning "Debug level CFG_DBG_* undefined, turned off debugging (default)!"
	#else
	    #define LOCAL_DBG CFG_DBG_/module/
	#endif
	#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG
*/


// _NO_ include guard as indended for use in several .c files!

// remove defines from other modules
#undef TRACE_DBG_H
#undef TRACE_DBG_M
#undef TRACE_DBG_L

// do the trace mapping for the current module
#if (LOCAL_DBG == TR_DBG_L)
    #define TRACE_DBG_L(...)    TRACE_PRINT(__VA_ARGS__)
    #define TRACE_DBG_M(...)    TRACE_PRINT(__VA_ARGS__)
    #define TRACE_DBG_H(...)    TRACE_PRINT(__VA_ARGS__)
#elif (LOCAL_DBG == TR_DBG_M)
    #define TRACE_DBG_L(...)
    #define TRACE_DBG_M(...)    TRACE_PRINT(__VA_ARGS__)
    #define TRACE_DBG_H(...)    TRACE_PRINT(__VA_ARGS__)
#elif (LOCAL_DBG == TR_DBG_H)
    #define TRACE_DBG_L(...)
    #define TRACE_DBG_M(...)
    #define TRACE_DBG_H(...)    TRACE_PRINT(__VA_ARGS__)
#else
    #define TRACE_DBG_L(...)
    #define TRACE_DBG_M(...)
    #define TRACE_DBG_H(...)
#endif
