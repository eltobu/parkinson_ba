/****************************************************************************
 *
  *   Customer: AoT
 *   Project#: 00-00
 *       Name: AOT_SW
 *
 *     Module: debuginterface
 *      State: ready for general use.
 * Originator: Gillen
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/SAM7_Module/trunk/hal/trace/dbgu.c $
 *  $Revision: 16811 $
 *      $Date: 2014-08-26 10:51:03 +0200 (Di, 26 Aug 2014) $
 *    $Author: Speck $
 *
 *  Developed by Art of Technology AG, 2009
 *
 ****************************************************************************
 * Version History:
 *
 *
 *****************************************************************************/
/**
 * @file
 * Detailed description in header file
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//      Headers
//------------------------------------------------------------------------------

#include "trace.h"
#include <stddef.h> // use standard library for NULL type, e.g. for NRF framework
#include "board.h"

/*---------------------------- Global Variable ------------------------------*/
#ifndef TRACES_NO
TRACE_PrintFunctionType TRACE_mPrintFunction = NULL;	///< user printf() type function

//------------------------------------------------------------------------------
//      Local functions (helper)
//------------------------------------------------------------------------------

/**
 * TRACE_devNull
 * dummy (empty) printf() with no action which is used if user does not
 * specify a print function
 *
 * @param format, ...	same as printf()
 */
static uint32_t TRACE_devNull(const char *format, ...)
{
  //  intended do nothing
  return (0);
}

//------------------------------------------------------------------------------
//      Exported functions
//------------------------------------------------------------------------------

void TRACE_RegisterPrintFunction(TRACE_PrintFunctionType pPrintFunction)
{
	if (pPrintFunction != NULL)
	{
		TRACE_mPrintFunction = pPrintFunction;
	}
	else
	{
		TRACE_mPrintFunction = TRACE_devNull;
	}
}

//------------------------------------------------------------------------------

void TRACE_INIT(uint32_t mck, TRACE_PrintFunctionType pPrintFunction)
{
	TRACE_RegisterPrintFunction(pPrintFunction);
#if !defined(BRD_DBGU_MODE) || !defined(BRD_DBGU_BAUDRATE)
	// E.g. NRF is configured via SDK so it's save to skip
	#if !defined(NRF52)
		#warning TRACE_INIT: BRD_DBGU_MODE and/or BRD_DBGU_BAUDRATE missing, skipping configuration!
	#endif
#else
	DBGU_Configure(BRD_DBGU_MODE, BRD_DBGU_BAUDRATE, mck);
#endif
}

//------------------------------------------------------------------------------
#endif //TRACES_NO
/* eof */
