/****************************************************************************
 *
 *   Customer: AoT
 *   Project#: 00-00
 *       Name: AOT_SW
 *
 *     Module: trace.h
 *      State: ready for use
 * Originator: Thomas Gillen
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/trace/trace.h $
 *  $Revision: 24948 $
 *      $Date: 2018-06-01 16:31:36 +0200 (Fri, 01 Jun 2018) $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2009
 *
 ****************************************************************************
 * Version History:
 * 00.10	initial version derived from several ATMEL trace.h files
 * 			with own extensions
 * 00.20	Ready for general use
 * 00.21    Additional feature TR_PRINT to have uniform interface
 *
 ****************************************************************************
 */

#ifndef TRACE_H_
#define TRACE_H_

//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------
#include "dbgu.h"

//------------------------------------------------------------------------------
//         Definitions
//------------------------------------------------------------------------------

/**
 * Module dependent debug level definitions
 * required by module_config.h, trace_dbg_late_include.h and within modules using trace
 */
#define TR_DBG_NO   0   ///< no debug traces for the given module
#define TR_DBG_H    1   ///< high level debug traces for the given module
#define TR_DBG_M    2   ///< medium level debug traces for the given module
#define TR_DBG_L    3   ///< low level debug traces for the given module

//------------------------------------------------------------------------------

/** user defined printf() function for debug trace output
 *  TRACE_PRINT macro maps to this function pointer */
typedef uint32_t (*TRACE_PrintFunctionType)(const char *format, ...);

/** extern declaration or printf function pointer */
extern TRACE_PrintFunctionType TRACE_mPrintFunction;
//------------------------------------------------------------------------------
//      functions
//------------------------------------------------------------------------------

#ifdef TRACES_NO
  // Traces are disabled => tell preprocessor to remove
  #define TRACE_INIT(...)
  #define TRACE_RegisterPrintFunction(...)
  #define TRACE_PRINT(...)
#else
  // Traces are active

  /**
   * TRACE_INIT
   *
   * Initialize the trace hardware and register a function for trace printout
   * this function can be changed by calling TRACE_RegisterPrintFunction,
   * assigning a NULL pointer will disable the output
   * mode and baudrate are set via board.h:  BRD_DBGU_BAUDRATE / BRD_DBGU_MODE
   *
   * @param mck				master clock frequency in Hz
   *                        propose to use CLK_GetMCKspeed()
   * @param pPrintFunction	function of type printf() providing the
   *                        printout functionality
   */
  void TRACE_INIT(uint32_t mck, TRACE_PrintFunctionType pPrintFunction);

  //------------------------------------------------------------------------------

  /**
   * TRACE_RegisterPrintFunction
   *
   * Register a function for trace printout, assigning a NULL pointer
   * will disable the output
   *
   * @param pPrintFunction	function of type printf() providing the
   *                                printout functionality
   */
  void TRACE_RegisterPrintFunction(TRACE_PrintFunctionType pPrintFunction);

  //------------------------------------------------------------------------------

  /**
   * TRACE_PRINT
   *
   * This function call is mapped to the aot printout function
   * registered during TRACE_INIT or TRACE_RegisterPrintFunction
   * implemented as macro because va* functions not reentrant
   *
   * @param format, ...	same as printf()
   */
  #define TRACE_PRINT(...)	TRACE_mPrintFunction(__VA_ARGS__);
#endif

//------------------------------------------------------------------------------
#endif //#ifndef TRACE_H_
 // end of file
