/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: Generic application error handler for Nordic errors
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/util/app_error_aot.c $
 *  $Revision: 24948 $
 *      $Date: 2018-06-01 16:31:36 +0200 (Fri, 01 Jun 2018) $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * tbd
 *
 ****************************************************************************
 */

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stdint.h>

/* headers from other modules ---------------------------------------------*/
#include "app_error.h"
#include "dbgu.h"

/*--------------------------------------------------------------------------+
|  Macro                                                                    |
+--------------------------------------------------------------------------*/
#ifdef TR_DBG
	#ifdef TR_DBG_USE_RTT
		#define TRACE_DBG(...)      printf(__VA_ARGS__)
	#else
		#define TRACE_DBG(...)      aot_printf(__VA_ARGS__)
	#endif //TR_DBG_USE_RTT
#else
	#define TRACE_DBG(...)
#endif

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/**
 * Default application error handler which is called/used mainly by the
 * Nordic nRF modules. This function overrides the weak Nordic function.
 */
void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
	error_info_t* 	p_error_info;
	assert_info_t*	p_assert_info;

    switch (id)
    {
        case NRF_FAULT_ID_SDK_ERROR:
    		p_error_info = (error_info_t*) info;
    		aot_printf("ERROR: NRF_FAULT_ID_SDK_ERROR\n");
    		aot_printf("\terr_code: %lu\n", p_error_info->err_code);
    		aot_printf("\tline_num: %u\n", p_error_info->line_num);
    		if (p_error_info->p_file_name != NULL)
    			aot_printf("\tfile_name: %s\n", p_error_info->p_file_name);
    		else
    			aot_printf("\tfile_name: \n");
            break;

        case NRF_FAULT_ID_SDK_ASSERT:
    		p_assert_info = (assert_info_t*) info;
    		aot_printf("ERROR: NRF_FAULT_ID_SDK_ASSERT\n");
    		aot_printf("\tline_num: %u\n", p_assert_info->line_num);
    		if (p_assert_info->p_file_name != NULL)
				aot_printf("\tfile_name: %s\n", p_assert_info->p_file_name);
			else
				aot_printf("\tfile_name: \n");
    		aot_printf("\tfile_name: %s\n", p_assert_info->p_file_name);
            break;

        case 4097:
            aot_printf("ERROR: Writing Flash while BLE ON\n");
            aot_printf("pc=0x%08lx info=0x%08lx\n", pc, info);
            break;

        default:
            aot_printf("default app_error_fault_handler(), ID = %ld\n", id);
            aot_printf("pc=0x%08lx info=0x%08lx\n", pc, info);
    }
}

