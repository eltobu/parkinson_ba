/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: APP_ERROR_AOT_H
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/util/app_error_aot.h $
 *  $Revision: 25291 $
 *      $Date: 2018-07-02 09:33:57 +0200 (Mon, 02 Jul 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * This is the AoT application error header file.
 *
 ****************************************************************************
 */

#ifndef SHARE_AOT_NRF_UTIL_APP_ERROR_AOT_H_
#define SHARE_AOT_NRF_UTIL_APP_ERROR_AOT_H_

#include <string.h>
#include "nrf.h"

/*--------------------------------------------------------------------------+
|  macros			                                                  		|
+--------------------------------------------------------------------------*/

#define __FILENAME__ (strrchr(__FILE__,'/')+1)

/**@brief Macro for calling error handler function if supplied error code
 * 		  any other than NRF_SUCCESS and leaving the current function.
 *
 * @param[in] ERR_CODE Error code supplied to the error handler.
 */
#define RETURN_ON_NRF_ERROR(ERR_CODE)   \
		APP_ERROR_CHECK(ERR_CODE); 		\
        if (ERR_CODE != NRF_SUCCESS)    \
        {                               \
            return (ERR_CODE);          \
        }

/**@brief Macro for calling error handler function if supplied error code
 *        any other than NRF_SUCCESS and leaving the current function.
 *
 * @param[in] ERR_CODE Error code supplied to the error handler.
 */
#define RESET_ON_MALLOC_ERROR(ERR_CODE) \
        do \
        { \
            if (ERR_CODE != ERR_OK) \
            { \
                NVIC_SystemReset(); \
            } \
        } while (0)



#endif /* SHARE_AOT_NRF_UTIL_APP_ERROR_AOT_H_ */
