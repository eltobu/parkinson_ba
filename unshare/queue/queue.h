/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: GENERIC_QUEUE_QUEUE_H_
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/queue/queue.h $
 *  $Revision: 24160 $
 *      $Date: 2018-04-13 16:47:50 +0200 (Fri, 13 Apr 2018) $
 *    $Author: Schwinghammer $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */
/**
 * @file
 * This is the queue module header file.
 *
 ****************************************************************************
 */

#ifndef GENERIC_QUEUE_QUEUE_H_
#define GENERIC_QUEUE_QUEUE_H_

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stdint.h>

/* headers from other modules ---------------------------------------------*/

#define QUEUE_TAG_SIZE    4   // queue tag string size

/*--------------------------------------------------------------------------+
 |  types                                                                   |
 +--------------------------------------------------------------------------*/

/************************************************************************************//**
* @struct     __message_entry_t
* @brief      message entry
* @typedef    message_entry_t
* @see        __message_entry_t
****************************************************************************************/
typedef struct tagObjNode_t
{
    struct tagObjNode_t* next;        //!< pointer to next entry or NULL
    uint8_t au8_data[]; //!< space for the message itself
} ObjNode_t;

/************************************************************************************//**
* @struct     __message_head_t
* @brief      message head
* @details    message queue, where messages can be sent to or received from
* @typedef    message_head_t
* @see        __message_head_t
****************************************************************************************/
typedef struct tagQueue_t
{
  ObjNode_t* head;            //!< pointer to first entry or NULL
  ObjNode_t* tail;            //!< pointer to last entry or NULL
  char acTag[QUEUE_TAG_SIZE];
} Queue_t;

/*--------------------------------------------------------------------------+
 |  function prototypes                                                     |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**@brief Initialize the queue system.
 *
 * This function initializes the first entries of all defined queues.
 */
void queue_init(Queue_t* pQueue, char* pcTag);

/*--------------------------------------------------------------------------*/
/**@brief Clear a queue.
 *
 * @param[in]   queue   Queue to clear.
 */
void queue_clear(Queue_t* const pQueue);

/**
 * @brief Copy a queue.
 *        The queues keep their tag name / tag name is not copied.
 *
 * @param[in]   source      Queue to copy.
 * @param[out]  target      Target queue.
 * @retval      ERR_OK      Success.
 * @retval      ERR_ERROR   Error on copy. Target is empty.
 */
uint32_t queue_copy(const Queue_t * const pSource, Queue_t * const pTarget);

/*--------------------------------------------------------------------------*/
/**
 * @brief Move a queue.
 *        The queues keep their tag name / tag name is not moved.
 *
 * @param[in]   source      Queue to move.
 * @param[out]  target      Target queue.
 * @retval      ERR_OK      Success.
 * @retval      ERR_ERROR   When memory allocation on move operation failed.
 */
uint32_t queue_move(Queue_t* const pSource, Queue_t* const pTarget);

/*--------------------------------------------------------------------------*/
/**@brief Returns the number of objects stored in a queue.
 *
 * @param[in]   queue       Input queue.
 * @return      uint32_t    Number of objects in input queue.
 */
uint32_t queue_get_object_count(const Queue_t* const pQueue);

/*--------------------------------------------------------------------------*/
/**@brief Returns the data size of a queue, without the management header.
 *
 * @param[in]   queue       Input queue.
 * @return      uint32_t    Data length of queue.
 */
uint32_t queue_get_queue_length(const Queue_t * const pQueue);

/*--------------------------------------------------------------------------*/
/**@brief Returns the length of a given object.
 *
 * @param[in]   p_obj       Pointer to an object.
 * @return      uint16_t    Object data length.
 */
uint16_t queue_get_object_length(const void* const p_obj);

/*--------------------------------------------------------------------------*/
/**@brief Returns a pointer to the object at the beginning of the queue
 *        without removing it.
 *
 * @param[in]   queue       Queue to peek the object from.
 * @return      void*       Pointer to the object at the beginning of the queue.
 */
void* queue_peek(const Queue_t* const pQueue);

/*--------------------------------------------------------------------------*/
/**@brief Returns a pointer to an object at a specific position in the queue
 *        without removing it from the queue.
 *
 * @param[in]   queue       Queue to peek the object from.
 * @param[in]   obj_id      Id/Position of object in the queue.
 * @return      void*       Pointer to the object at the beginning of the queue.
 */
void* queue_peek_object_at_position(const Queue_t* const pQueue, const uint32_t obj_id);

/*--------------------------------------------------------------------------*/
/**@brief Returns a pointer to the object at the beginning of the queue and
 *        removes it from the queue (clear header).
 *        Memory is NOT cleared, thus this operation is safe until queue_clear()
 *        is called.
 *
 * ATTENTION: Object memory must be released with queue_release_object_memory()
 *            after message processing.
 *
 * @param[in]   queue       Queue to pop the object from.
 * @return      void*       Pointer to the object at the beginning of the queue.
 */
void* queue_pop(Queue_t* pQueue);

/*--------------------------------------------------------------------------*/
/**@brief Releases the memory of an object.
 *
 * @param[in]   p_obj       Pointer to an object.
 */
void queue_release_object_memory(const void* p_obj);

/*--------------------------------------------------------------------------*/
/**@brief Push an element to the tail of the queue.
 *        Returns an error code when something goes wrong.
 *
 * @param[in]   queue       Queue to push the object to.
 * @param[in]   p_obj       Pointer to an object.
 * @param[in]   length      Length of the object.
 * @retval      ERR_OK      Success.
 * @retval      ERR_ERROR   When memory allocation failed.
 */
uint32_t queue_push(Queue_t* const pQueue, const void* p_obj, const uint16_t length);

/*--------------------------------------------------------------------------*/
/**@brief Get queue name.
 *
 * @param[in]   queue       Queue to get name.
 * @return      void*       Pointer to queue name.
 */
const char * queue_get_name(Queue_t* pQueue);

/*--------------------------------------------------------------------------*/
/**@brief Trace a single queue if not empty.
 *
 * @param[in]   queue       Queue to trace.
 */
void queue_trace_queue(Queue_t* pQueue);


#endif /* GENERIC_QUEUE_QUEUE_H_ */
