/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: Queue
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/queue/queue.c $
 *  $Revision: 24160 $
 *      $Date: 2018-04-13 16:47:50 +0200 (Fri, 13 Apr 2018) $
 *    $Author: Schwinghammer $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */
/**
 * @file
 * Queue module based on the message module. Standard queue operations are
 * wrapped around the message module.
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "module_config.h"
#include "queue.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stddef.h>
#include <string.h>
#if (CFG_QUEUE_TRACE_QUEUE_USING_SPRINTF == 1)
#include <stdio.h>
#include <stdlib.h>
#endif

/* headers from other modules ---------------------------------------------*/
#include "error.h"
#include "memorymanagement.h"
#include "trace.h"

/*--------------------------------------------------------------------------+
 |  debugging and check configuration                                       |
 +--------------------------------------------------------------------------*/
#undef LOCAL_DBG
#ifndef CFG_DBG_QUEUE
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_QUEUE undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_QUEUE
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

#if (QUEUE_TAG_SIZE != MM_TAG_SIZE)
    #warning "Attention: the queue tag size doesn't match the memory block tag size! Ajusting one of them when using extended memory block headers."
#endif

/*--------------------------------------------------------------------------+
 |  constants                                                               |
 +--------------------------------------------------------------------------*/
#define QUEUE_SEARCH_TIMEOUT       250
#define QUEUE_SEARCH_TIMEOUT_END   0

/*--------------------------------------------------------------------------+
 |  types                                                                   |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  module variables                                                        |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  functions                                                               |
 +--------------------------------------------------------------------------*/

/**
 * @brief Initialize the queue system.
 *
 * This function initializes the first entries of all defined queues.
 */
void queue_init(Queue_t* pQueue, char* pcTag)
{
    TRACE_DBG_H("Initializing queue %s\n", pcTag);

    pQueue->head = NULL;
    pQueue->tail = NULL;
    memcpy(pQueue->acTag, pcTag, QUEUE_TAG_SIZE);
}

/**
 * @brief Clear a queue.
 *
 * @param[in]   queue   Queue to clear.
 */
void queue_clear(Queue_t* const pQueue)
{
    while (queue_peek(pQueue) != NULL)
    {
        queue_release_object_memory(queue_pop(pQueue));
    }
}

/**
 * @brief Copy a queue.
 *        The queues keep their tag name / tag name is not copied.
 *
 * @param[in]   source      Queue to copy.
 * @param[out]  target      Target queue.
 * @retval      ERR_OK      Success.
 * @retval      ERR_ERROR   Error on copy. Target is empty.
 */
uint32_t queue_copy(const Queue_t * const pSource, Queue_t * const pTarget)
{
    uint32_t i;
    void * p_obj;

    // make sure target queue is cleared
    queue_clear(pTarget);

    // copy
    if (queue_get_object_count(pSource) > 0)
    {
        for (i = 0; i < queue_get_object_count(pSource); i++)
        {
            p_obj = queue_peek_object_at_position(pSource, i);
            if (p_obj != NULL)
            {
                if (queue_push(pTarget, p_obj, queue_get_object_length(p_obj)) != ERR_OK)
                {
                    // error
                    queue_clear(pTarget);
                    return ERR_ERROR;
                }
            }
        }
    }

    return ERR_OK;
}

/**
 * @brief Move a queue.
 *        The queues keep their tag name / tag name is not moved.
 *
 * @param[in]   source      Queue to move.
 * @param[out]  target      Target queue.
 * @retval      ERR_OK      Success.
 * @retval      ERR_ERROR   When memory allocation on move operation failed.
 */
uint32_t queue_move(Queue_t * const pSource, Queue_t * const pTarget)
{
    // make sure target queue is cleared
    queue_clear(pTarget);

    // move head and tail from source to target
    pTarget->head = pSource->head;
    pTarget->tail = pSource->tail;
    pSource->head = NULL;
    pSource->tail = NULL;

    return ERR_OK;
}

/**
 * @brief Returns the number of objects stored in a queue.
 *
 * @param[in]   queue       Input queue.
 * @return      uint32_t    Number of objects in input queue.
 */
uint32_t queue_get_object_count(const Queue_t* const pQueue)
{

    ObjNode_t* p_node = pQueue->head;  // set object node to head
    uint32_t u32_count = 0;
    uint32_t u32_timeout = QUEUE_SEARCH_TIMEOUT;

    while ( (p_node != NULL) && (u32_timeout > QUEUE_SEARCH_TIMEOUT_END) )
    {
        u32_count++;
        p_node = p_node->next;  // get next node
        u32_timeout--;
    }

    return u32_count;
}

/**
 * @brief Returns the data size of a queue, without the management header.
 *
 * @param[in]   queue       Input queue.
 * @return      uint32_t    Data length of queue.
 */
uint32_t queue_get_queue_length(const Queue_t * const pQueue)
{
    uint32_t i;
    void * p_obj;
    uint32_t u32DataSize = 0;

    if (queue_get_object_count(pQueue) > 0)
    {
        for (i = 0; i < queue_get_object_count(pQueue); i++)
        {
            p_obj = queue_peek_object_at_position(pQueue, i);
            if (p_obj != NULL)
            {
                // get object length
                u32DataSize += queue_get_object_length(p_obj);
            }
        }
        return u32DataSize;
    }

    return 0;
}


/**
 * @brief Returns the length of a given object.
 *
 * @param[in]   p_obj       Pointer to an object.
 * @return      uint16_t    Object data length.
 */
uint16_t queue_get_object_length(const void* const p_obj)
{
    ObjNode_t* p_node = (ObjNode_t*)p_obj;
    p_node--;   // adjust to message header
    return (MM_getLength(p_node) - sizeof(ObjNode_t));
}

/**
 * @brief Returns a pointer to the object at the beginning of the queue
 *        without removing it.
 *
 * @param[in]   queue       Queue to peek the object from.
 * @return      void*       Pointer to the object at the beginning of the queue.
 */
void* queue_peek(const Queue_t* const pQueue)
{
    if (pQueue->head == NULL)
    {
        return NULL;
    }

    return ((ObjNode_t*)(pQueue->head))->au8_data;
}

/**
 * @brief Returns a pointer to an object at a specific position in the queue
 *        without removing it from the queue.
 *
 * @param[in]   queue       Queue to peek the object from.
 * @param[in]   obj_id      Id/Position of object in the queue.
 * @return      void*       Pointer to the object at the beginning of the queue.
 */
void* queue_peek_object_at_position(const Queue_t* const pQueue, const uint32_t obj_id)
{
    ObjNode_t* p_node = pQueue->head;
    uint32_t u32_timeout = QUEUE_SEARCH_TIMEOUT;
    uint32_t u32_count = 0;
    void * p_obj = NULL;

    while ( (p_node != NULL) && (u32_timeout > QUEUE_SEARCH_TIMEOUT_END) )
    {
        u32_timeout--;
        if (u32_count == obj_id)
        {
            p_obj = p_node->au8_data;
            u32_timeout = QUEUE_SEARCH_TIMEOUT_END;
        }
        p_node = p_node->next;
        u32_count++;
    }

    return p_obj;
}

/**
 * @brief Returns a pointer to the object at the beginning of the queue and
 *        removes it from the queue (clear header).
 *        Memory is NOT cleared, thus this operation is safe until queue_clear()
 *        is called.
 *
 * ATTENTION: Object memory must be released with queue_release_object_memory()
 *            after message processing.
 *
 * @param[in]   queue       Queue to pop the object from.
 * @return      void*       Pointer to the object at the beginning of the queue.
 */
void* queue_pop(Queue_t* pQueue)
{
    if (pQueue->head == NULL)
    {
        return NULL;
    }

    ObjNode_t* p_node = pQueue->head;

    // remove message from queue
    // update head
    pQueue->head = pQueue->head->next;

    if (pQueue->head == NULL)
    {
        pQueue->tail = NULL;
    }

    return p_node->au8_data;
}

/**
 * @brief Releases the memory of an object.
 *
 * @param[in]   p_obj       Pointer to an object.
 */
void queue_release_object_memory(const void* p_obj)
{
    //todo: check if p_obj is already cleared in the queue.
    /* If not, return error. -> not allowed to release the memory of an object
     * that is still part of the queue - in case it was peeked instead of
     * popped.
     */
    ObjNode_t* p_node = (ObjNode_t*)p_obj;
    p_node--;   // adjust to message header
    MM_free(p_node);

    //message_free(p_obj);
}

/**
 * @brief Push an element to the tail of the queue.
 *        Returns an error code when something goes wrong.
 *
 * @param[in]   queue       Queue to push the object to.
 * @param[in]   p_obj       Pointer to an object.
 * @param[in]   length      Length of the object.
 * @retval      ERR_OK      Success.
 * @retval      ERR_ERROR   Memory allocation failed.
 */
uint32_t queue_push(Queue_t* const pQueue, const void* p_obj, const uint16_t length)
{
    ObjNode_t* p_new_node;

    TRACE_DBG_M("queue_push()\n");

    // allocate memory for new node
    TRACE_DBG_M("Allocating memory for new node.. ");
#if (CFG_MM_SMALL_BLOCK_HEADER_MODE == 1)
    p_new_node = (ObjNode_t*)MM_malloc(sizeof(ObjNode_t) + length);
#endif
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
    p_new_node = (ObjNode_t*)MM_malloc(sizeof(ObjNode_t) + length, pQueue, pQueue->acTag);
#endif

    if (p_new_node == NULL)
    {
        // memory allocation failed
        TRACE_DBG_M("FAILED\n");
        return ERR_ERROR;
    }
    TRACE_DBG_M("OK\n");


    // initialize new node
    p_new_node->next = NULL;

    // copy data
    if(p_obj != NULL)
    {
        TRACE_DBG_M("Copy data to new node\n");
        memcpy(p_new_node->au8_data, (uint8_t*)p_obj, length);
    }

    // update tail->next if it is not the first entry
    if (pQueue->tail != NULL)
    {
        pQueue->tail->next = p_new_node;
    }
    // update tail
    pQueue->tail = p_new_node;

    /* head == tail if it is the first entry
     */

    // update head if it is the first entry
    if (pQueue->head == NULL)
    {
        pQueue->head = p_new_node;
    }

    TRACE_DBG_M("queue_push() return ERR_OK\n");

    return ERR_OK;
}

/**
 * @brief Get queue name.
 *
 * @param[in]   queue       Queue to get name.
 * @return      void*       Pointer to queue name.
 */
const char * queue_get_name(Queue_t* pQueue)
{
    return pQueue->acTag;
}

/**
 * @brief Trace a single queue if not empty.
 *
 * @param[in]   queue       Queue to trace.
 */
void queue_trace_queue(Queue_t* pQueue)
{
    uint32_t u32DataLength = 0;
    uint32_t i;
    void * p_obj;

    u32DataLength = queue_get_queue_length(pQueue);
    if (u32DataLength == 0)
    {
        // no data to print
        return;
    }

#if (CFG_QUEUE_TRACE_QUEUE_USING_SPRINTF == 1)
    char acTraceBuf[u32DataLength * 2 + 128]; // length * 2 as each uint8 is printed with 2 characters
    memset(acTraceBuf, 0, u32DataLength * 2 + 128);
#endif

    const char *pcQtag = queue_get_name(pQueue);

#if (CFG_QUEUE_TRACE_QUEUE_USING_SPRINTF == 1)
    sprintf(acTraceBuf + strlen(acTraceBuf), "%c%c%c%c:\n", pcQtag[0], pcQtag[1], pcQtag[2], pcQtag[3]);
#else
    TRACE_PRINT("%c%c%c%c:\n", pcQtag[0], pcQtag[1], pcQtag[2], pcQtag[3]);
#endif

    for (i = 0; i < queue_get_object_count(pQueue); i++)
    {
        p_obj = queue_peek_object_at_position(pQueue, i);
        if (p_obj != NULL)
        {
            for (uint32_t j = 0; j < queue_get_object_length(p_obj); j++)
            {
#if (CFG_QUEUE_TRACE_QUEUE_USING_SPRINTF == 1)
                sprintf(acTraceBuf + strlen(acTraceBuf), "%02x", *(uint8_t*)(p_obj + j));
#else
                TRACE_PRINT("%02x", *(uint8_t*)(p_obj + j));
#endif
            }
#if (CFG_QUEUE_TRACE_QUEUE_USING_SPRINTF == 1)
            sprintf(acTraceBuf + strlen(acTraceBuf), "\n");
#else
            TRACE_PRINT("\n");
#endif
        }
    }

#if (CFG_QUEUE_TRACE_QUEUE_USING_SPRINTF == 1)
    TRACE_PRINT(acTraceBuf);
#endif
}

