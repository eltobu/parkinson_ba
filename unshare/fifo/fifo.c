/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 16-01
 *       Name: Generic Library
 *
 *     Module: FIFO
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/fifo/fifo.c $
 *  $Revision: 25386 $
 *      $Date: 2018-07-06 13:59:12 +0200 (Fri, 06 Jul 2018) $
 *    $Author: Schwinghammer $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "fifo.h"

/* standard-headers (ANSI) ------------------------------------------------*/

/* headers from other modules ---------------------------------------------*/
#include "module_config.h"
#include "trace.h"

#ifdef CFG_MODULE_FIFO_TESTS_ACTIVE
#include <assert.h>
#endif

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_FIFO
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_FIFO undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_FIFO
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  local helper functions                                                   |
+--------------------------------------------------------------------------*/

/**
 * @brief Calculate FIFO increment
 *
 * @details Calculates next FIFO position based on current position and maximum size.
 *
 * @param[in] iFIFOpos  Actual FIFO position
 * @param[in] iMaxFifo  Size of FIFO (ArraySize-1)
 *
 * @retval  New FIFO position
 */
int incFifo (int iFIFOpos, int iMaxFIFO)
{
    if (iFIFOpos == iMaxFIFO)
    {
        iFIFOpos = 0x00;
    }
    else
    {
        iFIFOpos++;
    }
    return (iFIFOpos);
}
/*--------------------------------------------------------------------------*/

/**
 * @brief Get amount of free storage in FIFO.
 *
 * @details Calculates free space in FIFO.
 *
 * @param[in] iFIFOin   Position of FIFOin
 * @param[in] iFIFOout  Position of FIFOout
 * @param[in] iMAXFifo  Size of FIFO (ArraySize-1)
 *
 * @retval  Amount of free space in FIFO
 */
int fifofree (int iFIFOin, int iFIFOout, int iMaxFIFO)
{
    if (iFIFOin < iFIFOout)
    {
        return (iFIFOout - iFIFOin);
    }
    else
    {
        if (iFIFOin == iFIFOout)
        {
            // ACHTUNG max + 1 muss gemacht werden, da die 0 ebenfalls ein Speicherplatz ist
            return (iMaxFIFO + 1);
        }
        else
        {
            // ACHTUNG max + 1 muss gemacht werden, da die 0 ebenfalls ein Speicherplatz ist
            return ((iMaxFIFO + 1) - (iFIFOin - iFIFOout));
        }
    }
    return 0;
} // End of fifofree
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

// Status Abfragen vom FIFO
int fifo_Status (int iS, int iFIFOin, int iFIFOout, int iMAXFifo)
{
    switch (iS)
    {
        case DATEN_IM_FIFO:						// Gibt zur�ck, wieviel Daten im Fifo sind
            return (iMAXFifo + 1) - fifofree (iFIFOin, iFIFOout, iMAXFifo);
        break;
        case DATEN_FREE_IM_FIFO:				// Gibt zur�ck, wieviel Bytes noch frei sind
            return (fifofree (iFIFOin, iFIFOout, iMAXFifo) - 1);
        break;
    }
    return 0;
} // End of fifo_Status
/*--------------------------------------------------------------------------*/

// Daten vom Fifo holen
int fifo_POP (char *pFifo, int *pFIFOout, int iMaxFIFO)
{
    char cX;
    // Werte muessen nun vom FIFO geladen werden
    cX = pFifo[*pFIFOout];

    // FIFO Counter Berechnen
    *pFIFOout = incFifo(*pFIFOout, iMaxFIFO);

    return (cX);
} // End of fifo_POP
/*--------------------------------------------------------------------------*/


// Daten in den Fifo kopieren
void fifo_PUSH (char *pFifo, int *pFIFOin, int iMaxFIFO, char cX)
{
    if (pFifo != NULL)
    {
        // Werte die uebergeben wurden, muessen nun in den FIFO geladen werden
        pFifo[*pFIFOin] = cX;

        // FIFO Counter Berechnen
        *pFIFOin = incFifo (*pFIFOin, iMaxFIFO);
    }
} // End of fifo_PUSH
/*--------------------------------------------------------------------------*/

#ifdef CFG_MODULE_FIFO_TESTS_ACTIVE
// FIFO test
void FIFO_testMacros(void)
{
#define TST_BUFF_SIZE 4
    uint8_t u8In  = 0;
    uint8_t u8Out  = 0;
    uint32_t u32Buffer[TST_BUFF_SIZE+1];
    uint32_t u32Data = 0;
    uint32_t u32Size = 0;
    uint32_t u32I = 0;
    bool bIsFull = 0;

    TRACE_PRINT("Start FIFO test\n");
    TRACE_PRINT("Single element...\n");

    FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("FREE: %d\n", u32Size);

    assert(u32Size == TST_BUFF_SIZE); // Buffer is empty

    FIFO_OCCUPIED(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("OCCUPIED: %d\n", u32Size);

    assert(u32Size == 0); // Buffer is empty

    // Add one element
    FIFO_PUSH(u32Buffer, u8In, TST_BUFF_SIZE, 0xDEADBEEF);
    TRACE_PRINT("PUSH\n");

    FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("FREE: %d\n", u32Size);

    assert(u32Size == 3); // One element in buffer

    FIFO_OCCUPIED(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("OCCUPIED: %d\n", u32Size);

    // Get one element without removing it
    FIFO_PEEK(u32Buffer, u8Out, u32Data);
    TRACE_PRINT("PEEK: 0x%08X\n", u32Data);

    FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    assert(u32Size == 3); // Still one element in buffer

    // Get one element and remove it
    FIFO_POP(u32Buffer, u8Out, TST_BUFF_SIZE, u32Data);
    TRACE_PRINT("POP: 0x%08X\n", u32Data);

    FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("FREE: %d\n", u32Size);

    assert(u32Size == TST_BUFF_SIZE); // Buffer is empty

    FIFO_OCCUPIED(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("OCCUPIED: %d\n", u32Size);

    FIFO_IS_FULL(u8In, u8Out, TST_BUFF_SIZE, bIsFull);
    TRACE_PRINT("IS_FULL: %d\n", bIsFull);

    assert(!bIsFull); // Buffer is empty (4)

    TRACE_PRINT("Two element...\n");

    // Add two elements to buffer
    for (u32I=0; u32I<2; u32I++)
    {
        FIFO_PUSH(u32Buffer, u8In, TST_BUFF_SIZE, u32I);
        TRACE_PRINT("PUSH\n");
    }

    FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("FREE: %d\n", u32Size);

    assert(u32Size == 2); // Two elements in buffer free

    FIFO_OCCUPIED(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("OCCUPIED: %d\n", u32Size);

    assert(u32Size == 2); // Two elements in buffer

    FIFO_IS_FULL(u8In, u8Out, TST_BUFF_SIZE, bIsFull);
    TRACE_PRINT("IS_FULL: %d\n", bIsFull);

    assert(!bIsFull); // Buffer is not full (2)

    // Get and remove two elements from buffer
    for (u32I=0; u32I<2; u32I++)
    {
        FIFO_POP(u32Buffer, u8Out, TST_BUFF_SIZE, u32Data);
        TRACE_PRINT("POP: 0x%08X\n", u32Data);
    }

    FIFO_IS_FULL(u8In, u8Out, TST_BUFF_SIZE, bIsFull);
    TRACE_PRINT("IS_FULL: %d\n", bIsFull);

    assert(!bIsFull); // Buffer is not full (4)

    TRACE_PRINT("Four element...\n");

    // Add four elements to buffer
    for (u32I=0; u32I<TST_BUFF_SIZE; u32I++)
    {
        FIFO_PUSH(u32Buffer, u8In, TST_BUFF_SIZE, u32I);
        TRACE_PRINT("PUSH\n");
    }

    FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("FREE: %d\n", u32Size);

    assert(u32Size == 0); // Zero elements in buffer free

    FIFO_OCCUPIED(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("OCCUPIED: %d\n", u32Size);

    assert(u32Size == TST_BUFF_SIZE); // Four elements in buffer

    FIFO_IS_FULL(u8In, u8Out, TST_BUFF_SIZE, bIsFull);
    TRACE_PRINT("IS_FULL: %d\n", bIsFull);

    assert(bIsFull); // Buffer is full (4)

    // Remove four elements from buffer
    for (u32I=0; u32I<TST_BUFF_SIZE; u32I++)
    {
        FIFO_POP(u32Buffer, u8Out, TST_BUFF_SIZE, u32Data);
        TRACE_PRINT("POP: 0x%08X\n", u32Data);
    }

    FIFO_IS_FULL(u8In, u8Out, TST_BUFF_SIZE, bIsFull);
    TRACE_PRINT("IS_FULL: %d\n", bIsFull);

    assert(!bIsFull); // Buffer is not full (4)

    // Test Buffer overflow
    TRACE_PRINT("Test FIFO overflow\n");

    // Add 6 elements to buffer
    for (u32I=0; u32I<6; u32I++)
    {
        FIFO_PUSH(u32Buffer, u8In, TST_BUFF_SIZE, u32I);
        TRACE_PRINT("PUSH\n");
    }

    FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("FREE: %d\n", u32Size);

    assert(u32Size == 3); // Four elements in buffer free

    FIFO_OCCUPIED(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("OCCUPIED: %d\n", u32Size);

    assert(u32Size == 1); // One elements in buffer

    FIFO_IS_FULL(u8In, u8Out, TST_BUFF_SIZE, bIsFull);
    TRACE_PRINT("IS_FULL: %d\n", bIsFull);

    assert(!bIsFull); // Buffer is full (4)

    // Remove 5 elements from buffer
    for (u32I=0; u32I<5; u32I++)
    {
        FIFO_POP(u32Buffer, u8Out, TST_BUFF_SIZE, u32Data);
        TRACE_PRINT("POP: 0x%08X\n", u32Data);
    }

    FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("FREE: %d\n", u32Size);

    assert(u32Size == 3); // Four elements in buffer free

    FIFO_OCCUPIED(u8In, u8Out, TST_BUFF_SIZE, u32Size);
    TRACE_PRINT("OCCUPIED: %d\n", u32Size);

    assert(u32Size == 1); // One elements in buffer

    TRACE_PRINT("End FIFO test\n");
}
/*--------------------------------------------------------------------------*/
#endif
