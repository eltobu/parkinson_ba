/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 16-01
 *       Name: Generic Library
 *
 *     Module: FIFO
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/fifo/fifo.h $
 *  $Revision: 24955 $
 *      $Date: 2018-06-04 10:49:43 +0200 (Mon, 04 Jun 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Module FIFO: This module contains functions and macros to work with circular buffers
 *
 * ATTENTION:   It's by design intended to have very slim functions without much of error checking (Buffer overflow!)
 *              Be aware of what you are doing and choose the buffer size wisely.
 *
 * NOTE:        Size of the buffer must be +1 larger then the size provided to the macros/functions (FIFOmax)
 *              This module is interrupt safe as long as there is only one producer and only one consumer.
 *
 * HOWTO USE:   Example as BLE RX FIFO
 *
 * #define CONFIG_BLE_FIFO_RX_FIFO_SIZE 1100
 *
 * char    BLE_Rx_Fifo[CONFIG_BLE_FIFO_RX_FIFO_SIZE + 1];  // Empfangsbuffer
 * int     BLE_Rx_FIFOin = 0;                  // Input Zaehler
 * int     BLE_Rx_FIFOout = 0;                 // Output Zaehler
 *
 * void ble_fifo_clear_rx_fifo(void)
 *    {
 *        BLE_Rx_FIFOin = 0;
 *        BLE_Rx_FIFOout = 0;
 *    }
 *
 * char ble_fifo_pop_rx_fifo(void)
 *    {
 *        return fifo_POP(BLE_Rx_Fifo, &BLE_Rx_FIFOout, CONFIG_BLE_FIFO_RX_FIFO_SIZE);
 *    }
 *
 * void ble_fifo_push_rx_fifo(char x)
 *    {
 *        fifo_PUSH (BLE_Rx_Fifo, &BLE_Rx_FIFOin, CONFIG_BLE_FIFO_RX_FIFO_SIZE, x);
 *    }
 *
 * int ble_fifo_get_status_rx_fifo(int s)
 *    {
 *        return fifo_Status (s, BLE_Rx_FIFOin, BLE_Rx_FIFOout, CONFIG_BLE_FIFO_RX_FIFO_SIZE);
 *    }
 *
 * HOWTO USE MACROS:
 *
 * #define TST_BUFF_SIZE 4
 * uint8_t u8In  = 0;
 * uint8_t u8Out  = 0;
 * uint32_t u32Buffer[TST_BUFF_SIZE+1];
 * uint32_t u32Size = 0;
 *
 * // Get free space in FIFO
 * FIFO_FREE(u8In, u8Out, TST_BUFF_SIZE, u32Size);
 *
 * // Get occupied space in FIFO
 * FIFO_OCCUPIED(u8In, u8Out, TST_BUFF_SIZE, u32Size);
 *
 * // Add one element
 * FIFO_PUSH(u32Buffer, u8In, TST_BUFF_SIZE, 0xDEADBEEF);
 *
 * // Get one element without removing it
 * FIFO_PEEK(u32Buffer, u8Out, u32Data);
 *
 * // Get one element and remove it
 * FIFO_POP(u32Buffer, u8Out, TST_BUFF_SIZE, u32Data);
 *
 ****************************************************************************
 */

#ifndef FIFO_H
#define FIFO_H

/* Includes needed by this header  ----------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/* module configuration ---------------------------------------------------*/
#include "module_config.h"


#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global defines                                                           |
+--------------------------------------------------------------------------*/
/**
 * @brief Get an element from the FIFO without removing it.
 *
 * @details Function will get one element from buffer without removing it. It does not check if buffer is empty.
 *
 * @param Fifo[]: Name of the array
 * @param FIFOout: Location of the element to return.
 * @param Value: Variable to assign the value from the fifo.
 */
#define FIFO_PEEK(Fifo, FIFOout, Value) { \
    Value = Fifo[FIFOout];}

/**
 * @brief Get and remove one element from the FIFO.
 *
 * @details Function will remove one element from buffer. It does not check if buffer is empty.
 *
 * @param Fifo[]: Name of the array
 * @param FIFOout: Location of the element to return.
 * @param FIFOmax: Fifo size.
 * @param Value: Variable to assign the value from the fifo.
 */
#define FIFO_POP(Fifo, FIFOout, FIFOmax, Value) { \
    Value = Fifo[FIFOout]; \
    if (FIFOout == FIFOmax) FIFOout = 0; \
    else FIFOout++;}

/**
 * @brief Put one element into the FIFO.
 *
 * @details Function will add one element to buffer. It does not check if buffer is full.
 *
 * @param Fifo[]: Name of the array
 * @param FIFOin: Location of the new element.
 * @param FIFOmax: Fifo size.
 * @param Value: Value to put in the fifo.
 */
#define FIFO_PUSH(Fifo, FIFOin, FIFOmax, Value) { \
    Fifo[FIFOin] = Value; \
    if (FIFOin == FIFOmax) FIFOin = 0; \
    else FIFOin++;}

/**
 * @brief Get amount of free storage in FIFO.
 *
 * @param FIFOin: Position of input element.
 * @param FIFOout: Position of output element.
 * @param FIFOmax: Fifo size.
 * @param Value: Variable to assign amount of free space.
 */
#define FIFO_FREE(FIFOin, FIFOout, FIFOmax, Value) { \
    if (FIFOin < FIFOout) Value = ((int) FIFOout - FIFOin - 1); \
    else { if (FIFOin == FIFOout) Value = FIFOmax; else Value = ((int) FIFOmax - (FIFOin - FIFOout));};}

/**
 * @brief Get amount of occupied storage in FIFO.
 *
 * @param FIFOin: Position of input element.
 * @param FIFOout: Position of output element.
 * @param FIFOmax: Fifo size.
 * @param Value: Variable to assign amount of occupied space.
 */
#define FIFO_OCCUPIED(FIFOin, FIFOout, FIFOmax, Value) { \
    if (FIFOin < FIFOout) Value = ((int) (FIFOmax + 1) - (FIFOout - FIFOin)); \
    else { if (FIFOin == FIFOout) Value = ((int) (FIFOmax + 1) - (FIFOmax + 1)); else Value = (FIFOin - FIFOout);};}

/**
 * @brief Get amount of occupied storage in FIFO.
 *
 * @param FIFOin: Position of input element.
 * @param FIFOout: Position of output element.
 * @param FIFOmax: Fifo size.
 * @param Value[bool]: Variable to assign status (true/false).
 */
#define FIFO_IS_FULL(FIFOin, FIFOout, FIFOmax, Value) { \
    if (FIFOin < FIFOout) Value = (FIFOmax == ((int) (FIFOmax + 1) - (FIFOout - FIFOin))); \
    else { if (FIFOin == FIFOout) Value = (FIFOmax == ((int) (FIFOmax + 1) - (FIFOmax + 1))); else Value = (FIFOmax == (FIFOin - FIFOout));};}

// Statusabfrage fuer:
#define DATEN_IM_FIFO               0x01
#define DATEN_FREE_IM_FIFO          0x02

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/

/**
 * @brief Get status of fifo
 *
 * @details Get amount of memory free or occupied dependent on supplied argument.
 *
 * @param[in] iS        DATEN_IM_FIFO or DATEN_FREE_IM_FIFO
 * @param[in] iFIFOin   Position of FIFOin
 * @param[in] iFIFOout  Position of FIFOout
 * @param[in] iMAXFifo  Size of FIFO (ArraySize-1)
 *
 * @retval  Memory amount as requested by argument
 */
int fifo_Status (int iS, int iFIFOin, int iFIFOout, int iMAXFifo);

/*-------------------------------------------------------------------------*/

/**
 * @brief Get and remove one element from the FIFO.
 *
 * @details Function will remove one element from buffer. It does not check if buffer is empty.
 *
 * @param[in] pFifo     Pointer to FIFO array
 * @param[in] iFIFOout  Position of FIFOout
 * @param[in] iMAXFifo  Size of FIFO (ArraySize-1)
 *
 * @retval  Value at actual position in FIFO
 */
 int fifo_POP (char *pFifo, int *pFIFOout, int iMaxFIFO);

/*-------------------------------------------------------------------------*/

/**
 * @brief Put one element into the FIFO.
 *
 * @details Function will add one element to buffer. It does not check if buffer is full.
 *
 * @param[in] pFifo     Pointer to FIFO array
 * @param[in] iFIFOout  Position of FIFOout
 * @param[in] iMAXFifo  Size of FIFO (ArraySize-1)
 * @param[in] cX        Value to put in FIFO
 */
 void fifo_PUSH (char *pFifo, int *pFIFOin, int iMaxFIFO, char cX);

/*-------------------------------------------------------------------------*/

/**
 * @brief FIFO Macro test
 *
 * @details Tests the FIFO macros
 */
#ifdef CFG_MODULE_FIFO_TESTS_ACTIVE
void FIFO_testMacros(void);
#endif

/*-------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // FIFO_H

