/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: GPIO driver
 *      State: reviewed
 * Originator: Leuenberger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/gpio/gpio.c $
 *  $Revision: 21538 $
 *      $Date: 2017-11-27 17:55:25 +0100 (Mon, 27 Nov 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/** @file
 ****************************************************************************
 * @brief detailed description in header file
 *
 ***************************************************************************/


/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/
#include "gpio.h"
#include "nrf_gpio.h"
#include "app_gpiote.h"

/* headers from other modules ---------------------------------------------*/
#include "error.h"
#include "module_config.h"

/*--------------------------------------------------------------------------+
 | check configuration                                                      |
 +--------------------------------------------------------------------------*/
// make necessary structures as small as possible, use typical if not defined in module_config.h
#ifndef CFG_GPIO_MAX_FUNCTIONS
    #define CFG_GPIO_MAX_FUNCTIONS 0
#endif

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/
/**
 Structure to handle internal function list.
 */
typedef struct tag_FunctionInfo {
    GPIO_CallbackType Function;    ///< address of an active function
    app_gpiote_user_id_t user_id;  ///< internal user id
} FunctionInfo_t;

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/
/**
 List of functions
 */
static FunctionInfo_t registeredFunctions[CFG_GPIO_MAX_FUNCTIONS];
static uint8_t registeredFunctionsCount;

/*--------------------------------------------------------------------------+
|  local helper functions                                                   |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

void GPIO_init() {
#if CFG_GPIO_MAX_FUNCTIONS != 0
		APP_GPIOTE_INIT(CFG_GPIO_MAX_FUNCTIONS);
		registeredFunctionsCount = 0;
#endif
}

void GPIO_pinWrite(uint32_t PinNumber, uint32_t Value)
{
	nrf_gpio_pin_write(PinNumber, Value);
}

/*-------------------------------------------------------------------------*/

uint32_t GPIO_pinRead(uint32_t PinNumber)
{
	return nrf_gpio_pin_read(PinNumber);
}

/*-------------------------------------------------------------------------*/

void GPIO_pinSet(uint32_t PinNumber)
{
	nrf_gpio_pin_set(PinNumber);
}

/*-------------------------------------------------------------------------*/

void GPIO_pinClear(uint32_t PinNumber)
{
	nrf_gpio_pin_clear(PinNumber);
}

/*-------------------------------------------------------------------------*/

void GPIO_pinToggle(uint32_t PinNumber)
{
	nrf_gpio_pin_toggle(PinNumber);
}

/*-------------------------------------------------------------------------*/

void GPIO_pinConfigInput(uint32_t PinNumber, GPIO_PinPull_t PullConfig)
{
    nrf_gpio_pin_pull_t PullUp;
    switch (PullConfig)
    {
    case GPIO_PIN_NOPULL:
        PullUp = NRF_GPIO_PIN_NOPULL;
        break;
    case GPIO_PIN_PULLDOWN:
        PullUp = NRF_GPIO_PIN_PULLDOWN;
        break;
    case GPIO_PIN_PULLUP:
        PullUp = NRF_GPIO_PIN_PULLUP;
        break;
    default:
        while(1); // What the hell is here configured
    }
	nrf_gpio_cfg_input(PinNumber, PullUp);
}

/*-------------------------------------------------------------------------*/

void GPIO_pinConfigOutput(uint32_t PinNumber)
{
	nrf_gpio_cfg_output(PinNumber);
}

/*-------------------------------------------------------------------------*/

void GPIO_pinConfigDefault(uint32_t PinNumber)
{
	nrf_gpio_cfg_default(PinNumber);
}

/*-------------------------------------------------------------------------*/

uint32_t GPIO_pinConfigInputWithCallback(uint32_t PinNumber, GPIO_PinPull_t PullConfig, GPIO_PinEvent_t PinEvent, GPIO_CallbackType Function) {
	if (registeredFunctionsCount >= CFG_GPIO_MAX_FUNCTIONS)
		return ERR_ERROR;

	registeredFunctions[registeredFunctionsCount].Function = Function;

	uint32_t rising, falling;
	switch (PinEvent) {
	case GPIO_PIN_RISING:
		rising = (1 << PinNumber);
		falling = 0;
		break;

	case GPIO_PIN_FALLING:
		rising = 0;
		falling = (1 << PinNumber);
		break;

	default:
		rising = 0;
		falling = 0;
		break;
	}
	uint32_t status;
	status = app_gpiote_user_register(&registeredFunctions[registeredFunctionsCount].user_id,
			&rising, &falling, (app_gpiote_event_handler_t) registeredFunctions[registeredFunctionsCount].Function);
	APP_ERROR_CHECK(status);
	if (status != NRF_SUCCESS)
		return ERR_ERROR;

	nrf_gpio_pin_pull_t PinPull;
	switch (PullConfig)	{
	case GPIO_PIN_NOPULL:
		PinPull = NRF_GPIO_PIN_NOPULL;
		break;
	case GPIO_PIN_PULLDOWN:
		PinPull = NRF_GPIO_PIN_PULLDOWN;
		break;
	case GPIO_PIN_PULLUP:
		PinPull = NRF_GPIO_PIN_PULLUP;
		break;
	default:
		while(1); // What the hell is here configured
	}
	NRF_GPIO_Type * reg = nrf_gpio_pin_port_decode(&PinNumber);
	reg->PIN_CNF[PinNumber] &= ~GPIO_PIN_CNF_PULL_Msk;
	reg->PIN_CNF[PinNumber] |= (PinPull << GPIO_PIN_CNF_PULL_Pos);

	status = app_gpiote_user_enable(registeredFunctions[registeredFunctionsCount].user_id);
	APP_ERROR_CHECK(status);
	if (status != NRF_SUCCESS)
		return ERR_ERROR;

	++registeredFunctionsCount;

	return ERR_OK;
}

/*-------------------------------------------------------------------------*/
