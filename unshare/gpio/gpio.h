/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: GPIO driver
 *      State: reviewed
 * Originator: Leuenberger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/gpio/gpio.h $
 *  $Revision: 21538 $
 *      $Date: 2017-11-27 17:55:25 +0100 (Mon, 27 Nov 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/** @file
 ****************************************************************************
 * @brief 	This module contains functions to control the GPIOs of the nRF
 * 			devices.
 *
 ***************************************************************************/


#ifndef GPIO_H
#define GPIO_H

/* Includes needed by this header  ----------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global defines                                                           |
+--------------------------------------------------------------------------*/

/**
 * @brief Enumerator used for selecting the pin to be pulled down or up at
 * the time of pin configuration.
 */
typedef enum
{
    GPIO_PIN_NOPULL,    ///<  Pin pull-up resistor disabled.
    GPIO_PIN_PULLDOWN,  ///<  Pin pull-down resistor enabled.
    GPIO_PIN_PULLUP,    ///<  Pin pull-up resistor enabled.
} GPIO_PinPull_t;

/**
 * @brief Enumerator used for selecting the pin to be pulled down or up at
 * the time of pin configuration.
 */
typedef enum
{
    GPIO_PIN_RISING,    ///<  Pin event from low to high.
    GPIO_PIN_FALLING,   ///<  Pin event from high to low.
} GPIO_PinEvent_t;


/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/
/**
 *
 * Callback, a function that is called when the specified pin event occurs
 *
 * */
typedef void (*GPIO_CallbackType)(uint32_t* event_pins_rising, uint32_t* event_pins_falling);


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/

/**
 * @brief Function to initialize the GPIO module
 *
 * @param MaxInterruptUsers Pointer to the PWM context structure
 */
void GPIO_init();

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for writing a value to a GPIO pin.
 *
 * Note that the pin must be configured as an output for this
 * function to have any effect.
 *
 * @param PinNumber Specifies the pin number to write.
 *
 * @param Value Specifies the value to be written to the pin.
 * @arg 0 Clears the pin.
 * @arg >=1 Sets the pin.
 */
void GPIO_pinWrite(uint32_t PinNumber, uint32_t Value);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for reading the input level of a GPIO pin.
 *
 * Note that the pin must have input connected for the value
 * returned from this function to be valid.
 *
 * @param PinNumber Specifies the pin number to read.
 *
 * @return 0 if the pin input level is low. Positive value if the pin is high.
 */
uint32_t GPIO_pinRead(uint32_t PinNumber);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for setting a GPIO pin.
 *
 * Note that the pin must be configured as an output for this function
 * to have any effect.
 *
 * @param PinNumber Specifies the pin number to set.
 */
void GPIO_pinSet(uint32_t PinNumber);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for clearing a GPIO pin.
 *
 * Note that the pin must be configured as an output for this
 * function to have any effect.
 *
 * @param PinNumber Specifies the pin number to clear.
 */
void GPIO_pinClear(uint32_t PinNumber);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for toggling a GPIO pin.
 *
 * Note that the pin must be configured as an output for this
 * function to have any effect.
 *
 * @param PinNumber Specifies the pin number to toggle.
 */
void GPIO_pinToggle(uint32_t PinNumber);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for configuring the given GPIO pin number as input,
 *        hiding inner details.
 *        This function can be used to configure a pin as simple input.
 *
 * @param PinNumber  Specifies the pin number.
 * @param PullConfig State of the pin range pull resistor
 *                   (no pull, pulled down, or pulled high).
 *
 * @note  Sense capability on the pin is disabled and input is connected to
 *        buffer so that the GPIO->IN register is readable.
 */
void GPIO_pinConfigInput(uint32_t PinNumber, GPIO_PinPull_t PullConfig);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for configuring the given GPIO pin number as output,
 *        hiding inner details.
 *        This function can be used to configure a pin as simple output
 *        with gate driving GPIO_PIN_CNF_DRIVE_S0S1 (normal cases).
 *
 * @param PinNumber Specifies the pin number.
 *
 * @note  Sense capability on the pin is disabled and input is disconnected
 *        from the buffer as the pins are configured as output.
 */
void GPIO_pinConfigOutput(uint32_t PinNumber);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for resetting pin configuration to its default state.
 *
 * @param PinNumber Specifies the pin number.
 *
 * @note  Pin is disconnected in order to safe power
 */
void GPIO_pinConfigDefault(uint32_t PinNumber);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function for configuring the given GPIO pin number as input
 *        and registering a callback function for a pin event
 *
 * @param PinNumber Specifies the pin number.
 * @param PullConfig State of the pin range pull resistor
 *                   (no pull, pulled down, or pulled high).
 * @param PinEvent Wether rising or falling edge triggers event.
 * @param Function Address of the callback function
 */
uint32_t GPIO_pinConfigInputWithCallback(uint32_t PinNumber, GPIO_PinPull_t PullConfig, GPIO_PinEvent_t PinEvent, GPIO_CallbackType Function);

/*-------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // GPIO_H
