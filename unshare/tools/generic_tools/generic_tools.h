/*
 * generic_tools.h
 *
 *  Created on: 13.03.2018
 *      Author: VM
 */

#ifndef SUBSYS_TOOLS_GENERIC_TOOLS_H_
#define SUBSYS_TOOLS_GENERIC_TOOLS_H_

#include <stdint.h>

uint16_t GENT_bswap_16(uint16_t u16Val);
uint32_t GENT_bswap_32(uint32_t u32Value);

/**
 * @brief Traces an array. Optionally with a title.
 *
 * @param[in] pau8Data  Pointer to data.
 * @param[in] u16Length Length of data to trace.
 * @param[in] title     Pointer to a title string. Set NULL when not used.
 */
void GENT_traceArray(const uint8_t * const pau8Data,
                     const uint16_t u16Length,
                     const char * const title);


#endif /* SUBSYS_TOOLS_GENERIC_TOOLS_H_ */
