/*
 * generic_tools.c
 *
 *  Created on: 13.03.2018
 *      Author: VM
 */

/* module configuration ---------------------------------------------------*/
#include "module_config.h"

/* headers for this module ------------------------------------------------*/
#include "generic_tools.h"

/* standard-headers (ANSI) ------------------------------------------------*/
#include <stddef.h>
#if (CFG_GENT_TRACE_ARRAY_USES_SPRINTF_ENABLE == 1)
#include <stdio.h>
#include <string.h>
#endif

/* headers from other modules ---------------------------------------------*/
#include "trace.h"

/*--------------------------------------------------------------------------+
 |  debugging and check configuration                                       |
 +--------------------------------------------------------------------------*/
#undef LOCAL_DBG
#ifndef CFG_GENT_TRACE_LEVEL
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_GENT_TRACE_LEVEL undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_GENT_TRACE_LEVEL
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 |  functions                                                               |
 +--------------------------------------------------------------------------*/

uint16_t GENT_bswap_16(uint16_t u16Val)
{
    uint16_t u16result = 0;

    u16result |= (u16Val & 0x00FF) << 8;
    u16result |= (u16Val & 0xFF00) >> 8;

    return u16result;
}

uint32_t GENT_bswap_32(uint32_t u32Value)
{
    uint32_t u32result = 0;

    u32result |= (u32Value & 0x000000FF) << 24;
    u32result |= (u32Value & 0x0000FF00) << 8;
    u32result |= (u32Value & 0x00FF0000) >> 8;
    u32result |= (u32Value & 0xFF000000) >> 24;

    return u32result;
}

/**
 * @brief Traces an array. Optionally with a title.
 *
 * @param[in] pau8Data  Pointer to data.
 * @param[in] u16Length Length of data to trace.
 * @param[in] title     Pointer to a title string. Set NULL when not used.
 */
void GENT_traceArray(const uint8_t * const pau8Data,
                     const uint16_t u16Length,
                     const char * const title)
{
#if (CFG_GENT_TRACE_ARRAY_USES_SPRINTF_ENABLE == 1)

    char acTraceBuf[u16Length * 2 + 128]; // length * 2 as each uint8 is printed with 2 characters
    memset(acTraceBuf, 0, u16Length * 2 + 128);

    if (title != NULL)
    {
        if (strlen(title) < 128)
        {
            // keep title < 128 as last char is required for '\n'
            sprintf(acTraceBuf, "%s", title);
        }
    }

    for (uint8_t u16I = 0; u16I < u16Length; u16I++)
    {
        sprintf(acTraceBuf + strlen(acTraceBuf), "%02x", pau8Data[u16I]);
    }
    sprintf(acTraceBuf + strlen(acTraceBuf), "\n");

    TRACE_PRINT(acTraceBuf);

#else
    if (title != NULL)
    {
        TRACE_PRINT("%s", title);
    }

    for (uint8_t u16I = 0; u16I < u16Length; u16I++)
    {
        TRACE_PRINT("%02x", pau8Data[u16I]);
    }
    TRACE_PRINT("\n");
#endif
}
