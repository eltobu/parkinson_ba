/* ***************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: Local time
 *      State: reviewed by Art of Technology xx.2017
 * Originator: Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/system_time/system_time.h $
 *  $Revision: 22164 $
 *      $Date: 2017-12-22 14:26:42 +0100 (Fri, 22 Dec 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/* *
 * @file
 * Module Local Time:
 * By using a timer interrupt is the local time updated.
 *
 **************************************************************************** */

#ifndef SYSTEM_TIME_H_
#define SYSTEM_TIME_H_

/* Includes needed by this header -----------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*--------------------------------------------------------------------------+
 |  global constants                                                         |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  global types                                                             |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  function prototypes for C                                                |
 +--------------------------------------------------------------------------*/

/**
 * @brief Initializes the local time module after startup
 *
 * Code samples:
 * TIME_init();
 *
 * */
void TIME_init(void);

/*--------------------------------------------------------------------------*/

/**
 * @brief Synchronizes the internal millisecond counter to a RTC.
 *
 * Code samples:
 * // Convert "Seconds Since 1970" into milliseconds
 * TIME_synch((uint64_t)RTC_getRTC() * 1000);
 *
 * */
void TIME_synch(uint64_t NewMilliSeconds);

/*--------------------------------------------------------------------------*/

/**
 * @brief Returns the milliseconds since startup.
 *
 * Code samples:
 * u64_ActualTime = TIME_getMilliSeconds();
 *
 * @return   [ms] since startup.
 *
 * */
uint64_t TIME_getMilliSeconds(void);

/*--------------------------------------------------------------------------*/

/**
 * @brief Waits the given time in [us].
 *
 * Code samples:
 * TIME_waitMicroSeconds(12);
 *
 * @param MicroSeconds    [us] to wait (busy wait)
 *
 * */
void TIME_waitMicroSeconds(uint32_t MicroSeconds);

/*--------------------------------------------------------------------------*/

/**
 * @brief Returns the time when a timeout will be triggered.
 *
 * Code samples:
 * u64_TriggerTime = TIME_setTimeout(78);
 *
 * @param MilliSeconds2Wait    [ms] to maximum wait
 *
 * @return   [ms] Timeout time.
 *
 * */
uint64_t TIME_setTimeout(uint32_t MilliSeconds2Wait);

/*--------------------------------------------------------------------------*/

/**
 * @brief Detects when the time is gone.
 *
 * Code samples:
 * u64_TriggerTime = TIME_setTimeout(78);
 * if (TIME_isTimeout(u64_TriggerTime))
 *     setTimeoutEvent();
 *
 * @param MilliSeconds2Wait    [ms] to maximum wait
 *
 * @return   true, when time is smaller than actual one.
 *
 * */
bool TIME_isTimeout(uint64_t TriggerTime);

/*==========================================================================*/
#ifdef  __cplusplus
}
#endif

#endif  // SYSTEM_TIME_H_
