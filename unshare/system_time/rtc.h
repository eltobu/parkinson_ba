/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: RTC driver
 *      State: reviewed
 * Originator: M.Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/system_time/rtc.h $
 *  $Revision: 22267 $
 *      $Date: 2018-01-08 10:19:56 +0100 (Mon, 08 Jan 2018) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Module RTC:
 *
 * @brief This module contains functions to control the PWM module.
 *
 * INFO:
 * The RTC in sdk_config.h must be enabled.
 * Best usage:
 * Set "RTC_ENABLED 1".
 * Set "RTC2_ENABLED 1" if instance 2 is used inside context
 *
 * ATTENTION:
 * RTC0 is used by the Softdevice. Free if no Softdevice is used
 * RTC1 is used by the app_timer.  Free if no BLE is used
 * RTC2 is free for the user application
 *
 * RESTRICTION:
 * - Only one RTC can be handled with this module.
 *   Otherwise the interrupt and register drivers must be rewritten here.
 * - Only RTC2 is used internally. RTC0 and RTC1 cannot be handled.
 *
 ****************************************************************************
 */

#ifndef RTC_H
#define RTC_H

/* Includes needed by this header  ----------------------------------------*/
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global defines                                                           |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/

/**
 * Callback, a function that is called when the time update interval occurs
 *
 * @param   u32RTC_Seconds    The newly updated second counter
 */
typedef void (*RTC_IntervalCallback)(int64_t i64RTC_Seconds);


/**
 * Context definition to setup all the required hardware
 */
typedef struct tagRTC_Context
{
	int64_t             i64UpdateSeconds; ///< [1..x sec] Time update interval
    RTC_IntervalCallback Callback;         ///< NULL when not used. Runs in interrupt
} RTC_Context;


/**
 * The values are incremented as defined in context (Power saving)
 */
typedef struct tagRTC_Time
{
	int64_t i64RTC;        ///< Real time clock, Seconds since 1970
    uint32_t i32Year;       ///< 0..
    uint32_t u32Month;      ///< 1..12
    uint32_t u32DayOfMonth; ///< 1..31
    uint32_t u32Hours;      ///< 0..23 Hours
    uint32_t u32Minutes;    ///< 0..59 Minutes
    uint32_t u32Seconds;    ///< 0..59 Seconds
} RTC_Time;


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/

/**
 * @brief Function to initialize the RTC module
 *
 * Code samples:
 * static RTC_Context m_Context =
 * {
 *     .u32UpdateSeconds = 3600,
 *     .Callback         = UpdateHandler,
 * };
 * RTC_init(&m_Context);
 *
 * @param   pContext    Pointer to the PWM context structure
 *
 * @retval              ERR_OK if operation was successful
 */
uint32_t RTC_init(const RTC_Context *pContext);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function to de-initialize the RTC module
 *
 * @retval              ERR_OK if operation was successful
 */
uint32_t RTC_deinit(void);

/*-------------------------------------------------------------------------*/

/**
 * @brief Get real time clock information
 *
 * @param pTime     Pointer to the RTC time structure
 */
void RTC_getSystemTime(RTC_Time *pTime);

/*-------------------------------------------------------------------------*/

/**
 * @brief Set real time clock
 *
 * @note: u32RTC and the others (human readable) can be set inconsistently
 *        which is desired for something like local time
 *
 * @param Time      RTC time structure, initialized by caller
 *
 * @retval          ERR_OK if operation was successful with legal values
 */
uint32_t RTC_setSystemTime(RTC_Time Time);

/*-------------------------------------------------------------------------*/

/**
 * @brief Get seconds from real time clock
 *
 * @retval          Seconds since 1970
 */
int64_t RTC_getRTC(void);

/*-------------------------------------------------------------------------*/

/**
 * @brief Set UNIX time format as seconds since 1970
 *        The human readable time will be set accordingly
 *
 * @note: See http://www.onlineconversion.com/unix_time.htm
 *
 * @param i64Seconds       Seconds since 1970
 */
void RTC_setRTC(int64_t i64Seconds);

/*-------------------------------------------------------------------------*/

/**
 * @brief Module test, only active during developing
 */
void RTC_test(void);

/*-------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // RTC_H
