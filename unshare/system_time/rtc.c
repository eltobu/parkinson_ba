/****************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: RTC driver
 *      State: reviewed
 * Originator: M.Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/system_time/rtc.c $
 *  $Revision: 22267 $
 *      $Date: 2018-01-08 10:19:56 +0100 (Mon, 08 Jan 2018) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "error.h"
#include "trace.h"
#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"
#include <stdbool.h>
#include <inttypes.h>
#include <rtc.h>

/* -------------------------------------------------------------------------- */

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_RTC
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_RTC undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_RTC
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/**
 * Error occurred on wrong Parameters given
 */
const ERR_ErrorInfo RTC_PARAMETER_ERROR = {
        .moduleNumber = ERR_MOD_RTC,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 1,
        .lineNumber = 0,
        ERRORSTRING("RTC parameter Error")
};

/**
 * Error occurred on runtime error
 */
const ERR_ErrorInfo RTC_RUNTIME_ERROR = {
        .moduleNumber = ERR_MOD_RTC,
        .errorClass = ERR_CLASS_ERROR,
        .errorNumber = 2,
        .lineNumber = 0,
        ERRORSTRING("RTC runtime Error")
};

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/**
 * The pre-scaler is setup to 32767 which is 1 Hz.
 * The Tick interrupt has 8Hz and NOT 1Hz ??
 * The NRF tools produce this hack.
 * The counter value has to be multiplied by 8 to have seconds.
 */
#define SEC_TO_COUNTER(Seconds)     (8 * Seconds)
#define COUNTER_TO_SEC()            (m_Rtc.p_reg->COUNTER / 8)

#define COMPARE_CHANNEL 0  ///< Compare channel 0 of 4 will be used

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/

/**
 * Declaring an instance of nrf_drv_rtc for RTC2.
 */
static const nrf_drv_rtc_t m_Rtc = NRF_DRV_RTC_INSTANCE(2);

/**
 * Actual (last updated) time
 * The time structure should stay in memory during reboot.
 */
static RTC_Time m_Time __attribute__ ((section (".noinit")));

static int64_t             m_Seconds;  ///< Update interval
static RTC_IntervalCallback m_Callback; ///< The callback given by the context


/*--------------------------------------------------------------------------+
|  local helper functions                                                   |
+--------------------------------------------------------------------------*/

/**
 * @brief: Time structure updater.
 *
 * @param pTime   The structure to be initialize to the first possible date
 */
static void initTimeStruct(RTC_Time *pTime)
{
    pTime->i64RTC        = 0;
    pTime->u32Seconds    = 0;
    pTime->u32Minutes    = 0;
    pTime->u32Hours      = 0;
    pTime->u32DayOfMonth = 1;
    pTime->u32Month      = 1;
    pTime->i32Year       = 1970;

} // End of initTimeStruct

/*-------------------------------------------------------------------------*/

/**
 * @brief: Checks time structure values.
 *
 * @param pTime   The structure to be initialize to the first possible date
 *
 * @retval        true: OK, false: need initialization
 */
static bool isTimeStructOK(RTC_Time *pTime)
{
    bool isOK = true;

    if (pTime->u32Seconds > 59)
    {
        isOK = false;
    }
    if (pTime->u32Minutes > 59)
    {
        isOK = false;
    }
    if (pTime->u32Hours > 23)
    {
        isOK = false;
    }
    if (pTime->u32DayOfMonth == 0 || pTime->u32DayOfMonth > 31)
    {
        isOK = false;
    }
    if (pTime->u32Month == 0 || pTime->u32Month > 12)
    {
        isOK = false;
    }
    return (isOK);

} // End of isTimeStructOK

/*-------------------------------------------------------------------------*/

/**
 * @brief: Time structure updater and validator.
 *
 * @param pTime     The time structure to be updated
 * @param Seconds   The seconds to increment
 */
static void addNewSeconds(RTC_Time *pTime, int64_t Seconds)
{
//    bool doCorrection;

    pTime->i64RTC += Seconds;
    // not needed for this application (too slow)
//    pTime->u32Seconds += Seconds;
//    while (pTime->u32Seconds >= 60)
//    {
//        pTime->u32Seconds -= 60;
//        pTime->u32Minutes++;
//    }
//    while (pTime->u32Minutes >= 60)
//    {
//        pTime->u32Minutes -= 60;
//        pTime->u32Hours++;
//    }
//    while (pTime->u32Hours >= 24)
//    {
//        pTime->u32Hours -= 24;
//        pTime->u32DayOfMonth++;
//    }
//    do
//    {
//        doCorrection = false;
//        if ((pTime->u32Month == 1  && pTime->u32DayOfMonth > 31)  // January
//        ||  (pTime->u32Month == 3  && pTime->u32DayOfMonth > 31)  // March
//        ||  (pTime->u32Month == 5  && pTime->u32DayOfMonth > 31)  // Mai
//        ||  (pTime->u32Month == 7  && pTime->u32DayOfMonth > 31)  // July
//        ||  (pTime->u32Month == 8  && pTime->u32DayOfMonth > 31)  // August
//        ||  (pTime->u32Month == 10 && pTime->u32DayOfMonth > 31)  // October
//        ||  (pTime->u32Month == 12 && pTime->u32DayOfMonth > 31)) // December
//        {
//            pTime->u32DayOfMonth -= 31;
//            pTime->u32Month++;
//            doCorrection = true;
//        }
//        else if ((pTime->u32Month == 4  && pTime->u32DayOfMonth > 30)  // April
//        ||       (pTime->u32Month == 6  && pTime->u32DayOfMonth > 30)  // June
//        ||       (pTime->u32Month == 9  && pTime->u32DayOfMonth > 30)  // September
//        ||       (pTime->u32Month == 11 && pTime->u32DayOfMonth > 30)) // November
//        {
//            pTime->u32DayOfMonth -= 30;
//            pTime->u32Month++;
//            doCorrection = true;
//        }
//        else if (pTime->u32Month == 2 && pTime->u32DayOfMonth > 29 && pTime->i32Year % 4 == 0 &&
//        		(pTime->i32Year % 100 != 0 || pTime->i32Year % 1000 == 0))
//        {
//            // Leap Year
//            pTime->u32DayOfMonth -= 29;
//            pTime->u32Month++;
//            doCorrection = true;
//        }
//        else if (pTime->u32Month == 2 && pTime->u32DayOfMonth > 28)  // February
//        {
//            pTime->u32DayOfMonth -= 28;
//            pTime->u32Month++;
//            doCorrection = true;
//        }
//        if (pTime->u32Month > 12)
//        {
//            pTime->u32Month -= 12;
//            pTime->i32Year++;
//        }
//    } while (doCorrection);

} // End of addNewSeconds

/*-------------------------------------------------------------------------*/

/**
 * @brief: Function for handling the RTC interrupts.
 * Triggered on COMPARE0 match.
 *
 * @param int_type   The interrupt source type given by the NRF system
 */
static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
{
    if (int_type == NRF_DRV_RTC_INT_COMPARE0)
    {
        // Clear counter and re -enable COMPARE0 event & interrupt
        nrf_drv_rtc_counter_clear(&m_Rtc);
        nrf_drv_rtc_int_enable(&m_Rtc, NRF_RTC_INT_COMPARE0_MASK);

        addNewSeconds(&m_Time, m_Seconds);
        if (m_Callback != NULL)
        {
            m_Callback(m_Time.i64RTC);
        }
    }
    else
    {
        TRACE_DBG_L("RTC handler wrong interrupt event %d\n", int_type);
    }
} // End of rtc_handler


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

uint32_t RTC_init(const RTC_Context *pContext)
{
    uint32_t err_code;
    uint32_t Status = ERR_OK;

    // Validate the context settings
    if (pContext == NULL)
    {
        ERR_setErrorParam(&RTC_PARAMETER_ERROR, __LINE__, NULL, 0, 0);
        return (ERR_ERROR);
    }
    if ((pContext->i64UpdateSeconds == 0) || (pContext->i64UpdateSeconds > 0x1FFFFFUL))
    {
        ERR_setErrorParam(&RTC_PARAMETER_ERROR, __LINE__, NULL,
                          0, pContext->i64UpdateSeconds);
        return (ERR_ERROR);
    }
    TRACE_DBG_L("######## RTC_init() %ld, Date %ld-%lu-%lu, Time %lu:%lu:%lu\n",
    		(uint32_t) m_Time.i64RTC, m_Time.i32Year, m_Time.u32Month, m_Time.u32DayOfMonth,
            m_Time.u32Hours, m_Time.u32Minutes, m_Time.u32Seconds);
    if (! isTimeStructOK(&m_Time))
    {
        initTimeStruct(&m_Time); // Set to 1.1.1970
    }
    m_Callback = pContext->Callback;
    m_Seconds  = pContext->i64UpdateSeconds;

    // Initialize LFCLK, if not already.
    err_code = nrf_drv_clock_init();
    if (err_code == NRF_SUCCESS)  // Otherwise it is already initialized
    {
        nrf_drv_clock_lfclk_request(NULL);
    }

    // Initialize RTC instance
    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
    config.prescaler = RTC_FREQ_TO_PRESCALER(1); // 1 Hz
    TRACE_DBG_L("RTC prescaler %d\n", config.prescaler);

    err_code = nrf_drv_rtc_init(&m_Rtc, &config, rtc_handler);
    if (err_code != NRF_SUCCESS)
    {
        ERR_setErrorParam(&RTC_RUNTIME_ERROR, __LINE__, NULL, 0, err_code);
        TRACE_DBG_L("nrf_drv_rtc_init Error %d\n", err_code);
        Status = ERR_ERROR;
    }
    // Enable tick event & interrupt
    nrf_drv_rtc_tick_enable(&m_Rtc, false); // false: Tick interrupt disabled

    // Set compare channel to trigger interrupt enabled (true) with the defined seconds
    err_code = nrf_drv_rtc_cc_set(&m_Rtc, COMPARE_CHANNEL,
                                  SEC_TO_COUNTER(pContext->i64UpdateSeconds), true);
    // Interrupt yes
    if (err_code != NRF_SUCCESS)
    {
        ERR_setErrorParam(&RTC_RUNTIME_ERROR, __LINE__, NULL, 0, err_code);
        TRACE_DBG_L("nrf_drv_rtc_init Error %d\n", err_code);
        Status = ERR_ERROR;
    }
    // Start counting with a cleared counter
    nrf_drv_rtc_counter_clear(&m_Rtc); // Should be cleared already, just to be save

    // Power on RTC instance
    nrf_drv_rtc_enable(&m_Rtc);

	return (Status);

} // End of RTC_init

/*-------------------------------------------------------------------------*/

uint32_t RTC_deinit(void)
{
    uint32_t err_code;
    uint32_t Status = ERR_OK;

    nrf_drv_rtc_disable(&m_Rtc);
    err_code = nrf_drv_rtc_cc_disable(&m_Rtc, COMPARE_CHANNEL);
    if (err_code != NRF_SUCCESS)
    {
        ERR_setErrorParam(&RTC_RUNTIME_ERROR, __LINE__, NULL, 0, err_code);
        TRACE_DBG_L("nrf_drv_rtc_cc_disable Error %d\n", err_code);
        Status = ERR_ERROR;
    }
    nrf_drv_rtc_tick_disable(&m_Rtc);
    nrf_drv_rtc_uninit(&m_Rtc);

    return (Status);

} // End of RTC_deinit

/*-------------------------------------------------------------------------*/

void RTC_getSystemTime(RTC_Time *pTime)
{
    if (pTime != NULL)
    {
        *pTime = m_Time;
        addNewSeconds(pTime, COUNTER_TO_SEC());
    }
    else
    {
        TRACE_DBG_H("RTC_getSystemTime passing NULL\n");
        ERR_setErrorParam(&RTC_PARAMETER_ERROR, __LINE__, NULL, 0, 0);
    }
} // End of RTC_getSystemTime

/*-------------------------------------------------------------------------*/

uint32_t RTC_setSystemTime(RTC_Time Time)
{
    if (isTimeStructOK(&Time))
    {
        // Clear counter that there is no offset left at re-read
        nrf_drv_rtc_counter_clear(&m_Rtc);

        m_Time = Time;
        return (ERR_OK);
    }
    else
    {
        TRACE_DBG_H("RTC_setSystemTime passing garbage\n");
        ERR_setErrorParam(&RTC_PARAMETER_ERROR, __LINE__, NULL, 0, 0);
        return (ERR_ERROR);
    }

} // End of RTC_setSystemTime

/*-------------------------------------------------------------------------*/

int64_t RTC_getRTC(void)
{
    RTC_Time TempTime = m_Time;
    addNewSeconds(&TempTime, COUNTER_TO_SEC());

    return (TempTime.i64RTC);

} // End of RTC_getRTC

/*-------------------------------------------------------------------------*/

void RTC_setRTC(int64_t i64Seconds)
{
    // Clear counter that there is no offset left at re-read.
    // Clear counter first. It takes some little time to do.
    nrf_drv_rtc_counter_clear(&m_Rtc);

    initTimeStruct(&m_Time);
    addNewSeconds(&m_Time, i64Seconds);

} // End of RTC_setRTC

/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/

#if 0
#include <system_time/system_time.h>
static RTC_Time MyTime;

static void setNiceTime(void)
{
    MyTime.i64RTC = 1513878075;
    MyTime.i32Year = 2017;
    MyTime.u32Month = 12;
    MyTime.u32DayOfMonth = 21;
    MyTime.u32Hours = 17;
    MyTime.u32Minutes = 41;
    MyTime.u32Seconds = 15;
}

static void printMyTime(void)
{
    TRACE_DBG_L("RTC %ld, Date %ld-%lu-%lu, Time %lu:%lu:%lu\n",
            (uint32_t) MyTime.i64RTC, MyTime.i32Year, MyTime.u32Month, MyTime.u32DayOfMonth,
            MyTime.u32Hours, MyTime.u32Minutes, MyTime.u32Seconds);
	TRACE_DBG_L("Minutes %lu\n", MyTime.u32Minutes);
}

static void UpdateHandler(int64_t i64RTC_Seconds)
{
    TRACE_DBG_L("Handler Time %ld\n", (uint32_t) i64RTC_Seconds);
}

static RTC_Context m_rtc =
{
    .i64UpdateSeconds = 5,
    .Callback         = UpdateHandler,
};

void RTC_test(void)
{
    uint32_t Status = ERR_OK;

    Status = RTC_init(&m_rtc);
    TRACE_DBG_L("Stat %d, RTC_init\n", Status);

    // Test time after restart (boot)
    RTC_getSystemTime(&MyTime);
    printMyTime();

    RTC_setRTC(68267045);
    RTC_getSystemTime(&MyTime);
    printMyTime();
    TRACE_DBG_L("RTC_setRTC was  01 Mar 1972 03:04:05, 1. Leap Year\n");

    TIME_waitMicroSeconds(4000000); // 4 sec
    TRACE_DBG_L("RTC_getRTC %ld\n", (uint32_t) RTC_getRTC());

    MyTime.u32DayOfMonth = 0;
    Status = RTC_setSystemTime(MyTime);
    TRACE_DBG_L("Stat %d, MUST FAIL RTC_setSystemTime\n", Status);

    setNiceTime();
    Status = RTC_setSystemTime(MyTime);
    TRACE_DBG_L("Stat %d, RTC_setSystemTime (Nice Time)\n", Status);
    RTC_getSystemTime(&MyTime);
    printMyTime();

    TIME_waitMicroSeconds(15000000);
    Status = RTC_deinit();
    TRACE_DBG_L("Stat %d, RTC_deinit test the previous time stays the same\n", Status);

    RTC_getSystemTime(&MyTime);
    printMyTime();

    TIME_waitMicroSeconds(3000000);
    Status = RTC_init(&m_rtc);
    TRACE_DBG_L("Stat %d, RTC_init again\n", Status);
    RTC_getSystemTime(&MyTime);
    printMyTime();

    RTC_setRTC(946684800); // Sat, 01 Jan 2000 00:00:00 GMT
    RTC_getSystemTime(&MyTime);
    printMyTime();
    TRACE_DBG_L("RTC_setRTC was 1 Jan 2000 00:00:00\n");

    TIME_waitMicroSeconds(7000000);
    Status = RTC_deinit();
    TRACE_DBG_L("Stat %d, Finale RTC_deinit done\n", Status);

} // End of RTC_test
#endif

/*-------------------------------------------------------------------------*/
