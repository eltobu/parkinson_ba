/* ***************************************************************************
 *
 *   Customer: Art of Technology
 *   Project#: 00.00
 *       Name: NRF Library
 *
 *     Module: Local time
 *      State: reviewed by Art of Technology xx.2017
 * Originator: Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/nRF5/AOT-NRF/branches/SDK13.1.0/hal/system_time/system_time.c $
 *  $Revision: 22164 $
 *      $Date: 2017-12-22 14:26:42 +0100 (Fri, 22 Dec 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include <system_time.h>
#include "nrf.h"
#include "nrf_delay.h"
#include "trace.h"

/* -------------------------------------------------------------------------- */

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_SYSTEM_TIME
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_SYSTEM_TIME undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_SYSTEM_TIME
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 | constants                                                                 |
 +--------------------------------------------------------------------------*/

#define SYSTICK_INTERVAL 50                       ///< [ms] Interrupt interval
#define SYSTICK_DIVIDER (1000 / SYSTICK_INTERVAL) ///< 1000=1ms,100=10ms,20=50ms
#define SYSTICK_VALUE (SystemCoreClock / SYSTICK_DIVIDER) ///< SysTick->VAL set

#define ONE_MS (SYSTICK_VALUE / SYSTICK_INTERVAL)  ///< 1 ms count down value

/*--------------------------------------------------------------------------+
 |  module-global types                                                      |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  module-global variables                                                  |
 +--------------------------------------------------------------------------*/

static bool NeedStartupInitialisation = true; ///< No multiple allocation

static volatile uint64_t m_u64_MilliSeconds;

/*--------------------------------------------------------------------------+
 |  local functions                                                          |
 +--------------------------------------------------------------------------*/

/* SysTick interrupt Handler. The function MUST be defined exactly like this */
void SysTick_Handler(void)
{
    m_u64_MilliSeconds += SYSTICK_INTERVAL;
}

/*--------------------------------------------------------------------------+
 |  functions                                                                |
 +--------------------------------------------------------------------------*/

uint64_t TIME_getMilliSeconds(void)
{
    volatile uint64_t Time1, Time2;
    uint32_t CountDown;
    uint32_t MillisecondsSinceInterrupt;
    uint32_t Timeout = 0;
    do
    {
        Timeout++;
        Time1 = m_u64_MilliSeconds;
        CountDown = SysTick->VAL;
        Time2 = m_u64_MilliSeconds;
    } while((Time1 != Time2) && (Timeout < 5));

    if (Timeout >= 5)
    {
        TRACE_DBG_H("ERROR reading SYSTICK %d\n", Timeout);
        return (m_u64_MilliSeconds);
    }
    MillisecondsSinceInterrupt = (SYSTICK_VALUE - CountDown) / ONE_MS;

    return (Time1 + (uint64_t)MillisecondsSinceInterrupt);

} // End of TIME_getMilliSeconds

/*--------------------------------------------------------------------------*/

void TIME_waitMicroSeconds(uint32_t MicroSeconds)
{
    nrf_delay_us(MicroSeconds);

} // End of TIME_waitMicroSeconds

/*--------------------------------------------------------------------------*/

void TIME_init(void)
{
    m_u64_MilliSeconds = 0;
    if (NeedStartupInitialisation)
    {
        NeedStartupInitialisation = false;

        TRACE_DBG_L("Clock %d, Conf %d\n", SystemCoreClock, SYSTICK_VALUE);
        if (SysTick_Config(SYSTICK_VALUE) == 0)
        {
            NVIC_EnableIRQ(SysTick_IRQn);
        }
    }
} // End of TIME_init

/*--------------------------------------------------------------------------*/

void TIME_synch(uint64_t NewMilliSeconds)
{
    NVIC_DisableIRQ(SysTick_IRQn);
    m_u64_MilliSeconds = NewMilliSeconds;
    NVIC_EnableIRQ(SysTick_IRQn);

} // End of TIME_synch

/*--------------------------------------------------------------------------*/

uint64_t TIME_setTimeout(uint32_t MilliSeconds2Wait)
{
    return((uint64_t)MilliSeconds2Wait + TIME_getMilliSeconds());

} // End of TIME_setTimeout

/*--------------------------------------------------------------------------*/

bool TIME_isTimeout(uint64_t TriggerTime)
{
    if (TriggerTime < TIME_getMilliSeconds())
    {
        return (true);
    }
    else
    {
        return (false);
    }
} // End of TIME_isTimeout

/*--------------------------------------------------------------------------*/
