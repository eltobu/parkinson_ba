/** @file
 *
 *    Customer: A.M.I.
 *    Project#: 16-01
 * ProjectName: Uroseal
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/ETH/Parkinson_BA/ModuleA/trunk/external/board.h $
 *  $Revision: 25361 $
 *      $Date: 2018-07-05 00:17:48 +0200 (Thu, 05 Jul 2018) $
 *    $Author: buechli $
 *
 *  $ Originator: Leuenberger
 *  @copyright Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 * @brief this module contains board specific definitions
 *
 ***************************************************************************/
#ifndef SRC_BOARD_H_
#define SRC_BOARD_H_

/* Includes needed by this header  ----------------------------------------*/
#include "module_config.h"
#include "gpio.h"
#include "adc.h"
#include "pwm.h"
#include "spi.h"
#include "KX122.h"
#include "MT29F.h"
#include "FM25V.h"
#include "ble_central.h"

#if (TARGETTYPE != STANDALONE_BOOTLOADER)
#include "fstorage.h"   // Softdevice Flash Driver
#include "flash.h"      // Flashdriver HAL
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  Definitions                                                              |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
| Pin Definitions                                                           |
+--------------------------------------------------------------------------*/

/**
 * Power Switches
 */
#define BRD_POWER_BAT_CHECK         23          ///< pin to control power of battery measurement
#define BRD_POWER_MOTOR             8           ///< pin to control power of motor

/**
 * GP outputs
 */
#define BRD_LED_R_OUT               31          ///< red led
#define BRD_LED_G_OUT               30          ///< green led
#define BRD_LED_B_OUT               29          ///< blue led

/**
 * GP inputs
 */
#define BRD_REED_IN                 24          ///< detect state of reed switch
#define BRD_BTN_IN                  25          ///< detect state of button
#define BRD_ACC_INT1                18          ///< interrupt pin 1 of accelerometer
#define BRD_ACC_INT2                19          ///< interrupt pin 2 of accelerometer

/**
 * PWM
 */
#define BRD_MOTOR_PWM               7           ///< Motor PWM
#define BRD_BUZZER_PWM              13          ///< Buzzer PWM

/**
 * UART
 */
#define BRD_UART_TX                 3           ///< UART TX pin
#define BRD_UART_RX                 6           ///< UART RX pin
#define BRD_UART_RTS                4           ///< UART RTS pin
#define BRD_UART_CTS                5           ///< UART CTS pin

/**
 * SPI FRAM
 */
#define BRD_FRAM_MOSI               20          ///< MOSI pin for FRAM
#define BRD_FRAM_MISO               15          ///< MISO pin for FRAM
#define BRD_FRAM_CLK                21          ///< CLK pin for FRAM
#define BRD_FRAM_CS                 11          ///< CS pin for FRAM

/**
 * SPI Flash/Accelerometer
 */
#define BRD_FLASH_ACC_MOSI          16          ///< MOSI pin for flash or accelerometer
#define BRD_FLASH_ACC_MISO          17          ///< MISO pin for flash or accelerometer
#define BRD_FLASH_ACC_CLK           12          ///< CLK pin for flash or accelerometer
#define BRD_FLASH_ACC_CS            14          ///< CS pin for flash or accelerometer

/**
 * ADC
 */
#define BRD_BAT_VOLT_ANALOG         0           ///< analog pin for battery voltage sensing

/**
 * ADC Channels
 *
 * @note    ADC channels are independent from ADC input pins
 */
#define BRD_ADC_MODULE_CHANNEL_BATTERY          0           ///< battery voltage channel

/**
 * ADC Settings
 */
#define BRD_ADC_SETTINGS_BATTERY_GAIN           NRF_SAADC_GAIN1

#define BRD_ADC_RESOLUTION                      12      ///< ADC resolution [8,10,12,14]
#define BRD_ADC_MOVING_AVERAGE                  16      ///< ADC moving average samples
#define BRD_ADC_SAMPLING_FREQUENCY              8000    ///< ADC moving average sampling rate
#define BRD_ADC_RESOLUTION_MAX_VALUE            4095    ///< ADC maximum value at specified resolution
#define BRD_ADC_REFERENCE_MV                    4680    ///< internal reference voltage of ADC in mV


/**
 * Analog-Digital Converter Configurations
 */
#define BRD_ADC_CONFIG_BATTERY  \
{                               \
    .resistor_p = NRF_SAADC_RESISTOR_DISABLED,                  /*//< disable resistors */\
    .resistor_n = NRF_SAADC_RESISTOR_DISABLED,                  /*//< disable resistors */\
    .gain       = BRD_ADC_SETTINGS_BATTERY_GAIN,                /*//< gain factor */ \
    .reference  = NRF_SAADC_REFERENCE_INTERNAL,                 /*//< reference voltage */ \
    .acq_time   = NRF_SAADC_ACQTIME_10US,                       /*//< Acquisition time in us */ \
    .mode       = NRF_SAADC_MODE_SINGLE_ENDED,                  /*//< single ended measurement mode */ \
    .burst      = NRF_SAADC_BURST_DISABLED,                     /*//< No burst, no oversampling */ \
    .pin_p      = (nrf_saadc_input_t)(BRD_BAT_VOLT_ANALOG+1),   /*//< input channel, internal VDD */ \
    .pin_n      = NRF_SAADC_INPUT_DISABLED                      /*//< negative input channel */ \
}

/**
 * Macros for conversion of ADC values to physical units
 */

#define BRD_ADC_CONVERSION_BATTERY_IN_MV(x)     ((uint32_t)((uint32_t)x*(uint32_t)BRD_ADC_REFERENCE_MV)/BRD_ADC_RESOLUTION_MAX_VALUE)   ///< conversion from adc ticks to battery voltage in mV


/**
 * Resources: Timers
 *
 * @note    Timer 0 is reserved by the Soft Device
 */
#define BRD_ADC_DRIVER_CLOCK                2   ///< timer used for ADC in adc_drv


/**
 * Resources: SPI / TWI (I2C)
 *
 * @note    SPI0/1 and TWI0/1 share same registers and can not be used simultaneously
 */
#define BRD_SPI_FRAM                        0   ///< use SPI 0 for FRAM
#define BRD_SPI_FLASH_ACC                   1   ///< use SPI 1 for flash or accelerometer

/**
 * PWM Channels
 *
 * @note    PWM channels are independent from PWM pins
 */
#define BRD_PWM0_MODULE_CHANNEL_MOTOR        0   ///< motor channel
#define BRD_PWM1_MODULE_CHANNEL_BUZZER       0   ///< buzzer channel

/**
 * PWM settings
 */
#define BRD_MOTOR_PWM_FREQUENCY_HZ          20000          ///< PWM frequency for motor control
#define BRD_BUZZER_PWM_FREQUENCY_HZ         4000           ///< PWM frequency for motor control

/**
 * MT29F configuration
 */
#define BRD_MT29F_PAGE_SIZE        2048         ///< Number of bytes per page
#define BRD_MT29F_SPARE_SIZE       64           ///< Number of spare bytes per page (without ECC area)
#define BRD_MT29F_BLOCK_SIZE       64           ///< Number of pages per block
#define BRD_MT29F_DEVICE_SIZE      4096         ///< Total number of blocks
#define BRD_MT29F_DIES             2            ///< Number of dies
#define BRD_MT29F_DIE_SELECT_POS   17           ///< Position in page address for die selection
#define BRD_MT29F_PAGE_MASK        0x3F         ///< Mask for extracting page within block

/**
 * FM25V configuration
 */
#define BRD_FM25V_SIZE             2048         ///< Total number of bytes

/**
 * KX122 configuration
 */
#define BRD_KX122_SIZE             2048         ///< Total number of bytes

/*-------------------------------------------------------------------------*/
/**
 * Clock configuration
 */
/* Nordic development board */
// Low frequency clock source to be used by the SoftDevice
#ifdef CONFIG_NRF_NORDIC
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_XTAL,            \
                                 .rc_ctiv       = 0,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}
#endif // CONFIG_NRF_NORDIC

/* Rigado board */
// Low frequency clock source to be used by the SoftDevice
#ifdef CONFIG_NRF_RIGADO
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_RC,              \
                                 .rc_ctiv       = 16,                               \
                                 .rc_temp_ctiv  = 4,                                \
                                 .xtal_accuracy = 0} // accuracy ignored when using RC
#endif // CONFIG_NRF_RIGADO

/*--------------------------------------------------------------------------+
| Memory Definitions                                                        |
+--------------------------------------------------------------------------*/

/**
 * Flash definitions
 */

// Size of a flash page in bytes.
#if    defined (NRF51)
    #if !defined (FS_PAGE_SIZE)
        #define FS_PAGE_SIZE    (1024)
    #endif
#elif (defined (NRF52) || defined(NRF52840_XXAA))
    #if !defined (FS_PAGE_SIZE)
        #define FS_PAGE_SIZE    (4096)
    #endif
#else
    #error "Can't set FS_PAGE_SIZE!"
#endif

// Maximum number of bytes to be written to flash in a single operation
#if    defined (NRF51)
    #if !defined (NRF_FSTORAGE_SD_MAX_WRITE_SIZE)
        #define NRF_FSTORAGE_SD_MAX_WRITE_SIZE    (1024)
    #endif
#elif defined (NRF52)
    #if !defined (NRF_FSTORAGE_SD_MAX_WRITE_SIZE)
        #define NRF_FSTORAGE_SD_MAX_WRITE_SIZE    (4096)
    #endif
#else
    #error "Can't set NRF_FSTORAGE_SD_MAX_WRITE_SIZE!"
#endif

/**
 * BRD_FLASH_BASE is the base address of the FLASH-Memory
 * rename processor definition for generic use
 */
#define BRD_FLASH_BASE          0x00000000

/**
 * size in bytes of the FLASH-Memory
 * rename processor definition for generic use
 */
#define BRD_FLASH_SIZE          0x00080000 // 512kB

/**
 * Number of bytes in one Flash Block
 */
#define BRD_FLASH_PAGESIZE      FS_PAGE_SIZE

/**
 * Number of dwords in one Flash Block
 */
#define BRD_FLASH_PAGESIZE_DW  (BRD_FLASH_PAGESIZE / 4)

/**
 * mask to ensure valid flash page number
 */
#define BRD_FLASH_PAGEMASK          ((BRD_FLASH_SIZE / BRD_FLASH_PAGESIZE) - 1)

/**
 * Mask to find the flash page address of an address within the flash memory
 */
#define BRD_FLASH_PAGEALIGNMASK     (~(BRD_FLASH_PAGESIZE-1))

/**
 * Memory organization must be a multiple of BRD_FLASH_PAGESIZE
 */

/**
 * Soft Device Address (Currently not used inside code)
 * Softdevice Size in Byte (Multiple of BRD_FLASH_PAGESIZE)
 */
#define BRD_FLASH_SOFTDEVICE_START_ADDRESS   0x00000000
#define BRD_FLASH_SOFTDEVICE_SIZE            0x0001F000

/**
 * Application Address and Size in Byte (Multiple of BRD_FLASH_PAGESIZE)
 *
 * Start Addresses MUST fit with the linker definitions from the bootloader project
 */
#define BRD_FLASH_APP_START_ADDRESS          0x0001F000
#define BRD_FLASH_APP_SIZE                   0x0002A000

/**
 * Application Configuration Address and Size in Byte (Multiple of BRD_FLASH_PAGESIZE)
 */
#define BRD_FLASH_APP_CONF_ADDRESS           0x00049000
#define BRD_FLASH_APP_CONF_SIZE              0x00001000

/**
 * Application Update Source Address and Size in Byte (Multiple of BRD_FLASH_PAGESIZE)
 */
#define BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS  0x0004A000
#define BRD_FLASH_APP_UPDATE_SOURCE_SIZE     0x0002B000

/**
 * Bootloader Address and Size in Byte (Multiple of BRD_FLASH_PAGESIZE)
 * Addresses MUST fit with the linker definitions from the Bootloader project
 */
#define BRD_FLASH_BOOTLOADER_START_ADDRESS   0x00075000
#define BRD_FLASH_BOOTLOADER_SIZE            0x0000A000

/**
 * Bootloader Configuration Address and Size in Byte (Multiple of BRD_FLASH_PAGESIZE)
 */
#define BRD_FLASH_BOOTLOADER_CONF_ADDRESS    0x0007F000
#define BRD_FLASH_BOOTLOADER_CONF_SIZE       0x00001000

// **** FLASH module memory area definitions ****

#if (TARGETTYPE != STANDALONE_BOOTLOADER)

// export the context
extern FLASH_Context BRD_FLASH_APPUPLOAD;
extern FLASH_Context BRD_FLASH_BOOTCONF;
extern FLASH_Context BRD_FLASH_APPCONF;

/**
 * Application Upload memory area
 */
FS_REGISTER_AOT(User_AppUpload, BRD_FLASH_APP_UPDATE_SOURCE_SIZE, BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS, BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE, 0xFE);

/**
 * Boot configuration memory area
 */
FS_REGISTER_AOT(User_BootConf, BRD_FLASH_BOOTLOADER_CONF_SIZE, BRD_FLASH_BOOTLOADER_CONF_ADDRESS, BRD_FLASH_BOOTLOADER_CONF_ADDRESS + BRD_FLASH_BOOTLOADER_CONF_SIZE, 0xFD);

/**
 * Application configuration memory area
 */
FS_REGISTER_AOT(User_AppConf, BRD_FLASH_APP_CONF_SIZE, BRD_FLASH_APP_CONF_ADDRESS, BRD_FLASH_APP_CONF_ADDRESS + BRD_FLASH_APP_CONF_SIZE, 0xFC);
#endif

/**
 * FRAM definitions
 */
#define BRD_FRAM_DEVICE_INFO_START_ADDRESS   0x00000000
#define BRD_FRAM_DEVICE_INFO_SIZE            0x0000000A
#define BRD_FRAM_FLASH_INFO_START_ADDRESS    0x00000400
#define BRD_FRAM_FLASH_INFO_SIZE             0x00000353
#define BRD_FRAM_PERSISTENT_START_ADDRESS    0x00000800
#define BRD_FRAM_PERSISTENT_SIZE             0x00000800
#define BRD_FRAM_PATTERN_START_ADDRESS       0x00001000
#define BRD_FRAM_PATTERN_SIZE                0x00000800
#define BRD_FRAM_RING_BUFFER_L_START_ADDRESS 0x00001800
#define BRD_FRAM_RING_BUFFER_L_SIZE          0x0001F400
#define BRD_FRAM_RING_BUFFER_R_START_ADDRESS 0x00020C00
#define BRD_FRAM_RING_BUFFER_R_SIZE          0x0001F400

/**
 * FRAM Patterns
 */
#define BRD_FRAM_PATTERN_LEFT_START_ADDRESS  0x00001000
#define BRD_FRAM_PATTERN_LEFT_SIZE           0x00000200
#define BRD_FRAM_PATTERN_RIGHT_START_ADDRESS 0x00001400
#define BRD_FRAM_PATTERN_RIGHT_SIZE          0x00000200

/*--------------------------------------------------------------------------+
 |  function prototypes for C                                                |
 +--------------------------------------------------------------------------*/

/**
 * @brief Get pointer of ADC configs
 *
 * @retval  pointer to ADC configs
 * */
ADC_Channel* BRD_getADCBatteryChannel();

/**
 * @brief Get pointer of PWM context
 *
 * @retval  pointer to PWM context
 * */
PWM_Context* BRD_getPWM0Context();

/**
 * @brief Get pointer of PWM context
 *
 * @retval  pointer to PWM context
 * */
PWM_Context* BRD_getPWM1Context();

/**
 * @brief Get pointer of FM25V context
 *
 * @retval  pointer to FM25V context
 * */
KX122_Context* BRD_getKX122Context();

/**
 * @brief Get pointer of MT29F context
 *
 * @retval  pointer to MT29F context
 * */
MT29F_Context* BRD_getMT29FContext();

/**
 * @brief Get pointer of FM25V context
 *
 * @retval  pointer to FM25V context
 * */
FM25V_Context* BRD_getFM25VContext();

/**
 * @brief Get pointer of BLE central context
 *
 * @retval  pointer to BLE central context
 */
BLE_C_Context* BRD_getBLEContext(void);

/*--------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* SRC_BOARD_H_ */
