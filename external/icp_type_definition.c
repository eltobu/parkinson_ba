/****************************************************************************
 *
 *   Customer: AOT AG
 *   Project#: 17-03
 *       Name: Generic Library
 *
 *     Module: interchange_protocol_type_definition
 *      State: Not formally tested
 * Originator: Hedinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/interchange_protocol/templates/icp_type_definition.c $
 *  $Revision: 24740 $
 *      $Date: 2018-05-24 12:19:08 +0200 (Thu, 24 May 2018) $
 *    $Author: hedinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "icp_type.h"

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/
static const uint16_t m_au16_container_definition[] = {	TYPE_CONTAINER_GENERIC_1, \
														TYPE_FW_UPDATE_EXECUTE_1, \
														TYPE_CERTIFICATE_1, \
													};

/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/
const char* ICPT_getTypeName(Interchange_Protocol_Type type)
{
    switch (type)
    {
    case TYPE_DEFAULT: return "TYPE_DEFAULT";
    case TYPE_PROTOCOL_VERSION: return "TYPE_PROTOCOL_VERSION";
    case TYPE_CONTAINER_GENERIC_1: return "TYPE_CONTAINER_GENERIC_1";
    case TYPE_RESPONSE_1: return "TYPE_RESPONSE_1";
    case TYPE_STATUS_1: return "TYPE_STATUS_1";
    case TYPE_FW_UPDATE_PREPARE_1: return "TYPE_FW_UPDATE_PREPARE_1";
    case TYPE_FW_UPDATE_IMAGE_1: return "TYPE_FW_UPDATE_IMAGE_1";
    case TYPE_FW_UPDATE_EXECUTE_1: return "TYPE_FW_UPDATE_EXECUTE_1";
    case TYPE_FW_UPDATE_START_ADDR_1: return "TYPE_FW_UPDATE_START_ADDR_1";
    case TYPE_FW_UPDATE_STOP_ADDR_1: return "TYPE_FW_UPDATE_STOP_ADDR_1";
    case TYPE_FW_UPDATE_CRC_1: return "TYPE_FW_UPDATE_CRC_1";
    case TYPE_CERTIFICATE_1: return "TYPE_CERTIFICATE_1";
    case TYPE_CERTIFICATE_HEADER_1: return "TYPE_CERTIFICATE_HEADER_1";
    case TYPE_PUBLIC_KEY_1: return "TYPE_PUBLIC_KEY_1";
    case TYPE_SIGNATURE_1: return "TYPE_SIGNATURE_1";
    case TYPE_DEBUG_1: return "TYPE_DEBUG_1";
    case TYPE_TIME_1: return "TYPE_TIME_1";
    case TYPE_DEVICE_INFO_REQUEST_1: return "TYPE_DEVICE_INFO_REQUEST_1";
    case TYPE_DEVICE_INFO_1: return "TYPE_DEVICE_INFO_1";
    case TYPE_ASSIGN_ROLE_1: return "TYPE_ASSIGN_ROLE_1";
    case TYPE_PATTERN_1: return "TYPE_PATTERN_1";
    case TYPE_VIBRATION_1: return "TYPE_VIBRATION_1";
    case TYPE_SYNC_1: return "TYPE_SYNC_1";
    case TYPE_ACCELERATION_DATA_1: return "TYPE_ACCELERATION_DATA_1";
    case TYPE_EVENT_COUNT_REQUEST_1: return "TYPE_EVENT_COUNT_REQUEST_1";
    case TYPE_EVENT_COUNT_1: return "TYPE_EVENT_COUNT_1";
    case TYPE_EVENT_DATA_REQUEST_1: return "TYPE_EVENT_DATA_REQUEST_1";
    case TYPE_EVENT_DATA_HEADER_1: return "TYPE_EVENT_DATA_HEADER_1";
    case TYPE_EVENT_DATA_FRAGMENT_1: return "TYPE_EVENT_DATA_FRAGMENT_1";
    case TYPE_EVENT_CLEAR_1: return "TYPE_EVENT_CLEAR_1";
    case TYPE_RESET_1: return "TYPE_RESET_1";
    case TYPE_BATTERY_REQUEST_1: return "TYPE_BATTERY_REQUEST_1";
    case TYPE_BATTERY_1: return "TYPE_BATTERY_1";
    case TYPE_ACK_1: return "TYPE_ACK_1";
    case TYPE_BBM_SCAN_1: return "TYPE_BBM_SCAN_1";
    case TYPE_BBM_RESET_1: return "TYPE_BBM_RESET_1";
    case TYPE_BBM_UUID_REQUEST_1: return "TYPE_BBM_UUID_REQUEST_1";
    case TYPE_BBM_UUID_1: return "TYPE_BBM_UUID_1";
    case TYPE_UUID_REQUEST_1: return "TYPE_UUID_REQUEST_1";
    case TYPE_UUID_1: return "TYPE_UUID_1";
    case TYPE_LAST: return "Warning: TYPE_LAST found";
    }
    return "Warning: TYPE unknown!";
}

uint32_t get_container_definition_length (void)
{
	return (sizeof (m_au16_container_definition));
}

uint16_t * get_container_definition (void)
{
	return (uint16_t *)m_au16_container_definition;
}

/* eof */
