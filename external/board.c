/****************************************************************************
 *
 *   Customer: A.M.I.
 *   Project#: 16-01
 *       Name: Uroseal
 *
 *     Module: Board
 *      State: Not formally tested
 * Originator: Iliev
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/A.M.I/Uroseal/Implant/trunk/external/board.c $
 *  $Revision: 13477 $
 *      $Date: 2012-06-12 09:30:40 +0200 (Di, 06 Jan 2018) $
 *    $Author: Iliev $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ***************************************************************************/
/**
 * @file
 * Module: board
 *
 * In this module the hardware pin configuration is set up.
 *
 ***************************************************************************/

/*--------------------------------------------------------------------------+
|  Includes                                                                 |
+--------------------------------------------------------------------------*/
#include <board.h>

/*--------------------------------------------------------------------------+
|  Global variables                                                         |
+--------------------------------------------------------------------------*/

/*
 * EVAL board PIN DEFINITIONS
 * do NOT change
 */


/*
 * PIN ASSIGNMENTS
 * Place the Final HW pin definitions (and names) in here
 * and --if Eval Boards are used during development-- do the mapping 
 */


// *** FLASH context ***
#if (TARGETTYPE != STANDALONE_BOOTLOADER)
/*
 * FLASH user area application upload
 */
FLASH_Context BRD_FLASH_APPUPLOAD =
{
    .u32StartAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS,
    .u32EndAddr = (uint32_t) BRD_FLASH_APP_UPDATE_SOURCE_ADDRESS + BRD_FLASH_APP_UPDATE_SOURCE_SIZE - 1,
    .pFstorage = &User_AppUpload,
};

/*
 * FLASH user area boot configuration
 */
FLASH_Context BRD_FLASH_BOOTCONF =
{
    .u32StartAddr = (uint32_t) BRD_FLASH_BOOTLOADER_CONF_ADDRESS,
    .u32EndAddr = (uint32_t) BRD_FLASH_BOOTLOADER_CONF_ADDRESS + BRD_FLASH_BOOTLOADER_CONF_SIZE - 1,
    .pFstorage = &User_BootConf,
};

/*
 * FLASH user area application configuration
 */
FLASH_Context BRD_FLASH_APPCONF =
{
    .u32StartAddr = (uint32_t) BRD_FLASH_APP_CONF_ADDRESS,
    .u32EndAddr = (uint32_t) BRD_FLASH_APP_CONF_ADDRESS + BRD_FLASH_APP_CONF_SIZE - 1,
    .pFstorage = &User_AppConf,
};
#endif

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/

/**
 * ADC peripheral channel config
 */
static nrf_saadc_channel_config_t m_ADC_BATTERY_config = BRD_ADC_CONFIG_BATTERY;
static ADC_Channel m_ADC_BATTERY = {
	.pConfig = &m_ADC_BATTERY_config,
	.u8Channel = BRD_ADC_MODULE_CHANNEL_BATTERY
};

/**
 * PWM peripheral context definition
 */
static PWM_Context m_PWM0 = {
    .u8Interface  = 0,
    .u32Frequency = BRD_MOTOR_PWM_FREQUENCY_HZ,
    .u8Pin1       = BRD_MOTOR_PWM,
    .u8Pin2       = 0,
    .u8Pin3       = 0,
    .u8Pin4       = 0,
    .bPin1Enabled = true,
    .bPin2Enabled = false,
    .bPin3Enabled = false,
	.bPin4Enabled = false,
};

/**
 * PWM peripheral context definition
 */
static PWM_Context m_PWM1 = {
    .u8Interface  = 1,
    .u32Frequency = BRD_BUZZER_PWM_FREQUENCY_HZ,
    .u8Pin1       = BRD_BUZZER_PWM,
    .u8Pin2       = 0,
    .u8Pin3       = 0,
    .u8Pin4       = 0,
    .bPin1Enabled = true,
    .bPin2Enabled = false,
    .bPin3Enabled = false,
	.bPin4Enabled = false,
};

/**
 * Accelerometer module context definition, where the SPI context is copied inside init
 */
static KX122_Context m_KX122 = {
	.INT1_Pin = BRD_ACC_INT1,
	.INT2_Pin = BRD_ACC_INT2,
    .spi = {
            .Instance = NRF_DRV_SPI_INSTANCE(BRD_SPI_FLASH_ACC),
            .CLK_Pin  = BRD_FLASH_ACC_CLK,
            .MOSI_Pin = BRD_FLASH_ACC_MOSI,
            .MISO_Pin = BRD_FLASH_ACC_MISO,
            .CS_Pin   = BRD_FLASH_ACC_CS,
            .useManualCS  = true,
            .eType        = SPI_TYPE_HW,
            .u32Frequency = 8000000,
            .ClockMode    = NRF_DRV_SPI_MODE_0,
            .BitOrder     = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
    },
};

/**
 * Flash module context definition, where the SPI context is copied inside init
 */
static MT29F_Context m_MT29F = {
    .pageSize     = BRD_MT29F_PAGE_SIZE,
    .spareSize    = BRD_MT29F_SPARE_SIZE,
    .blockSize    = BRD_MT29F_BLOCK_SIZE,
    .deviceSize   = BRD_MT29F_DEVICE_SIZE,
	.dies         = BRD_MT29F_DIES,
	.dieSelectPos = BRD_MT29F_DIE_SELECT_POS,
    .spi = {
            .Instance = NRF_DRV_SPI_INSTANCE(BRD_SPI_FLASH_ACC),
            .CLK_Pin  = BRD_FLASH_ACC_CLK,
            .MOSI_Pin = BRD_FLASH_ACC_MOSI,
            .MISO_Pin = BRD_FLASH_ACC_MISO,
            .CS_Pin   = BRD_FLASH_ACC_CS,
            .useManualCS  = true,
            .eType        = SPI_TYPE_HW,
            .u32Frequency = 8000000,
            .ClockMode    = NRF_DRV_SPI_MODE_0,
            .BitOrder     = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
    },
};

/**
 * FRAM module context definition, where the SPI context is copied inside init
 */
static FM25V_Context m_FM25V = {
    .size = BRD_FM25V_SIZE,
    .spi = {
            .Instance = NRF_DRV_SPI_INSTANCE(BRD_SPI_FRAM),
            .CLK_Pin  = BRD_FRAM_CLK,
            .MOSI_Pin = BRD_FRAM_MOSI,
            .MISO_Pin = BRD_FRAM_MISO,
            .CS_Pin   = BRD_FRAM_CS,
            .useManualCS  = true,
            .eType        = SPI_TYPE_HW,
            .u32Frequency = 8000000,
            .ClockMode    = NRF_DRV_SPI_MODE_0,
            .BitOrder     = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
    },
};

/**
 * @brief structure with service UUID and UUID-Type
 */
static ble_uuid_t m_nus_uuid =
  {
	.uuid = BLE_UUID_NUS_SERVICE,
	.type = BLE_UUID_TYPE_VENDOR_BEGIN
  };

/**
 * @brief Connection parameters requested for connection.
 */
static ble_gap_conn_params_t m_connection_param =
{
	.min_conn_interval = MSEC_TO_UNITS(20, UNIT_1_25_MS),  // Minimum connection interval
	.max_conn_interval = MSEC_TO_UNITS(75, UNIT_1_25_MS),  // Maximum connection interval
	.slave_latency     = 0,            					   // Slave latency
	.conn_sup_timeout  = MSEC_TO_UNITS(4000, UNIT_10_MS)   // Supervision time-out
};

/**
 * @brief BLE scan parameter
 */
static ble_gap_scan_params_t m_scan_params =
{
	.active         = 1,								  // If 1, performs active scanning (scan requests).
	.use_whitelist  = 0,								  // If 1, filter advertisers using current active whitelist
	.adv_dir_report = 1,								  // If 1, also report directed advertisements where the initiator field is set to a private resolvable address,
	                                                      //  even if the address did not resolve to an entry in the device identity list.
                                                          //  A report will be generated even if the peer is not in the whitelist.
	.interval       = MSEC_TO_UNITS(100, UNIT_0_625_MS),  // Determines scan interval in units of 0.625 millisecond
	.window         = MSEC_TO_UNITS(100, UNIT_0_625_MS),  // Determines scan window in units of 0.625 millisecond
	.timeout        = 0x0000,							  // Timeout when scanning. 0x0000 disables timeout
};
/*-------------------------------------------------------------------------*/
/**
 * BLE central context instance
 */
static BLE_C_Context m_bleContext =
{
	.u8_centralLinkCount = BLE_CENTRAL_CENTRAL_LINK_COUNT,
	.u8_peripheralLinkCount = 0,
	.scanParams = &m_scan_params,
	.conParams  = &m_connection_param,
	.f_dirAddrEna = true,
	.p_bleUuid = &m_nus_uuid,
	.pu8_macAddr = NULL,
};

/*--------------------------------------------------------------------------+
|  local helper functions                                                   |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/


ADC_Channel* BRD_getADCBatteryChannel() {
	return &m_ADC_BATTERY;
}

PWM_Context* BRD_getPWM0Context() {
	return &m_PWM0;
}


PWM_Context* BRD_getPWM1Context() {
	return &m_PWM1;
}
KX122_Context* BRD_getKX122Context() {
	return &m_KX122;
}

MT29F_Context* BRD_getMT29FContext() {
	return &m_MT29F;
}

FM25V_Context* BRD_getFM25VContext() {
	return &m_FM25V;
}

BLE_C_Context* BRD_getBLEContext(void) {
	return &m_bleContext;
}

/*--------------------------------------------------------------------------*/

/* eof */
