/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: VERSION_H
 *      State: Not formally tested
 * Originator: Schwinghammer
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/Publibike/Fahrradausleihstation/nRF_Firmware/trunk/version.h $
 *  $Revision: 23725 $
 *      $Date: 2018-03-22 11:38:49 +0100 (Do, 22 Mrz 2018) $
 *    $Author: Schwinghammer $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef VERSION_H_
#define VERSION_H_

#include <stdint.h>

/* Parameter definition */
#define NRF_FW_VERSION_LENGTH           3

#define FW_MAJOR    ((uint8_t)   0) ///< uint8_t major version, increased when breaking compatibility with previous versions
#define FW_MINOR    ((uint8_t)   0) ///< uint8_t xxy -> xx: release, y: bugfix
#define	FW_DEV	    ((uint8_t)   1) ///< uint8_t 0: Release, >0: dev version / release candidates

/**
 * @file
 * This is the version header file.
 *
 * 0.33.2
 * n/a something done
 *
 ****************************************************************************
 */

uint8_t* version_get_array(void);

uint8_t version_get_length(void);

#endif /* VERSION_H_ */
