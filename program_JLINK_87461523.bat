@cd /D %~dp0
nrfjprog -s 87461523 --recover                                                                    -f nrf52
nrfjprog -s 87461523 --eraseall                                                                   -f nrf52
nrfjprog -s 87461523 --program lib\components\softdevice\s132\hex\s132_nrf52_4.0.2_softdevice.hex -f nrf52 --sectorerase
nrfjprog -s 87461523 --program Debug\Parkinson_BA.hex                                             -f nrf52 --sectorerase
nrfjprog -s 87461523 --reset                                                                      -f nrf52