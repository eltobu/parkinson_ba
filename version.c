/*
 * version.c
 *
 *  Created on: 24.11.2017
 *      Author: VM
 */

#include "version.h"

static uint8_t au8_nrf_fw_version[NRF_FW_VERSION_LENGTH] = {FW_MAJOR, FW_MINOR, FW_DEV};

uint8_t* version_get_array(void)
{
    return au8_nrf_fw_version;
}

uint8_t version_get_length(void)
{
    return NRF_FW_VERSION_LENGTH;
}
