@cd /D %~dp0
nrfjprog --recover                                                                    -f nrf52
nrfjprog --eraseall                                                                   -f nrf52
nrfjprog --program lib\components\softdevice\s132\hex\s132_nrf52_4.0.2_softdevice.hex -f nrf52 --sectorerase
nrfjprog --program Debug\Parkinson_BA.hex                                             -f nrf52 --sectorerase
nrfjprog --reset                                                                      -f nrf52