/****************************************************************************
 *
 *   Customer: A.M.I.
 *   Project#: 16-01
 *       Name: Uroseal
*
 *     Module: Module Configuration
 *      State: Not formally tested
 * Originator: Busslinger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/ETH/Parkinson_BA/ModuleA/trunk/module_config.h $
 *  $Revision: 25749 $
 *      $Date: 2018-08-18 12:45:30 +0200 (Sat, 18 Aug 2018) $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file module_config.h
 *
 * Project specific module configuration NOT board related.
 * Several common used modules can be optimized and configured for the specific
 * application. Configurations for these modules not hardware related
 * (which are located in board.h/.c) can be found here.
 *
 * You may leave in configurations not in use for the current project
 ****************************************************************************
 */

#ifndef MODULE_CONFIG_H_
#define MODULE_CONFIG_H_

/* Includes needed by this header -----------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * Macros
 */

#define MODULE_ENABLED(module) \
    ((defined(module ## _ENABLED) && (module ## _ENABLED)) ? 1 : 0)

/*-------------------------------------------------------------------------*/
// Trace configuration over all and module specific
/*-------------------------------------------------------------------------*/

#define AOT_STDOUT                      // aot_printf() code is enabled

#define TR_INFO     ///< INFO and ERROR will be shown
//#define TRACES_NO   ///< No traces in the final product (Info, error, H/M/L)

// Module depended trace settings (default TR_DBG_NO)
// For debugging use TR_DBG_H (urgent), TR_DBG_M or TR_DBG_L (blabber)
#define CFG_DBG_MAIN         TR_DBG_H   ///< For file main.c
#define CFG_DBG_SYS          TR_DBG_H   ///< For file sys.c
#define CFG_DBG_LOGIC1       TR_DBG_H   ///< For file fsm1.c
#define CFG_DBG_LOGIC2       TR_DBG_H   ///< For file fsm2.c
#define CFG_DBG_COM          TR_DBG_H   ///< For file com.c
#define CFG_DBG_FRAM         TR_DBG_H   ///< For file fram.c
#define CFG_DBG_EXT_FLASH    TR_DBG_H   ///< For file ext_flash.c
#define CFG_DBG_BATTERY      TR_DBG_NO  ///< For file battery.c
#define CFG_DBG_BUZZER       TR_DBG_NO  ///< For file buzzer.c
#define CFG_DBG_MOTOR        TR_DBG_NO  ///< For file motor.c
#define CFG_DBG_UART         TR_DBG_NO  ///< For file uart.c
#define CFG_DBG_ADC_DRIVER   TR_DBG_NO  ///< For file \adc_drv.c
#define CFG_DBG_SPI          TR_DBG_NO  ///< For file \spi.c
#define CFG_DBG_TWI          TR_DBG_NO  ///< For file \twi.c
#define CFG_DBG_ADC          TR_DBG_NO  ///< For file \adc.c
#define CFG_DBG_PWM          TR_DBG_NO  ///< For file \pwm.c
#define CFG_DBG_FLASH        TR_DBG_NO  ///< For file \flash.c
#define CFG_DBG_FW_LOADER    TR_DBG_NO  ///< For file \firmware_loader.c
#define CFG_DBG_SYSTEM_TIME  TR_DBG_NO  ///< For file \system_time.c
#define CFG_DBG_RTC          TR_DBG_NO  ///< For file \rtc.c
#define CFG_DBG_BLE          TR_DBG_NO  ///< For file \ble\advertising, services
#define CFG_DBG_BLE_PROT     TR_DBG_NO  ///< For file \ble_prot.c, ..
#define CFG_DBG_MESSAGE      TR_DBG_NO  ///< For file generic\..\message.c
#define CFG_DBG_SCED         TR_DBG_NO
#define CFG_DBG_FRUN         TR_DBG_NO
#define CFG_DBG_QUEUE        TR_DBG_NO
#define CFG_DBG_FIFO         TR_DBG_NO
#define CFG_DBG_ICP          TR_DBG_NO // Do not turn on when trace itself uses ICP
#define CFG_DBG_PWRH         TR_DBG_NO
#define CFG_GENT_TRACE_LEVEL TR_DBG_NO


/*-------------------------------------------------------------------------*/
// Testing
/*-------------------------------------------------------------------------*/
#define TEST_VIB_START
#undef TEST_VIB_START
#define TEST_VIB_DRIFT
#undef TEST_VIB_DRIFT
#define TEST_ACC_TRACE
#undef TEST_ACC_TRACE

/*-------------------------------------------------------------------------*/
// generic settings
/*-------------------------------------------------------------------------*/

#define CFG_NO  1   ///< no / don't use; MUST NOT CHANGE
#define CFG_YES 2   ///< yes / use; MUST NOT CHANGE


/*-------------------------------------------------------------------------*/
// HAL / SubSys
/*-------------------------------------------------------------------------*/

#define CFG_SPI_HW_USED

#define CFG_GPIO_MAX_FUNCTIONS 4

/**
 * Flash Module Configuration
 */
#define CFG_FLASH_DISABLE_ERRATA_CHECK      CFG_YES ///<configuration is known good
#define CFG_FLASH_HAL_TEST                  CFG_NO  ///<enable flash HAL test code

/*
 * Device name
 */
#define CFG_DEVICE_NAME "Parkinson TCD" /**< Name of device. Will be included in the advertising data. */
#define CFG_DEVICE_NAME_TYPE_PREFIX  " M"
#define CFG_DEVICE_NAME_NBR_PREFIX  " #"

/*-------------------------------------------------------------------------*/
// Acceleration Measurement
/*-------------------------------------------------------------------------*/

#define BAT_WARNING_VOLTAGE			3500							// [mV]
#define ACC_SAMPLE_FREQUENCY		KX122_OSA_50					// select frequency from @KX122_DataRate
#define ACC_SAMPLE_FREQUENCY_HZ		50								// select frequency in Hertz
#define ACC_RANGE					KX122_4G						// select range from @KX122_Range
#define ACC_MIN_RANGE_MG			-4000							// select minimum of range in mg
#define ACC_MAX_RANGE_MG			4000							// select maximum of range in mg
#define ACC_SAMPLES_PER_TRANSFER	10								// set threshold to generate interrupt
#define BUF_PAST_DATA_TIME			180								// past data within this period in seconds will be recorded
#define UART_RETRANSMIT_TIMEOUT		3000							// time in seconds before retransmitting a packet
#define UART_RETRANSMIT_MAX			3								// Maxmimum amount of times to resend a packet

/*-------------------------------------------------------------------------*/
// BLE
/*-------------------------------------------------------------------------*/

/*
 * A tag that refers to the BLE stack configuration we set with @ref sd_ble_cfg_set.
 * Default tag is @ref BLE_CONN_CFG_TAG_DEFAULT.
 */
#define BLE_CONN_CFG_TAG    1

/*
 * BLE Radio config
 */
#define BLE_TX_POWER		4      // accepted values are -40, -30, -20, -16, -12, -8, -4, 0, 3, and 4 dBm

/*
 * BLE Number of links
 */
#define BLE_CENTRAL_CENTRAL_LINK_COUNT		2
#define BLE_CENTRAL_PERIPHERAL_LINK_COUNT	0
#define BLE_CENTRAL_TOTAL_LINK_COUNT			BLE_CENTRAL_CENTRAL_LINK_COUNT + BLE_CENTRAL_PERIPHERAL_LINK_COUNT
#define BLE_PERIPHERAL_CENTRAL_LINK_COUNT		0
#define BLE_PERIPHERAL_PERIPHERAL_LINK_COUNT	1
#define BLE_PERIPHERAL_TOTAL_LINK_COUNT			BLE_PERIPHERAL_CENTRAL_LINK_COUNT + BLE_PERIPHERAL_PERIPHERAL_LINK_COUNT
#if BLE_M1_TOTAL_LINK_COUNT >= BLE_M2_TOTAL_LINK_COUNT
#define BLE_MAX_TOTAL_LINK_COUNT		BLE_CENTRAL_TOTAL_LINK_COUNT
#elif
#define BLE_MAX_TOTAL_LINK_COUNT		BLE_PERIPHERAL_TOTAL_LINK_COUNT
#endif

/*
 * BLE IRK
 */
#define BLE_USE_IRK			0
#define BLE_IRK {0x38, 0x88, 0x10, 0xa3,\
                 0x6d, 0xf7, 0xaa, 0xd5,\
                 0x5e, 0xcf, 0x9a, 0x30,\
                 0xa9, 0xce, 0x3b, 0x7d }

/*
 * Scheme for creating our UUIDs:
 *
 * Custom UUID: XXXXYYYY-0000-1000-8000-00805F9B34FB
 *
 * 1.	Take the base BLE UUID: 00000000-0000-1000-8000-00805F9B34FB
 * 2. 	Change the 2 last bytes with the company's ID: 					AoT VID = 1f95
 * 3. 	XXXX: 16-bit value identifying the device/product:				use 'PT' as hex = 5054
 * 4. 	YYYY: 16-bit value for the specific service or characteristic:	Base is 0000
 *
 * Parkinson Base UART Service Base UUID: 50540000-0000-1000-8000-00805F9B1f95
 *
 */

/*
 * BLE UART Service uuids
 */
#define BLE_BASE_UUID	{{0x95, 0x1f, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x54, 0x50}} /**< Used vendor specific UUID. */

#define BLE_UUID_NUS_SERVICE           0x0000                       /**< The UUID of the Nordic UART Service. */
#define BLE_UUID_NUS_RX_CHARACTERISTIC 0x0002                      	/**< The UUID of the TX Characteristic. */
#define BLE_UUID_NUS_TX_CHARACTERISTIC 0x0003                      	/**< The UUID of the RX Characteristic. */

#define BLE_NO_CONN_TIMEOUT_MS	3000

/*-------------------------------------------------------------------------*/
// BLE peripheral
/*-------------------------------------------------------------------------*/
#define CONF_BLE_PERIPHERAL_TRACE_MAC_ADDRESS_AT_INIT
#undef  CONF_BLE_PERIPHERAL_TRACE_MAC_ADDRESS_AT_INIT

/* Automatic reset of the nRF upon connection problems.
 * When defined: enabled, else disable */
#define CONF_BLE_RESET_ON_CONNECTION_PROBLEM
#undef CONF_BLE_RESET_ON_CONNECTION_PROBLEM

/*-------------------------------------------------------------------------*/
// BLE fifo
/*-------------------------------------------------------------------------*/
#define CONFIG_BLE_FIFO_RX_FIFO_SIZE 1100   //todo: adjust FIFO size to 64
#define CONFIG_BLE_FIFO_TX_FIFO_SIZE 0      // unused

/*-------------------------------------------------------------------------*/
// Protocol
/*-------------------------------------------------------------------------*/
#define PROTOCOL_VERSION_NRF            ((uint32_t)2)   ///< uint32_t protocol version, running number

/*-------------------------------------------------------------------------*/
// MM - Memory Management * memorymanagement.c
/*-------------------------------------------------------------------------*/
#define CFG_MM_ENABLE                       1           // 0: module disabled, 1: module enabled
#define CFG_MM_TRACE_LEVEL                  TR_DBG_NO   // module trace level
#define CFG_MM_TRACE_INITIALIZATION         0           // 0: disable, 1: enable
#define CFG_MM_BLOCK_TRACING_DELAY_MS       5           // delay in ms between traces of a block when listing all blocks
#define CFG_MM_HEAP_SIZE                    (20 * 1024) // size of heap in byte
#define CFG_MM_CHUNK_SIZE                   (16)        // size of chunk in byte
#define CFG_MM_SMALL_BLOCK_HEADER_MODE      1           // 0: disable, 1: enable
#define CFG_MM_EXTENDED_BLOCK_HEADER_MODE   0           // 0: disable, 1: enable

/*-------------------------------------------------------------------------*/
// UART FIFO -
/*-------------------------------------------------------------------------*/
#define USE_UART
#define CFG_UART_FIFO_SIZE_RX               32        // bytes
#define CFG_UART_FIFO_SIZE_TX				0

/*-------------------------------------------------------------------------*/
// External Flash -
/*-------------------------------------------------------------------------*/
#define CFG_EXT_FLASH_SPARE_BLOCKS          80        // blocks

/*-------------------------------------------------------------------------*/
// Application
/*-------------------------------------------------------------------------*/
#define CONFIG_NRF_NORDIC				// DISABLE for RIGADO. NOT ACTIVE

#ifndef CONFIG_NRF_NORDIC
	#define CONFIG_NRF_RIGADO
#endif

/*-------------------------------------------------------------------------*/

#endif /*MODULE_CONFIG_H_*/
