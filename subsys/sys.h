/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: System
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef SYS_H_
#define SYS_H_

#include <stdint.h>
#include <stdbool.h>
#include "module_config.h"

#ifdef __cplusplus
extern "C" {
#endif

// Communication Callbacks
void SYS_time_cb(int64_t time);
void SYS_role_cb(uint8_t assigned_role);

void SYS_init();
void SYS_wait();
void SYS_test();
char* SYS_get_name();
uint16_t SYS_get_name_length();
uint8_t SYS_get_module_type();
bool SYS_is_wrist_module();
bool SYS_is_leg_module();
uint32_t SYS_get_serial_number();
bool SYS_is_left();
bool SYS_is_right();

#ifdef __cplusplus
}
#endif

#endif /* SYS_H_ */
