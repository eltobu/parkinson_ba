/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Logic Leg Module
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef LOGIC2_H_
#define LOGIC2_H_

#include <stdint.h>
#include "module_config.h"

#ifdef __cplusplus
extern "C" {
#endif

// Communication Callbacks
void LOGIC2_vibration_cb(uint8_t* data, uint16_t length);

void LOGIC2_init();
void LOGIC2_handler();
void LOGIC2_status();

#ifdef __cplusplus
}
#endif

#endif /* LOGIC2_H_ */
