/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: FRAM Buffer
 *      State: Not formally tested
 * Originator: Büchli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include "fram.h"

#include "board.h"

// AoT Shared
#include "error.h"
#include "trace.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_FRAM
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_FRAM undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_FRAM
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 |  variables                                                               |
 +--------------------------------------------------------------------------*/

static int64_t rb_time[2];
static uint32_t rb_head[2];
static uint32_t rb_read[2];
static uint8_t rb_first[2];

/*--------------------------------------------------------------------------+
 |  functions                                                               |
 +--------------------------------------------------------------------------*/

uint32_t FRAM_init() {
	uint32_t status = FM25V_init(BRD_getFM25VContext());

	FRAM_clear_ring_buffer(0);
	FRAM_clear_ring_buffer(1);

	return status;
}

uint32_t FRAM_read_device_info(uint8_t* data, uint16_t address, uint16_t size) {
	if (address + size > BRD_FRAM_DEVICE_INFO_SIZE)
		return ERR_ERROR;
	return FM25V_readChunk(BRD_getFM25VContext(),
	BRD_FRAM_DEVICE_INFO_START_ADDRESS + address, data, size, NULL);
}

uint32_t FRAM_write_device_info(uint8_t* data, uint16_t address, uint16_t size) {
	if (address + size > BRD_FRAM_DEVICE_INFO_SIZE)
		return ERR_ERROR;
	uint32_t status = FM25V_writeChunk(BRD_getFM25VContext(),
	BRD_FRAM_DEVICE_INFO_START_ADDRESS + address, data, size, NULL);
	RETURN_ON_ERROR(status);
	return FM25V_readChunk(BRD_getFM25VContext(),
	BRD_FRAM_DEVICE_INFO_START_ADDRESS + address, data, size, NULL);
}

uint32_t FRAM_read_flash_info(uint8_t* data, uint16_t address, uint16_t size) {
	if (address + size > BRD_FRAM_FLASH_INFO_SIZE)
		return ERR_ERROR;
	return FM25V_readChunk(BRD_getFM25VContext(),
	BRD_FRAM_FLASH_INFO_START_ADDRESS + address, data, size, NULL);
}

uint32_t FRAM_write_flash_info(uint8_t* data, uint16_t address, uint16_t size) {
	if (address + size > BRD_FRAM_FLASH_INFO_SIZE)
		return ERR_ERROR;
	uint32_t status = FM25V_writeChunk(BRD_getFM25VContext(),
	BRD_FRAM_FLASH_INFO_START_ADDRESS + address, data, size, NULL);
	RETURN_ON_ERROR(status);
	return FM25V_readChunk(BRD_getFM25VContext(),
	BRD_FRAM_FLASH_INFO_START_ADDRESS + address, data, size, NULL);
}

uint32_t FRAM_read_persistent_data(uint8_t* data, uint16_t address,
		uint16_t size) {
	if (address + size > BRD_FRAM_PERSISTENT_SIZE)
		return ERR_ERROR;
	return FM25V_readChunk(BRD_getFM25VContext(),
	BRD_FRAM_PERSISTENT_START_ADDRESS + address, data, size, NULL);
}

uint32_t FRAM_write_persistent_data(uint8_t* data, uint16_t address,
		uint16_t size) {
	if (address + size > BRD_FRAM_PERSISTENT_SIZE)
		return ERR_ERROR;
	uint32_t status = FM25V_writeChunk(BRD_getFM25VContext(),
	BRD_FRAM_PERSISTENT_START_ADDRESS + address, data, size, NULL);
	RETURN_ON_ERROR(status);
	return FM25V_readChunk(BRD_getFM25VContext(),
	BRD_FRAM_PERSISTENT_START_ADDRESS + address, data, size, NULL);
}

uint32_t FRAM_save_pattern(uint8_t role, uint8_t* pattern, uint16_t length) {
	uint32_t pattern_start;
	uint32_t pattern_size;
	if (!role) {
		pattern_start = BRD_FRAM_PATTERN_LEFT_START_ADDRESS;
		pattern_size = BRD_FRAM_PATTERN_LEFT_SIZE;
	} else {
		pattern_start = BRD_FRAM_PATTERN_RIGHT_START_ADDRESS;
		pattern_size = BRD_FRAM_PATTERN_RIGHT_SIZE;
	}

	if (length >= pattern_size) {
		return ERR_ERROR;
	}

	return FM25V_writeChunk(BRD_getFM25VContext(), pattern_start, pattern,
			length, NULL);
}

uint32_t FRAM_load_pattern(uint8_t role, uint8_t* pattern, uint16_t* length) {
	uint32_t pattern_start;
	uint32_t pattern_size;
	if (!role) {
		pattern_start = BRD_FRAM_PATTERN_LEFT_START_ADDRESS;
		pattern_size = BRD_FRAM_PATTERN_LEFT_SIZE;
	} else {
		pattern_start = BRD_FRAM_PATTERN_RIGHT_START_ADDRESS;
		pattern_size = BRD_FRAM_PATTERN_RIGHT_SIZE;
	}

	uint32_t status = FM25V_readChunk(BRD_getFM25VContext(), pattern_start,
			pattern, pattern_size, NULL);
	*length = 2 + 2 * pattern[0];
	return status;
}

uint32_t FRAM_read_ring_buffer_start(uint8_t role, int64_t time) {
	uint32_t rb_size;
	if (!role) {
		rb_size = BRD_FRAM_RING_BUFFER_L_SIZE;
	} else {
		rb_size = BRD_FRAM_RING_BUFFER_R_SIZE;
	}

	if (time < rb_time[role]) {
		uint32_t negPos = 6
				* ((rb_time[role] - time) * (int64_t) ACC_SAMPLE_FREQUENCY_HZ
						/ 1000L);
		// safety fallback
		if (negPos > rb_size) {
			negPos = 6 * BUF_PAST_DATA_TIME * ACC_SAMPLE_FREQUENCY_HZ;
		}
		if (rb_head[role] > negPos) {
			rb_read[role] = rb_head[role] - negPos;
		} else if (!rb_first[role]) {
			rb_read[role] = rb_size - negPos + rb_head[role];
		} else {
			rb_read[role] = 0;
		}
		return ERR_OK;
	} else {
		rb_read[role] = rb_head[role] - 1;
		return ERR_ERROR;
	}
}

uint32_t FRAM_remaining_ring_buffer_data(uint8_t role) {
	uint32_t rb_size;
	if (!role) {
		rb_size = BRD_FRAM_RING_BUFFER_L_SIZE;
	} else {
		rb_size = BRD_FRAM_RING_BUFFER_R_SIZE;
	}

	if (rb_head[role] >= rb_read[role]) {
		return rb_head[role] - rb_read[role];
	} else {
		return rb_size - (rb_read[role] - rb_head[role]);
	}
}

uint32_t FRAM_read_ring_buffer(uint8_t role, uint8_t* data, uint16_t size) {
	uint32_t status;

	uint32_t rb_start;
	uint32_t rb_size;
	if (!role) {
		rb_start = BRD_FRAM_RING_BUFFER_L_START_ADDRESS;
		rb_size = BRD_FRAM_RING_BUFFER_L_SIZE;
	} else {
		rb_start = BRD_FRAM_RING_BUFFER_R_START_ADDRESS;
		rb_size = BRD_FRAM_RING_BUFFER_R_SIZE;
	}

	if (rb_read[role] + size <= rb_size) {
		uint32_t idx = rb_start + rb_read[role];

		status = FM25V_readChunk(BRD_getFM25VContext(), idx, data, size, NULL);
		RETURN_ON_ERROR(status);

		rb_read[role] += size;
	} else {
		uint32_t sizeA = rb_size - rb_read[role];
		uint32_t sizeB = size - sizeA;
		uint32_t idxA = rb_start + rb_read[role];
		uint32_t idxB = rb_start;

		status = FM25V_readChunk(BRD_getFM25VContext(), idxA, data, sizeA,
		NULL);
		RETURN_ON_ERROR(status);
		status = FM25V_readChunk(BRD_getFM25VContext(), idxB, data + sizeA,
				sizeB,
				NULL);
		RETURN_ON_ERROR(status);

		rb_read[role] = sizeB;
	}
	return ERR_OK;
}

uint32_t FRAM_append_ring_buffer(uint8_t role, uint8_t* data, uint8_t size,
		int64_t time) {
	uint32_t status;

	uint32_t rb_start;
	uint32_t rb_size;
	if (!role) {
		rb_start = BRD_FRAM_RING_BUFFER_L_START_ADDRESS;
		rb_size = BRD_FRAM_RING_BUFFER_L_SIZE;
	} else {
		rb_start = BRD_FRAM_RING_BUFFER_R_START_ADDRESS;
		rb_size = BRD_FRAM_RING_BUFFER_R_SIZE;
	}

	if (rb_head[role] + size <= rb_size) {
		uint32_t idx = rb_start + rb_head[role];

		status = FM25V_writeChunk(BRD_getFM25VContext(), idx, data, size, NULL);
		RETURN_ON_ERROR(status);

		rb_head[role] += size;
		rb_time[role] = time;
	} else {
		uint32_t sizeA = rb_size - rb_head[role];
		uint32_t sizeB = size - sizeA;
		uint32_t idxA = rb_start + rb_head[role];
		uint32_t idxB = rb_start;

		status = FM25V_writeChunk(BRD_getFM25VContext(), idxA, data, sizeA,
		NULL);
		RETURN_ON_ERROR(status);
		status = FM25V_writeChunk(BRD_getFM25VContext(), idxB, data + sizeA,
				sizeB,
				NULL);
		RETURN_ON_ERROR(status);

		rb_head[role] = sizeB;
		rb_time[role] = time;
		rb_first[role] = 0;
	}
	return ERR_OK;
}

void FRAM_clear_ring_buffer(uint8_t role) {
	rb_time[role] = 0;
	rb_head[role] = 0;
	rb_first[role] = 1;
}
