/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Battery
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include "battery.h"

#include "board.h"

// AoT Shared
#include "adc_drv.h"
#include "error.h"
#include "system_time.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_BATTERY
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_BATTERY undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_BATTERY
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

uint32_t BATTERY_init() {
	GPIO_pinConfigOutput(BRD_POWER_BAT_CHECK);
	return ADC_channelInit(BRD_getADCBatteryChannel());
}

uint16_t BATTERY_get_voltage() {
	uint32_t status;
	int16_t value;

	GPIO_pinSet(BRD_POWER_BAT_CHECK);
	status = ADC_startSampling();
	if (status != ERR_OK)
		return 0;

	TIME_waitMicroSeconds(2000);

	ADC_getValue(BRD_getADCBatteryChannel()->u8Channel, &value);

	ADC_stopSampling();
	GPIO_pinClear(BRD_POWER_BAT_CHECK);

	return BRD_ADC_CONVERSION_BATTERY_IN_MV(value);
}
