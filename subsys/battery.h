/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Battery
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef BATTERY_H_
#define BATTERY_H_

#include <stdint.h>
#include "module_config.h"

#ifdef __cplusplus
extern "C" {
#endif

uint32_t BATTERY_init();
uint16_t BATTERY_get_voltage();

#ifdef __cplusplus
}
#endif

#endif /* BATTERY_H_ */
