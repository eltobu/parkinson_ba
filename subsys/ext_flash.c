/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: External Flash
 *      State: Not formally tested
 * Originator: Büchli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include "ext_flash.h"

#include "board.h"

// AoT Shared
#include "error.h"
#include "trace.h"

#include "fram.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_EXT_FLASH
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_EXT_FLASH undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_EXT_FLASH
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 |  variables                                                               |
 +--------------------------------------------------------------------------*/

static uint8_t page_buffer[2048];

static int64_t header_time;
static uint16_t header_frequency;
static uint16_t header_min_range;
static uint16_t header_max_range;

static uint16_t event_count = 0;
static uint8_t recording = 0;
static uint16_t start_block;
static uint16_t active_page;
static uint16_t pos_on_page;

static uint8_t bad_block_map[512];
static uint16_t replacement_map[CFG_EXT_FLASH_SPARE_BLOCKS][2];
static uint8_t replacement_map_size;
static uint16_t block_offset;

/*--------------------------------------------------------------------------+
 |  functions                                                               |
 +--------------------------------------------------------------------------*/

uint32_t EXT_FLASH_init() {
	uint32_t status;

	// read bad block map
	status = FRAM_read_flash_info((uint8_t*) bad_block_map, 0, 512);
	RETURN_ON_ERROR(status);

	// read replacement map
	status = FRAM_read_flash_info((uint8_t*) replacement_map, 512,
			4 * CFG_EXT_FLASH_SPARE_BLOCKS);
	RETURN_ON_ERROR(status);

	// read replacement map size
	status = FRAM_read_flash_info((uint8_t*) &replacement_map_size, 832, 1);
	RETURN_ON_ERROR(status);

	// read block offset
	status = FRAM_read_flash_info((uint8_t*) &block_offset, 833, 2);
	RETURN_ON_ERROR(status);

	BRD_getMT29FContext()->badBlockCallback = &EXT_FLASH_register_bad_block;
	status = MT29F_init(BRD_getMT29FContext());
	RETURN_ON_ERROR(status);

	// read event count
	status = FRAM_read_device_info((uint8_t*) &event_count, 8, 2);
	RETURN_ON_ERROR(status);

	recording = 0;

	for (uint16_t i = 0; i < replacement_map_size; ++i) {
		TRACE_DBG_L("RME: %d %d\n", replacement_map[i][0],
				replacement_map[i][1]);
	}

	return ERR_OK;
}

uint32_t EXT_FLASH_read_uuid(uint8_t* data) {
	return MT29F_readUniqueId(BRD_getMT29FContext(), data, NULL);
}

uint32_t EXT_FLASH_read_bbm_uuid(uint8_t* data) {
	return FRAM_read_flash_info(data, 835, 16);
}

uint32_t EXT_FLASH_write_bbm_uuid(uint8_t* data) {
	return FRAM_write_flash_info(data, 835, 16);
}

uint32_t EXT_FLASH_read_factory_bad_block_map() {
	uint32_t status;

	uint8_t uuid[16];
	status = EXT_FLASH_read_uuid(uuid);
	RETURN_ON_ERROR(status);
	uint8_t bbm_uuid[16];
	status = EXT_FLASH_read_bbm_uuid(bbm_uuid);
	RETURN_ON_ERROR(status);
	if (memcmp(uuid, bbm_uuid, 16) != 0) {
		EXT_FLASH_write_bbm_uuid(uuid);

		status = EXT_FLASH_reset_bad_block_map();
		RETURN_ON_ERROR(status);

		uint32_t ok = 0;
		uint32_t err = 0;
		uint32_t bad = 0;
		uint32_t norm = 0;
		uint8_t data[BRD_MT29F_SPARE_SIZE];
		for (uint16_t j = 0; j < BRD_MT29F_DEVICE_SIZE; ++j) {
			status = MT29F_readPage(BRD_getMT29FContext(),
					(0 + j) * BRD_MT29F_BLOCK_SIZE, (uint8_t*) &data,
					MT29F_SPARE,
					NULL);
			if (status == ERR_OK)
				++ok;
			if (status == ERR_ERROR)
				++err;
			if (data[0] == 0x00) {
				++bad;
				status = EXT_FLASH_register_bad_block(j);
				BREAK_ON_ERROR(status);
			}
			if (data[0] == 0xFF)
				++norm;
		}
		TRACE_PRINT("FACTORY BAD BLOCK SCAN FINISHED!\n");
		TRACE_DBG_H("OK: %d ERR: %d BAD: %d NORM: %d\n", ok, err, bad, norm);
	}

	return ERR_OK;
}

uint32_t EXT_FLASH_reset_bad_block_map() {
	uint32_t status;

	TRACE_DBG_H("RMS: %d BO: %d EC: %d\n", replacement_map_size, block_offset,
			event_count);

	// reset bad block map
	for (uint16_t i = 0; i < 512; ++i) {
		bad_block_map[i] = 0;
	}
	status = FRAM_write_flash_info((uint8_t*) bad_block_map, 0, 512);
	RETURN_ON_ERROR(status);

	// reset replacement map
	for (uint16_t i = 0; i < CFG_EXT_FLASH_SPARE_BLOCKS; ++i) {
		replacement_map[i][1] = 0;
	}
	status = FRAM_write_flash_info((uint8_t*) replacement_map, 512,
			4 * CFG_EXT_FLASH_SPARE_BLOCKS);
	RETURN_ON_ERROR(status);

	// reset replacement map size
	replacement_map_size = 0;
	status = FRAM_write_flash_info((uint8_t*) &replacement_map_size, 832, 1);
	RETURN_ON_ERROR(status);

	// reset block offset
	block_offset = 0;
	status = FRAM_write_flash_info((uint8_t*) &block_offset, 833, 2);
	RETURN_ON_ERROR(status);

	// reset event count
	event_count = 0;
	status = FRAM_write_device_info((uint8_t*) &event_count, 8, 2);
	RETURN_ON_ERROR(status);

	TRACE_DBG_H("RMS: %d BO: %d EC: %d\n", replacement_map_size, block_offset,
			event_count);

	return ERR_OK;
}

uint32_t EXT_FLASH_logic_to_physical(uint32_t logic, uint32_t* physical) {
	uint16_t block = logic / BRD_MT29F_BLOCK_SIZE;
	uint8_t pos = block / 8;
	uint8_t mask = 1 << (block % 8);

	if (bad_block_map[pos] & mask) {
		for (uint16_t i = 0; i < replacement_map_size; ++i) {
			if (replacement_map[i][0] == logic) {
				*physical = (replacement_map[i][1] * BRD_MT29F_BLOCK_SIZE)
						| (logic & BRD_MT29F_PAGE_MASK);
				return ERR_OK;
			}
		}
		return ERR_ERROR;
	} else {
		*physical = logic;
		return ERR_OK;
	}
}

uint32_t EXT_FLASH_register_bad_block(uint32_t address) {
	uint32_t status;

	TRACE_DBG_H("RBB: %d %d\n", address, replacement_map_size);

	uint8_t pos = address / 8;
	uint8_t mask = 1 << (address % 8);

	// update bad block map
	bad_block_map[pos] |= mask;
	status = FRAM_write_flash_info((uint8_t*) &bad_block_map[pos], pos, 1);
	RETURN_ON_ERROR(status);

	if (replacement_map_size >= CFG_EXT_FLASH_SPARE_BLOCKS)
		return ERR_ERROR;

	// add replacement
	replacement_map[replacement_map_size][0] = address;
	replacement_map[replacement_map_size][1] = BRD_MT29F_DEVICE_SIZE
			- CFG_EXT_FLASH_SPARE_BLOCKS + replacement_map_size;

	status = FRAM_write_flash_info(
			(uint8_t*) &replacement_map[replacement_map_size][0],
			512 + 4 * replacement_map_size, 4);
	RETURN_ON_ERROR(status);

	++replacement_map_size;
	status = FRAM_write_flash_info((uint8_t*) &replacement_map_size, 832, 1);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t EXT_FLASH_read(uint32_t pageAddress, uint16_t start, uint8_t* data,
		uint16_t size) {
	uint32_t status;

	status = EXT_FLASH_logic_to_physical(pageAddress, &pageAddress);
	RETURN_ON_ERROR(status);

	status = MT29F_readChunk(BRD_getMT29FContext(), pageAddress, start, data,
			size, NULL);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t EXT_FLASH_write(uint32_t pageAddress, uint16_t start, uint8_t* data,
		uint16_t size) {
	uint32_t status;

	status = EXT_FLASH_logic_to_physical(pageAddress, &pageAddress);
	RETURN_ON_ERROR(status);

	status = MT29F_writeChunk(BRD_getMT29FContext(), pageAddress, start, data,
			size, NULL);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint32_t EXT_FLASH_erase(uint16_t blockAddress) {
	uint32_t status;

	status = MT29F_eraseBlock(BRD_getMT29FContext(), blockAddress, NULL);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}

uint16_t EXT_FLASH_get_event_count() {
	return event_count;
}

uint32_t EXT_FLASH_read_event_header(uint16_t pos, uint8_t* data,
		uint16_t* size) {
	uint32_t status;

	uint16_t read_start_block = pos * 2;
	if (read_start_block < BRD_MT29F_DEVICE_SIZE - CFG_EXT_FLASH_SPARE_BLOCKS) {
		// add block offset
		read_start_block = (block_offset + read_start_block)
				% (BRD_MT29F_DEVICE_SIZE - CFG_EXT_FLASH_SPARE_BLOCKS);

		// read header
		status = EXT_FLASH_read(read_start_block * BRD_MT29F_BLOCK_SIZE, 0,
				data, 18);
		RETURN_ON_ERROR(status);
		*size = 18;
	}

	return ERR_OK;
}

uint32_t EXT_FLASH_read_event(uint16_t pos, uint32_t offset, uint8_t* data,
		uint16_t size) {
	uint32_t status;

	uint16_t read_start_block = pos * 2;
	if (read_start_block < BRD_MT29F_DEVICE_SIZE - CFG_EXT_FLASH_SPARE_BLOCKS) {
		//add block offset
		read_start_block = (block_offset + read_start_block)
				% (BRD_MT29F_DEVICE_SIZE - CFG_EXT_FLASH_SPARE_BLOCKS);

		// read data
		if (offset % 2048 + size <= BRD_MT29F_PAGE_SIZE) {
			uint32_t pageIdx = read_start_block * BRD_MT29F_BLOCK_SIZE + 1
					+ offset / 2048;
			uint32_t idx = offset % 2048;

			status = EXT_FLASH_read(pageIdx, idx, data, size);
			RETURN_ON_ERROR(status);
		} else {
			// only page aligned access is allowed
			return ERR_ERROR;
		}
	}

	return ERR_OK;
}

uint32_t EXT_FLASH_new_event_header(int64_t time) {
	uint32_t status;

	start_block = event_count * 2;

	if (start_block < BRD_MT29F_DEVICE_SIZE - CFG_EXT_FLASH_SPARE_BLOCKS) {
		recording = 1;
		// add block offset
		start_block = (block_offset + start_block)
				% (BRD_MT29F_DEVICE_SIZE - CFG_EXT_FLASH_SPARE_BLOCKS);

		// increment event count
		++event_count;

		// erase blocks
		status = EXT_FLASH_erase(start_block);
		RETURN_ON_ERROR(status);
		status = EXT_FLASH_erase(start_block + 1);
		RETURN_ON_ERROR(status);

		// save header
		header_time = time;
		header_frequency = ACC_SAMPLE_FREQUENCY_HZ;
		header_min_range = ACC_MIN_RANGE_MG;
		header_max_range = ACC_MAX_RANGE_MG;

		active_page = 1;
		pos_on_page = 0;

		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

uint32_t EXT_FLASH_append_event(uint8_t* data, uint16_t size) {
	uint32_t status;

	if (recording) {
		// append data
		if (active_page < 2 * BRD_MT29F_BLOCK_SIZE) {
			if (pos_on_page + size <= BRD_MT29F_PAGE_SIZE) {
				uint32_t idx = pos_on_page;

				memcpy(page_buffer + idx, data, size);

				pos_on_page += size;
			} else {
				uint32_t sizeA = BRD_MT29F_PAGE_SIZE - pos_on_page;
				uint32_t sizeB = size - sizeA;
				uint32_t pageIdxA = start_block * BRD_MT29F_BLOCK_SIZE
						+ active_page;
				uint32_t idxA = pos_on_page;
				uint32_t idxB = 0;

				memcpy(page_buffer + idxA, data, sizeA);

				status = EXT_FLASH_write(pageIdxA, 0, page_buffer,
				BRD_MT29F_PAGE_SIZE);
				RETURN_ON_ERROR(status);

				memcpy(page_buffer + idxB, data + sizeA, sizeB);

				++active_page;
				pos_on_page = sizeB;
			}
		} else {
			return ERR_ERROR;
		}

		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

uint32_t EXT_FLASH_end_event() {
	uint32_t status;

	if (recording) {
		if (active_page < 2 * BRD_MT29F_BLOCK_SIZE && pos_on_page > 0) {
			uint32_t pageIdx = start_block * BRD_MT29F_BLOCK_SIZE + active_page;

			status = EXT_FLASH_write(pageIdx, 0, page_buffer, pos_on_page);
			RETURN_ON_ERROR(status);
		}

		uint32_t length = (active_page - 1) * BRD_MT29F_PAGE_SIZE + pos_on_page;
		uint8_t buf[18];
		buf[0] = header_time & 0xFF;
		buf[1] = (header_time >> 8) & 0xFF;
		buf[2] = (header_time >> 16) & 0xFF;
		buf[3] = (header_time >> 24) & 0xFF;
		buf[4] = (header_time >> 32) & 0xFF;
		buf[5] = (header_time >> 40) & 0xFF;
		buf[6] = (header_time >> 48) & 0xFF;
		buf[7] = (header_time >> 56) & 0xFF;
		buf[8] = header_frequency & 0xFF;
		buf[9] = (header_frequency >> 8) & 0xFF;
		buf[10] = header_min_range & 0xFF;
		buf[11] = (header_min_range >> 8) & 0xFF;
		buf[12] = header_max_range & 0xFF;
		buf[13] = (header_max_range >> 8) & 0xFF;
		buf[14] = length & 0xFF;
		buf[15] = (length >> 8) & 0xFF;
		buf[16] = (length >> 16) & 0xFF;
		buf[17] = (length >> 24) & 0xFF;
		status = EXT_FLASH_write(start_block * BRD_MT29F_BLOCK_SIZE, 0, buf,
				18);
		RETURN_ON_ERROR(status);

		status = FRAM_write_device_info((uint8_t*) &event_count, 8, 2);
		RETURN_ON_ERROR(status);

		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

uint32_t EXT_FLASH_clear_events() {
	uint32_t status;

	// update block offset
	block_offset = (block_offset + 2 * event_count)
			% (BRD_MT29F_DEVICE_SIZE - CFG_EXT_FLASH_SPARE_BLOCKS);
	status = FRAM_write_flash_info((uint8_t*) &block_offset, 833, 2);
	RETURN_ON_ERROR(status);

	// reset event count
	event_count = 0;
	status = FRAM_write_device_info((uint8_t*) &event_count, 8, 2);
	RETURN_ON_ERROR(status);

	return ERR_OK;
}
