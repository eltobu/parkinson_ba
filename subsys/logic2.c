/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Logic Leg Module
 *      State: Not formally tested
 * Originator: Büchli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include <logic2.h>
#include "board.h"

// AoT Shared
#include "error.h"
#include "trace.h"
#include "rtc.h"
#include "system_time.h"
#include "function_run.h"
#include "gpio.h"
#include "sys.h"
#include "com.h"
#include "fram.h"
#include "ble_peripheral.h"
#include "battery.h"
#include "motor.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_LOGIC2
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_LOGIC2 undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_LOGIC2
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 |  typedefs                                                                |
 +--------------------------------------------------------------------------*/

typedef void* (*StateFunc)();

/*--------------------------------------------------------------------------+
 |  variables                                                               |
 +--------------------------------------------------------------------------*/

// FSM A
static StateFunc A_state;
static bool A_start;
static bool A_stop;
static uint8_t A_vibration_pattern[512];
static uint16_t A_vibration_length;
static uint16_t A_vibration_intensity;
static uint16_t A_vibration_pos;
static uint64_t A_wait_timeout;

static uint64_t acc_data_ready_time = 0;
static uint8_t vib_on = 0;
static uint8_t vib_on_late = 0;

/*--------------------------------------------------------------------------+
 |  FSM A                                                                   |
 +--------------------------------------------------------------------------*/

static void* A_idle();
static void* A_vibration_start();
static void* A_vibration_on();
static void* A_vibration_on_wait();
static void* A_vibration_off();
static void* A_vibration_off_wait();
static void* A_vibration_end();

static void* A_idle() {
	if (A_start)
		return A_vibration_start;
	else
		return A_idle;
}

static void* A_vibration_start() {
#ifdef TEST_VIB_START
	TRACE_PRINT("START\n");
#endif

	A_start = false;
	A_stop = false;

	if (SYS_is_left()) {
		FRAM_load_pattern(0, A_vibration_pattern, &A_vibration_length);
	} else if (SYS_is_right()) {
		FRAM_load_pattern(1, A_vibration_pattern, &A_vibration_length);
	} else {
		return A_idle;
	}

	if (A_vibration_pattern[1] > 200) {
		A_vibration_pattern[1] = 200;
	}
	A_vibration_intensity = ((uint32_t) A_vibration_pattern[1]) * 5 * 3000
			/ BATTERY_get_voltage();
	A_vibration_pos = 2;

	A_wait_timeout = TIME_getMilliSeconds();

	return A_vibration_on;
}

static void* A_vibration_on() {
#ifdef TEST_VIB_DRIFT
	TRACE_PRINT("ON\n");
#endif

	MOTOR_vibrate(A_vibration_intensity);
	vib_on = 1;
	vib_on_late = 1;

	A_wait_timeout += A_vibration_pattern[A_vibration_pos++] * 10;

	return A_vibration_on_wait;
}

static void* A_vibration_on_wait() {
	if (TIME_isTimeout(A_wait_timeout)) {
		return A_vibration_off;
	} else if (A_stop) {
		return A_vibration_end;
	} else {
		return A_vibration_on_wait;
	}
}

static void* A_vibration_off() {
	MOTOR_vibrate(0);
	vib_on = 0;

	A_wait_timeout += A_vibration_pattern[A_vibration_pos++] * 10;
	return A_vibration_off_wait;
}

static void* A_vibration_off_wait() {
	if (TIME_isTimeout(A_wait_timeout)) {
		if (A_vibration_pos < A_vibration_length) {
			return A_vibration_on;
		} else {
			return A_vibration_end;
		}
	} else if (A_stop) {
		return A_vibration_end;
	} else {
		return A_vibration_off_wait;
	}
}

static void* A_vibration_end() {
	A_start = false;
	A_stop = false;

	MOTOR_vibrate(0);
	vib_on = 0;

	return A_idle;
}

// Reed
static void LOGIC2_reed_handler_debounced();
static void LOGIC2_reed_handler() {
	FUNR_unregister(LOGIC2_reed_handler_debounced);
	FUNR_register(LOGIC2_reed_handler_debounced, 50);
}

static void LOGIC2_reed_handler_debounced() {
	if (GPIO_pinRead(BRD_REED_IN) == 1)
		return;

	TRACE_DBG_M("Reed activated!\n");

	LOGIC2_status();
}

// Accelerometer Interrupts
static void LOGIC2_acc_int1_handler() {
	TRACE_DBG_M("Accelerometer Interrupt 1!\n");

	if (acc_data_ready_time) {
		// not yet processed
		TRACE_DBG_H("ACC INT too fast!\n");
	}

	acc_data_ready_time = TIME_getMilliSeconds();
}

static void LOGIC2_acc_int2_handler() {
	TRACE_DBG_M("Accelerometer Interrupt 2!\n");

	LOGIC2_status();
}

// Blinking LED
static void LOGIC2_blink_handler_slow_late();
static void LOGIC2_blink_handler_slow() {
	if (ble_peripheral_get_conn_handle() != BLE_CONN_HANDLE_INVALID)
		GPIO_pinSet(BRD_LED_B_OUT);

	FUNR_register(LOGIC2_blink_handler_slow_late, 100);
}

static void LOGIC2_blink_handler_slow_late() {
	GPIO_pinClear(BRD_LED_B_OUT);

	FUNR_register(LOGIC2_blink_handler_slow, 4900);
}

static void LOGIC2_blink_handler_fast_late();
static void LOGIC2_blink_handler_fast() {
	if (ble_peripheral_get_conn_handle() == BLE_CONN_HANDLE_INVALID)
		GPIO_pinSet(BRD_LED_B_OUT);

	FUNR_register(LOGIC2_blink_handler_fast_late, 100);
}

static void LOGIC2_blink_handler_fast_late() {
	GPIO_pinClear(BRD_LED_B_OUT);

	FUNR_register(LOGIC2_blink_handler_fast, 400);
}

void LOGIC2_init() {
	// Initialize FSMs
	A_start = false;
	A_stop = false;
	A_state = A_idle;

	// LEDs
	GPIO_pinConfigOutput(BRD_LED_R_OUT);
	GPIO_pinConfigOutput(BRD_LED_G_OUT);
	GPIO_pinConfigOutput(BRD_LED_B_OUT);

	// UI
	GPIO_pinConfigInputWithCallback(BRD_REED_IN, GPIO_PIN_NOPULL,
			GPIO_PIN_FALLING, LOGIC2_reed_handler);
	GPIO_pinConfigInputWithCallback(BRD_ACC_INT1, GPIO_PIN_NOPULL,
			GPIO_PIN_RISING, LOGIC2_acc_int1_handler);
	GPIO_pinConfigInputWithCallback(BRD_ACC_INT2, GPIO_PIN_NOPULL,
			GPIO_PIN_RISING, LOGIC2_acc_int2_handler);

	FUNR_register(LOGIC2_blink_handler_slow, 500);
	FUNR_register(LOGIC2_blink_handler_fast, 500);
}

void LOGIC2_handler() {
	A_state = A_state();

	if (acc_data_ready_time) {
		uint16_t length = 10 + ACC_SAMPLES_PER_TRANSFER * 6;
		uint8_t buf[length];
		uint8_t* data = &buf[10];
		KX122_readBuffer(BRD_getKX122Context(), data, KX122_16BIT,
		ACC_SAMPLES_PER_TRANSFER, NULL);

#ifdef TEST_ACC_TRACE
		for (uint8_t i = 0; i < ACC_SAMPLES_PER_TRANSFER; ++i) {
			TRACE_PRINT("%d, %d, %d\n",
					(int16_t ) (data[i * 6 + 0] | data[i * 6 + 1] << 8),
					(int16_t ) (data[i * 6 + 2] | data[i * 6 + 3] << 8),
					(int16_t ) (data[i * 6 + 4] | data[i * 6 + 5] << 8));
		}
#endif

		if (vib_on || vib_on_late) {
			if (!vib_on) {
				vib_on_late = 0;
			}
			for (uint8_t i = 0; i < ACC_SAMPLES_PER_TRANSFER * 6; ++i) {
				data[i] = 0;
			}
		}

		buf[0] = acc_data_ready_time & 0xFF;
		buf[1] = (acc_data_ready_time >> 8) & 0xFF;
		buf[2] = (acc_data_ready_time >> 16) & 0xFF;
		buf[3] = (acc_data_ready_time >> 24) & 0xFF;
		buf[4] = (acc_data_ready_time >> 32) & 0xFF;
		buf[5] = (acc_data_ready_time >> 40) & 0xFF;
		buf[6] = (acc_data_ready_time >> 48) & 0xFF;
		buf[7] = (acc_data_ready_time >> 56) & 0xFF;
		buf[8] = ACC_SAMPLES_PER_TRANSFER & 0xFF;
		buf[9] = (ACC_SAMPLES_PER_TRANSFER >> 8) & 0xFF;
		COM_peripheral_send_icp(0, TYPE_ACCELERATION_DATA_1, buf, length);
		acc_data_ready_time = 0;

		// probably unnecessary
//		FRAM_append_ring_buffer(0, data, ACC_SAMPLES_PER_TRANSFER * 6,
//				acc_data_ready_time);
	}
}

static void LOGIC2_status_end();
void LOGIC2_status() {
	FUNR_unregister(LOGIC2_status_end);

	if (SYS_is_left()) {
		GPIO_pinSet(BRD_LED_R_OUT);
		GPIO_pinClear(BRD_LED_G_OUT);
	}
	if (SYS_is_right()) {
		GPIO_pinClear(BRD_LED_R_OUT);
		GPIO_pinSet(BRD_LED_G_OUT);
	}

	FUNR_register(LOGIC2_status_end, 10000);
}

static void LOGIC2_status_end() {
	GPIO_pinClear(BRD_LED_R_OUT);
	GPIO_pinClear(BRD_LED_G_OUT);
}

// Communication Callbacks
void LOGIC2_vibration_cb(uint8_t* data, uint16_t length) {
	if (length == 1) {
		if (data[0] == 1) {
			uint8_t buf[8];
			*((uint64_t*) buf) = TIME_getMilliSeconds();
			COM_peripheral_send_icp(0, TYPE_SYNC_1, buf, 8);

			LOGIC2_status();

			A_start = true;
		} else if (data[0] == 0) {
			LOGIC2_status();

			A_stop = true;
		}
	}
}
