/****************************************************************************
 *
 *   Customer: A.M.I.
 *   Project#: 16-01
 *       Name: Uroseal
 *
 *     Module: ADC data and channel handler
 *      State: Not formally tested
 * Originator: Leuenberger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/A.M.I/Uroseal/Implant/trunk/subsys/adc.h $
 *  $Revision: 22002 $
 *      $Date: 2017-12-14 17:22:32 +0100 (Thu, 14 Dec 2017) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Module adc: ADC data handler
 *
 * This module is the interface between the ADC driver and the data users.
 *
 ****************************************************************************
 */

#ifndef ADC_H
#define ADC_H

/* Includes needed by this header  ----------------------------------------*/
#include "adc_drv.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global defines                                                           |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/

/**
 *  Analog-to-digital converter channel structure
 */
typedef struct tagADC_Channel
{
    nrf_saadc_channel_config_t *pConfig;  ///< Ptr to ADC channel config
    uint8_t                    u8Channel; ///< ADC number [0 AD_CHANNELS-1]
} ADC_Channel;


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/

/**
 * @brief Function to initialize the ADC driver module for this application
 *
 * @details This function will set up all the necessary parameters.
 * 			No channels are initialized.
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t ADC_init(void);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function to de-initialize the ADC driver
 *
 * @details Closes ADC module and sets pin to default confiq.
 * 			All channels are closed and pins are set to initial state.
 */
void ADC_deinit(void);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function to set up an ADC channel
 *
 * @details This function will configures ALL the actual ADC channels.
 *
 * @param   pChannel    Pointer to ADC channel configuration
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t ADC_channelInit(ADC_Channel *pChannel);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function to remove an ADC channel
 *
 * @param   pChannel    Pointer to ADC channel configuration
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t ADC_channelDeinit(ADC_Channel *pChannel);

/*-------------------------------------------------------------------------*/

/**
 * @brief Starts sampling to get new values soon
 *
 * @retval  ERR_OK if operation was successful, error code otherwise
 */
uint32_t ADC_startSampling(void);

/*-------------------------------------------------------------------------*/

/**
 * @brief Stops sampling to get a calm channel de-init
 */
void ADC_stopSampling(void);

/*-------------------------------------------------------------------------*/

/**
 * @brief Function to read the value of an ADC channel
 *
 * @param   Channel     Channel number of requested value
 * @param   pValue      Pointer to result register.
 *                      0 is returned when busy (After 1. trigger)
 */
void ADC_getValue(uint8_t Channel, int16_t *pValue);

/*-------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // ADC_H
