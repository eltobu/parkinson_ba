/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: External Flash
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef EXT_FLASH_H_
#define EXT_FLASH_H_

#include <stdint.h>
#include "module_config.h"

#ifdef __cplusplus
extern "C" {
#endif

uint32_t EXT_FLASH_init();
// Flash
uint32_t EXT_FLASH_read_uuid(uint8_t* data);
uint32_t EXT_FLASH_read_bbm_uuid(uint8_t* data);
uint32_t EXT_FLASH_write_bbm_uuid(uint8_t* data);
uint32_t EXT_FLASH_read_factory_bad_block_map();
uint32_t EXT_FLASH_reset_bad_block_map();
uint32_t EXT_FLASH_logic_to_physical(uint32_t logic, uint32_t* physical);
uint32_t EXT_FLASH_register_bad_block(uint32_t address);
uint32_t EXT_FLASH_read(uint32_t pageAddress, uint16_t start, uint8_t* data, uint16_t size);
uint32_t EXT_FLASH_write(uint32_t pageAddress, uint16_t start, uint8_t* data, uint16_t size);
uint32_t EXT_FLASH_erase(uint16_t blockAddress);
// Events
uint16_t EXT_FLASH_get_event_count();
uint32_t EXT_FLASH_read_event_header(uint16_t pos, uint8_t* data, uint16_t* size);
uint32_t EXT_FLASH_read_event(uint16_t pos, uint32_t offset, uint8_t* data, uint16_t size);
uint32_t EXT_FLASH_new_event_header(int64_t time);
uint32_t EXT_FLASH_append_event(uint8_t* data, uint16_t size);
uint32_t EXT_FLASH_end_event();
uint32_t EXT_FLASH_clear_events();

#ifdef __cplusplus
}
#endif

#endif /* EXT_FLASH_H_ */
