/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: FRAM
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef FRAM_H_
#define FRAM_H_

#include <stdint.h>
#include "module_config.h"

#ifdef __cplusplus
extern "C" {
#endif

uint32_t FRAM_init();
// Device Info
uint32_t FRAM_read_device_info(uint8_t* data, uint16_t address, uint16_t size);
uint32_t FRAM_write_device_info(uint8_t* data, uint16_t address, uint16_t size);
// Flash Info
uint32_t FRAM_read_flash_info(uint8_t* data, uint16_t address, uint16_t size);
uint32_t FRAM_write_flash_info(uint8_t* data, uint16_t address, uint16_t size);
uint32_t FRAM_read_persistent_data(uint8_t* data, uint16_t address,
		uint16_t size);
uint32_t FRAM_write_persistent_data(uint8_t* data, uint16_t address,
		uint16_t size);
uint32_t FRAM_save_pattern(uint8_t role, uint8_t* pattern, uint16_t length);
uint32_t FRAM_load_pattern(uint8_t role, uint8_t* pattern, uint16_t* length);
uint32_t FRAM_read_ring_buffer_start(uint8_t role, int64_t time);
uint32_t FRAM_remaining_ring_buffer_data(uint8_t role);
uint32_t FRAM_read_ring_buffer(uint8_t role, uint8_t* data, uint16_t size);
uint32_t FRAM_append_ring_buffer(uint8_t role, uint8_t* data, uint8_t size,
		int64_t time);
void FRAM_clear_ring_buffer(uint8_t role);

#ifdef __cplusplus
}
#endif

#endif /* FRAM_H_ */
