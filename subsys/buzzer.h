/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Buzzer
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef BUZZER_H_
#define BUZZER_H_

#include <stdint.h>
#include "module_config.h"

#ifdef __cplusplus
extern "C" {
#endif

uint32_t BUZZER_init();
void BUZZER_on();
void BUZZER_on_freq(uint32_t frequency);
void BUZZER_off();

#ifdef __cplusplus
}
#endif

#endif /* BUZZER_H_ */
