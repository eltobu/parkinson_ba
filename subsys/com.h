/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Communication
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef COM_H_
#define COM_H_

#include <stdint.h>
#include "module_config.h"
#include "icp_type.h"

/*
 * Handle magic values
 */
#define UART_HANDLE         0x7F

#ifdef __cplusplus
extern "C" {
#endif

void COM_minimal_init();
void COM_minimal_send_raw(uint8_t handle, uint8_t* data, uint16_t length);
uint32_t COM_minimal_send_icp(uint8_t handle, Interchange_Protocol_Type ipt,
		uint8_t* data, uint16_t length);
void COM_minimal_handler();

void COM_peripheral_init();
void COM_peripheral_send_raw(uint8_t handle, uint8_t* data, uint16_t length);
uint32_t COM_peripheral_send_icp(uint8_t handle, Interchange_Protocol_Type ipt,
		uint8_t* data, uint16_t length);
void COM_peripheral_handler();

void COM_central_init();
void COM_central_send_raw(uint8_t handle, uint8_t* data, uint16_t length);
uint32_t COM_central_send_icp(uint8_t handle, Interchange_Protocol_Type ipt,
		uint8_t* data, uint16_t length);
void COM_central_handler();

#ifdef __cplusplus
}
#endif

#endif /* COM_H_ */
