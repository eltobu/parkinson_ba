/** @file
 *
 *    Customer: AoT
 *    Project#: 00-00
 * ProjectName: Art of Technology
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/ETH/Parkinson_BA/ModuleA/trunk/subsys/error_private.h $
 *  $Revision: 21193 $
 *      $Date: 2017-10-30 15:49:11 +0100 (Mon, 30 Oct 2017) $
 *    $Author: busslinger $
 *
 *  $ Originator: Busslinger
 *  @copyright Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 * @brief This is a temporary module until the error handling is well defined.
 *
 ***************************************************************************/


#ifndef ERROR_PRIVATE_H
#define ERROR_PRIVATE_H

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif // ERROR_PRIVATE_H
