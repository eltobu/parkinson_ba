/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Buzzer
 *      State: Not formally tested
 * Originator: Büchli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include "buzzer.h"

#include "board.h"

// AoT Shared
#include "error.h"
#include "system_time.h"
#include "trace.h"
#include "pwm.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_BUZZER
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_BUZZER undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_BUZZER
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

uint32_t BUZZER_init() {
	uint32_t status;

	status = PWM_init(BRD_getPWM1Context(), 0, NULL);
	PWM_start(BRD_getPWM1Context());

	return status;
}

void BUZZER_on() {
	uint32_t status = PWM_generateOne(BRD_getPWM1Context(),
			BRD_PWM1_MODULE_CHANNEL_BUZZER, PWM_DUTY_CYCLE_MAX / 2, NULL);
	if (status != ERR_OK)
		TRACE_DBG_H("PWM generate failed");
}

void BUZZER_on_freq(uint32_t frequency) {
	PWM_stop(BRD_getPWM1Context());
	PWM_deinit(BRD_getPWM1Context());
	BRD_getPWM1Context()->u32Frequency = frequency;
	PWM_init(BRD_getPWM1Context(), 0, NULL);
	PWM_start(BRD_getPWM1Context());

	uint32_t status = PWM_generateOne(BRD_getPWM1Context(),
			BRD_PWM1_MODULE_CHANNEL_BUZZER, PWM_DUTY_CYCLE_MAX / 2, NULL);
	if (status != ERR_OK)
		TRACE_DBG_H("PWM generate failed");
}

void BUZZER_off() {
	uint32_t status = PWM_generateOne(BRD_getPWM1Context(),
	BRD_PWM1_MODULE_CHANNEL_BUZZER, 0, NULL);
	if (status != ERR_OK)
		TRACE_DBG_H("PWM generate failed");
}

