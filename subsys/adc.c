/****************************************************************************
 *
 *   Customer: A.M.I.
 *   Project#: 16-01
 *       Name: Uroseal
 *
 *     Module: ADC data and channel handler
 *      State: Not formally tested
 * Originator: Leuenberger
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/A.M.I/Uroseal/Implant/trunk/subsys/adc.c $
 *  $Revision: 22470 $
 *      $Date: 2018-01-16 13:14:29 +0100 (Tue, 16 Jan 2018) $
 *    $Author: busslinger $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************
 */
/**
 * @file
 * Detailed description in header file
 *
 ****************************************************************************
 */

/* headers for this module ------------------------------------------------*/
#include "adc.h"
#include "board.h"
#include "trace.h"
#include <stdbool.h>

/* -------------------------------------------------------------------------- */

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_ADC
    #define LOCAL_DBG  TR_DBG_NO
    #warning "Debug level CFG_DBG_ADC undefined, turned off debugging (default)!"
#else
    #define LOCAL_DBG  CFG_DBG_ADC_MANAGER
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/**
 * Moving average over the last burst samples and sampling frequency
 */
#define BURST_SIZE          BRD_ADC_MOVING_AVERAGE
#define BURST_FREQUENCY     BRD_ADC_SAMPLING_FREQUENCY


/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/

/**
 * It stores the pointers to all the active channels (NULL for not used ones)
 */
static nrf_saadc_channel_config_t *m_ChannelConfigs[AD_CHANNELS];

/**
 * Latest output values for each channel
 */
static int32_t m_ai32_Values[AD_CHANNELS];

/**
 * Ring buffer for moving average. Linear part and definition structure
 */
static uint8_t m_RB_Buffer[AD_CHANNELS * BURST_SIZE * sizeof(int32_t)];
static RB_RingBuf m_RingBuffer;

static volatile bool m_isBusy; ///< false: new values ready


/*--------------------------------------------------------------------------+
|  local helper functions                                                   |
+--------------------------------------------------------------------------*/

/**
 * @brief Returns the number  of actually active ADC channels
 *
 * @retval          Number of active channels
 */
static uint32_t getActiveChannels(void)
{
    uint32_t Count = 0;

    for (uint32_t i = 0; i < AD_CHANNELS; i++)
    {
        if (m_ChannelConfigs[i] != NULL)
        {
            Count++;
        }
    }
    return (Count);

} // End of getActiveChannels

/* -------------------------------------------------------------------------- */

/**
 * @brief Returns the value for the given channel
 *
 * @details The driver returns only the channel values configured.
 *          Example: CH-0, CH-3 are active.
 *          The Channel-0 is found on[0]. The Channel-3 is found on[1]
 *
 * @param   Channel The number smaller than AD_CHANNELS
 *
 * @retval          The value for the given channel
 */
static int16_t getChannelValue(uint8_t Channel)
{
#if 0  // Single triggered channel
    uint32_t Index = 0;

    for (uint8_t i = 0; i < AD_CHANNELS; i++)
    {
        if (m_ChannelConfigs[i] != NULL)
        {
            if (Channel == i)
            {
                return (m_ai32_Values[Index]);
            }
            Index++;
        }
    }
    return (0); // This channel is not configured

#else  // Moving average, endless sampling

    uint32_t Status;
    int32_t  i32value;

    Status = AD_getMovingAverage(Channel, &i32value, NULL);
    if (Status == ERR_OK)
    {
        return ((int16_t)i32value);
    }
    else
    {
        TRACE_DBG_H("AD_getMovingAverage ERROR %d\n", Status);
        return (0);
    }
#endif
} // End of getChannelValue

/* -------------------------------------------------------------------------- */

static void clearMemory(void)
{
    m_isBusy = true;
    for (int i = 0; i < AD_CHANNELS; i++)
    {
        m_ChannelConfigs[i] = NULL;
        m_ai32_Values[i]    = 0;
    }
} // End of clearMemory

/* -------------------------------------------------------------------------- */

static void ADC_Callback(ERR_pErrorInfo pError)
{
    m_isBusy = false;

} // End of ADC_Callback


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

uint32_t ADC_init(void)
{
    uint32_t Status;

    clearMemory();
    Status = AD_init(BRD_ADC_RESOLUTION, NULL);
    TRACE_DBG_L("Stat %d, ADC_init resolution %d\n", Status, BRD_ADC_RESOLUTION);

    return (Status);

} // End of ADC_init

/* -------------------------------------------------------------------------- */

void ADC_deinit(void)
{
    TRACE_DBG_L("ADC deinit\n");
	clearMemory();
	AD_deinit();

} // End of ADC_deinit

/* -------------------------------------------------------------------------- */

uint32_t ADC_channelInit(ADC_Channel *pChannel)
{
    uint32_t ActiveChannels;
    uint32_t Status = ERR_OK;
    bool     isRunning = ! AD_isIdle();

    if (isRunning)
    {
        AD_stopTask();
    }
    if (pChannel->u8Channel >= AD_CHANNELS)
    {
        TRACE_DBG_H("ADC_channelInit(): Max < %d channels allowed, given %d",
                    AD_CHANNELS, (int)pChannel->u8Channel);
        return (ERR_ERROR);
    }
    m_ChannelConfigs[pChannel->u8Channel] = pChannel->pConfig;

    ActiveChannels = getActiveChannels();

    // Configure the ADC driver
    if (ActiveChannels > 0)
    {
        RB_init(&m_RingBuffer, m_RB_Buffer,
                BURST_SIZE * ActiveChannels, sizeof(int32_t));

        Status = AD_taskConfigure(AD_OM_MOVING_AVG_2_ARRAY, m_ChannelConfigs,
                                  BURST_SIZE, BURST_FREQUENCY, ADC_Callback,
                                  m_ai32_Values, &m_RingBuffer, NULL);
    }
    if (isRunning && Status == ERR_OK)
    {
        Status = ADC_startSampling();
    }
    TRACE_DBG_L("Stat %d, ADC_channelInit Nr.%d, ActiveChannels %d\n",
                 Status, pChannel->u8Channel, ActiveChannels);
	return (Status);

} // End of ADC_channelInit

/* -------------------------------------------------------------------------- */

uint32_t ADC_channelDeinit(ADC_Channel *pChannel)
{
    uint32_t Status;
    ADC_Channel Temp = *pChannel;

    Temp.pConfig = NULL;
    Status = ADC_channelInit(&Temp);

    TRACE_DBG_L("Stat %d, ADC_channelDeinit Nr.%d\n", Status, pChannel->u8Channel);
    return (Status);

} // End of ADC_channelDeinit

/* -------------------------------------------------------------------------- */

uint32_t ADC_startSampling(void)
{
    uint32_t Status;

    m_isBusy = true;
    Status = AD_startTask(NULL);

    TRACE_DBG_L("Stat %d, ADC_startSampling\n", Status);
    return (Status);

} // End of ADC_startSampling

/* -------------------------------------------------------------------------- */

void ADC_stopSampling(void)
{
    AD_stopTask();

} // End of ADC_stopSampling

/* -------------------------------------------------------------------------- */

void ADC_getValue(uint8_t Channel, int16_t *pValue)
{
    if (m_isBusy)
    {
        *pValue = 0;
        return;
    }
    if (Channel >= AD_CHANNELS)
    {
        TRACE_DBG_H("ADC_getValue(): Max < %d channels allowed, given %d",
                    AD_CHANNELS, (int)Channel);
        *pValue = 0;
    }
    *pValue = getChannelValue(Channel);

} // End of ADC_getValue

/* -------------------------------------------------------------------------- */
