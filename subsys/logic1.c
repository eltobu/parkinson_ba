/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Logic Wrist Module
 *      State: Not formally tested
 * Originator: Büchli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include "logic1.h"

#include "board.h"

// AoT Shared
#include "memorymanagement.h"
#include "error.h"
#include "trace.h"
#include "rtc.h"
#include "system_time.h"
#include "function_run.h"
#include "gpio.h"
#include "sys.h"
#include "com.h"
#include "fram.h"
#include "ext_flash.h"
#include "ble_central.h"
#include "battery.h"
#include "buzzer.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_LOGIC1
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_LOGIC1 undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_LOGIC1
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 |  typedefs                                                                |
 +--------------------------------------------------------------------------*/

typedef void* (*StateFunc)();

/*--------------------------------------------------------------------------+
 |  variables                                                               |
 +--------------------------------------------------------------------------*/

// FSM A
static StateFunc A_state;
static bool A_button;
static bool A_sync[2];
static bool A_data;
static uint8_t A_vibration_pattern[512];
static uint16_t A_vibration_length;
static uint16_t A_duration_left;
static uint16_t A_duration_right;
static uint32_t A_data_pos;
static uint64_t A_wait_timeout;

static uint8_t buffer[1200];

static int64_t time_trigger;
static int64_t time_sync[2];

static uint8_t* bufferL = &buffer[0];
static uint8_t* bufferR = &buffer[300];
static uint8_t* bufferC = &buffer[600];
static uint32_t buffer_size;

static uint8_t battery_low = 0b000; // bit position indicates module: 0: M1; 1: M2L; 2: M2R;
static uint8_t battery_low_copy;

static uint16_t event_pos = 0xFFFF;
static uint32_t event_offset;
static uint32_t event_end;
static uint16_t event_size;
static uint8_t event_retransmit_counter;

/*--------------------------------------------------------------------------+
 |  FSM A                                                                   |
 +--------------------------------------------------------------------------*/

static void* A_idle();
static void* A_triggered();
static void* A_save_header();
static void* A_wait_for_sync();
static void* A_save_past_data();
static void* A_wait_for_data();
static void* A_save_data();
static void* A_stopped();

static void* A_idle() {
	if (A_button)
		return A_triggered;
	else
		return A_idle;
}

static void* A_triggered() {
	A_button = false;

	// send vibration start
	uint8_t buf = 1;
	COM_central_send_icp(0, TYPE_VIBRATION_1, &buf, 1);
	COM_central_send_icp(1, TYPE_VIBRATION_1, &buf, 1);
	time_trigger = RTC_getRTC();

	// calculate pattern length
	FRAM_load_pattern(0, A_vibration_pattern, &A_vibration_length);
	A_duration_left = 0;
	for (int i = 2; i < A_vibration_length; ++i)
		A_duration_left += A_vibration_pattern[i];
	FRAM_load_pattern(1, A_vibration_pattern, &A_vibration_length);
	A_duration_right = 0;
	for (int i = 2; i < A_vibration_length; ++i)
		A_duration_right += A_vibration_pattern[i];

	// set timeout to longer pattern
	if (A_duration_left > A_duration_right)
		A_wait_timeout = TIME_setTimeout(A_duration_left * 10 + 500);
	else
		A_wait_timeout = TIME_setTimeout(A_duration_right * 10 + 500);

	A_sync[0] = false;
	A_sync[1] = false;
	return A_wait_for_sync;
}

static void* A_wait_for_sync() {
	if (A_sync[0] && A_sync[1])
		return A_save_header;
	else if (A_button || TIME_isTimeout(A_wait_timeout))
		return A_stopped;
	else
		return A_wait_for_sync;
}

static void* A_save_header() {
	EXT_FLASH_new_event_header(time_trigger);
	FRAM_read_ring_buffer_start(0, time_sync[0] - BUF_PAST_DATA_TIME * 1000);
	FRAM_read_ring_buffer_start(1, time_sync[1] - BUF_PAST_DATA_TIME * 1000);

	A_data_pos = 0;
	return A_save_past_data;
}

static void* A_save_past_data() {
	uint32_t size[2];
	size[0] = FRAM_remaining_ring_buffer_data(0);
	if (size[0] % 6 != 0) {
		TRACE_DBG_H("Ring Buffer 0 unexpected length: %d\n", size[0]);
	}
	size[1] = FRAM_remaining_ring_buffer_data(1);
	if (size[1] % 6 != 0) {
		TRACE_DBG_H("Ring Buffer 1 unexpected length: %d\n", size[1]);
	}
	buffer_size = ((size[0] < size[1]) ? size[0] : size[1]);
	uint8_t repeat = 0;
	if (buffer_size > 300) {
		buffer_size = 300;
		repeat = 1;
	}
	FRAM_read_ring_buffer(0, bufferL, buffer_size);
	FRAM_read_ring_buffer(1, bufferR, buffer_size);
	for (uint16_t i = 0; i < buffer_size / 6; ++i) {
		bufferC[i * 12 + 0] = bufferL[i * 6 + 0];
		bufferC[i * 12 + 1] = bufferL[i * 6 + 1];
		bufferC[i * 12 + 2] = bufferL[i * 6 + 2];
		bufferC[i * 12 + 3] = bufferL[i * 6 + 3];
		bufferC[i * 12 + 4] = bufferL[i * 6 + 4];
		bufferC[i * 12 + 5] = bufferL[i * 6 + 5];
		bufferC[i * 12 + 6] = bufferR[i * 6 + 0];
		bufferC[i * 12 + 7] = bufferR[i * 6 + 1];
		bufferC[i * 12 + 8] = bufferR[i * 6 + 2];
		bufferC[i * 12 + 9] = bufferR[i * 6 + 3];
		bufferC[i * 12 + 10] = bufferR[i * 6 + 4];
		bufferC[i * 12 + 11] = bufferR[i * 6 + 5];
	}
	EXT_FLASH_append_event(bufferC, 2 * buffer_size);

	if (repeat) {
		return A_save_past_data;
	} else {
		A_data = false;
		return A_wait_for_data;
	}
}

static void* A_wait_for_data() {
	if (A_data)
		return A_save_data;
	else if (A_button || TIME_isTimeout(A_wait_timeout))
		return A_stopped;
	else
		return A_wait_for_data;
}

static void* A_save_data() {
	uint32_t size[2];
	size[0] = FRAM_remaining_ring_buffer_data(0);
	if (size[0] % 6 != 0) {
		TRACE_DBG_H("Ring Buffer 0 unexpected length: %d\n", size[0]);
	}
	size[1] = FRAM_remaining_ring_buffer_data(1);
	if (size[1] % 6 != 0) {
		TRACE_DBG_H("Ring Buffer 1 unexpected length: %d\n", size[1]);
	}
	buffer_size = ((size[0] < size[1]) ? size[0] : size[1]);
	uint8_t repeat = 0;
	if (buffer_size > 300) {
		buffer_size = 300;
		repeat = 1;
	}
	FRAM_read_ring_buffer(0, bufferL, buffer_size);
	FRAM_read_ring_buffer(1, bufferR, buffer_size);
	for (uint16_t i = 0; i < buffer_size / 6; ++i) {
		bufferC[i * 12 + 0] = bufferL[i * 6 + 0];
		bufferC[i * 12 + 1] = bufferL[i * 6 + 1];
		bufferC[i * 12 + 2] = bufferL[i * 6 + 2];
		bufferC[i * 12 + 3] = bufferL[i * 6 + 3];
		bufferC[i * 12 + 4] = bufferL[i * 6 + 4];
		bufferC[i * 12 + 5] = bufferL[i * 6 + 5];
		bufferC[i * 12 + 6] = bufferR[i * 6 + 0];
		bufferC[i * 12 + 7] = bufferR[i * 6 + 1];
		bufferC[i * 12 + 8] = bufferR[i * 6 + 2];
		bufferC[i * 12 + 9] = bufferR[i * 6 + 3];
		bufferC[i * 12 + 10] = bufferR[i * 6 + 4];
		bufferC[i * 12 + 11] = bufferR[i * 6 + 5];
	}
	EXT_FLASH_append_event(bufferC, 2 * buffer_size);

	if (repeat) {
		return A_save_data;
	} else {
		A_data = false;
		return A_wait_for_data;
	}
}

static void* A_stopped() {
	A_button = false;

	EXT_FLASH_end_event();

	// send vibration stop
	uint8_t buf = 0;
	COM_central_send_icp(0, TYPE_VIBRATION_1, &buf, 1);
	COM_central_send_icp(1, TYPE_VIBRATION_1, &buf, 1);
	return A_idle;
}

// Reed
static void LOGIC1_reed_handler_debounced();
static void LOGIC1_reed_handler() {
	FUNR_unregister(LOGIC1_reed_handler_debounced);
	FUNR_register(LOGIC1_reed_handler_debounced, 50);
}

static void LOGIC1_reed_handler_debounced() {
	if (GPIO_pinRead(BRD_REED_IN) == 1)
		return;

	TRACE_DBG_M("Reed activated!\n");

	LOGIC1_status(3);
}

// Button
static void LOGIC1_button_handler_debounced();
static void LOGIC1_button_handler() {
	FUNR_unregister(LOGIC1_button_handler_debounced);
	FUNR_register(LOGIC1_button_handler_debounced, 50);
}

static void LOGIC1_button_handler_debounced() {
	if (GPIO_pinRead(BRD_BTN_IN) == 1)
		return;

	TRACE_DBG_M("Button pressed!\n");

	LOGIC1_status(3);

	if (event_pos == 0xFFFF) { // check no event is downloading
		A_button = true;
	}
}

// Blinking LED
static void LOGIC1_blink_handler_slow_late();
static void LOGIC1_blink_handler_slow() {
	if (ble_c_get_conn_handle(0) != BLE_CONN_HANDLE_INVALID
			&& ble_c_get_conn_handle(1) != BLE_CONN_HANDLE_INVALID)
		GPIO_pinSet(BRD_LED_B_OUT);

	FUNR_register(LOGIC1_blink_handler_slow_late, 100);
}

static void LOGIC1_blink_handler_slow_late() {
	GPIO_pinClear(BRD_LED_B_OUT);

	FUNR_register(LOGIC1_blink_handler_slow, 4900);
}

static void LOGIC1_blink_handler_fast_late();
static void LOGIC1_blink_handler_fast() {
	if (ble_c_get_conn_handle(0) == BLE_CONN_HANDLE_INVALID
			|| ble_c_get_conn_handle(1) == BLE_CONN_HANDLE_INVALID)
		GPIO_pinSet(BRD_LED_B_OUT);

	FUNR_register(LOGIC1_blink_handler_fast_late, 100);
}

static void LOGIC1_blink_handler_fast_late() {
	GPIO_pinClear(BRD_LED_B_OUT);

	FUNR_register(LOGIC1_blink_handler_fast, 400);
}

// Buzzer
static void LOGIC1_buzzer1();
static void LOGIC1_buzzer() {
	battery_low_copy = battery_low;
	if (!battery_low_copy) {
		FUNR_register(LOGIC1_buzzer, 10000);
		return;
	}

	if (battery_low_copy & 0b001) {
		BUZZER_on_freq(4186);
	} else {
		BUZZER_on_freq(2093);
	}

	FUNR_register(LOGIC1_buzzer1, 200);
}

static void LOGIC1_buzzer2();
static void LOGIC1_buzzer1() {
	BUZZER_off();

	FUNR_register(LOGIC1_buzzer2, 200);
}

static void LOGIC1_buzzer3();
static void LOGIC1_buzzer2() {
	if (battery_low_copy & 0b010) {
		BUZZER_on_freq(4186);
	} else {
		BUZZER_on_freq(2093);
	}

	FUNR_register(LOGIC1_buzzer3, 200);
}

static void LOGIC1_buzzer4();
static void LOGIC1_buzzer3() {
	BUZZER_off();

	FUNR_register(LOGIC1_buzzer4, 200);
}

static void LOGIC1_buzzer5();
static void LOGIC1_buzzer4() {
	if (battery_low_copy & 0b100) {
		BUZZER_on_freq(4186);
	} else {
		BUZZER_on_freq(2093);
	}

	FUNR_register(LOGIC1_buzzer5, 200);
}

static void LOGIC1_buzzer5() {
	BUZZER_off();

	FUNR_register(LOGIC1_buzzer, 9000);
}

// Battery Check
static void LOGIC1_battery_check() {
	// Check own battery
	uint16_t voltage = BATTERY_get_voltage();
	if (voltage < BAT_WARNING_VOLTAGE - 20) {
		battery_low |= 0b001;
	}
	if (voltage > BAT_WARNING_VOLTAGE + 20) {
		battery_low &= 0b110;
	}

	// Check left battery
	if (ble_c_get_conn_handle(0) != BLE_CONN_HANDLE_INVALID) {
		COM_central_send_icp(0, TYPE_BATTERY_REQUEST_1, NULL, 0);
	} else {
		battery_low &= 0b101;
	}

	// Check right battery
	if (ble_c_get_conn_handle(1) != BLE_CONN_HANDLE_INVALID) {
		COM_central_send_icp(1, TYPE_BATTERY_REQUEST_1, NULL, 0);
	} else {
		battery_low &= 0b011;
	}

	FUNR_register(LOGIC1_battery_check, 5000);
}

void LOGIC1_init() {
	// Initialize FSMs
	A_button = false;
	A_sync[0] = false;
	A_sync[1] = false;
	A_data = false;
	A_state = A_idle;

	// Initialize LEDs
	GPIO_pinConfigOutput(BRD_LED_R_OUT);
	GPIO_pinConfigOutput(BRD_LED_G_OUT);
	GPIO_pinConfigOutput(BRD_LED_B_OUT);

	// Initialize UI
	GPIO_pinConfigInputWithCallback(BRD_REED_IN, GPIO_PIN_NOPULL,
			GPIO_PIN_FALLING, LOGIC1_reed_handler);
	GPIO_pinConfigInputWithCallback(BRD_BTN_IN, GPIO_PIN_PULLUP,
			GPIO_PIN_FALLING, LOGIC1_button_handler);

	// Initialize Event Download
	event_pos = 0xFFFF;

	FUNR_register(LOGIC1_blink_handler_slow, 500);
	FUNR_register(LOGIC1_blink_handler_fast, 500);
	FUNR_register(LOGIC1_buzzer, 500);
	FUNR_register(LOGIC1_battery_check, 500);
}

void LOGIC1_handler() {
	A_state = A_state();
}

static void LOGIC1_status_end();
void LOGIC1_status(uint8_t select) {
	FUNR_unregister(LOGIC1_status_end);

	if (select & 1) {
		GPIO_pinSet(BRD_LED_R_OUT);
	}
	if (select & 2) {
		GPIO_pinSet(BRD_LED_G_OUT);
	}

	FUNR_register(LOGIC1_status_end, 10000);
}

static void LOGIC1_status_end() {
	GPIO_pinClear(BRD_LED_R_OUT);
	GPIO_pinClear(BRD_LED_G_OUT);
}

// Communication Callbacks
void LOGIC1_sync_cb(uint8_t role, uint64_t time) {
	time_sync[role] = time;
	A_sync[role] = true;
}

void LOGIC1_acceleration_data_cb(uint8_t role, uint8_t* data, uint16_t length,
		uint64_t time) {
	FRAM_append_ring_buffer(role, data, length * 6, time);
	A_data = true;
}

void LOGIC1_event_count_request_cb() {
	uint8_t buf[2];
	uint16_t event_count = EXT_FLASH_get_event_count();
	buf[0] = event_count & 0xFF;
	buf[1] = (event_count >> 8) & 0xFF;
	COM_central_send_icp(UART_HANDLE, TYPE_EVENT_COUNT_1, buf, 2);
}

static void data_header_timeout();
void LOGIC1_event_data_request_cb(uint16_t pos) {
	uint32_t status;

	if (A_state == A_idle) { // check no event is recording
		status = EXT_FLASH_read_event_header(pos, buffer, &event_size);
		STOP_ON_ERROR(status);
		event_retransmit_counter = 1;
		status = COM_central_send_icp(UART_HANDLE, TYPE_EVENT_DATA_HEADER_1,
				buffer, event_size);
		STOP_ON_ERROR(status);
		FUNR_register(data_header_timeout, 3000);

		event_pos = pos;
		event_offset = 0;
		event_end = buffer[14] | (buffer[15] << 8) | (buffer[16] << 16)
				| (buffer[17] << 24);

		if (event_end > (BRD_MT29F_BLOCK_SIZE * 2 - 1) * BRD_MT29F_PAGE_SIZE) {
			event_pos = 0xFFFF;
		}
	}
}

static void data_header_timeout() {
	++event_retransmit_counter;
	COM_central_send_icp(UART_HANDLE, TYPE_EVENT_DATA_HEADER_1, buffer,
			event_size);
	if (event_retransmit_counter < UART_RETRANSMIT_MAX) {
		FUNR_register(data_header_timeout, 3000);
	} else {
		event_pos = 0xFFFF;
	}
}

static void data_fragment_timeout();
void LOGIC1_event_data_ack_cb(uint16_t id) {
	uint32_t status;

	FUNR_unregister(data_header_timeout);
	FUNR_unregister(data_fragment_timeout);
	if (event_pos == 0xFFFF) {
		return;
	}
	if (id != 0xFFFF && event_offset < event_end) {
		status = EXT_FLASH_read_event(event_pos, event_offset, buffer, 1024);
		STOP_ON_ERROR(status);
		event_retransmit_counter = 1;
		status = COM_central_send_icp(UART_HANDLE, TYPE_EVENT_DATA_FRAGMENT_1,
				buffer, 1024);
		STOP_ON_ERROR(status);
		FUNR_register(data_fragment_timeout, 3000);

		event_offset += 1024;
	} else {
		event_pos = 0xFFFF;
	}
}

static void data_fragment_timeout() {
	++event_retransmit_counter;
	COM_central_send_icp(UART_HANDLE, TYPE_EVENT_DATA_FRAGMENT_1, buffer, 1024);
	if (event_retransmit_counter < UART_RETRANSMIT_MAX) {
		FUNR_register(data_fragment_timeout, 3000);
	} else {
		event_pos = 0xFFFF;
	}
}

void LOGIC1_event_clear_cb() {
	if (A_state == A_idle) { // check no event is recording
		EXT_FLASH_clear_events();
	}
}

void LOGIC1_battery_cb(uint8_t role, uint16_t voltage) {
	if (voltage < BAT_WARNING_VOLTAGE - 20) {
		battery_low |= (role) ? 0b100 : 0b010;
	}
	if (voltage > BAT_WARNING_VOLTAGE + 20) {
		battery_low &= (role) ? 0b011 : 0b101;
	}
}

void LOGIC1_scan_bad_blocks_cb() {
	EXT_FLASH_read_factory_bad_block_map();
}

void LOGIC1_reset_bad_blocks_cb() {
	EXT_FLASH_reset_bad_block_map();
}

void LOGIC1_bbm_uuid_request_cb() {
	uint32_t status;
	uint8_t buf[16];
	status = EXT_FLASH_read_bbm_uuid(buf);
	STOP_ON_ERROR(status);

	TRACE_DBG_M("BBM UUID: 0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\n",
			buf[15], buf[14], buf[13], buf[12], buf[11], buf[10], buf[9], buf[8],
			buf[7], buf[6], buf[5], buf[4], buf[3], buf[2], buf[1], buf[0]);

	status = COM_central_send_icp(UART_HANDLE, TYPE_BBM_UUID_1, buf, 16);
	STOP_ON_ERROR(status);
}

void LOGIC1_uuid_request_cb() {
	uint32_t status;
	uint8_t buf[16];
	status = EXT_FLASH_read_uuid(buf);
	STOP_ON_ERROR(status);

	TRACE_DBG_M("    UUID: 0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\n",
			buf[15], buf[14], buf[13], buf[12], buf[11], buf[10], buf[9], buf[8],
			buf[7], buf[6], buf[5], buf[4], buf[3], buf[2], buf[1], buf[0]);

	status = COM_central_send_icp(UART_HANDLE, TYPE_UUID_1, buf, 16);
	STOP_ON_ERROR(status);
}
