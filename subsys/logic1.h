/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Logic Wrist Module
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#ifndef LOGIC1_H_
#define LOGIC1_H_

#include <stdint.h>
#include "module_config.h"

#ifdef __cplusplus
extern "C" {
#endif

// Communication Callbacks
void LOGIC1_sync_cb(uint8_t role, uint64_t time);
void LOGIC1_acceleration_data_cb(uint8_t role, uint8_t* data, uint16_t length, uint64_t time);
void LOGIC1_event_count_request_cb();
void LOGIC1_event_data_request_cb(uint16_t pos);
void LOGIC1_event_data_ack_cb(uint16_t id);
void LOGIC1_event_clear_cb();
void LOGIC1_battery_cb(uint8_t role, uint16_t voltage);
void LOGIC1_scan_bad_blocks_cb();
void LOGIC1_reset_bad_blocks_cb();
void LOGIC1_bbm_uuid_request_cb();
void LOGIC1_uuid_request_cb();

void LOGIC1_init();
void LOGIC1_handler();
void LOGIC1_status(uint8_t select);

#ifdef __cplusplus
}
#endif

#endif /* LOGIC1_H_ */
