/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Motor
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include "motor.h"

#include "board.h"

// AoT Shared
#include "error.h"
#include "system_time.h"
#include "trace.h"
#include "gpio.h"
#include "pwm.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_MOTOR
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_MOTOR undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_MOTOR
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

uint32_t MOTOR_init() {
	uint32_t status;

	GPIO_pinConfigOutput(BRD_POWER_MOTOR);

	status = PWM_init(BRD_getPWM0Context(), 0, NULL);
	PWM_start(BRD_getPWM0Context());

	return status;
}

void MOTOR_vibrate(uint16_t intensity) {
	if (intensity > 0) {
		uint32_t status = PWM_generateOne(BRD_getPWM0Context(),
				BRD_PWM0_MODULE_CHANNEL_MOTOR, intensity, NULL);
		if (status != ERR_OK)
			TRACE_DBG_H("PWM generate failed");
		GPIO_pinSet(BRD_POWER_MOTOR);
	} else {
		GPIO_pinClear(BRD_POWER_MOTOR);
	}
}
