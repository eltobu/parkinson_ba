/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: System
 *      State: Not formally tested
 * Originator: B�chli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include "sys.h"

#include "board.h"

// NRF SDK
#include "app_error.h"
#include "app_timer.h"
#include "app_gpiote.h"
#include "app_button.h"

// AoT Shared
#include "module_config.h"
#include "error.h"
#include "memorymanagement.h"
#include "rtc.h"
#include "system_time.h"
#include "function_run.h"
#include "uart.h"
#include "trace.h"

// Subsys
#include "fram.h"
#include "battery.h"
#include "buzzer.h"
#include "motor.h"
#include "com.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_SYS
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_SYS undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_SYS
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 |  variables                                                               |
 +--------------------------------------------------------------------------*/

static char name[32];
static uint16_t nameLength;
static uint8_t moduleType;
static uint32_t serialNumber;
static uint8_t role = 0;

/**
 * Real Time Clock context and RTC update handler running at interrupt priority.
 */
static void RtcUpdateHandler(int64_t SecondsSince1970);
static RTC_Context m_RTC_Context = { .i64UpdateSeconds = 10, .Callback =
		(RTC_IntervalCallback) RtcUpdateHandler, };

/**
 * @brief All time specific applications, modules must be synchronized.
 *
 * @param   u32Seconds    The newly updated second counter
 */
static void updateAllTimeApps(int64_t i64Seconds) {
	uint64_t LastMilliseconds;
	uint64_t NewMilliseconds;

	LastMilliseconds = TIME_getMilliSeconds();
	TIME_synch(i64Seconds * 1000);
	NewMilliseconds = TIME_getMilliSeconds();
	FUNR_synchTime(LastMilliseconds, NewMilliseconds);
}

/**
 * @brief The function is called every time the RTC wakes up.
 * Time is given inside RTC context
 *
 * @param   SecondsSince1970    The newly updated second counter
 */
static void RtcUpdateHandler(int64_t SecondsSince1970) {
	updateAllTimeApps(SecondsSince1970);
}

/**
 * @Application Error Handling.
 *
 * @param pError        pointer to structure containing error information
 * @param u32errParam1  parameter
 * @param u32errParam2  parameter
 */
static void ErrCallback(const ERR_pErrorInfo pError, uint32_t u32errParam1,
		uint32_t u32errParam2) {
	// All other warnings and errors
	TRACE_DBG_H("** ERROR MODULE TRAP *********************\n");
	TRACE_DBG_H("* Module 0x%lx, Class 0x%x\n", pError->moduleNumber,
			pError->errorClass);
	TRACE_DBG_H("* Line %d, ErrorNumber 0x%x\n", pError->lineNumber,
			pError->errorNumber);
	TRACE_DBG_H("* %s\n", pError->errorString);
	TRACE_DBG_H("* Parameter1: %ld / 0x%lx\n", u32errParam1, u32errParam1);
	TRACE_DBG_H("* Parameter2: %ld / 0x%lx\n", u32errParam2, u32errParam2);
	TRACE_DBG_H("******************************************\n");
}

// Communication Callbacks
void SYS_time_cb(int64_t time) {
	RTC_setRTC(time);
	TRACE_DBG_H("TIME UPDATED: 0x%08X %08X\n", (uint32_t ) (RTC_getRTC() >> 32),
			(uint32_t ) RTC_getRTC());
}

void SYS_role_cb(uint8_t assigned_role) {
	role = assigned_role;
}

void SYS_init() {
	uint32_t status;

	MM_initialize();

	// enable DC-DC-Regulator in Soft-Device configuration
	NRF_POWER->DCDCEN = 1;

	// Initialize clocks & softdevice
	nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
	SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
	sd_softdevice_disable(); // HW Bootup is done for the other devices

	// Initialize trace & error handler
	UART_init();
	UART_initTxQueue();
	TRACE_INIT(0, aot_printf);
	ERR_init(ErrCallback);

	// Initialize timing stuff
	status = app_timer_init();
	APP_ERROR_CHECK(status);
	status = RTC_init(&m_RTC_Context);
	if (status != ERR_OK)
		TRACE_DBG_H("RTC init failed: %d", status);
	TIME_init();
	TIME_synch(RTC_getRTC() * 1000); // Maybe a soft-reset and the time remained

	// Initialize Function Run
	FUNR_init(TIME_getMilliSeconds, NULL);

	// Initialize FRAM
	status = FRAM_init();
	if (status != ERR_OK)
		TRACE_DBG_H("FRAM init failed: %d", status);

	// Read Device Info
	uint8_t buf[8];
	FRAM_read_device_info(buf, 0, 8);
	moduleType = buf[0];
	serialNumber = (uint32_t) buf[4] | ((uint32_t) buf[5] << 8)
			| ((uint32_t) buf[6] << 16) | ((uint32_t) buf[7] << 24);

	// Fallback: Detect Module Type
	if (moduleType != 1 && moduleType != 2) {
		moduleType = 0; // Test Mode
//		GPIO_pinConfigInput(BRD_ACC_INT1, GPIO_PIN_PULLUP);
//		uint8_t int1up = GPIO_pinRead(BRD_ACC_INT1);
//		GPIO_pinConfigInput(BRD_ACC_INT2, GPIO_PIN_PULLUP);
//		uint8_t int2up = GPIO_pinRead(BRD_ACC_INT2);
//		GPIO_pinConfigInput(BRD_ACC_INT1, GPIO_PIN_PULLDOWN);
//		uint8_t int1down = GPIO_pinRead(BRD_ACC_INT1);
//		GPIO_pinConfigInput(BRD_ACC_INT2, GPIO_PIN_PULLDOWN);
//		uint8_t int2down = GPIO_pinRead(BRD_ACC_INT2);
//		if ((int1up != int1down) && (int2up != int2down))
//			moduleType = 1;
//		else
//			moduleType = 2;
	}

	// Setup Device Name
	nameLength = strlen(CFG_DEVICE_NAME) + strlen(CFG_DEVICE_NAME_TYPE_PREFIX)
			+ 1 + strlen(CFG_DEVICE_NAME_NBR_PREFIX) + 5;
	if (nameLength >= 32) {
		TRACE_DBG_H("Device Name Length too long: %d\n", nameLength);
		return;
	}
	memcpy(name, (const uint8_t *) CFG_DEVICE_NAME, strlen(CFG_DEVICE_NAME));

	memcpy(name + strlen(CFG_DEVICE_NAME),
			(const uint8_t *) CFG_DEVICE_NAME_TYPE_PREFIX,
			strlen(CFG_DEVICE_NAME_TYPE_PREFIX));
	snprintf(
			(char *) name + strlen(CFG_DEVICE_NAME)
					+ strlen(CFG_DEVICE_NAME_TYPE_PREFIX), 2, "%01d",
			moduleType);

	memcpy(
			name + strlen(CFG_DEVICE_NAME) + strlen(CFG_DEVICE_NAME_TYPE_PREFIX)
					+ 1, (const uint8_t *) CFG_DEVICE_NAME_NBR_PREFIX,
			strlen(CFG_DEVICE_NAME_NBR_PREFIX));
	snprintf(
			(char *) name + strlen(CFG_DEVICE_NAME)
					+ strlen(CFG_DEVICE_NAME_TYPE_PREFIX) + 1
					+ strlen(CFG_DEVICE_NAME_NBR_PREFIX), 6, "%05ld",
			serialNumber & 0xFFFF);
}

void SYS_wait() {
	// Clear exceptions and PendingIRQ from the FPU unit
	// Pending FPU IRQs or exceptions will prevent the CPU from entering sleep mode
	__set_FPSCR(__get_FPSCR() & ~(0x0000009F)); // FPU_EXCEPTION_MASK: 0x0000009F
	(void) __get_FPSCR();
	NVIC_ClearPendingIRQ(FPU_IRQn);

	uint8_t isEnabled;
	sd_softdevice_is_enabled(&isEnabled);
	if ((bool) isEnabled) {
		// Sleep while nothing to do and soft-device is active
		uint32_t status = sd_app_evt_wait();
		if (status != NRF_SUCCESS) {
			TRACE_DBG_H("sd_app_evt_wait() ERROR %d\n", status);
		}
	} else {
		// Use directly __WFE and __SEV since the SoftDevice is not available
		// Wait for event.
		__WFE();

		// Clear Event Register.
		__SEV();
		__WFE();
	}
}

void SYS_test() {
	TRACE_PRINT("--->>> TEST MODE ENTERED <<<---\n");

	GPIO_pinConfigOutput(BRD_LED_R_OUT);
	GPIO_pinConfigOutput(BRD_LED_G_OUT);
	GPIO_pinConfigOutput(BRD_LED_B_OUT);
	GPIO_pinConfigInput(BRD_REED_IN, GPIO_PIN_NOPULL);
	GPIO_pinConfigInput(BRD_BTN_IN, GPIO_PIN_PULLUP);

	// LED Test
	GPIO_pinSet(BRD_LED_R_OUT);
	TIME_waitMicroSeconds(500 * 1000);
	GPIO_pinClear(BRD_LED_R_OUT);
	GPIO_pinSet(BRD_LED_G_OUT);
	TIME_waitMicroSeconds(500 * 1000);
	GPIO_pinClear(BRD_LED_G_OUT);
	GPIO_pinSet(BRD_LED_B_OUT);
	TIME_waitMicroSeconds(500 * 1000);
	GPIO_pinClear(BRD_LED_B_OUT);

	TIME_waitMicroSeconds(1000 * 1000);

	// FRAM Test
	uint8_t fm25vId[9];
	uint32_t status = FM25V_readId(BRD_getFM25VContext(), fm25vId, NULL);
	TRACE_PRINT("FM25V ID:0x%02X (0xC2) 0x%02X (0x25) 0x%02X (0x08)\n",
			fm25vId[6], fm25vId[7], fm25vId[8])
	if (status == ERR_OK && fm25vId[6] == 0xC2 && fm25vId[7] == 0x25
			&& fm25vId[8] == 0x08)
		GPIO_pinSet(BRD_LED_R_OUT);

	TIME_waitMicroSeconds(500 * 1000);

	// Detect Type
	GPIO_pinConfigInput(BRD_ACC_INT1, GPIO_PIN_PULLUP);
	uint8_t int1up = GPIO_pinRead(BRD_ACC_INT1);
	GPIO_pinConfigInput(BRD_ACC_INT2, GPIO_PIN_PULLUP);
	uint8_t int2up = GPIO_pinRead(BRD_ACC_INT2);
	GPIO_pinConfigInput(BRD_ACC_INT1, GPIO_PIN_PULLDOWN);
	uint8_t int1down = GPIO_pinRead(BRD_ACC_INT1);
	GPIO_pinConfigInput(BRD_ACC_INT2, GPIO_PIN_PULLDOWN);
	uint8_t int2down = GPIO_pinRead(BRD_ACC_INT2);
	if ((int1up != int1down) && (int2up != int2down)) { // Wrist Module
		// Flash Test
		uint32_t status1 = MT29F_init(BRD_getMT29FContext());
		uint8_t mt29fId[2];
		uint32_t status2 = MT29F_readId(BRD_getMT29FContext(), mt29fId, NULL);
		TRACE_PRINT("MT29F ID: 0x%02X (0x2C) 0x%02X (0x36)\n", mt29fId[0],
				mt29fId[1]);
		if (status1 == ERR_OK && status2 == ERR_OK && mt29fId[0] == 0x2C
				&& mt29fId[1] == 0x36)
			GPIO_pinSet(BRD_LED_G_OUT);

		TIME_waitMicroSeconds(1000 * 1000);

		// Buzzer Test
		BUZZER_init();
		BUZZER_on();
		TIME_waitMicroSeconds(500 * 1000);
		BUZZER_off();
	} else { // Leg Module
		// Accelerometer Test
		uint32_t status1 = KX122_init(BRD_getKX122Context());
		uint8_t kx122Id[1];
		uint32_t status2 = KX122_whoAmI(BRD_getKX122Context(), kx122Id, NULL);
		TRACE_PRINT("KX122 ID: 0x%02X (0x1B)\n", kx122Id[0]);
		if (status1 == ERR_OK && status2 == ERR_OK && kx122Id[0] == 0x1B)
			GPIO_pinSet(BRD_LED_B_OUT);

		TIME_waitMicroSeconds(1000 * 1000);

		// Motor Test
		MOTOR_init();
		MOTOR_vibrate(1000);
		TIME_waitMicroSeconds(500 * 1000);
		MOTOR_vibrate(0);
	}
	GPIO_pinClear(BRD_LED_R_OUT);
	GPIO_pinClear(BRD_LED_G_OUT);
	GPIO_pinClear(BRD_LED_B_OUT);

	// Battery Test
	uint16_t voltage = BATTERY_get_voltage();
	TRACE_PRINT("Battery Voltage: %d\n", voltage);
	if (voltage < 2500 || voltage > 4500)
		GPIO_pinSet(BRD_LED_B_OUT);

	COM_minimal_init();

	// Reed & Button Test
	while (true) {
		if (!GPIO_pinRead(BRD_REED_IN))
			GPIO_pinSet(BRD_LED_R_OUT);
		else
			GPIO_pinClear(BRD_LED_R_OUT);

		if (!GPIO_pinRead(BRD_BTN_IN))
			GPIO_pinSet(BRD_LED_G_OUT);
		else
			GPIO_pinClear(BRD_LED_G_OUT);

		COM_minimal_handler();

		UART_handler();

		if (!GPIO_pinRead(BRD_REED_IN) && !GPIO_pinRead(BRD_BTN_IN)) {
			uint16_t mult = 600;

			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(510);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(770);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(550 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(575 * mult);

			BUZZER_on_freq(510);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(450 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(400 * mult);
			BUZZER_on_freq(320);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(500 * mult);
			BUZZER_on_freq(440);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(480);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(330 * mult);
			BUZZER_on_freq(450);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(200 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(200 * mult);
			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(50 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(860);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(700);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(50 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(350 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(520);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(580);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(480);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(500 * mult);

			BUZZER_on_freq(510);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(450 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(400 * mult);
			BUZZER_on_freq(320);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(500 * mult);
			BUZZER_on_freq(440);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(480);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(330 * mult);
			BUZZER_on_freq(450);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(200 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(200 * mult);
			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(50 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(860);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(700);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(50 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(350 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(520);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(580);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(480);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(500 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(720);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(680);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(620);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(650);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(570);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(220 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(720);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(680);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(620);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(650);
			TIME_waitMicroSeconds(200 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(1020);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(1020);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(1020);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(720);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(680);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(620);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(650);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(570);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(420 * mult);

			BUZZER_on_freq(585);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(450 * mult);

			BUZZER_on_freq(550);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(420 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(360 * mult);

			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(720);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(680);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(620);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(650);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(570);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(220 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(720);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(680);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(620);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(650);
			TIME_waitMicroSeconds(200 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(1020);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(1020);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(1020);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(720);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(680);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(620);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(650);
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(570);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(420 * mult);

			BUZZER_on_freq(585);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(450 * mult);

			BUZZER_on_freq(550);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(420 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(360 * mult);

			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(60 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(60 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(350 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(580);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(350 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(600 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(60 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(60 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(350 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(580);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(550 * mult);

			BUZZER_on_freq(870);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(325 * mult);
			BUZZER_on_freq(760);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(600 * mult);

			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(60 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(60 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(350 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(580);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(350 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(500);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(430);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(80 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(600 * mult);

			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(150 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(510);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_on_freq(660);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(300 * mult);
			BUZZER_on_freq(770);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(550 * mult);
			BUZZER_on_freq(380);
			TIME_waitMicroSeconds(100 * mult);
			BUZZER_off();
			TIME_waitMicroSeconds(575 * mult);
		}
	}
}

char* SYS_get_name() {
	return name;
}

uint16_t SYS_get_name_length() {
	return nameLength;
}

uint8_t SYS_get_module_type() {
	return moduleType;
}

bool SYS_is_wrist_module() {
	return moduleType == 1;
}

bool SYS_is_leg_module() {
	return moduleType == 2;
}

uint32_t SYS_get_serial_number() {
	return serialNumber;
}

bool SYS_is_left() {
	return role == 1;
}

bool SYS_is_right() {
	return role == 2;
}
