/****************************************************************************
 *
 *   Customer: ETH
 *   Project#:
 *       Name: Parkinson BA
 *
 *     Module: Communication
 *      State: Not formally tested
 * Originator: Büchli
 *
 *   $HeadURL:  $
 *  $Revision:  $
 *      $Date:  $
 *    $Author: buechli $
 *
 *  Developed by Art of Technology AG, 2018
 *
 ****************************************************************************
 */

#include "com.h"

#include "board.h"

// AoT Shared
#include "error.h"
#include "app_error_aot.h"
#include "trace.h"
#include "rtc.h"
#include "system_time.h"
#include "function_run.h"
#include "uart.h"
#include "ble_uart_fifo.h"
#include "ble_uart_queue.h"
#include "ble_advertising.h"
#include "ble_peripheral.h"
#include "ble_central.h"
#include "icp.h"
#include "icp_tools.h"
#include "generic_tools.h"
#include "memorymanagement.h"

#include "sys.h"
#include "fram.h"
#include "battery.h"
#include "logic1.h"
#include "logic2.h"

// *************************************************************
// * Debugging and check configuration
// *************************************************************
#undef LOCAL_DBG
#ifndef CFG_DBG_COM
#define LOCAL_DBG  TR_DBG_NO
#warning "Debug level CFG_DBG_COM undefined, turned off debugging (default)!"
#else
#define LOCAL_DBG  CFG_DBG_COM
#endif
#include "trace_dbg_late_include.h" //< map the TRACEs according to LOCAL_DBG

/*--------------------------------------------------------------------------+
 |  variables                                                               |
 +--------------------------------------------------------------------------*/

static uint8_t ble_buffer[BLE_MAX_TOTAL_LINK_COUNT][IPED_LENGTH_BUFFER];
static uint16_t ble_buffer_pos[BLE_MAX_TOTAL_LINK_COUNT];
static uint8_t uart_buffer[IPED_LENGTH_BUFFER];
static uint16_t uart_buffer_pos;
static uint8_t buffer_handle;

/*--------------------------------------------------------------------------+
 |  prototypes                                                       		|
 +--------------------------------------------------------------------------*/

static void COM_init();
static void COM_send_raw(uint8_t handle, uint8_t* data, uint16_t length,
		uint16_t frame_size);
static uint32_t COM_send_icp(uint8_t handle, Interchange_Protocol_Type ipt,
		uint8_t* data, uint16_t length,
		void (*send)(uint8_t handle, uint8_t* data, uint16_t length));
static void COM_data_handler();

static uint32_t COM_none_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_trace_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_time_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_device_info_request_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_device_info_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_reset_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_battery_request_cb(Interchange_Protocol_Header* iph);

/*--------------------------------------------------------------------------+
 |  common                                                                  |
 +--------------------------------------------------------------------------*/

static void COM_init() {
	// initialize queues & fifos
	for (uint8_t i = 0; i < BLE_MAX_TOTAL_LINK_COUNT; ++i) {
		BLE_initTxQueue(i);
		ble_fifo_clear_rx_fifo(i);
		BLE_Tx_CLEAR(i);
	}

	// initialize buffer
	for (uint8_t i = 0; i < BLE_MAX_TOTAL_LINK_COUNT; ++i) {
		ble_buffer_pos[i] = 0;
	}
	uart_buffer_pos = 0;
}

static void COM_send_raw(uint8_t handle, uint8_t* data, uint16_t length,
		uint16_t frame_size) {
	if (handle == UART_HANDLE) {
		queue_push(UART_getTxQueue(), data, length);
	} else {
		uint16_t nr_msgs = (length + frame_size - 1) / frame_size;
		for (uint8_t i = 0; i < nr_msgs; ++i) {
			uint16_t msg_length = length - i * frame_size;
			if (msg_length > frame_size)
				msg_length = frame_size;
			RESET_ON_MALLOC_ERROR(
					queue_push(BLE_getTxQueue(handle), &data[i * frame_size],
							msg_length));
		}
	}
}

static uint32_t COM_send_icp(uint8_t handle, Interchange_Protocol_Type ipt,
		uint8_t* data, uint16_t length,
		void (*send)(uint8_t handle, uint8_t* data, uint16_t length)) {
	uint32_t status = ERR_ERROR;
	uint8_t icp[IPED_LENGTH_BUFFER];
	uint16_t icp_length = 0;

	if (ICP_check_container((Interchange_Protocol_Header*) &ipt) != ERR_OK) {
		status = ICP_encode(ENCODE_ADD_CMD, ipt, data, length, icp,
				&icp_length);
	} else {
		return ERR_NOSYS;
	}

	status = ICP_encode(ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0, icp,
			&icp_length);

	send(handle, icp, icp_length);

	return status;
}

static void COM_data_handler() {
	uint32_t status;
	uint8_t icp[IPED_LENGTH_BUFFER];
	uint32_t icp_pos;

	for (uint8_t i = 0; i < BLE_MAX_TOTAL_LINK_COUNT; ++i) {
		if (ble_fifo_get_status_rx_fifo(i, DATEN_IM_FIFO) > 0) {
			while (ble_fifo_get_status_rx_fifo(i, DATEN_IM_FIFO) > 0
					&& ble_buffer_pos[i] < IPED_LENGTH_BUFFER) {
				ble_buffer[i][ble_buffer_pos[i]++] = ble_fifo_pop_rx_fifo(i);
			}

			buffer_handle = i;
			icp_pos = 0;
			do {
				status = ICP_decode(ble_buffer[i], ble_buffer_pos[i], &icp_pos,
						icp);

				if (status == ERR_OK
						|| status == ICP_ERR_DATA_RECEIVED_CORRECT) {
					// success, copy rest of buffer to front
					ble_buffer_pos[i] -= icp_pos;
					memcpy(ble_buffer[i], ble_buffer[i] + icp_pos,
							ble_buffer_pos[i]);
				} else if (status == ICP_ERR_WAITING_FOR_DATA) {
					// waiting for more data, do nothing
					break;
				} else {
					// failed, reset buffer
					ble_buffer_pos[i] = 0;
				}
			} while (ble_buffer_pos[i] > 0);
		}
	}

	if (UART_Rx_Status(DATEN_IM_FIFO) > 0) {
		while (UART_Rx_Status(DATEN_IM_FIFO) > 0
				&& uart_buffer_pos < IPED_LENGTH_BUFFER) {
			uart_buffer[uart_buffer_pos++] = UART_Rx_POP();
		}

		buffer_handle = UART_HANDLE;
		icp_pos = 0;
		do {
			status = ICP_decode(uart_buffer, uart_buffer_pos, &icp_pos, icp);

			if (status == ERR_OK || status == ICP_ERR_DATA_RECEIVED_CORRECT) {
				// success, copy rest of buffer to front
				uart_buffer_pos -= icp_pos;
				memcpy(uart_buffer, uart_buffer + icp_pos, uart_buffer_pos);
			} else if (status == ICP_ERR_WAITING_FOR_DATA) {
				// waiting for more data, do nothing
				break;
			} else {
				// failed, reset buffer
				uart_buffer_pos = 0;
			}
		} while (uart_buffer_pos > 0);
	}
}

static uint32_t COM_none_cb(Interchange_Protocol_Header* iph) {
	TRACE_DBG_L("DEFAULT HANDLER RECEIVED: %d %d\n", GENT_bswap_16(iph->u16Type), GENT_bswap_16(iph->u16Length));
	return ERR_OK;
}

static uint32_t COM_trace_cb(Interchange_Protocol_Header* iph) {
	iph->au8Data[GENT_bswap_16(iph->u16Length)] = 0;
	TRACE_PRINT("M2_%d -> %s", buffer_handle, iph->au8Data);
	return ERR_OK;
}

static uint32_t COM_time_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 8) {
		int64_t time = (int64_t) iph->au8Data[0]
				| ((int64_t) iph->au8Data[1] << 8)
				| ((int64_t) iph->au8Data[2] << 16)
				| ((int64_t) iph->au8Data[3] << 24)
				| ((int64_t) iph->au8Data[4] << 32)
				| ((int64_t) iph->au8Data[5] << 40)
				| ((int64_t) iph->au8Data[6] << 48)
				| ((int64_t) iph->au8Data[7] << 56);
		SYS_time_cb(time);
	}
	return ERR_OK;
}

static uint32_t COM_device_info_request_cb(Interchange_Protocol_Header* iph) {
	uint8_t buf[8];
	FRAM_read_device_info(buf, 0, 8);
	COM_peripheral_send_icp(buffer_handle, TYPE_DEVICE_INFO_1, buf, 8);
	return ERR_OK;
}

static uint32_t COM_device_info_cb(Interchange_Protocol_Header* iph) {
	FRAM_write_device_info(iph->au8Data, 0, GENT_bswap_16(iph->u16Length));
	return ERR_OK;
}

static uint32_t COM_reset_cb(Interchange_Protocol_Header* iph) {
	NVIC_SystemReset();
	return ERR_OK;
}

static uint32_t COM_battery_request_cb(Interchange_Protocol_Header* iph) {
	uint16_t batteryVoltage = BATTERY_get_voltage();
	COM_peripheral_send_icp(buffer_handle, TYPE_BATTERY_1,
			(uint8_t*) &batteryVoltage, 2);
	return ERR_OK;
}

/*--------------------------------------------------------------------------+
 |  minimal                                                              |
 +--------------------------------------------------------------------------*/

void COM_minimal_init() {
	COM_init();

	// initialize interchange protocol
	ICP_CMD_function_init();
	ICP_CMD_set_function(COM_none_cb, TYPE_DEFAULT);
	ICP_CMD_set_function(COM_trace_cb, TYPE_DEBUG_1);
	ICP_CMD_set_function(COM_time_cb, TYPE_TIME_1);
	ICP_CMD_set_function(COM_device_info_request_cb,
			TYPE_DEVICE_INFO_REQUEST_1);
	ICP_CMD_set_function(COM_device_info_cb, TYPE_DEVICE_INFO_1);
	ICP_CMD_set_function(COM_reset_cb, TYPE_RESET_1);
	ICP_CMD_set_function(COM_battery_request_cb, TYPE_BATTERY_REQUEST_1);
}

void COM_minimal_send_raw(uint8_t handle, uint8_t* data, uint16_t length) {
	COM_send_raw(handle, data, length, 0);
}

uint32_t COM_minimal_send_icp(uint8_t handle, Interchange_Protocol_Type ipt,
		uint8_t* data, uint16_t length) {
	return COM_send_icp(handle, ipt, data, length, COM_minimal_send_raw);
}

void COM_minimal_handler() {
	COM_data_handler();
}

/*--------------------------------------------------------------------------+
 |  peripheral                                                              |
 +--------------------------------------------------------------------------*/

static void COM_peripheral_on_ble_evt(ble_evt_t* p_ble_evt);
static void COM_peripheral_connected_status();
static uint32_t COM_peripheral_role_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_peripheral_pattern_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_peripheral_vibration_cb(Interchange_Protocol_Header* iph);

void COM_peripheral_init() {
	COM_init();

	// initialize interchange protocol
	ICP_CMD_function_init();
	ICP_CMD_set_function(COM_none_cb, TYPE_DEFAULT);
	ICP_CMD_set_function(COM_trace_cb, TYPE_DEBUG_1);
	ICP_CMD_set_function(COM_time_cb, TYPE_TIME_1);
	ICP_CMD_set_function(COM_device_info_request_cb,
			TYPE_DEVICE_INFO_REQUEST_1);
	ICP_CMD_set_function(COM_device_info_cb, TYPE_DEVICE_INFO_1);
	ICP_CMD_set_function(COM_reset_cb, TYPE_RESET_1);
	ICP_CMD_set_function(COM_battery_request_cb, TYPE_BATTERY_REQUEST_1);

	ICP_CMD_set_function(COM_peripheral_role_cb, TYPE_ASSIGN_ROLE_1);
	ICP_CMD_set_function(COM_peripheral_pattern_cb, TYPE_PATTERN_1);
	ICP_CMD_set_function(COM_peripheral_vibration_cb, TYPE_VIBRATION_1);

	// setup device name
	uint8_t* name = (uint8_t*) SYS_get_name();
	uint16_t nameLength = SYS_get_name_length();

	// setup uuids
	ble_uuid_t t_ble_uuid;
	t_ble_uuid.uuid = BLE_UUID_NUS_SERVICE;
	t_ble_uuid.type = BLE_UUID_TYPE_VENDOR_BEGIN;

#if BLE_USE_IRK != 0
	// setup irk
	uint8_t irk[BLE_GAP_SEC_KEY_LEN] = BLE_IRK;
#endif

	// start ble peripheral
#if BLE_USE_IRK == 0
	uint32_t status = ble_peripheral_init(BLE_TX_POWER, BLE_ADV_MODE_SLOW, name,
			nameLength, &t_ble_uuid, NULL, NULL, 0, COM_peripheral_on_ble_evt);
#else
	uint32_t status = ble_peripheral_init(BLE_TX_POWER, BLE_ADV_MODE_SLOW, name,
			nameLength, &t_ble_uuid, NULL, irk, 0, COM_peripheral_on_ble_evt);
#endif
	if (status != NRF_SUCCESS) {
		TRACE_DBG_L("initBlePeripheral ERROR\n");
		return;
	}TRACE_DBG_L("initBlePeripheral OK\n");
}

void COM_peripheral_send_raw(uint8_t handle, uint8_t* data, uint16_t length) {
	COM_send_raw(handle, data, length, ble_peripheral_get_data_length());
}

uint32_t COM_peripheral_send_icp(uint8_t handle, Interchange_Protocol_Type ipt,
		uint8_t* data, uint16_t length) {
	return COM_send_icp(handle, ipt, data, length, COM_peripheral_send_raw);
}

void COM_peripheral_handler() {
	ble_peripheral();

	COM_data_handler();
}

static void COM_peripheral_on_ble_evt(ble_evt_t* p_ble_evt) {
	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		FUNR_register(COM_peripheral_connected_status, 3000);
		TRACE_DBG_H("BLE(%d) connected!\n", p_ble_evt->evt.gap_evt.conn_handle)
		;
		break;
	case BLE_GAP_EVT_DISCONNECTED:
		TRACE_DBG_H("BLE(%d) disconnected!\n",
				p_ble_evt->evt.gap_evt.conn_handle)
		;
		break;
	}
}

static void COM_peripheral_connected_status() {
	LOGIC2_status();
}

static uint32_t COM_peripheral_role_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 1) {
		SYS_role_cb(iph->au8Data[0]);
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_peripheral_vibration_cb(Interchange_Protocol_Header* iph) {
	LOGIC2_vibration_cb(iph->au8Data, GENT_bswap_16(iph->u16Length));
	return ERR_OK;
}

static uint32_t COM_peripheral_pattern_cb(Interchange_Protocol_Header* iph) {
	uint8_t* dataLeft = iph->au8Data;
	uint16_t lengthLeft = 2 + 2 * dataLeft[0];
	uint8_t* dataRight = iph->au8Data + 2 + 2 * dataLeft[0];
	uint16_t lengthRight = 2 + 2 * dataRight[0];
	if (lengthLeft + lengthRight <= GENT_bswap_16(iph->u16Length)) {
		FRAM_save_pattern(0, dataLeft, lengthLeft);
		FRAM_save_pattern(1, dataRight, lengthRight);
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

/*--------------------------------------------------------------------------+
 |  central                                                                 |
 +--------------------------------------------------------------------------*/

static void COM_central_on_ble_evt(ble_evt_t* p_ble_evt);
static void COM_central_connected_status(uint8_t handle);
static void COM_central_connected_status_0();
static void COM_central_connected_status_1();
static void COM_central_connected(uint8_t handle);
static void COM_central_connected_0();
static void COM_central_connected_1();
static uint32_t COM_central_pattern_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_central_sync_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_central_acceleration_data_cb(
		Interchange_Protocol_Header* iph);
static uint32_t COM_central_event_count_request_cb(
		Interchange_Protocol_Header* iph);
static uint32_t COM_central_event_data_request_cb(
		Interchange_Protocol_Header* iph);
static uint32_t COM_central_event_clear_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_central_battery_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_central_ack_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_central_scan_bad_blocks_cb(Interchange_Protocol_Header* iph);
static uint32_t COM_central_reset_bad_blocks_cb(
		Interchange_Protocol_Header* iph);
static uint32_t COM_central_bbm_uuid_request_cb(
		Interchange_Protocol_Header* iph);
static uint32_t COM_central_uuid_request_cb(Interchange_Protocol_Header* iph);

void COM_central_init() {
	COM_init();

	// initialize interchange protocol
	ICP_CMD_function_init();
	ICP_CMD_set_function(COM_none_cb, TYPE_DEFAULT);
	ICP_CMD_set_function(COM_trace_cb, TYPE_DEBUG_1);
	ICP_CMD_set_function(COM_time_cb, TYPE_TIME_1);
	ICP_CMD_set_function(COM_device_info_request_cb,
			TYPE_DEVICE_INFO_REQUEST_1);
	ICP_CMD_set_function(COM_device_info_cb, TYPE_DEVICE_INFO_1);
	ICP_CMD_set_function(COM_reset_cb, TYPE_RESET_1);
	ICP_CMD_set_function(COM_battery_request_cb, TYPE_BATTERY_REQUEST_1);

	ICP_CMD_set_function(COM_central_pattern_cb, TYPE_PATTERN_1);
	ICP_CMD_set_function(COM_central_sync_cb, TYPE_SYNC_1);
	ICP_CMD_set_function(COM_central_acceleration_data_cb,
			TYPE_ACCELERATION_DATA_1);
	ICP_CMD_set_function(COM_central_event_count_request_cb,
			TYPE_EVENT_COUNT_REQUEST_1);
	ICP_CMD_set_function(COM_central_event_data_request_cb,
			TYPE_EVENT_DATA_REQUEST_1);
	ICP_CMD_set_function(COM_central_event_clear_cb, TYPE_EVENT_CLEAR_1);
	ICP_CMD_set_function(COM_central_battery_cb, TYPE_BATTERY_1);
	ICP_CMD_set_function(COM_central_ack_cb, TYPE_ACK_1);
	ICP_CMD_set_function(COM_central_scan_bad_blocks_cb, TYPE_BBM_SCAN_1);
	ICP_CMD_set_function(COM_central_reset_bad_blocks_cb, TYPE_BBM_RESET_1);
	ICP_CMD_set_function(COM_central_bbm_uuid_request_cb,
			TYPE_BBM_UUID_REQUEST_1);
	ICP_CMD_set_function(COM_central_uuid_request_cb, TYPE_UUID_REQUEST_1);

#if BLE_USE_IRK != 0
	// setup irk
	uint8_t irk[BLE_GAP_SEC_KEY_LEN] = BLE_IRK;
#endif

	// start ble central
#if BLE_USE_IRK == 0
	ble_c_init(BRD_getBLEContext(), BLE_TX_POWER, NULL, 0,
			COM_central_on_ble_evt);
#else
	ble_c_init(BRD_getBLEContext(), BLE_TX_POWER, irk, 0, COM_central_on_ble_evt);
#endif

	ble_c_scan_start();
}

void COM_central_send_raw(uint8_t handle, uint8_t* data, uint16_t length) {
	COM_send_raw(handle, data, length, ble_central_get_data_length());
}

uint32_t COM_central_send_icp(uint8_t handle, Interchange_Protocol_Type ipt,
		uint8_t* data, uint16_t length) {
	return COM_send_icp(handle, ipt, data, length, COM_central_send_raw);
}

void COM_central_handler() {
	ble_central();

	COM_data_handler();
}

static void COM_central_on_ble_evt(ble_evt_t* p_ble_evt) {
	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		switch (p_ble_evt->evt.gap_evt.conn_handle) {
		case 0:
			FUNR_register(COM_central_connected_0, 100);
			FUNR_register(COM_central_connected_status_0, 3000);
			break;
		case 1:
			FUNR_register(COM_central_connected_1, 100);
			FUNR_register(COM_central_connected_status_1, 3000);
			break;
		}
		TRACE_DBG_H("BLE(%d) connected!\n", p_ble_evt->evt.gap_evt.conn_handle)
		;
		break;
	case BLE_GAP_EVT_DISCONNECTED:
		TRACE_DBG_H("BLE(%d) disconnected!\n",
				p_ble_evt->evt.gap_evt.conn_handle)
		;
		break;
	}
}

static void COM_central_connected(uint8_t handle) {
	uint8_t role = handle + 1;
	COM_central_send_icp(handle, TYPE_ASSIGN_ROLE_1, &role, 1);
	COM_central_send_icp(handle, TYPE_BATTERY_REQUEST_1, NULL, 0);

#if (CFG_MM_SMALL_BLOCK_HEADER_MODE == 1)
	uint8_t* buf = MM_malloc(
	BRD_FRAM_PATTERN_LEFT_SIZE + BRD_FRAM_PATTERN_RIGHT_SIZE);
#endif
#if (CFG_MM_EXTENDED_BLOCK_HEADER_MODE == 1)
	uint8_t* buf = MM_malloc(
			BRD_FRAM_PATTERN_LEFT_SIZE + BRD_FRAM_PATTERN_RIGHT_SIZE, pInstance,
			"PATT");
#endif
	uint16_t lengthLeft;
	FRAM_load_pattern(0, buf, &lengthLeft);
	uint16_t lengthRight;
	FRAM_load_pattern(1, buf + lengthLeft, &lengthRight);
	COM_central_send_icp(handle, TYPE_PATTERN_1, buf, lengthLeft + lengthRight);
	COM_central_send_icp(UART_HANDLE, TYPE_PATTERN_1, buf,
			lengthLeft + lengthRight);
}

static void COM_central_connected_0() {
	COM_central_connected(0);
}

static void COM_central_connected_1() {
	COM_central_connected(1);
}

static void COM_central_connected_status(uint8_t handle) {
	LOGIC1_status(handle + 1);
}

static void COM_central_connected_status_0() {
	COM_central_connected_status(0);
}

static void COM_central_connected_status_1() {
	COM_central_connected_status(1);
}

static uint32_t COM_central_pattern_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) < 1) {
		return ERR_ERROR;
	}
	uint8_t* dataLeft = iph->au8Data;
	uint16_t lengthLeft = 2 + 2 * dataLeft[0];
	uint8_t* dataRight = iph->au8Data + 2 + 2 * dataLeft[0];
	uint16_t lengthRight = 2 + 2 * dataRight[0];
	if (lengthLeft + lengthRight <= GENT_bswap_16(iph->u16Length)) {
		COM_central_send_icp(0, TYPE_PATTERN_1, iph->au8Data,
				GENT_bswap_16(iph->u16Length));
		COM_central_send_icp(1, TYPE_PATTERN_1, iph->au8Data,
				GENT_bswap_16(iph->u16Length));
		FRAM_save_pattern(0, dataLeft, lengthLeft);
		FRAM_save_pattern(1, dataRight, lengthRight);
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_sync_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 8) {
		int64_t time = (uint64_t) iph->au8Data[0]
				| ((uint64_t) iph->au8Data[1] << 8)
				| ((uint64_t) iph->au8Data[2] << 16)
				| ((uint64_t) iph->au8Data[3] << 24)
				| ((uint64_t) iph->au8Data[4] << 32)
				| ((uint64_t) iph->au8Data[5] << 40)
				| ((uint64_t) iph->au8Data[6] << 48)
				| ((uint64_t) iph->au8Data[7] << 56);
		LOGIC1_sync_cb(buffer_handle, time);
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_acceleration_data_cb(
		Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) < 10) {
		return ERR_ERROR;
	}
	uint64_t time = (int64_t) iph->au8Data[0] | ((int64_t) iph->au8Data[1] << 8)
			| ((int64_t) iph->au8Data[2] << 16)
			| ((int64_t) iph->au8Data[3] << 24)
			| ((int64_t) iph->au8Data[4] << 32)
			| ((int64_t) iph->au8Data[5] << 40)
			| ((int64_t) iph->au8Data[6] << 48)
			| ((int64_t) iph->au8Data[7] << 56);
	uint16_t length = (uint16_t) iph->au8Data[8]
			| ((uint16_t) iph->au8Data[9] << 8);
	if (10 + length * 6 <= GENT_bswap_16(iph->u16Length)) {
		LOGIC1_acceleration_data_cb(buffer_handle, iph->au8Data + 10, length,
				time);
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_event_count_request_cb(
		Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 0) {
		LOGIC1_event_count_request_cb();
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_event_data_request_cb(
		Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 2) {
		uint16_t pos = (uint16_t) iph->au8Data[0]
				| ((uint16_t) iph->au8Data[1] << 8);
		LOGIC1_event_data_request_cb(pos);
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_event_clear_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 0) {
		LOGIC1_event_clear_cb();
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_battery_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 2) {
		LOGIC1_battery_cb(buffer_handle,
				(uint16_t) iph->au8Data[0] | ((uint16_t) iph->au8Data[1] << 8));
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_ack_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 4) {
		if (((uint16_t) iph->au8Data[0] | ((uint16_t) iph->au8Data[1] << 8))
				== TYPE_EVENT_DATA_FRAGMENT_1) {
			LOGIC1_event_data_ack_cb(
					(uint16_t) iph->au8Data[2]
							| ((uint16_t) iph->au8Data[3] << 8));
		}
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_scan_bad_blocks_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 4) {
		uint8_t serial[4];
		FRAM_read_device_info(serial, 4, 4);
		uint8_t match = 1;
		for (uint8_t i = 0; i < 4; ++i) {
			if (iph->au8Data[i] != serial[i]) {
				match = 0;
			}
		}
		if (match) {
			LOGIC1_scan_bad_blocks_cb();
			return ERR_OK;
		} else {
			return ERR_ERROR;
		}
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_reset_bad_blocks_cb(
		Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 4) {
		uint8_t serial[4];
		FRAM_read_device_info(serial, 4, 4);
		uint8_t match = 1;
		for (uint8_t i = 0; i < 4; ++i) {
			if (iph->au8Data[i] != serial[i]) {
				match = 0;
			}
		}
		if (match) {
			LOGIC1_reset_bad_blocks_cb();
			return ERR_OK;
		} else {
			return ERR_ERROR;
		}
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_bbm_uuid_request_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 0) {
		LOGIC1_bbm_uuid_request_cb();
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}

static uint32_t COM_central_uuid_request_cb(Interchange_Protocol_Header* iph) {
	if (GENT_bswap_16(iph->u16Length) == 0) {
		LOGIC1_uuid_request_cb();
		return ERR_OK;
	} else {
		return ERR_ERROR;
	}
}
