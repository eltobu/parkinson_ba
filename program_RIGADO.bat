@cd /D %~dp0
nrfjprog -s 682971328 --recover                                                                    -f nrf52
nrfjprog -s 682971328 --eraseall                                                                   -f nrf52
nrfjprog -s 682971328 --program lib\components\softdevice\s132\hex\s132_nrf52_4.0.2_softdevice.hex -f nrf52 --sectorerase
nrfjprog -s 682971328 --program Debug\Parkinson_BA.hex                                             -f nrf52 --sectorerase
nrfjprog -s 682971328 --reset                                                                      -f nrf52