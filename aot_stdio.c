/* ***************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: Test
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://svn.local.aotag.ch/aot/Software/FrameworkAndLibrary/Generic_Modules/trunk/templates/aot_stdio.c $
 *  $Revision: 24151 $
 *      $Date: 2018-04-13 14:27:03 +0200 (Fri, 13 Apr 2018) $
 *    $Author: iliev $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

#include "module_config.h"

#ifdef AOT_STDOUT

#include "sdk_config.h"
#include <stdlib.h>
#include "SEGGER_RTT.h"

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

#include "aot_stdio.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>

#include "sys.h"
#include "uart.h"
#include "ble_uart_queue.h"
#include "icp.h"

/*--------------------------------------------------------------------------+
 |  local constants and macros                                               |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  module-global types                                                      |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  module-global variables                                                  |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  declaration of local functions (helper functions)                        |
 +--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
 |  functions                                                                |
 +--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * Function:
 */

/*-------------------------------------------------------------------------*/
/**
 * Function:
 */

/*-------------------------------------------------------------------------*/
/**
 * Function:
 */

/*-------------------------------------------------------------------------*/
/**
 * Function:
 */

#if(0)
extern int printf (__const char *__restrict __format, ...)
{
	//uint8_t		au8_encoded_data[IPED_LENGTH_BUFFER];
	//uint8_t		au8_Buffer_data[IPED_LENGTH_BUFFER];

	int ret_status = 0;
	//uint16_t	u16_length = 0;

	va_list args;
	va_start(args,__format);
	ret_status = vprintf(__format, args);
	//ret_status = sprintf(au8_Buffer_data, __format, args);
	va_end(args);

	//interchange_protocol_encode(ENCODE_ADD_CMD, TYPE_STATION_DEBUG_1, (uint8_t *)&ptr, len, au8_encoded_data, &u16_length);
	//interchange_protocol_encode(ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, au8_encoded_data, &u16_length);
	//protocol_send_data_to_queue(au8_encoded_data, u16_length, UART_getTxQueue());

	return ret_status;
}

uint32_t aot_stdio_test (int file, char *ptr, int len)
{
	(void) file; /* Not used, avoid warning */
	uint8_t au8_encoded_data[IPED_LENGTH_BUFFER];
	uint16_t u16_length = 0;

	interchange_protocol_encode(ENCODE_ADD_CMD, TYPE_STATION_DEBUG_1, (uint8_t *)ptr, len, au8_encoded_data, &u16_length);
	interchange_protocol_encode(ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, au8_encoded_data, &u16_length);

	protocol_send_data_to_queue(au8_encoded_data, u16_length, UART_getTxQueue());

	return len;
}
#endif

uint32_t aot_printf(const char *fmt, ...) {
	uint8_t buffer[AOTIO_BUFFER_SIZE];
	uint16_t length = 0;

	memset(buffer, 0x00, AOTIO_BUFFER_SIZE);

	va_list args;

#if 0
	va_start(args, fmt);
	length = vsnprintf((char*) buffer, AOTIO_BUFFER_SIZE, fmt, args);
	buffer[length + 1] = 0; // Terminate string
	queue_push(UART_getTxQueue(), buffer, length);
	va_end(args);
#else
	uint8_t data[AOTIO_BUFFER_SIZE];
	va_start(args, fmt);
	vsprintf((char*) buffer, fmt, args);
	ICP_encode(ENCODE_ADD_CMD, TYPE_DEBUG_1, buffer, strlen((char*) buffer),
			data, &length);
	ICP_encode(ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, data, &length);
	queue_push(UART_getTxQueue(), data, length);
	va_end(args);
#endif

	return length;
}

#if (0)
/*********************************************************************
 *
 *       _write()
 *
 * Function description
 *   Low-level write function.
 *   libc subroutines will use this system routine for output to all files,
 *   including stdout.
 *   Write data via IPED.
 */
int aot_write(int file, char *ptr, int len)
{
	(void) file; /* Not used, avoid warning */
	uint8_t au8_encoded_data[IPED_LENGTH_BUFFER];
	uint16_t u16_length = 0;

	interchange_protocol_encode(ENCODE_ADD_CMD, TYPE_STATION_DEBUG_1, (uint8_t *)ptr, len, au8_encoded_data, &u16_length);
	interchange_protocol_encode(ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, au8_encoded_data, &u16_length);

	protocol_send_data_to_queue(au8_encoded_data, u16_length, UART_getTxQueue());

	return len;
}

/*********************************************************************
 *
 *       _write_r()
 *
 * Function description
 *   Low-level reentrant write function.
 *   libc subroutines will use this system routine for output to all files,
 *   including stdout.
 *   Write data via IPED.
 */
int aot_write_r(struct _reent *r, int file, char *ptr, int len)
{
	(void) file; /* Not used, avoid warning */
	(void) r; /* Not used, avoid warning */
	uint8_t au8_encoded_data[IPED_LENGTH_BUFFER];
	uint16_t u16_length = 0;

	interchange_protocol_encode(ENCODE_ADD_CMD, TYPE_STATION_DEBUG_1, (uint8_t *)ptr, len, au8_encoded_data, &u16_length);
	interchange_protocol_encode(ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, au8_encoded_data, &u16_length);

	protocol_send_data_to_queue(au8_encoded_data, u16_length, UART_getTxQueue());

	return len;
}
#endif

#endif // AOT_STDOUT

/* eof */
